open BinNums
open Bool
open Eqtype
open Expr
open Label
open Seq
open Sopn
open Ssralg
open Ssrbool
open Type
open Utils0
open Var0
open Wsize

type 'asm_op linstr_r =
| Lopn of lval list * 'asm_op sopn * pexpr list
| Lalign
| Llabel of label
| Lgoto of remote_label
| Ligoto of pexpr
| LstoreLabel of lval * label
| Lcond of pexpr * label

type 'asm_op linstr = { li_ii : instr_info; li_i : 'asm_op linstr_r }

type 'asm_op lcmd = 'asm_op linstr list

type 'asm_op lfundef = { lfd_info : fun_info; lfd_align : wsize;
                         lfd_tyin : stype list; lfd_arg : var_i list;
                         lfd_body : 'asm_op lcmd; lfd_tyout : stype list;
                         lfd_res : var_i list; lfd_export : bool;
                         lfd_callee_saved : Var.var list;
                         lfd_total_stack : coq_Z }

type 'asm_op lprog = { lp_rip : Equality.sort; lp_rsp : Equality.sort;
                       lp_globs : GRing.ComRing.sort list;
                       lp_funcs : (funname * 'asm_op lfundef) list }

(** val eqb_r : 'a1 asmOp -> 'a1 linstr_r -> 'a1 linstr_r -> bool **)

let eqb_r asmop i1 i2 =
  match i1 with
  | Lopn (lv1, o1, e1) ->
    (match i2 with
     | Lopn (lv2, o2, e2) ->
       (&&)
         ((&&)
           (eq_op (seq_eqType lval_eqType) (Obj.magic lv1) (Obj.magic lv2))
           (eq_op (sopn_eqType asmop) (Obj.magic o1) (Obj.magic o2)))
         (eq_op (seq_eqType pexpr_eqType) (Obj.magic e1) (Obj.magic e2))
     | _ -> false)
  | Lalign -> (match i2 with
               | Lalign -> true
               | _ -> false)
  | Llabel l1 ->
    (match i2 with
     | Llabel l2 -> eq_op pos_eqType (Obj.magic l1) (Obj.magic l2)
     | _ -> false)
  | Lgoto l1 ->
    (match i2 with
     | Lgoto l2 ->
       eq_op (prod_eqType pos_eqType pos_eqType) (Obj.magic l1) (Obj.magic l2)
     | _ -> false)
  | Ligoto e1 ->
    (match i2 with
     | Ligoto e2 -> eq_op pexpr_eqType (Obj.magic e1) (Obj.magic e2)
     | _ -> false)
  | LstoreLabel (lv1, lbl1) ->
    (match i2 with
     | LstoreLabel (lv2, lbl2) ->
       (&&) (eq_op lval_eqType (Obj.magic lv1) (Obj.magic lv2))
         (eq_op pos_eqType (Obj.magic lbl1) (Obj.magic lbl2))
     | _ -> false)
  | Lcond (e1, l1) ->
    (match i2 with
     | Lcond (e2, l2) ->
       (&&) (eq_op pexpr_eqType (Obj.magic e1) (Obj.magic e2))
         (eq_op pos_eqType (Obj.magic l1) (Obj.magic l2))
     | _ -> false)

(** val eqb_r_axiom : 'a1 asmOp -> 'a1 linstr_r Equality.axiom **)

let eqb_r_axiom asmop _top_assumption_ =
  let _evar_0_ = fun lv1 o1 e1 __top_assumption_ ->
    let _evar_0_ = fun lv2 o2 e2 ->
      equivP
        ((&&)
          ((&&) (eq_op (seq_eqType lval_eqType) lv1 lv2)
            (eq_op (sopn_eqType asmop) o1 o2))
          (eq_op (seq_eqType pexpr_eqType) e1 e2))
        (andP
          ((&&) (eq_op (seq_eqType lval_eqType) lv1 lv2)
            (eq_op (sopn_eqType asmop) o1 o2))
          (eq_op (seq_eqType pexpr_eqType) e1 e2))
    in
    let _evar_0_0 = ReflectF in
    let _evar_0_1 = fun _ -> ReflectF in
    let _evar_0_2 = fun _ -> ReflectF in
    let _evar_0_3 = fun _ -> ReflectF in
    let _evar_0_4 = fun _ _ -> ReflectF in
    let _evar_0_5 = fun _ _ -> ReflectF in
    (match __top_assumption_ with
     | Lopn (l, s, l0) -> Obj.magic _evar_0_ l s l0
     | Lalign -> _evar_0_0
     | Llabel l -> _evar_0_1 l
     | Lgoto r -> _evar_0_2 r
     | Ligoto p -> _evar_0_3 p
     | LstoreLabel (l, l0) -> _evar_0_4 l l0
     | Lcond (p, l) -> _evar_0_5 p l)
  in
  let _evar_0_0 = fun __top_assumption_ ->
    let _evar_0_0 = fun _ _ _ -> ReflectF in
    let _evar_0_1 = ReflectT in
    let _evar_0_2 = fun _ -> ReflectF in
    let _evar_0_3 = fun _ -> ReflectF in
    let _evar_0_4 = fun _ -> ReflectF in
    let _evar_0_5 = fun _ _ -> ReflectF in
    let _evar_0_6 = fun _ _ -> ReflectF in
    (match __top_assumption_ with
     | Lopn (l, s, l0) -> _evar_0_0 l s l0
     | Lalign -> _evar_0_1
     | Llabel l -> _evar_0_2 l
     | Lgoto r -> _evar_0_3 r
     | Ligoto p -> _evar_0_4 p
     | LstoreLabel (l, l0) -> _evar_0_5 l l0
     | Lcond (p, l) -> _evar_0_6 p l)
  in
  let _evar_0_1 = fun l1 __top_assumption_ ->
    let _evar_0_1 = fun _ _ _ -> ReflectF in
    let _evar_0_2 = ReflectF in
    let _evar_0_3 = fun l2 ->
      equivP (eq_op pos_eqType l1 l2) (eqP pos_eqType l1 l2)
    in
    let _evar_0_4 = fun _ -> ReflectF in
    let _evar_0_5 = fun _ -> ReflectF in
    let _evar_0_6 = fun _ _ -> ReflectF in
    let _evar_0_7 = fun _ _ -> ReflectF in
    (match __top_assumption_ with
     | Lopn (l, s, l0) -> _evar_0_1 l s l0
     | Lalign -> _evar_0_2
     | Llabel l -> Obj.magic _evar_0_3 l
     | Lgoto r -> _evar_0_4 r
     | Ligoto p -> _evar_0_5 p
     | LstoreLabel (l, l0) -> _evar_0_6 l l0
     | Lcond (p, l) -> _evar_0_7 p l)
  in
  let _evar_0_2 = fun l1 __top_assumption_ ->
    let _evar_0_2 = fun _ _ _ -> ReflectF in
    let _evar_0_3 = ReflectF in
    let _evar_0_4 = fun _ -> ReflectF in
    let _evar_0_5 = fun l2 ->
      equivP (eq_op (prod_eqType pos_eqType pos_eqType) l1 l2)
        (eqP (prod_eqType pos_eqType pos_eqType) l1 l2)
    in
    let _evar_0_6 = fun _ -> ReflectF in
    let _evar_0_7 = fun _ _ -> ReflectF in
    let _evar_0_8 = fun _ _ -> ReflectF in
    (match __top_assumption_ with
     | Lopn (l, s, l0) -> _evar_0_2 l s l0
     | Lalign -> _evar_0_3
     | Llabel l -> _evar_0_4 l
     | Lgoto r -> Obj.magic _evar_0_5 r
     | Ligoto p -> _evar_0_6 p
     | LstoreLabel (l, l0) -> _evar_0_7 l l0
     | Lcond (p, l) -> _evar_0_8 p l)
  in
  let _evar_0_3 = fun e1 __top_assumption_ ->
    let _evar_0_3 = fun _ _ _ -> ReflectF in
    let _evar_0_4 = ReflectF in
    let _evar_0_5 = fun _ -> ReflectF in
    let _evar_0_6 = fun _ -> ReflectF in
    let _evar_0_7 = fun e2 ->
      equivP (eq_op pexpr_eqType e1 e2) (eqP pexpr_eqType e1 e2)
    in
    let _evar_0_8 = fun _ _ -> ReflectF in
    let _evar_0_9 = fun _ _ -> ReflectF in
    (match __top_assumption_ with
     | Lopn (l, s, l0) -> _evar_0_3 l s l0
     | Lalign -> _evar_0_4
     | Llabel l -> _evar_0_5 l
     | Lgoto r -> _evar_0_6 r
     | Ligoto p -> Obj.magic _evar_0_7 p
     | LstoreLabel (l, l0) -> _evar_0_8 l l0
     | Lcond (p, l) -> _evar_0_9 p l)
  in
  let _evar_0_4 = fun lv1 l1 __top_assumption_ ->
    let _evar_0_4 = fun _ _ _ -> ReflectF in
    let _evar_0_5 = ReflectF in
    let _evar_0_6 = fun _ -> ReflectF in
    let _evar_0_7 = fun _ -> ReflectF in
    let _evar_0_8 = fun _ -> ReflectF in
    let _evar_0_9 = fun lv2 l2 ->
      equivP ((&&) (eq_op lval_eqType lv1 lv2) (eq_op pos_eqType l1 l2))
        (andP (eq_op lval_eqType lv1 lv2) (eq_op pos_eqType l1 l2))
    in
    let _evar_0_10 = fun _ _ -> ReflectF in
    (match __top_assumption_ with
     | Lopn (l, s, l0) -> _evar_0_4 l s l0
     | Lalign -> _evar_0_5
     | Llabel l -> _evar_0_6 l
     | Lgoto r -> _evar_0_7 r
     | Ligoto p -> _evar_0_8 p
     | LstoreLabel (l, l0) -> Obj.magic _evar_0_9 l l0
     | Lcond (p, l) -> _evar_0_10 p l)
  in
  let _evar_0_5 = fun e1 l1 __top_assumption_ ->
    let _evar_0_5 = fun _ _ _ -> ReflectF in
    let _evar_0_6 = ReflectF in
    let _evar_0_7 = fun _ -> ReflectF in
    let _evar_0_8 = fun _ -> ReflectF in
    let _evar_0_9 = fun _ -> ReflectF in
    let _evar_0_10 = fun _ _ -> ReflectF in
    let _evar_0_11 = fun e2 l2 ->
      equivP ((&&) (eq_op pexpr_eqType e1 e2) (eq_op pos_eqType l1 l2))
        (andP (eq_op pexpr_eqType e1 e2) (eq_op pos_eqType l1 l2))
    in
    (match __top_assumption_ with
     | Lopn (l, s, l0) -> _evar_0_5 l s l0
     | Lalign -> _evar_0_6
     | Llabel l -> _evar_0_7 l
     | Lgoto r -> _evar_0_8 r
     | Ligoto p -> _evar_0_9 p
     | LstoreLabel (l, l0) -> _evar_0_10 l l0
     | Lcond (p, l) -> Obj.magic _evar_0_11 p l)
  in
  (match _top_assumption_ with
   | Lopn (l, s, l0) -> Obj.magic _evar_0_ l s l0
   | Lalign -> _evar_0_0
   | Llabel l -> Obj.magic _evar_0_1 l
   | Lgoto r -> Obj.magic _evar_0_2 r
   | Ligoto p -> Obj.magic _evar_0_3 p
   | LstoreLabel (l, l0) -> Obj.magic _evar_0_4 l l0
   | Lcond (p, l) -> Obj.magic _evar_0_5 p l)

(** val linstr_r_eqMixin : 'a1 asmOp -> 'a1 linstr_r Equality.mixin_of **)

let linstr_r_eqMixin asmop =
  { Equality.op = (eqb_r asmop); Equality.mixin_of__1 = (eqb_r_axiom asmop) }

(** val linstr_r_eqType : 'a1 asmOp -> Equality.coq_type **)

let linstr_r_eqType asmop =
  Obj.magic linstr_r_eqMixin asmop
