open BinNums
open Bool
open Eqtype
open Expr
open Label
open Seq
open Sopn
open Ssralg
open Ssrbool
open Type
open Utils0
open Var0
open Wsize

type 'asm_op linstr_r =
| Lopn of lval list * 'asm_op sopn * pexpr list
| Lalign
| Llabel of label
| Lgoto of remote_label
| Ligoto of pexpr
| LstoreLabel of lval * label
| Lcond of pexpr * label

type 'asm_op linstr = { li_ii : instr_info; li_i : 'asm_op linstr_r }

type 'asm_op lcmd = 'asm_op linstr list

type 'asm_op lfundef = { lfd_info : fun_info; lfd_align : wsize;
                         lfd_tyin : stype list; lfd_arg : var_i list;
                         lfd_body : 'asm_op lcmd; lfd_tyout : stype list;
                         lfd_res : var_i list; lfd_export : bool;
                         lfd_callee_saved : Var.var list;
                         lfd_total_stack : coq_Z }

type 'asm_op lprog = { lp_rip : Equality.sort; lp_rsp : Equality.sort;
                       lp_globs : GRing.ComRing.sort list;
                       lp_funcs : (funname * 'asm_op lfundef) list }

val eqb_r : 'a1 asmOp -> 'a1 linstr_r -> 'a1 linstr_r -> bool

val eqb_r_axiom : 'a1 asmOp -> 'a1 linstr_r Equality.axiom

val linstr_r_eqMixin : 'a1 asmOp -> 'a1 linstr_r Equality.mixin_of

val linstr_r_eqType : 'a1 asmOp -> Equality.coq_type
