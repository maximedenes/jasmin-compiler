open Datatypes
open Compiler_util
open Eqtype
open Expr
open Global
open Seq
open Sopn
open Type
open Utils0
open Var0

module E =
 struct
  (** val pass : char list **)

  let pass =
    'm'::('a'::('k'::('e'::(' '::('r'::('e'::('f'::('e'::('r'::('e'::('n'::('c'::('e'::(' '::('a'::('r'::('g'::('u'::('m'::('e'::('n'::('t'::('s'::[])))))))))))))))))))))))

  (** val make_ref_error : instr_info -> char list -> pp_error_loc **)

  let make_ref_error =
    pp_internal_error_s_at pass
 end

(** val with_var :
    'a1 asmOp -> (glob_decl list -> Var.var -> Equality.sort) ->
    Equality.coq_type -> progT -> 'a1 prog -> var_i -> Var.var -> var_i **)

let with_var _ fresh_id _ _ p xi x =
  let x' = { Var.vtype = (Var.vtype x); Var.vname = (fresh_id p.p_globs x) }
  in
  { v_var = x'; v_info = xi.v_info }

(** val is_reg_ptr_expr :
    'a1 asmOp -> (Var.var -> bool) -> (glob_decl list -> Var.var ->
    Equality.sort) -> Equality.coq_type -> progT -> 'a1 prog -> Var.var ->
    pexpr -> var_i option **)

let is_reg_ptr_expr asmop is_reg_ptr fresh_id t0 pT p x = function
| Pvar x' ->
  if (&&) (is_reg_ptr x) ((||) (is_glob x') (negb (is_reg_ptr x'.gv.v_var)))
  then Some (with_var asmop fresh_id t0 pT p x'.gv x)
  else None
| Psub (_, _, _, x', _) -> Some (with_var asmop fresh_id t0 pT p x'.gv x)
| _ -> None

(** val is_reg_ptr_lval :
    'a1 asmOp -> (Var.var -> bool) -> (glob_decl list -> Var.var ->
    Equality.sort) -> Equality.coq_type -> progT -> 'a1 prog -> Var.var ->
    lval -> var_i option **)

let is_reg_ptr_lval asmop is_reg_ptr fresh_id t0 pT p x = function
| Lvar x' ->
  if (&&) (is_reg_ptr x) (negb (is_reg_ptr x'.v_var))
  then Some (with_var asmop fresh_id t0 pT p x' x)
  else None
| Lasub (_, _, _, x', _) -> Some (with_var asmop fresh_id t0 pT p x' x)
| _ -> None

(** val make_prologue :
    'a1 asmOp -> (Var.var -> bool) -> (glob_decl list -> Var.var ->
    Equality.sort) -> Equality.coq_type -> progT -> 'a1 prog -> instr_info ->
    Sv.t -> var_i list -> stype list -> pexpr list -> (pp_error_loc, 'a1
    instr list * pexpr list) result **)

let rec make_prologue asmop is_reg_ptr fresh_id t0 pT p ii x xs tys es =
  match xs with
  | [] ->
    (match tys with
     | [] ->
       (match es with
        | [] -> Ok ([], [])
        | _ :: _ ->
          Error
            (E.make_ref_error ii
              ('a'::('s'::('s'::('e'::('r'::('t'::(' '::('f'::('a'::('l'::('s'::('e'::(' '::('('::('p'::('r'::('o'::('l'::('o'::('g'::('u'::('e'::(')'::[])))))))))))))))))))))))))
     | _ :: _ ->
       Error
         (E.make_ref_error ii
           ('a'::('s'::('s'::('e'::('r'::('t'::(' '::('f'::('a'::('l'::('s'::('e'::(' '::('('::('p'::('r'::('o'::('l'::('o'::('g'::('u'::('e'::(')'::[])))))))))))))))))))))))))
  | x0 :: xs0 ->
    (match tys with
     | [] ->
       Error
         (E.make_ref_error ii
           ('a'::('s'::('s'::('e'::('r'::('t'::(' '::('f'::('a'::('l'::('s'::('e'::(' '::('('::('p'::('r'::('o'::('l'::('o'::('g'::('u'::('e'::(')'::[]))))))))))))))))))))))))
     | ty :: tys0 ->
       (match es with
        | [] ->
          Error
            (E.make_ref_error ii
              ('a'::('s'::('s'::('e'::('r'::('t'::(' '::('f'::('a'::('l'::('s'::('e'::(' '::('('::('p'::('r'::('o'::('l'::('o'::('g'::('u'::('e'::(')'::[]))))))))))))))))))))))))
        | e :: es0 ->
          let x1 = x0.v_var in
          (match is_reg_ptr_expr asmop is_reg_ptr fresh_id t0 pT p x1 e with
           | Some y ->
             if (&&)
                  (eq_op stype_eqType (Obj.magic ty)
                    (Obj.magic Var.vtype y.v_var))
                  ((&&) (negb (is_sbool (Obj.magic ty)))
                    (negb (Sv.mem (Obj.magic y.v_var) x)))
             then (match make_prologue asmop is_reg_ptr fresh_id t0 pT p ii
                           (Sv.add (Obj.magic y.v_var) x) xs0 tys0 es0 with
                   | Ok x2 ->
                     let (p0, es') = x2 in
                     Ok (((MkI (ii, (Cassgn ((Lvar y), AT_rename, ty,
                     e)))) :: p0), ((coq_Plvar y) :: es'))
                   | Error s -> Error s)
             else let s =
                    E.make_ref_error ii
                      ('b'::('a'::('d'::(' '::('f'::('r'::('e'::('s'::('h'::(' '::('i'::('d'::(' '::('('::('p'::('r'::('o'::('l'::('o'::('g'::('u'::('e'::(')'::[])))))))))))))))))))))))
                  in
                  Error s
           | None ->
             (match make_prologue asmop is_reg_ptr fresh_id t0 pT p ii x xs0
                      tys0 es0 with
              | Ok x2 -> let (p0, es') = x2 in Ok (p0, (e :: es'))
              | Error s -> Error s))))

type pseudo_instr =
| PI_lv of lval
| PI_i of lval * stype * var_i

(** val make_pseudo_epilogue :
    'a1 asmOp -> (Var.var -> bool) -> (glob_decl list -> Var.var ->
    Equality.sort) -> Equality.coq_type -> progT -> 'a1 prog -> instr_info ->
    Sv.t -> var_i list -> stype list -> lval list -> (pp_error_loc,
    pseudo_instr list) result **)

let rec make_pseudo_epilogue asmop is_reg_ptr fresh_id t0 pT p ii x xs tys rs =
  match xs with
  | [] ->
    (match tys with
     | [] ->
       (match rs with
        | [] -> Ok []
        | _ :: _ ->
          Error
            (E.make_ref_error ii
              ('a'::('s'::('s'::('e'::('r'::('t'::(' '::('f'::('a'::('l'::('s'::('e'::(' '::('('::('e'::('p'::('i'::('l'::('o'::('g'::('u'::('e'::(')'::[])))))))))))))))))))))))))
     | _ :: _ ->
       Error
         (E.make_ref_error ii
           ('a'::('s'::('s'::('e'::('r'::('t'::(' '::('f'::('a'::('l'::('s'::('e'::(' '::('('::('e'::('p'::('i'::('l'::('o'::('g'::('u'::('e'::(')'::[])))))))))))))))))))))))))
  | x0 :: xs0 ->
    (match tys with
     | [] ->
       Error
         (E.make_ref_error ii
           ('a'::('s'::('s'::('e'::('r'::('t'::(' '::('f'::('a'::('l'::('s'::('e'::(' '::('('::('e'::('p'::('i'::('l'::('o'::('g'::('u'::('e'::(')'::[]))))))))))))))))))))))))
     | ty :: tys0 ->
       (match rs with
        | [] ->
          Error
            (E.make_ref_error ii
              ('a'::('s'::('s'::('e'::('r'::('t'::(' '::('f'::('a'::('l'::('s'::('e'::(' '::('('::('e'::('p'::('i'::('l'::('o'::('g'::('u'::('e'::(')'::[]))))))))))))))))))))))))
        | r :: rs0 ->
          let x1 = x0.v_var in
          (match is_reg_ptr_lval asmop is_reg_ptr fresh_id t0 pT p x1 r with
           | Some y ->
             if (&&)
                  (eq_op stype_eqType (Obj.magic ty)
                    (Obj.magic Var.vtype y.v_var))
                  ((&&) (negb (is_sbool (Obj.magic ty)))
                    (negb (Sv.mem (Obj.magic y.v_var) x)))
             then (match make_pseudo_epilogue asmop is_reg_ptr fresh_id t0 pT
                           p ii x xs0 tys0 rs0 with
                   | Ok x2 ->
                     Ok ((PI_lv (Lvar y)) :: ((PI_i (r, ty, y)) :: x2))
                   | Error s -> Error s)
             else let s =
                    E.make_ref_error ii
                      ('b'::('a'::('d'::(' '::('f'::('r'::('e'::('s'::('h'::(' '::('i'::('d'::(' '::('('::('e'::('p'::('i'::('l'::('o'::('g'::('u'::('e'::(')'::[])))))))))))))))))))))))
                  in
                  Error s
           | None ->
             (match make_pseudo_epilogue asmop is_reg_ptr fresh_id t0 pT p ii
                      x xs0 tys0 rs0 with
              | Ok x2 -> Ok ((PI_lv r) :: x2)
              | Error s -> Error s))))

(** val mk_ep_i :
    'a1 asmOp -> instr_info -> lval -> stype -> var_i -> 'a1 instr **)

let mk_ep_i _ ii r ty y =
  MkI (ii, (Cassgn (r, AT_rename, ty, (coq_Plvar y))))

(** val noload : pexpr -> bool **)

let rec noload = function
| Pget (_, _, _, e0) -> noload e0
| Psub (_, _, _, _, e0) -> noload e0
| Pload (_, _, _) -> false
| Papp1 (_, e0) -> noload e0
| Papp2 (_, e1, e2) -> (&&) (noload e1) (noload e2)
| PappN (_, es) -> all noload es
| Pif (_, e1, e2, e3) -> (&&) (noload e1) ((&&) (noload e2) (noload e3))
| _ -> true

(** val wf_lv : lval -> bool **)

let wf_lv = function
| Lvar _ -> true
| Lasub (_, _, _, _, e) -> noload e
| _ -> false

(** val swapable :
    'a1 asmOp -> instr_info -> pseudo_instr list -> (pp_error_loc, lval
    list * 'a1 instr list) result **)

let rec swapable asmop ii = function
| [] -> Ok ([], [])
| p0 :: pis0 ->
  (match p0 with
   | PI_lv lv ->
     (match swapable asmop ii pis0 with
      | Ok x -> let (lvs, ep) = x in Ok ((lv :: lvs), ep)
      | Error s -> Error s)
   | PI_i (r, ty, y) ->
     (match swapable asmop ii pis0 with
      | Ok x ->
        let (lvs, ep) = x in
        let i = mk_ep_i asmop ii r ty y in
        if disjoint (read_rvs lvs) (write_I asmop i)
        then if disjoint (vrvs lvs)
                  (Sv.union (write_I asmop i) (read_I asmop i))
             then if wf_lv r
                  then Ok (lvs, (i :: ep))
                  else let s =
                         E.make_ref_error ii
                           ('c'::('a'::('n'::('n'::('o'::('t'::(' '::('s'::('w'::('a'::('p'::(' '::('3'::[])))))))))))))
                       in
                       Error s
             else let s =
                    E.make_ref_error ii
                      ('c'::('a'::('n'::('n'::('o'::('t'::(' '::('s'::('w'::('a'::('p'::(' '::('2'::[])))))))))))))
                  in
                  Error s
        else let s =
               E.make_ref_error ii
                 ('c'::('a'::('n'::('n'::('o'::('t'::(' '::('s'::('w'::('a'::('p'::(' '::('1'::[])))))))))))))
             in
             Error s
      | Error s -> Error s))

(** val make_epilogue :
    'a1 asmOp -> (Var.var -> bool) -> (glob_decl list -> Var.var ->
    Equality.sort) -> Equality.coq_type -> progT -> 'a1 prog -> instr_info ->
    Sv.t -> var_i list -> stype list -> lval list -> (pp_error_loc, lval
    list * 'a1 instr list) result **)

let make_epilogue asmop is_reg_ptr fresh_id t0 pT p ii x xs tys rs =
  match make_pseudo_epilogue asmop is_reg_ptr fresh_id t0 pT p ii x xs tys rs with
  | Ok x0 -> swapable asmop ii x0
  | Error s -> Error s

(** val update_c :
    'a1 asmOp -> ('a1 instr -> 'a1 instr list cexec) -> 'a1 instr list ->
    (pp_error_loc, 'a1 instr list) result **)

let update_c _ update_i0 c =
  match mapM update_i0 c with
  | Ok x -> Ok (flatten x)
  | Error s -> Error s

(** val update_i :
    'a1 asmOp -> (Var.var -> bool) -> (glob_decl list -> Var.var ->
    Equality.sort) -> Equality.coq_type -> progT -> 'a1 prog -> (funname ->
    ((var_i list * stype list) * var_i list) * stype list) -> Sv.t -> 'a1
    instr -> 'a1 instr list cexec **)

let rec update_i asmop is_reg_ptr fresh_id t0 pT p get_sig0 x i = match i with
| MkI (ii, ir) ->
  (match ir with
   | Cif (b, c1, c2) ->
     (match update_c asmop
              (update_i asmop is_reg_ptr fresh_id t0 pT p get_sig0 x) c1 with
      | Ok x0 ->
        (match update_c asmop
                 (update_i asmop is_reg_ptr fresh_id t0 pT p get_sig0 x) c2 with
         | Ok x1 -> Ok ((MkI (ii, (Cif (b, x0, x1)))) :: [])
         | Error s -> Error s)
      | Error s -> Error s)
   | Cfor (x0, r, c) ->
     (match update_c asmop
              (update_i asmop is_reg_ptr fresh_id t0 pT p get_sig0 x) c with
      | Ok x1 -> Ok ((MkI (ii, (Cfor (x0, r, x1)))) :: [])
      | Error s -> Error s)
   | Cwhile (a, c, e, c') ->
     (match update_c asmop
              (update_i asmop is_reg_ptr fresh_id t0 pT p get_sig0 x) c with
      | Ok x0 ->
        (match update_c asmop
                 (update_i asmop is_reg_ptr fresh_id t0 pT p get_sig0 x) c' with
         | Ok x1 -> Ok ((MkI (ii, (Cwhile (a, x0, e, x1)))) :: [])
         | Error s -> Error s)
      | Error s -> Error s)
   | Ccall (ini, xs, fn, es) ->
     let (p0, treturns) = get_sig0 fn in
     let (p1, returns) = p0 in
     let (params, tparams) = p1 in
     (match make_prologue asmop is_reg_ptr fresh_id t0 pT p ii x params
              tparams es with
      | Ok x0 ->
        let (prologue, es0) = x0 in
        (match make_epilogue asmop is_reg_ptr fresh_id t0 pT p ii x returns
                 treturns xs with
         | Ok x1 ->
           let (xs0, epilogue) = x1 in
           Ok
           (cat prologue ((MkI (ii, (Ccall (ini, xs0, fn,
             es0)))) :: epilogue))
         | Error s -> Error s)
      | Error s -> Error s)
   | _ -> Ok (i :: []))

(** val update_fd :
    'a1 asmOp -> (Var.var -> bool) -> (glob_decl list -> Var.var ->
    Equality.sort) -> Equality.coq_type -> progT -> 'a1 prog -> (funname ->
    ((var_i list * stype list) * var_i list) * stype list) -> 'a1 fundef ->
    (pp_error_loc, ('a1, Equality.sort) _fundef) result **)

let update_fd asmop is_reg_ptr fresh_id t0 pT p get_sig0 fd =
  let body = fd.f_body in
  let write = write_c asmop body in
  let read = read_c asmop body in
  let returns = read_es (map coq_Plvar fd.f_res) in
  let x = Sv.union returns (Sv.union write read) in
  (match update_c asmop
           (update_i asmop is_reg_ptr fresh_id t0 pT p get_sig0 x) body with
   | Ok x0 -> Ok (with_body asmop fd x0)
   | Error s -> Error s)

(** val get_sig :
    'a1 asmOp -> Equality.coq_type -> progT -> 'a1 prog -> funname -> ((var_i
    list * stype list) * var_i list) * stype list **)

let get_sig _ _ _ p n =
  match get_fundef p.p_funcs n with
  | Some fd -> (((fd.f_params, fd.f_tyin), fd.f_res), fd.f_tyout)
  | None -> ((([], []), []), [])

(** val makereference_prog :
    'a1 asmOp -> (Var.var -> bool) -> (glob_decl list -> Var.var ->
    Equality.sort) -> Equality.coq_type -> progT -> 'a1 prog -> 'a1 prog cexec **)

let makereference_prog asmop is_reg_ptr fresh_id t0 pT p =
  match map_cfprog_gen (fun x -> x.f_info)
          (update_fd asmop is_reg_ptr fresh_id t0 pT p
            (get_sig asmop t0 pT p)) p.p_funcs with
  | Ok x -> Ok { p_funcs = x; p_globs = p.p_globs; p_extra = p.p_extra }
  | Error s -> Error s
