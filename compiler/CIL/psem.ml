open BinNums
open Sumbool
open Eqtype
open Expr
open Low_memory
open Memory_model
open Sem
open Ssralg
open Type
open Utils0
open Values
open Var0
open Warray_
open Word0
open Wsize

type __ = Obj.t

type pword = { pw_size : wsize; pw_word : GRing.ComRing.sort }

type psem_t = __

(** val pword_of_word : wsize -> GRing.ComRing.sort -> pword **)

let pword_of_word s w =
  { pw_size = s; pw_word = w }

(** val to_pword : wsize -> value -> pword exec **)

let to_pword s = function
| Vword (s', w) ->
  Ok
    (if sumbool_of_bool (cmp_le wsize_cmp s' s)
     then { pw_size = s'; pw_word = w }
     else pword_of_word s (zero_extend s s' w))
| Vundef t0 -> (match t0 with
                | Coq_sword _ -> undef_error
                | _ -> type_error)
| _ -> type_error

(** val pof_val : stype -> value -> psem_t exec **)

let pof_val = function
| Coq_sbool -> Obj.magic to_bool
| Coq_sint -> Obj.magic to_int
| Coq_sarr n -> Obj.magic to_arr n
| Coq_sword s -> Obj.magic to_pword s

(** val wextend_type : Equality.sort -> Equality.sort -> bool **)

let wextend_type t1 t2 =
  (||) (eq_op stype_eqType t1 t2)
    (match Obj.magic t1 with
     | Coq_sword s1 ->
       (match Obj.magic t2 with
        | Coq_sword s2 -> cmp_le wsize_cmp s1 s2
        | _ -> false)
     | _ -> false)

(** val pundef_addr : stype -> psem_t exec **)

let pundef_addr = function
| Coq_sarr n -> Ok (Obj.magic WArray.empty n)
| _ -> undef_error

(** val vmap0 : psem_t exec Fv.t **)

let vmap0 =
  Fv.empty (fun x -> pundef_addr (Var.vtype x))

(** val set_var :
    psem_t exec Fv.t -> Var.var -> value -> psem_t exec Fv.t exec **)

let set_var m x v =
  on_vu (fun v0 -> Fv.set m x (Ok v0))
    (if is_sbool (Obj.magic Var.vtype x)
     then Ok (Fv.set m x (pundef_addr (Var.vtype x)))
     else type_error) (pof_val (Var.vtype x) v)

type estate = { emem : Memory.mem; evm : psem_t exec Fv.t }

(** val with_vm : coq_PointerData -> estate -> psem_t exec Fv.t -> estate **)

let with_vm _ s vm =
  { emem = s.emem; evm = vm }

(** val write_var :
    coq_PointerData -> var_i -> value -> estate -> estate exec **)

let write_var pd x v s =
  match set_var s.evm x.v_var v with
  | Ok x0 -> Ok (with_vm pd s x0)
  | Error s0 -> Error s0

(** val write_vars :
    coq_PointerData -> var_i list -> value list -> estate -> (error, estate)
    result **)

let write_vars pd xs vs s =
  fold2 ErrType (write_var pd) xs vs s

type semCallParams = { init_state : (Equality.sort -> extra_prog_t ->
                                    extra_val_t -> estate -> estate exec);
                       finalize : (Equality.sort -> Memory.mem -> Memory.mem) }

(** val sCP_unit : coq_PointerData -> semCallParams **)

let sCP_unit _ =
  { init_state = (fun _ _ _ s -> Ok s); finalize = (fun _ m -> m) }

(** val init_stk_state :
    coq_PointerData -> stk_fun_extra -> sprog_extra -> GRing.ComRing.sort ->
    estate -> (error, estate) result **)

let init_stk_state pd sf pe wrip s =
  let m1 = s.emem in
  (match (Memory.coq_M pd).alloc_stack m1 sf.sf_align sf.sf_stk_sz
           sf.sf_stk_extra_sz with
   | Ok x ->
     write_vars pd ({ v_var = { Var.vtype = (Coq_sword (coq_Uptr pd));
       Var.vname = pe.sp_rsp }; v_info = Coq_xH } :: ({ v_var = { Var.vtype =
       (Coq_sword (coq_Uptr pd)); Var.vname = pe.sp_rip }; v_info =
       Coq_xH } :: [])) ((Vword ((coq_Uptr pd),
       (top_stack pd (Memory.coq_CM pd) (Memory.coq_M pd) x))) :: ((Vword
       ((coq_Uptr pd), wrip)) :: [])) { emem = x; evm = vmap0 }
   | Error s0 -> Error s0)

(** val finalize_stk_mem :
    coq_PointerData -> stk_fun_extra -> Memory.mem -> Memory.mem **)

let finalize_stk_mem pd _ m =
  (Memory.coq_M pd).free_stack m

(** val sCP_stack : coq_PointerData -> semCallParams **)

let sCP_stack pd =
  { init_state = (Obj.magic init_stk_state pd); finalize =
    (Obj.magic finalize_stk_mem pd) }
