open Arch_extra
open Compiler
open Sopn
open X86_decl
open X86_extra
open X86_instr_decl

val is_move_op :
  (register, xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op
  asm_op_t -> bool

val aparams : architecture_params
