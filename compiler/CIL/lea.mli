open Eqtype
open Expr
open Memory_model
open Ssralg
open Utils0
open Word0
open Wsize

type lea = { lea_disp : GRing.ComRing.sort; lea_base : var_i option;
             lea_scale : GRing.ComRing.sort; lea_offset : var_i option }

val lea_const : coq_PointerData -> GRing.ComRing.sort -> lea

val lea_var : coq_PointerData -> var_i -> lea

val mkLea :
  coq_PointerData -> GRing.ComRing.sort -> var_i option -> Equality.sort ->
  var_i option -> lea

val lea_mul : coq_PointerData -> lea -> lea -> lea option

val lea_add : coq_PointerData -> lea -> lea -> lea option

val lea_sub : coq_PointerData -> lea -> lea -> lea option

val mk_lea_rec : coq_PointerData -> wsize -> pexpr -> lea option

val push_cast_sz : wsize -> pexpr -> pexpr

val push_cast : pexpr -> pexpr

val mk_lea : coq_PointerData -> wsize -> pexpr -> lea option
