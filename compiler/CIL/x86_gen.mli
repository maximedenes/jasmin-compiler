open Datatypes
open Arch_decl
open Arch_extra
open Asm_gen
open Compiler_util
open Eqtype
open Expr
open Linear
open Memory_model
open Seq
open Ssrbool
open Type
open Utils0
open Var0
open Wsize
open X86_decl
open X86_extra
open X86_instr_decl

val fail : instr_info -> char list -> pp_error_loc

val not_condt : condt -> condt

val or_condt : instr_info -> pexpr -> condt -> condt -> condt cexec

val and_condt :
  instr_info -> pexpr -> condt -> condt -> (pp_error_loc, condt) result

val assemble_cond_r : instr_info -> pexpr -> condt cexec

val assemble_cond : instr_info -> pexpr -> condt cexec

val assemble_i :
  Var.var -> (register, xmm_register, rflag, condt, x86_op, x86_extra_op)
  extended_op linstr -> (register, xmm_register, rflag, condt, x86_op) asm_i
  cexec

val assemble_c :
  Var.var -> (register, xmm_register, rflag, condt, x86_op, x86_extra_op)
  extended_op lcmd -> (register, xmm_register, rflag, condt, x86_op) asm_i
  list cexec

val asm_typed_reg_of_var :
  Var.var -> (register, xmm_register, rflag, condt) asm_typed_reg cexec

val assemble_fd :
  register -> Var.var -> (register, xmm_register, rflag, condt, x86_op,
  x86_extra_op) extended_op lfundef -> (pp_error_loc, (register,
  xmm_register, rflag, condt, x86_op) asm_fundef) result

val mk_rip : Equality.sort -> Var.var

val assemble_prog :
  (register, xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op
  lprog -> (register, xmm_register, rflag, condt, x86_op) asm_prog cexec
