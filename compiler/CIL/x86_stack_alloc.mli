open BinNums
open Arch_decl
open Arch_extra
open Eqtype
open Expr
open Memory_model
open SsrZ
open Stack_alloc
open X86_decl
open X86_extra
open X86_instr_decl

val lea_ptr :
  lval -> assgn_tag -> pexpr -> coq_Z -> (register, xmm_register, rflag,
  condt, x86_op, x86_extra_op) extended_op instr_r

val mov_ptr : lval -> assgn_tag -> pexpr -> x86_extended_op instr_r

type mov_kind =
| MK_LEA
| MK_MOV

val mk_mov : vptr_kind -> mov_kind

val x86_mov_ofs :
  lval -> assgn_tag -> vptr_kind -> pexpr -> coq_Z -> (register,
  xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op instr_r option
