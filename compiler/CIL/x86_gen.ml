open Datatypes
open Arch_decl
open Arch_extra
open Asm_gen
open Compiler_util
open Eqtype
open Expr
open Linear
open Memory_model
open Seq
open Ssrbool
open Type
open Utils0
open Var0
open Wsize
open X86_decl
open X86_extra
open X86_instr_decl

(** val fail : instr_info -> char list -> pp_error_loc **)

let fail ii msg =
  Asm_gen.E.error ii
    (pp_box ((PPEstring
      ('s'::('t'::('o'::('r'::('e'::('-'::('l'::('a'::('b'::('e'::('l'::(':'::[]))))))))))))) :: ((PPEstring
      msg) :: [])))

(** val not_condt : condt -> condt **)

let not_condt = function
| O_ct -> NO_ct
| NO_ct -> O_ct
| B_ct -> NB_ct
| NB_ct -> B_ct
| E_ct -> NE_ct
| NE_ct -> E_ct
| BE_ct -> NBE_ct
| NBE_ct -> BE_ct
| S_ct -> NS_ct
| NS_ct -> S_ct
| P_ct -> NP_ct
| NP_ct -> P_ct
| L_ct -> NL_ct
| NL_ct -> L_ct
| LE_ct -> NLE_ct
| NLE_ct -> LE_ct

(** val or_condt : instr_info -> pexpr -> condt -> condt -> condt cexec **)

let or_condt ii e c1 c2 =
  match c1 with
  | B_ct ->
    (match c2 with
     | E_ct -> Ok BE_ct
     | _ ->
       Error
         (Asm_gen.E.berror ii e
           ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('c'::('o'::('n'::('d'::('i'::('t'::('i'::('o'::('n'::(' '::('('::('O'::('R'::(')'::[]))))))))))))))))))))))))
  | E_ct ->
    (match c2 with
     | B_ct -> Ok BE_ct
     | L_ct -> Ok LE_ct
     | _ ->
       Error
         (Asm_gen.E.berror ii e
           ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('c'::('o'::('n'::('d'::('i'::('t'::('i'::('o'::('n'::(' '::('('::('O'::('R'::(')'::[]))))))))))))))))))))))))
  | L_ct ->
    (match c2 with
     | E_ct -> Ok LE_ct
     | _ ->
       Error
         (Asm_gen.E.berror ii e
           ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('c'::('o'::('n'::('d'::('i'::('t'::('i'::('o'::('n'::(' '::('('::('O'::('R'::(')'::[]))))))))))))))))))))))))
  | _ ->
    Error
      (Asm_gen.E.berror ii e
        ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('c'::('o'::('n'::('d'::('i'::('t'::('i'::('o'::('n'::(' '::('('::('O'::('R'::(')'::[])))))))))))))))))))))))

(** val and_condt :
    instr_info -> pexpr -> condt -> condt -> (pp_error_loc, condt) result **)

let and_condt ii e c1 c2 =
  match c1 with
  | NB_ct ->
    (match c2 with
     | NE_ct -> Ok NBE_ct
     | _ ->
       Error
         (Asm_gen.E.berror ii e
           ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('c'::('o'::('n'::('d'::('i'::('t'::('i'::('o'::('n'::(' '::('('::('A'::('N'::('D'::(')'::[])))))))))))))))))))))))))
  | NE_ct ->
    (match c2 with
     | NB_ct -> Ok NBE_ct
     | NL_ct -> Ok NLE_ct
     | _ ->
       Error
         (Asm_gen.E.berror ii e
           ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('c'::('o'::('n'::('d'::('i'::('t'::('i'::('o'::('n'::(' '::('('::('A'::('N'::('D'::(')'::[])))))))))))))))))))))))))
  | NL_ct ->
    (match c2 with
     | NE_ct -> Ok NLE_ct
     | _ ->
       Error
         (Asm_gen.E.berror ii e
           ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('c'::('o'::('n'::('d'::('i'::('t'::('i'::('o'::('n'::(' '::('('::('A'::('N'::('D'::(')'::[])))))))))))))))))))))))))
  | _ ->
    Error
      (Asm_gen.E.berror ii e
        ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('c'::('o'::('n'::('d'::('i'::('t'::('i'::('o'::('n'::(' '::('('::('A'::('N'::('D'::(')'::[]))))))))))))))))))))))))

(** val assemble_cond_r : instr_info -> pexpr -> condt cexec **)

let rec assemble_cond_r ii e = match e with
| Pvar v ->
  (match of_var_e Coq_sbool x86_rflag_toS ii v.gv with
   | Ok x ->
     (match x with
      | CF -> Ok B_ct
      | PF -> Ok P_ct
      | ZF -> Ok E_ct
      | SF -> Ok S_ct
      | OF -> Ok O_ct
      | DF ->
        Error
          (Asm_gen.E.berror ii e
            ('C'::('a'::('n'::('n'::('o'::('t'::(' '::('b'::('r'::('a'::('n'::('c'::('h'::(' '::('o'::('n'::(' '::('D'::('F'::[])))))))))))))))))))))
   | Error s -> Error s)
| Papp1 (s, e0) ->
  (match s with
   | Onot ->
     (match assemble_cond_r ii e0 with
      | Ok x -> Ok (not_condt x)
      | Error s0 -> Error s0)
   | _ ->
     Error
       (Asm_gen.E.berror ii e
         ('d'::('o'::('n'::('\''::('t'::(' '::('k'::('n'::('o'::('w'::('n'::(' '::('h'::('o'::('w'::(' '::('t'::('o'::(' '::('c'::('o'::('m'::('p'::('i'::('l'::('e'::(' '::('t'::('h'::('e'::(' '::('c'::('o'::('n'::('d'::('i'::('t'::('i'::('o'::('n'::[]))))))))))))))))))))))))))))))))))))))))))
| Papp2 (s, e1, e2) ->
  (match s with
   | Obeq ->
     (match e1 with
      | Pvar x1 ->
        (match e2 with
         | Pvar x2 ->
           (match of_var_e Coq_sbool x86_rflag_toS ii x1.gv with
            | Ok x ->
              (match of_var_e Coq_sbool x86_rflag_toS ii x2.gv with
               | Ok x0 ->
                 if (||)
                      ((&&) (eq_op rflag_eqType (Obj.magic x) (Obj.magic SF))
                        (eq_op rflag_eqType (Obj.magic x0) (Obj.magic OF)))
                      ((&&) (eq_op rflag_eqType (Obj.magic x) (Obj.magic OF))
                        (eq_op rflag_eqType (Obj.magic x0) (Obj.magic SF)))
                 then Ok NL_ct
                 else Error
                        (Asm_gen.E.berror ii e
                          ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('c'::('o'::('n'::('d'::('i'::('t'::('i'::('o'::('n'::(' '::('('::('N'::('L'::(')'::[])))))))))))))))))))))))
               | Error s0 -> Error s0)
            | Error s0 -> Error s0)
         | _ ->
           Error
             (Asm_gen.E.berror ii e
               ('d'::('o'::('n'::('\''::('t'::(' '::('k'::('n'::('o'::('w'::('n'::(' '::('h'::('o'::('w'::(' '::('t'::('o'::(' '::('c'::('o'::('m'::('p'::('i'::('l'::('e'::(' '::('t'::('h'::('e'::(' '::('c'::('o'::('n'::('d'::('i'::('t'::('i'::('o'::('n'::[]))))))))))))))))))))))))))))))))))))))))))
      | _ ->
        Error
          (Asm_gen.E.berror ii e
            ('d'::('o'::('n'::('\''::('t'::(' '::('k'::('n'::('o'::('w'::('n'::(' '::('h'::('o'::('w'::(' '::('t'::('o'::(' '::('c'::('o'::('m'::('p'::('i'::('l'::('e'::(' '::('t'::('h'::('e'::(' '::('c'::('o'::('n'::('d'::('i'::('t'::('i'::('o'::('n'::[]))))))))))))))))))))))))))))))))))))))))))
   | Oand ->
     (match assemble_cond_r ii e1 with
      | Ok x ->
        (match assemble_cond_r ii e2 with
         | Ok x0 -> and_condt ii e x x0
         | Error s0 -> Error s0)
      | Error s0 -> Error s0)
   | Oor ->
     (match assemble_cond_r ii e1 with
      | Ok x ->
        (match assemble_cond_r ii e2 with
         | Ok x0 -> or_condt ii e x x0
         | Error s0 -> Error s0)
      | Error s0 -> Error s0)
   | _ ->
     Error
       (Asm_gen.E.berror ii e
         ('d'::('o'::('n'::('\''::('t'::(' '::('k'::('n'::('o'::('w'::('n'::(' '::('h'::('o'::('w'::(' '::('t'::('o'::(' '::('c'::('o'::('m'::('p'::('i'::('l'::('e'::(' '::('t'::('h'::('e'::(' '::('c'::('o'::('n'::('d'::('i'::('t'::('i'::('o'::('n'::[]))))))))))))))))))))))))))))))))))))))))))
| Pif (_, p, p0, p1) ->
  (match p with
   | Pvar v1 ->
     (match p0 with
      | Pvar v2 ->
        (match p1 with
         | Papp1 (s0, p2) ->
           (match s0 with
            | Onot ->
              (match p2 with
               | Pvar vn2 ->
                 (match of_var_e Coq_sbool x86_rflag_toS ii v1.gv with
                  | Ok x ->
                    (match of_var_e Coq_sbool x86_rflag_toS ii v2.gv with
                     | Ok x0 ->
                       (match of_var_e Coq_sbool x86_rflag_toS ii vn2.gv with
                        | Ok x1 ->
                          if (||)
                               ((&&)
                                 (eq_op rflag_eqType (Obj.magic x)
                                   (Obj.magic SF))
                                 ((&&)
                                   (eq_op rflag_eqType (Obj.magic x1)
                                     (Obj.magic OF))
                                   (eq_op rflag_eqType (Obj.magic x0)
                                     (Obj.magic OF))))
                               ((&&)
                                 (eq_op rflag_eqType (Obj.magic x)
                                   (Obj.magic OF))
                                 ((&&)
                                   (eq_op rflag_eqType (Obj.magic x1)
                                     (Obj.magic SF))
                                   (eq_op rflag_eqType (Obj.magic x0)
                                     (Obj.magic SF))))
                          then Ok NL_ct
                          else Error
                                 (Asm_gen.E.berror ii e
                                   ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('c'::('o'::('n'::('d'::('i'::('t'::('i'::('o'::('n'::(' '::('('::('N'::('L'::(')'::[])))))))))))))))))))))))
                        | Error s -> Error s)
                     | Error s -> Error s)
                  | Error s -> Error s)
               | _ ->
                 Error
                   (Asm_gen.E.berror ii e
                     ('d'::('o'::('n'::('\''::('t'::(' '::('k'::('n'::('o'::('w'::('n'::(' '::('h'::('o'::('w'::(' '::('t'::('o'::(' '::('c'::('o'::('m'::('p'::('i'::('l'::('e'::(' '::('t'::('h'::('e'::(' '::('c'::('o'::('n'::('d'::('i'::('t'::('i'::('o'::('n'::[]))))))))))))))))))))))))))))))))))))))))))
            | _ ->
              Error
                (Asm_gen.E.berror ii e
                  ('d'::('o'::('n'::('\''::('t'::(' '::('k'::('n'::('o'::('w'::('n'::(' '::('h'::('o'::('w'::(' '::('t'::('o'::(' '::('c'::('o'::('m'::('p'::('i'::('l'::('e'::(' '::('t'::('h'::('e'::(' '::('c'::('o'::('n'::('d'::('i'::('t'::('i'::('o'::('n'::[]))))))))))))))))))))))))))))))))))))))))))
         | _ ->
           Error
             (Asm_gen.E.berror ii e
               ('d'::('o'::('n'::('\''::('t'::(' '::('k'::('n'::('o'::('w'::('n'::(' '::('h'::('o'::('w'::(' '::('t'::('o'::(' '::('c'::('o'::('m'::('p'::('i'::('l'::('e'::(' '::('t'::('h'::('e'::(' '::('c'::('o'::('n'::('d'::('i'::('t'::('i'::('o'::('n'::[]))))))))))))))))))))))))))))))))))))))))))
      | Papp1 (s0, p2) ->
        (match s0 with
         | Onot ->
           (match p2 with
            | Pvar vn2 ->
              (match p1 with
               | Pvar v2 ->
                 (match of_var_e Coq_sbool x86_rflag_toS ii v1.gv with
                  | Ok x ->
                    (match of_var_e Coq_sbool x86_rflag_toS ii vn2.gv with
                     | Ok x0 ->
                       (match of_var_e Coq_sbool x86_rflag_toS ii v2.gv with
                        | Ok x1 ->
                          if (||)
                               ((&&)
                                 (eq_op rflag_eqType (Obj.magic x)
                                   (Obj.magic SF))
                                 ((&&)
                                   (eq_op rflag_eqType (Obj.magic x0)
                                     (Obj.magic OF))
                                   (eq_op rflag_eqType (Obj.magic x1)
                                     (Obj.magic OF))))
                               ((&&)
                                 (eq_op rflag_eqType (Obj.magic x)
                                   (Obj.magic OF))
                                 ((&&)
                                   (eq_op rflag_eqType (Obj.magic x0)
                                     (Obj.magic SF))
                                   (eq_op rflag_eqType (Obj.magic x1)
                                     (Obj.magic SF))))
                          then Ok L_ct
                          else Error
                                 (Asm_gen.E.berror ii e
                                   ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('c'::('o'::('n'::('d'::('i'::('t'::('i'::('o'::('n'::(' '::('('::('L'::(')'::[]))))))))))))))))))))))
                        | Error s -> Error s)
                     | Error s -> Error s)
                  | Error s -> Error s)
               | _ ->
                 Error
                   (Asm_gen.E.berror ii e
                     ('d'::('o'::('n'::('\''::('t'::(' '::('k'::('n'::('o'::('w'::('n'::(' '::('h'::('o'::('w'::(' '::('t'::('o'::(' '::('c'::('o'::('m'::('p'::('i'::('l'::('e'::(' '::('t'::('h'::('e'::(' '::('c'::('o'::('n'::('d'::('i'::('t'::('i'::('o'::('n'::[]))))))))))))))))))))))))))))))))))))))))))
            | _ ->
              Error
                (Asm_gen.E.berror ii e
                  ('d'::('o'::('n'::('\''::('t'::(' '::('k'::('n'::('o'::('w'::('n'::(' '::('h'::('o'::('w'::(' '::('t'::('o'::(' '::('c'::('o'::('m'::('p'::('i'::('l'::('e'::(' '::('t'::('h'::('e'::(' '::('c'::('o'::('n'::('d'::('i'::('t'::('i'::('o'::('n'::[]))))))))))))))))))))))))))))))))))))))))))
         | _ ->
           Error
             (Asm_gen.E.berror ii e
               ('d'::('o'::('n'::('\''::('t'::(' '::('k'::('n'::('o'::('w'::('n'::(' '::('h'::('o'::('w'::(' '::('t'::('o'::(' '::('c'::('o'::('m'::('p'::('i'::('l'::('e'::(' '::('t'::('h'::('e'::(' '::('c'::('o'::('n'::('d'::('i'::('t'::('i'::('o'::('n'::[]))))))))))))))))))))))))))))))))))))))))))
      | _ ->
        Error
          (Asm_gen.E.berror ii e
            ('d'::('o'::('n'::('\''::('t'::(' '::('k'::('n'::('o'::('w'::('n'::(' '::('h'::('o'::('w'::(' '::('t'::('o'::(' '::('c'::('o'::('m'::('p'::('i'::('l'::('e'::(' '::('t'::('h'::('e'::(' '::('c'::('o'::('n'::('d'::('i'::('t'::('i'::('o'::('n'::[]))))))))))))))))))))))))))))))))))))))))))
   | _ ->
     Error
       (Asm_gen.E.berror ii e
         ('d'::('o'::('n'::('\''::('t'::(' '::('k'::('n'::('o'::('w'::('n'::(' '::('h'::('o'::('w'::(' '::('t'::('o'::(' '::('c'::('o'::('m'::('p'::('i'::('l'::('e'::(' '::('t'::('h'::('e'::(' '::('c'::('o'::('n'::('d'::('i'::('t'::('i'::('o'::('n'::[]))))))))))))))))))))))))))))))))))))))))))
| _ ->
  Error
    (Asm_gen.E.berror ii e
      ('d'::('o'::('n'::('\''::('t'::(' '::('k'::('n'::('o'::('w'::('n'::(' '::('h'::('o'::('w'::(' '::('t'::('o'::(' '::('c'::('o'::('m'::('p'::('i'::('l'::('e'::(' '::('t'::('h'::('e'::(' '::('c'::('o'::('n'::('d'::('i'::('t'::('i'::('o'::('n'::[])))))))))))))))))))))))))))))))))))))))))

(** val assemble_cond : instr_info -> pexpr -> condt cexec **)

let assemble_cond =
  assemble_cond_r

(** val assemble_i :
    Var.var -> (register, xmm_register, rflag, condt, x86_op, x86_extra_op)
    extended_op linstr -> (register, xmm_register, rflag, condt, x86_op)
    asm_i cexec **)

let assemble_i rip i =
  let { li_ii = ii; li_i = ir } = i in
  (match ir with
   | Lopn (ds, op, es) ->
     (match assemble_sopn x86_extra assemble_cond rip ii op ds es with
      | Ok x -> Ok (AsmOp ((fst x), (snd x)))
      | Error s -> Error s)
   | Lalign -> Ok ALIGN
   | Llabel lbl -> Ok (LABEL lbl)
   | Lgoto lbl -> Ok (JMP lbl)
   | Ligoto e ->
     (match e with
      | Papp1 (_, _) ->
        let s =
          Asm_gen.E.werror ii e
            ('L'::('i'::('g'::('o'::('t'::('o'::('/'::('J'::('M'::('P'::('I'::[])))))))))))
        in
        Error s
      | _ ->
        (match assemble_word x86_extra AK_mem rip ii
                 (coq_Uptr (arch_pd x86_decl)) e with
         | Ok x -> Ok (JMPI x)
         | Error s -> Error s))
   | LstoreLabel (x, lbl) ->
     (match x with
      | Lnone (_, _) ->
        let s = fail ii ('n'::('o'::('n'::('e'::[])))) in Error s
      | Lvar x0 ->
        (match of_var (Coq_sword U64) x86_reg_toS x0.v_var with
         | Some r -> Ok (STORELABEL (r, lbl))
         | None ->
           let s = fail ii ('b'::('a'::('d'::(' '::('v'::('a'::('r'::[])))))))
           in
           Error s)
      | Lmem (_, _, _) ->
        let s = fail ii ('s'::('e'::('t'::(' '::('m'::('e'::('m'::[]))))))) in
        Error s
      | Laset (_, _, _, _) ->
        let s =
          fail ii
            ('s'::('e'::('t'::(' '::('a'::('r'::('r'::('a'::('y'::[])))))))))
        in
        Error s
      | Lasub (_, _, _, _, _) ->
        let s =
          fail ii
            ('s'::('u'::('b'::(' '::('a'::('r'::('r'::('a'::('y'::[])))))))))
        in
        Error s)
   | Lcond (e, l) ->
     (match assemble_cond ii e with
      | Ok x -> Ok (Jcc (l, x))
      | Error s -> Error s))

(** val assemble_c :
    Var.var -> (register, xmm_register, rflag, condt, x86_op, x86_extra_op)
    extended_op lcmd -> (register, xmm_register, rflag, condt, x86_op) asm_i
    list cexec **)

let assemble_c rip lc =
  mapM (assemble_i rip) lc

(** val asm_typed_reg_of_var :
    Var.var -> (register, xmm_register, rflag, condt) asm_typed_reg cexec **)

let asm_typed_reg_of_var x =
  let { Var.vtype = ty; Var.vname = n } = x in
  (match ty with
   | Coq_sbool ->
     (match of_string Coq_sbool x86_rflag_toS (Obj.magic n) with
      | Some r -> Ok (ABReg r)
      | None ->
        Error
          (Asm_gen.E.gen_error true None None (PPEstring
            ('6'::('q'::('J'::('Y'::('x'::('x'::('W'::('a'::('k'::('S'::('y'::('c'::('%'::[]))))))))))))))))
   | Coq_sint ->
     Error
       (Asm_gen.E.gen_error true None None (PPEstring
         ('v'::('B'::('u'::('u'::('8'::('N'::('v'::('7'::('A'::('F'::('R'::('C'::[]))))))))))))))
   | Coq_sarr _ ->
     Error
       (Asm_gen.E.gen_error true None None (PPEstring
         ('4'::('W'::('L'::('d'::('O'::('8'::('K'::('4'::('v'::('i'::('E'::('3'::[]))))))))))))))
   | Coq_sword sz ->
     (match sz with
      | U64 ->
        (match of_string (Coq_sword U64) x86_reg_toS (Obj.magic n) with
         | Some r -> Ok (ARReg r)
         | None ->
           Error
             (Asm_gen.E.gen_error true None None (PPEstring
               ('R'::('+'::('z'::('T'::('5'::('0'::('u'::('y'::('f'::('3'::('f'::('F'::[])))))))))))))))
      | U256 ->
        (match of_string (Coq_sword U256) x86_xreg_toS (Obj.magic n) with
         | Some r -> Ok (AXReg r)
         | None ->
           Error
             (Asm_gen.E.gen_error true None None (PPEstring
               ('D'::('h'::('9'::('l'::('3'::('1'::('M'::('J'::('e'::('a'::('f'::('V'::[])))))))))))))))
      | _ ->
        Error
          (Asm_gen.E.gen_error true None None (PPEstring
            ('+'::('y'::('2'::('S'::('v'::('S'::('1'::('t'::('6'::('p'::('z'::('B'::[]))))))))))))))))

(** val assemble_fd :
    register -> Var.var -> (register, xmm_register, rflag, condt, x86_op,
    x86_extra_op) extended_op lfundef -> (pp_error_loc, (register,
    xmm_register, rflag, condt, x86_op) asm_fundef) result **)

let assemble_fd sp rip fd =
  match assemble_c rip fd.lfd_body with
  | Ok x ->
    if negb
         (in_mem (Obj.magic to_var (Coq_sword U64) x86_reg_toS sp)
           (mem (seq_predType Var.var_eqType)
             (Obj.magic map (fun v -> v.v_var) fd.lfd_arg)))
    then if all (fun x0 ->
              match asm_typed_reg_of_var x0 with
              | Ok a -> (match a with
                         | ARReg _ -> true
                         | _ -> false)
              | Error _ -> false) fd.lfd_callee_saved
         then (match mapM (fun x0 -> asm_typed_reg_of_var x0.v_var) fd.lfd_arg with
               | Ok x0 ->
                 (match mapM (fun x1 -> asm_typed_reg_of_var x1.v_var)
                          fd.lfd_res with
                  | Ok x1 ->
                    Ok { asm_fd_align = fd.lfd_align; asm_fd_arg = x0;
                      asm_fd_body = x; asm_fd_res = x1; asm_fd_export =
                      fd.lfd_export; asm_fd_total_stack = fd.lfd_total_stack }
                  | Error s -> Error s)
               | Error s -> Error s)
         else let s =
                Asm_gen.E.gen_error true None None (PPEstring
                  ('S'::('a'::('v'::('e'::('d'::(' '::('v'::('a'::('r'::('i'::('a'::('b'::('l'::('e'::(' '::('i'::('s'::(' '::('n'::('o'::('t'::(' '::('a'::(' '::('r'::('e'::('g'::('i'::('s'::('t'::('e'::('r'::[])))))))))))))))))))))))))))))))))
              in
              Error s
    else let s =
           Asm_gen.E.gen_error true None None (PPEstring
             ('S'::('t'::('a'::('c'::('k'::(' '::('p'::('o'::('i'::('n'::('t'::('e'::('r'::(' '::('i'::('s'::(' '::('a'::('n'::(' '::('a'::('r'::('g'::('u'::('m'::('e'::('n'::('t'::[])))))))))))))))))))))))))))))
         in
         Error s
  | Error s -> Error s

(** val mk_rip : Equality.sort -> Var.var **)

let mk_rip name =
  { Var.vtype = (Coq_sword (coq_Uptr (arch_pd x86_decl))); Var.vname = name }

(** val assemble_prog :
    (register, xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op
    lprog -> (register, xmm_register, rflag, condt, x86_op) asm_prog cexec **)

let assemble_prog p =
  let rip = mk_rip p.lp_rip in
  if eq_op (option_eqType reg_eqType) (Obj.magic to_reg x86_extra rip)
       (Obj.magic None)
  then if eq_op (option_eqType reg_eqType)
            (Obj.magic of_string (Coq_sword U64) x86_reg_toS p.lp_rsp)
            (Obj.magic (Some RSP))
       then (match map_cfprog_gen (fun l -> l.lfd_info) (assemble_fd RSP rip)
                     p.lp_funcs with
             | Ok x -> Ok { asm_globs = p.lp_globs; asm_funcs = x }
             | Error s -> Error s)
       else let s =
              Asm_gen.E.gen_error true None None (PPEstring
                ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('R'::('S'::('P'::[]))))))))))))
            in
            Error s
  else let s =
         Asm_gen.E.gen_error true None None (PPEstring
           ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('R'::('I'::('P'::[]))))))))))))
       in
       Error s
