open BinNums
open Bool
open Datatypes
open Eqtype
open Fintype
open Label
open Memory_model
open Sem_type
open Seq
open SsrZ
open Ssralg
open Ssrbool
open Ssrnat
open Strings
open Type
open Utils0
open Word0
open Wsize

type __ = Obj.t

type 't coq_ToString = { category : char list; _finC : 't finTypeC;
                         to_string : ('t -> char list);
                         strings : (char list * 't) list }

val rtype : stype -> 'a1 coq_ToString -> stype

type ('reg, 'xreg, 'rflag, 'cond) arch_decl = { reg_size : wsize;
                                                xreg_size : wsize;
                                                cond_eqC : 'cond eqTypeC;
                                                toS_r : 'reg coq_ToString;
                                                toS_x : 'xreg coq_ToString;
                                                toS_f : 'rflag coq_ToString }

val arch_pd : ('a1, 'a2, 'a3, 'a4) arch_decl -> coq_PointerData

type ('reg, 'xreg, 'rflag, 'cond) reg_t = 'reg

type ('reg, 'xreg, 'rflag, 'cond) xreg_t = 'xreg

type ('reg, 'xreg, 'rflag, 'cond) rflag_t = 'rflag

type ('reg, 'xreg, 'rflag, 'cond) cond_t = 'cond

type ('reg, 'xreg, 'rflag, 'cond) reg_address = { ad_disp : GRing.ComRing.sort;
                                                  ad_base : ('reg, 'xreg,
                                                            'rflag, 'cond)
                                                            reg_t option;
                                                  ad_scale : nat;
                                                  ad_offset : ('reg, 'xreg,
                                                              'rflag, 'cond)
                                                              reg_t option }

type ('reg, 'xreg, 'rflag, 'cond) address =
| Areg of ('reg, 'xreg, 'rflag, 'cond) reg_address
| Arip of GRing.ComRing.sort

val oeq_reg :
  ('a1, 'a2, 'a3, 'a4) arch_decl -> ('a1, 'a2, 'a3, 'a4) reg_t option ->
  ('a1, 'a2, 'a3, 'a4) reg_t option -> bool

val reg_address_beq :
  ('a1, 'a2, 'a3, 'a4) arch_decl -> ('a1, 'a2, 'a3, 'a4) reg_address -> ('a1,
  'a2, 'a3, 'a4) reg_address -> bool

val reg_address_eq_axiom :
  ('a1, 'a2, 'a3, 'a4) arch_decl -> ('a1, 'a2, 'a3, 'a4) reg_address
  Equality.axiom

val reg_address_eqMixin :
  ('a1, 'a2, 'a3, 'a4) arch_decl -> ('a1, 'a2, 'a3, 'a4) reg_address
  Equality.mixin_of

val reg_address_eqType : ('a1, 'a2, 'a3, 'a4) arch_decl -> Equality.coq_type

val address_beq :
  ('a1, 'a2, 'a3, 'a4) arch_decl -> ('a1, 'a2, 'a3, 'a4) address -> ('a1,
  'a2, 'a3, 'a4) address -> bool

val address_eq_axiom :
  ('a1, 'a2, 'a3, 'a4) arch_decl -> ('a1, 'a2, 'a3, 'a4) address
  Equality.axiom

val address_eqMixin :
  ('a1, 'a2, 'a3, 'a4) arch_decl -> ('a1, 'a2, 'a3, 'a4) address
  Equality.mixin_of

val address_eqType : ('a1, 'a2, 'a3, 'a4) arch_decl -> Equality.coq_type

val rflags : ('a1, 'a2, 'a3, 'a4) arch_decl -> 'a3 list

type ('reg, 'xreg, 'rflag, 'cond) asm_arg =
| Condt of ('reg, 'xreg, 'rflag, 'cond) cond_t
| Imm of wsize * GRing.ComRing.sort
| Reg of ('reg, 'xreg, 'rflag, 'cond) reg_t
| Addr of ('reg, 'xreg, 'rflag, 'cond) address
| XReg of ('reg, 'xreg, 'rflag, 'cond) xreg_t

type ('reg, 'xreg, 'rflag, 'cond) asm_args =
  ('reg, 'xreg, 'rflag, 'cond) asm_arg list

val asm_arg_beq :
  ('a1, 'a2, 'a3, 'a4) arch_decl -> ('a1, 'a2, 'a3, 'a4) asm_arg -> ('a1,
  'a2, 'a3, 'a4) asm_arg -> bool

val asm_arg_eq_axiom :
  ('a1, 'a2, 'a3, 'a4) arch_decl -> ('a1, 'a2, 'a3, 'a4) asm_arg
  Equality.axiom

val asm_arg_eqMixin :
  ('a1, 'a2, 'a3, 'a4) arch_decl -> ('a1, 'a2, 'a3, 'a4) asm_arg
  Equality.mixin_of

val asm_arg_eqType : ('a1, 'a2, 'a3, 'a4) arch_decl -> Equality.coq_type

type msb_flag =
| MSB_CLEAR
| MSB_MERGE

val msb_flag_beq : msb_flag -> msb_flag -> bool

val msb_flag_eq_axiom : msb_flag Equality.axiom

val msb_flag_eqMixin : msb_flag Equality.mixin_of

val msb_flag_eqType : Equality.coq_type

type ('reg, 'xreg, 'rflag, 'cond) implicit_arg =
| IArflag of ('reg, 'xreg, 'rflag, 'cond) rflag_t
| IAreg of ('reg, 'xreg, 'rflag, 'cond) reg_t

type addr_kind =
| AK_compute
| AK_mem

type ('reg, 'xreg, 'rflag, 'cond) arg_desc =
| ADImplicit of ('reg, 'xreg, 'rflag, 'cond) implicit_arg
| ADExplicit of addr_kind * nat * ('reg, 'xreg, 'rflag, 'cond) reg_t option

val coq_F :
  ('a1, 'a2, 'a3, 'a4) arch_decl -> ('a1, 'a2, 'a3, 'a4) rflag_t -> ('a1,
  'a2, 'a3, 'a4) arg_desc

val coq_R :
  ('a1, 'a2, 'a3, 'a4) arch_decl -> ('a1, 'a2, 'a3, 'a4) reg_t -> ('a1, 'a2,
  'a3, 'a4) arg_desc

val coq_E :
  ('a1, 'a2, 'a3, 'a4) arch_decl -> nat -> ('a1, 'a2, 'a3, 'a4) arg_desc

val coq_Ec :
  ('a1, 'a2, 'a3, 'a4) arch_decl -> nat -> ('a1, 'a2, 'a3, 'a4) arg_desc

val coq_Ef :
  ('a1, 'a2, 'a3, 'a4) arch_decl -> nat -> ('a1, 'a2, 'a3, 'a4) reg_t ->
  ('a1, 'a2, 'a3, 'a4) arg_desc

val check_oreg :
  ('a1, 'a2, 'a3, 'a4) arch_decl -> Equality.sort option -> ('a1, 'a2, 'a3,
  'a4) asm_arg -> bool

type arg_kind =
| CAcond
| CAreg
| CAxmm
| CAmem of bool
| CAimm of wsize

val arg_kind_beq : arg_kind -> arg_kind -> bool

val arg_kind_eq_axiom : arg_kind Equality.axiom

val arg_kind_eqMixin : arg_kind Equality.mixin_of

val arg_kind_eqType : Equality.coq_type

type arg_kinds = arg_kind list

type args_kinds = arg_kinds list

type i_args_kinds = args_kinds list

type ('reg, 'xreg, 'rflag, 'cond) pp_asm_op_ext =
| PP_error
| PP_name
| PP_iname of wsize
| PP_iname2 of char list * wsize * wsize
| PP_viname of velem * bool
| PP_viname2 of velem * velem
| PP_ct of ('reg, 'xreg, 'rflag, 'cond) asm_arg

type ('reg, 'xreg, 'rflag, 'cond) pp_asm_op = { pp_aop_name : char list;
                                                pp_aop_ext : ('reg, 'xreg,
                                                             'rflag, 'cond)
                                                             pp_asm_op_ext;
                                                pp_aop_args : (wsize * ('reg,
                                                              'xreg, 'rflag,
                                                              'cond) asm_arg)
                                                              list }

type ('reg, 'xreg, 'rflag, 'cond) instr_desc_t = { id_msb_flag : msb_flag;
                                                   id_tin : stype list;
                                                   id_in : ('reg, 'xreg,
                                                           'rflag, 'cond)
                                                           arg_desc list;
                                                   id_tout : stype list;
                                                   id_out : ('reg, 'xreg,
                                                            'rflag, 'cond)
                                                            arg_desc list;
                                                   id_semi : sem_tuple exec
                                                             sem_prod;
                                                   id_args_kinds : i_args_kinds;
                                                   id_nargs : nat;
                                                   id_str_jas : (unit ->
                                                                char list);
                                                   id_safe : safe_cond list;
                                                   id_wsize : wsize;
                                                   id_pp_asm : (('reg, 'xreg,
                                                               'rflag, 'cond)
                                                               asm_args ->
                                                               ('reg, 'xreg,
                                                               'rflag, 'cond)
                                                               pp_asm_op) }

type 'asm_op prim_constructor =
| PrimP of wsize * (wsize -> 'asm_op)
| PrimM of 'asm_op
| PrimV of (velem -> wsize -> 'asm_op)
| PrimSV of (signedness -> velem -> wsize -> 'asm_op)
| PrimX of (wsize -> wsize -> 'asm_op)
| PrimVV of (velem -> wsize -> velem -> wsize -> 'asm_op)

type ('reg, 'xreg, 'rflag, 'cond, 'asm_op) asm_op_decl = { _eqT : 'asm_op
                                                                  eqTypeC;
                                                           instr_desc_op : 
                                                           ('asm_op -> ('reg,
                                                           'xreg, 'rflag,
                                                           'cond)
                                                           instr_desc_t);
                                                           prim_string : 
                                                           (char list * 'asm_op
                                                           prim_constructor)
                                                           list }

type ('reg, 'xreg, 'rflag, 'cond, 'asm_op) asm_op_t' = 'asm_op

type ('reg, 'xreg, 'rflag, 'cond, 'asm_op) asm_op_msb_t =
  wsize option * 'asm_op

val extend_size : wsize -> stype -> stype

val wextend_size : wsize -> stype -> sem_ot -> sem_ot

val extend_tuple : wsize -> stype list -> sem_tuple -> sem_tuple

val apply_lprod : ('a1 -> 'a2) -> __ list -> 'a1 lprod -> 'a2 lprod

val is_not_CAmem : arg_kind -> bool

val exclude_mem_args_kinds :
  ('a1, 'a2, 'a3, 'a4) arch_decl -> ('a1, 'a2, 'a3, 'a4) arg_desc ->
  args_kinds -> args_kinds

val exclude_mem_i_args_kinds :
  ('a1, 'a2, 'a3, 'a4) arch_decl -> ('a1, 'a2, 'a3, 'a4) arg_desc ->
  i_args_kinds -> i_args_kinds

val exclude_mem_aux :
  ('a1, 'a2, 'a3, 'a4) arch_decl -> i_args_kinds -> ('a1, 'a2, 'a3, 'a4)
  arg_desc list -> i_args_kinds

val exclude_mem :
  ('a1, 'a2, 'a3, 'a4) arch_decl -> i_args_kinds -> ('a1, 'a2, 'a3, 'a4)
  arg_desc list -> i_args_kinds

val instr_desc :
  ('a1, 'a2, 'a3, 'a4) arch_decl -> ('a1, 'a2, 'a3, 'a4, 'a5) asm_op_decl ->
  ('a1, 'a2, 'a3, 'a4, 'a5) asm_op_msb_t -> ('a1, 'a2, 'a3, 'a4) instr_desc_t

type ('reg, 'xreg, 'rflag, 'cond, 'asm_op) asm_i =
| ALIGN
| LABEL of label
| STORELABEL of ('reg, 'xreg, 'rflag, 'cond) reg_t * label
| JMP of remote_label
| JMPI of ('reg, 'xreg, 'rflag, 'cond) asm_arg
| Jcc of label * ('reg, 'xreg, 'rflag, 'cond) cond_t
| JAL of ('reg, 'xreg, 'rflag, 'cond) reg_t * remote_label
| CALL of remote_label
| POPPC
| AsmOp of ('reg, 'xreg, 'rflag, 'cond, 'asm_op) asm_op_t'
   * ('reg, 'xreg, 'rflag, 'cond) asm_args

type ('reg, 'xreg, 'rflag, 'cond, 'asm_op) asm_code =
  ('reg, 'xreg, 'rflag, 'cond, 'asm_op) asm_i list

type ('reg, 'xreg, 'rflag, 'cond) asm_typed_reg =
| ARReg of ('reg, 'xreg, 'rflag, 'cond) reg_t
| AXReg of ('reg, 'xreg, 'rflag, 'cond) xreg_t
| ABReg of ('reg, 'xreg, 'rflag, 'cond) rflag_t

type ('reg, 'xreg, 'rflag, 'cond, 'asm_op) asm_fundef = { asm_fd_align : 
                                                          wsize;
                                                          asm_fd_arg : 
                                                          ('reg, 'xreg,
                                                          'rflag, 'cond)
                                                          asm_typed_reg list;
                                                          asm_fd_body : 
                                                          ('reg, 'xreg,
                                                          'rflag, 'cond,
                                                          'asm_op) asm_code;
                                                          asm_fd_res : 
                                                          ('reg, 'xreg,
                                                          'rflag, 'cond)
                                                          asm_typed_reg list;
                                                          asm_fd_export : 
                                                          bool;
                                                          asm_fd_total_stack : 
                                                          coq_Z }

type ('reg, 'xreg, 'rflag, 'cond, 'asm_op) asm_prog = { asm_globs : GRing.ComRing.sort
                                                                    list;
                                                        asm_funcs : (funname * ('reg,
                                                                    'xreg,
                                                                    'rflag,
                                                                    'cond,
                                                                    'asm_op)
                                                                    asm_fundef)
                                                                    list }

type ('reg, 'xreg, 'rflag, 'cond, 'asm_op) asm = { _arch_decl : ('reg, 'xreg,
                                                                'rflag,
                                                                'cond)
                                                                arch_decl;
                                                   _asm_op_decl : ('reg,
                                                                  'xreg,
                                                                  'rflag,
                                                                  'cond,
                                                                  'asm_op)
                                                                  asm_op_decl;
                                                   eval_cond : ((('reg,
                                                               'xreg, 'rflag,
                                                               'cond) rflag_t
                                                               -> bool exec)
                                                               -> ('reg,
                                                               'xreg, 'rflag,
                                                               'cond) cond_t
                                                               -> bool exec);
                                                   stack_pointer_register : 
                                                   ('reg, 'xreg, 'rflag,
                                                   'cond) reg_t }
