open BinInt
open BinNums
open Bool
open Datatypes
open Div
open Eqtype
open Global
open Memory_model
open Seq
open Sopn
open SsrZ
open Ssralg
open Ssrbool
open Ssreflect
open Ssrfun
open Type
open Utils0
open Var0
open Warray_
open Word0
open Wsize

type __ = Obj.t

type cmp_kind =
| Cmp_int
| Cmp_w of signedness * wsize

type op_kind =
| Op_int
| Op_w of wsize

type sop1 =
| Oword_of_int of wsize
| Oint_of_word of wsize
| Osignext of wsize * wsize
| Ozeroext of wsize * wsize
| Onot
| Olnot of wsize
| Oneg of op_kind

type sop2 =
| Obeq
| Oand
| Oor
| Oadd of op_kind
| Omul of op_kind
| Osub of op_kind
| Odiv of cmp_kind
| Omod of cmp_kind
| Oland of wsize
| Olor of wsize
| Olxor of wsize
| Olsr of wsize
| Olsl of op_kind
| Oasr of op_kind
| Oeq of op_kind
| Oneq of op_kind
| Olt of cmp_kind
| Ole of cmp_kind
| Ogt of cmp_kind
| Oge of cmp_kind
| Ovadd of velem * wsize
| Ovsub of velem * wsize
| Ovmul of velem * wsize
| Ovlsr of velem * wsize
| Ovlsl of velem * wsize
| Ovasr of velem * wsize

type combine_flags_core =
| CFC_O
| CFC_B
| CFC_E
| CFC_S
| CFC_L
| CFC_BE
| CFC_LE

type combine_flags =
| CF_LT of signedness
| CF_LE of signedness
| CF_EQ
| CF_NEQ
| CF_GE of signedness
| CF_GT of signedness

type opN =
| Opack of wsize * pelem
| Ocombine_flags of combine_flags

(** val internal_op_kind_beq : op_kind -> op_kind -> bool **)

let internal_op_kind_beq x y =
  match x with
  | Op_int -> (match y with
               | Op_int -> true
               | Op_w _ -> false)
  | Op_w x0 -> (match y with
                | Op_int -> false
                | Op_w x1 -> wsize_beq x0 x1)

(** val sop1_beq : sop1 -> sop1 -> bool **)

let sop1_beq x y =
  match x with
  | Oword_of_int x0 ->
    (match y with
     | Oword_of_int x1 -> wsize_beq x0 x1
     | _ -> false)
  | Oint_of_word x0 ->
    (match y with
     | Oint_of_word x1 -> wsize_beq x0 x1
     | _ -> false)
  | Osignext (x0, x1) ->
    (match y with
     | Osignext (x2, x3) -> (&&) (wsize_beq x0 x2) (wsize_beq x1 x3)
     | _ -> false)
  | Ozeroext (x0, x1) ->
    (match y with
     | Ozeroext (x2, x3) -> (&&) (wsize_beq x0 x2) (wsize_beq x1 x3)
     | _ -> false)
  | Onot -> (match y with
             | Onot -> true
             | _ -> false)
  | Olnot x0 -> (match y with
                 | Olnot x1 -> wsize_beq x0 x1
                 | _ -> false)
  | Oneg x0 ->
    (match y with
     | Oneg x1 -> internal_op_kind_beq x0 x1
     | _ -> false)

(** val sop1_eq_dec : sop1 -> sop1 -> bool **)

let sop1_eq_dec x y =
  let b = sop1_beq x y in if b then true else false

(** val sop1_eq_axiom : sop1 Equality.axiom **)

let sop1_eq_axiom x y =
  iffP (sop1_beq x y) (if sop1_beq x y then ReflectT else ReflectF)

(** val sop1_eqMixin : sop1 Equality.mixin_of **)

let sop1_eqMixin =
  { Equality.op = sop1_beq; Equality.mixin_of__1 = sop1_eq_axiom }

(** val sop1_eqType : Equality.coq_type **)

let sop1_eqType =
  Obj.magic sop1_eqMixin

(** val internal_signedness_beq : signedness -> signedness -> bool **)

let internal_signedness_beq x y =
  match x with
  | Signed -> (match y with
               | Signed -> true
               | Unsigned -> false)
  | Unsigned -> (match y with
                 | Signed -> false
                 | Unsigned -> true)

(** val internal_cmp_kind_beq : cmp_kind -> cmp_kind -> bool **)

let internal_cmp_kind_beq x y =
  match x with
  | Cmp_int -> (match y with
                | Cmp_int -> true
                | Cmp_w (_, _) -> false)
  | Cmp_w (x0, x1) ->
    (match y with
     | Cmp_int -> false
     | Cmp_w (x2, x3) ->
       (&&) (internal_signedness_beq x0 x2) (wsize_beq x1 x3))

(** val sop2_beq : sop2 -> sop2 -> bool **)

let sop2_beq x y =
  match x with
  | Obeq -> (match y with
             | Obeq -> true
             | _ -> false)
  | Oand -> (match y with
             | Oand -> true
             | _ -> false)
  | Oor -> (match y with
            | Oor -> true
            | _ -> false)
  | Oadd x0 ->
    (match y with
     | Oadd x1 -> internal_op_kind_beq x0 x1
     | _ -> false)
  | Omul x0 ->
    (match y with
     | Omul x1 -> internal_op_kind_beq x0 x1
     | _ -> false)
  | Osub x0 ->
    (match y with
     | Osub x1 -> internal_op_kind_beq x0 x1
     | _ -> false)
  | Odiv x0 ->
    (match y with
     | Odiv x1 -> internal_cmp_kind_beq x0 x1
     | _ -> false)
  | Omod x0 ->
    (match y with
     | Omod x1 -> internal_cmp_kind_beq x0 x1
     | _ -> false)
  | Oland x0 -> (match y with
                 | Oland x1 -> wsize_beq x0 x1
                 | _ -> false)
  | Olor x0 -> (match y with
                | Olor x1 -> wsize_beq x0 x1
                | _ -> false)
  | Olxor x0 -> (match y with
                 | Olxor x1 -> wsize_beq x0 x1
                 | _ -> false)
  | Olsr x0 -> (match y with
                | Olsr x1 -> wsize_beq x0 x1
                | _ -> false)
  | Olsl x0 ->
    (match y with
     | Olsl x1 -> internal_op_kind_beq x0 x1
     | _ -> false)
  | Oasr x0 ->
    (match y with
     | Oasr x1 -> internal_op_kind_beq x0 x1
     | _ -> false)
  | Oeq x0 -> (match y with
               | Oeq x1 -> internal_op_kind_beq x0 x1
               | _ -> false)
  | Oneq x0 ->
    (match y with
     | Oneq x1 -> internal_op_kind_beq x0 x1
     | _ -> false)
  | Olt x0 ->
    (match y with
     | Olt x1 -> internal_cmp_kind_beq x0 x1
     | _ -> false)
  | Ole x0 ->
    (match y with
     | Ole x1 -> internal_cmp_kind_beq x0 x1
     | _ -> false)
  | Ogt x0 ->
    (match y with
     | Ogt x1 -> internal_cmp_kind_beq x0 x1
     | _ -> false)
  | Oge x0 ->
    (match y with
     | Oge x1 -> internal_cmp_kind_beq x0 x1
     | _ -> false)
  | Ovadd (x0, x1) ->
    (match y with
     | Ovadd (x2, x3) -> (&&) (velem_beq x0 x2) (wsize_beq x1 x3)
     | _ -> false)
  | Ovsub (x0, x1) ->
    (match y with
     | Ovsub (x2, x3) -> (&&) (velem_beq x0 x2) (wsize_beq x1 x3)
     | _ -> false)
  | Ovmul (x0, x1) ->
    (match y with
     | Ovmul (x2, x3) -> (&&) (velem_beq x0 x2) (wsize_beq x1 x3)
     | _ -> false)
  | Ovlsr (x0, x1) ->
    (match y with
     | Ovlsr (x2, x3) -> (&&) (velem_beq x0 x2) (wsize_beq x1 x3)
     | _ -> false)
  | Ovlsl (x0, x1) ->
    (match y with
     | Ovlsl (x2, x3) -> (&&) (velem_beq x0 x2) (wsize_beq x1 x3)
     | _ -> false)
  | Ovasr (x0, x1) ->
    (match y with
     | Ovasr (x2, x3) -> (&&) (velem_beq x0 x2) (wsize_beq x1 x3)
     | _ -> false)

(** val sop2_eq_dec : sop2 -> sop2 -> bool **)

let sop2_eq_dec x y =
  let b = sop2_beq x y in if b then true else false

(** val sop2_eq_axiom : sop2 Equality.axiom **)

let sop2_eq_axiom x y =
  iffP (sop2_beq x y) (if sop2_beq x y then ReflectT else ReflectF)

(** val sop2_eqMixin : sop2 Equality.mixin_of **)

let sop2_eqMixin =
  { Equality.op = sop2_beq; Equality.mixin_of__1 = sop2_eq_axiom }

(** val sop2_eqType : Equality.coq_type **)

let sop2_eqType =
  Obj.magic sop2_eqMixin

(** val combine_flags_core_beq :
    combine_flags_core -> combine_flags_core -> bool **)

let combine_flags_core_beq x y =
  match x with
  | CFC_O -> (match y with
              | CFC_O -> true
              | _ -> false)
  | CFC_B -> (match y with
              | CFC_B -> true
              | _ -> false)
  | CFC_E -> (match y with
              | CFC_E -> true
              | _ -> false)
  | CFC_S -> (match y with
              | CFC_S -> true
              | _ -> false)
  | CFC_L -> (match y with
              | CFC_L -> true
              | _ -> false)
  | CFC_BE -> (match y with
               | CFC_BE -> true
               | _ -> false)
  | CFC_LE -> (match y with
               | CFC_LE -> true
               | _ -> false)

(** val combine_flags_core_eq_dec :
    combine_flags_core -> combine_flags_core -> bool **)

let combine_flags_core_eq_dec x y =
  let b = combine_flags_core_beq x y in if b then true else false

(** val combine_flags_core_eq_axiom : combine_flags_core Equality.axiom **)

let combine_flags_core_eq_axiom x y =
  iffP (combine_flags_core_beq x y)
    (if combine_flags_core_beq x y then ReflectT else ReflectF)

(** val combine_flags_core_eqMixin : combine_flags_core Equality.mixin_of **)

let combine_flags_core_eqMixin =
  { Equality.op = combine_flags_core_beq; Equality.mixin_of__1 =
    combine_flags_core_eq_axiom }

(** val combine_flags_core_eqType : Equality.coq_type **)

let combine_flags_core_eqType =
  Obj.magic combine_flags_core_eqMixin

(** val combine_flags_beq : combine_flags -> combine_flags -> bool **)

let combine_flags_beq x y =
  match x with
  | CF_LT x0 ->
    (match y with
     | CF_LT x1 -> internal_signedness_beq x0 x1
     | _ -> false)
  | CF_LE x0 ->
    (match y with
     | CF_LE x1 -> internal_signedness_beq x0 x1
     | _ -> false)
  | CF_EQ -> (match y with
              | CF_EQ -> true
              | _ -> false)
  | CF_NEQ -> (match y with
               | CF_NEQ -> true
               | _ -> false)
  | CF_GE x0 ->
    (match y with
     | CF_GE x1 -> internal_signedness_beq x0 x1
     | _ -> false)
  | CF_GT x0 ->
    (match y with
     | CF_GT x1 -> internal_signedness_beq x0 x1
     | _ -> false)

(** val combine_flags_eq_dec : combine_flags -> combine_flags -> bool **)

let combine_flags_eq_dec x y =
  let b = combine_flags_beq x y in if b then true else false

(** val combine_flags_eq_axiom : combine_flags Equality.axiom **)

let combine_flags_eq_axiom x y =
  iffP (combine_flags_beq x y)
    (if combine_flags_beq x y then ReflectT else ReflectF)

(** val combine_flags_eqMixin : combine_flags Equality.mixin_of **)

let combine_flags_eqMixin =
  { Equality.op = combine_flags_beq; Equality.mixin_of__1 =
    combine_flags_eq_axiom }

(** val combine_flags_eqType : Equality.coq_type **)

let combine_flags_eqType =
  Obj.magic combine_flags_eqMixin

(** val internal_pelem_beq : pelem -> pelem -> bool **)

let internal_pelem_beq x y =
  match x with
  | PE1 -> (match y with
            | PE1 -> true
            | _ -> false)
  | PE2 -> (match y with
            | PE2 -> true
            | _ -> false)
  | PE4 -> (match y with
            | PE4 -> true
            | _ -> false)
  | PE8 -> (match y with
            | PE8 -> true
            | _ -> false)
  | PE16 -> (match y with
             | PE16 -> true
             | _ -> false)
  | PE32 -> (match y with
             | PE32 -> true
             | _ -> false)
  | PE64 -> (match y with
             | PE64 -> true
             | _ -> false)
  | PE128 -> (match y with
              | PE128 -> true
              | _ -> false)

(** val opN_beq : opN -> opN -> bool **)

let opN_beq x y =
  match x with
  | Opack (x0, x1) ->
    (match y with
     | Opack (x2, x3) -> (&&) (wsize_beq x0 x2) (internal_pelem_beq x1 x3)
     | Ocombine_flags _ -> false)
  | Ocombine_flags x0 ->
    (match y with
     | Opack (_, _) -> false
     | Ocombine_flags x1 -> combine_flags_beq x0 x1)

(** val opN_eq_dec : opN -> opN -> bool **)

let opN_eq_dec x y =
  let b = opN_beq x y in if b then true else false

(** val opN_eq_axiom : opN Equality.axiom **)

let opN_eq_axiom x y =
  iffP (opN_beq x y) (if opN_beq x y then ReflectT else ReflectF)

(** val opN_eqMixin : opN Equality.mixin_of **)

let opN_eqMixin =
  { Equality.op = opN_beq; Equality.mixin_of__1 = opN_eq_axiom }

(** val opN_eqType : Equality.coq_type **)

let opN_eqType =
  Obj.magic opN_eqMixin

(** val type_of_op1 : sop1 -> stype * stype **)

let type_of_op1 = function
| Oword_of_int sz -> (Coq_sint, (Coq_sword sz))
| Oint_of_word sz -> ((Coq_sword sz), Coq_sint)
| Osignext (szo, szi) -> ((Coq_sword szi), (Coq_sword szo))
| Ozeroext (szo, szi) -> ((Coq_sword szi), (Coq_sword szo))
| Onot -> (Coq_sbool, Coq_sbool)
| Olnot sz -> let t0 = Coq_sword sz in (t0, t0)
| Oneg o0 ->
  (match o0 with
   | Op_int -> (Coq_sint, Coq_sint)
   | Op_w sz -> let t0 = Coq_sword sz in (t0, t0))

(** val type_of_op2 : sop2 -> (stype * stype) * stype **)

let type_of_op2 = function
| Oadd o0 ->
  (match o0 with
   | Op_int -> ((Coq_sint, Coq_sint), Coq_sint)
   | Op_w s -> let t0 = Coq_sword s in ((t0, t0), t0))
| Omul o0 ->
  (match o0 with
   | Op_int -> ((Coq_sint, Coq_sint), Coq_sint)
   | Op_w s -> let t0 = Coq_sword s in ((t0, t0), t0))
| Osub o0 ->
  (match o0 with
   | Op_int -> ((Coq_sint, Coq_sint), Coq_sint)
   | Op_w s -> let t0 = Coq_sword s in ((t0, t0), t0))
| Odiv c ->
  (match c with
   | Cmp_int -> ((Coq_sint, Coq_sint), Coq_sint)
   | Cmp_w (_, s) -> let t0 = Coq_sword s in ((t0, t0), t0))
| Omod c ->
  (match c with
   | Cmp_int -> ((Coq_sint, Coq_sint), Coq_sint)
   | Cmp_w (_, s) -> let t0 = Coq_sword s in ((t0, t0), t0))
| Oland s -> let t0 = Coq_sword s in ((t0, t0), t0)
| Olor s -> let t0 = Coq_sword s in ((t0, t0), t0)
| Olxor s -> let t0 = Coq_sword s in ((t0, t0), t0)
| Olsr s -> let t0 = Coq_sword s in ((t0, (Coq_sword U8)), t0)
| Olsl o0 ->
  (match o0 with
   | Op_int -> ((Coq_sint, Coq_sint), Coq_sint)
   | Op_w s -> let t0 = Coq_sword s in ((t0, (Coq_sword U8)), t0))
| Oasr o0 ->
  (match o0 with
   | Op_int -> ((Coq_sint, Coq_sint), Coq_sint)
   | Op_w s -> let t0 = Coq_sword s in ((t0, (Coq_sword U8)), t0))
| Oeq o0 ->
  (match o0 with
   | Op_int -> ((Coq_sint, Coq_sint), Coq_sbool)
   | Op_w s -> let t0 = Coq_sword s in ((t0, t0), Coq_sbool))
| Oneq o0 ->
  (match o0 with
   | Op_int -> ((Coq_sint, Coq_sint), Coq_sbool)
   | Op_w s -> let t0 = Coq_sword s in ((t0, t0), Coq_sbool))
| Olt c ->
  (match c with
   | Cmp_int -> ((Coq_sint, Coq_sint), Coq_sbool)
   | Cmp_w (_, s) -> let t0 = Coq_sword s in ((t0, t0), Coq_sbool))
| Ole c ->
  (match c with
   | Cmp_int -> ((Coq_sint, Coq_sint), Coq_sbool)
   | Cmp_w (_, s) -> let t0 = Coq_sword s in ((t0, t0), Coq_sbool))
| Ogt c ->
  (match c with
   | Cmp_int -> ((Coq_sint, Coq_sint), Coq_sbool)
   | Cmp_w (_, s) -> let t0 = Coq_sword s in ((t0, t0), Coq_sbool))
| Oge c ->
  (match c with
   | Cmp_int -> ((Coq_sint, Coq_sint), Coq_sbool)
   | Cmp_w (_, s) -> let t0 = Coq_sword s in ((t0, t0), Coq_sbool))
| Ovadd (_, s) -> let t0 = Coq_sword s in ((t0, t0), t0)
| Ovsub (_, s) -> let t0 = Coq_sword s in ((t0, t0), t0)
| Ovmul (_, s) -> let t0 = Coq_sword s in ((t0, t0), t0)
| Ovlsr (_, s) -> let t0 = Coq_sword s in ((t0, (Coq_sword U8)), t0)
| Ovlsl (_, s) -> let t0 = Coq_sword s in ((t0, (Coq_sword U8)), t0)
| Ovasr (_, s) -> let t0 = Coq_sword s in ((t0, (Coq_sword U8)), t0)
| _ -> ((Coq_sbool, Coq_sbool), Coq_sbool)

(** val cf_tbl : combine_flags -> bool * combine_flags_core **)

let cf_tbl = function
| CF_LT s ->
  (match s with
   | Signed -> (false, CFC_L)
   | Unsigned -> (false, CFC_B))
| CF_LE s ->
  (match s with
   | Signed -> (false, CFC_LE)
   | Unsigned -> (false, CFC_BE))
| CF_EQ -> (false, CFC_E)
| CF_NEQ -> (true, CFC_E)
| CF_GE s ->
  (match s with
   | Signed -> (true, CFC_L)
   | Unsigned -> (true, CFC_B))
| CF_GT s ->
  (match s with
   | Signed -> (true, CFC_LE)
   | Unsigned -> (true, CFC_BE))

(** val tin_combine_flags : stype list **)

let tin_combine_flags =
  Coq_sbool :: (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: [])))

(** val type_of_opN : opN -> stype list * stype **)

let type_of_opN = function
| Opack (ws, p) ->
  let n = divn (nat_of_wsize ws) (nat_of_pelem p) in
  ((nseq n Coq_sint), (Coq_sword ws))
| Ocombine_flags _ -> (tin_combine_flags, Coq_sbool)

type var_info = positive

type var_i = { v_var : Var.var; v_info : var_info }

(** val v_var : var_i -> Var.var **)

let v_var v =
  v.v_var

(** val v_info : var_i -> var_info **)

let v_info v =
  v.v_info

(** val var_i_beq : var_i -> var_i -> bool **)

let var_i_beq x1 x2 =
  let { v_var = x3; v_info = i1 } = x1 in
  let { v_var = x4; v_info = i2 } = x2 in
  (&&) (eq_op Var.var_eqType (Obj.magic x3) (Obj.magic x4))
    (eq_op pos_eqType (Obj.magic i1) (Obj.magic i2))

(** val var_i_eq_axiom : var_i Equality.axiom **)

let var_i_eq_axiom __top_assumption_ =
  let _evar_0_ = fun x xi __top_assumption_0 ->
    let _evar_0_ = fun y yi ->
      iffP ((&&) (eq_op Var.var_eqType x y) (eq_op pos_eqType xi yi))
        (andP (eq_op Var.var_eqType x y) (eq_op pos_eqType xi yi))
    in
    let { v_var = v_var0; v_info = v_info0 } = __top_assumption_0 in
    Obj.magic _evar_0_ v_var0 v_info0
  in
  let { v_var = v_var0; v_info = v_info0 } = __top_assumption_ in
  Obj.magic _evar_0_ v_var0 v_info0

(** val var_i_eqMixin : var_i Equality.mixin_of **)

let var_i_eqMixin =
  { Equality.op = var_i_beq; Equality.mixin_of__1 = var_i_eq_axiom }

(** val var_i_eqType : Equality.coq_type **)

let var_i_eqType =
  Obj.magic var_i_eqMixin

type var_attr = bool
  (* singleton inductive, whose constructor was VarA *)

(** val va_pub : var_attr -> bool **)

let va_pub v =
  v

type v_scope =
| Slocal
| Sglob

(** val v_scope_beq : v_scope -> v_scope -> bool **)

let v_scope_beq x y =
  match x with
  | Slocal -> (match y with
               | Slocal -> true
               | Sglob -> false)
  | Sglob -> (match y with
              | Slocal -> false
              | Sglob -> true)

(** val v_scope_eq_dec : v_scope -> v_scope -> bool **)

let v_scope_eq_dec x y =
  let b = v_scope_beq x y in if b then true else false

(** val v_scope_eq_axiom : v_scope Equality.axiom **)

let v_scope_eq_axiom x y =
  iffP (v_scope_beq x y) (if v_scope_beq x y then ReflectT else ReflectF)

(** val v_scope_eqMixin : v_scope Equality.mixin_of **)

let v_scope_eqMixin =
  { Equality.op = v_scope_beq; Equality.mixin_of__1 = v_scope_eq_axiom }

(** val v_scope_eqType : Equality.coq_type **)

let v_scope_eqType =
  Obj.magic v_scope_eqMixin

type gvar = { gv : var_i; gs : v_scope }

(** val gv : gvar -> var_i **)

let gv g =
  g.gv

(** val gs : gvar -> v_scope **)

let gs g =
  g.gs

(** val mk_gvar : var_i -> gvar **)

let mk_gvar x =
  { gv = x; gs = Sglob }

(** val mk_lvar : var_i -> gvar **)

let mk_lvar x =
  { gv = x; gs = Slocal }

(** val is_lvar : gvar -> bool **)

let is_lvar x =
  eq_op v_scope_eqType (Obj.magic x.gs) (Obj.magic Slocal)

(** val is_glob : gvar -> bool **)

let is_glob x =
  eq_op v_scope_eqType (Obj.magic x.gs) (Obj.magic Sglob)

(** val gvar_beq : gvar -> gvar -> bool **)

let gvar_beq x1 x2 =
  (&&) (eq_op v_scope_eqType (Obj.magic x1.gs) (Obj.magic x2.gs))
    (eq_op var_i_eqType (Obj.magic x1.gv) (Obj.magic x2.gv))

(** val gvar_eq_axiom : gvar Equality.axiom **)

let gvar_eq_axiom __top_assumption_ =
  let _evar_0_ = fun x1 k1 __top_assumption_0 ->
    let _evar_0_ = fun x2 k2 ->
      equivP
        ((&&) (eq_op v_scope_eqType (Obj.magic k1) (Obj.magic k2))
          (eq_op var_i_eqType (Obj.magic x1) (Obj.magic x2)))
        (andP (eq_op v_scope_eqType (Obj.magic k1) (Obj.magic k2))
          (eq_op var_i_eqType (Obj.magic x1) (Obj.magic x2)))
    in
    let { gv = gv0; gs = gs0 } = __top_assumption_0 in _evar_0_ gv0 gs0
  in
  let { gv = gv0; gs = gs0 } = __top_assumption_ in _evar_0_ gv0 gs0

(** val gvar_eqMixin : gvar Equality.mixin_of **)

let gvar_eqMixin =
  { Equality.op = gvar_beq; Equality.mixin_of__1 = gvar_eq_axiom }

(** val gvar_eqType : Equality.coq_type **)

let gvar_eqType =
  Obj.magic gvar_eqMixin

type pexpr =
| Pconst of coq_Z
| Pbool of bool
| Parr_init of positive
| Pvar of gvar
| Pget of arr_access * wsize * gvar * pexpr
| Psub of arr_access * wsize * positive * gvar * pexpr
| Pload of wsize * var_i * pexpr
| Papp1 of sop1 * pexpr
| Papp2 of sop2 * pexpr * pexpr
| PappN of opN * pexpr list
| Pif of stype * pexpr * pexpr * pexpr

(** val coq_Plvar : var_i -> pexpr **)

let coq_Plvar x =
  Pvar (mk_lvar x)

(** val pexpr_beq : pexpr -> pexpr -> bool **)

let rec pexpr_beq e1 e2 =
  match e1 with
  | Pconst n1 ->
    (match e2 with
     | Pconst n2 -> eq_op coq_Z_eqType (Obj.magic n1) (Obj.magic n2)
     | _ -> false)
  | Pbool b1 ->
    (match e2 with
     | Pbool b2 -> eq_op bool_eqType (Obj.magic b1) (Obj.magic b2)
     | _ -> false)
  | Parr_init n1 ->
    (match e2 with
     | Parr_init n2 -> eq_op pos_eqType (Obj.magic n1) (Obj.magic n2)
     | _ -> false)
  | Pvar x1 ->
    (match e2 with
     | Pvar x2 -> eq_op gvar_eqType (Obj.magic x1) (Obj.magic x2)
     | _ -> false)
  | Pget (aa1, sz1, x1, e3) ->
    (match e2 with
     | Pget (aa2, sz2, x2, e4) ->
       (&&)
         ((&&)
           ((&&) (eq_op arr_access_eqType (Obj.magic aa1) (Obj.magic aa2))
             (eq_op wsize_eqType (Obj.magic sz1) (Obj.magic sz2)))
           (eq_op gvar_eqType (Obj.magic x1) (Obj.magic x2)))
         (pexpr_beq e3 e4)
     | _ -> false)
  | Psub (aa1, sz1, len1, x1, e3) ->
    (match e2 with
     | Psub (aa2, sz2, len2, x2, e4) ->
       (&&)
         ((&&)
           ((&&)
             ((&&) (eq_op arr_access_eqType (Obj.magic aa1) (Obj.magic aa2))
               (eq_op wsize_eqType (Obj.magic sz1) (Obj.magic sz2)))
             (eq_op pos_eqType (Obj.magic len1) (Obj.magic len2)))
           (eq_op gvar_eqType (Obj.magic x1) (Obj.magic x2)))
         (pexpr_beq e3 e4)
     | _ -> false)
  | Pload (sz1, x1, e3) ->
    (match e2 with
     | Pload (sz2, x2, e4) ->
       (&&)
         ((&&) (eq_op wsize_eqType (Obj.magic sz1) (Obj.magic sz2))
           (eq_op var_i_eqType (Obj.magic x1) (Obj.magic x2)))
         (pexpr_beq e3 e4)
     | _ -> false)
  | Papp1 (o1, e3) ->
    (match e2 with
     | Papp1 (o2, e4) ->
       (&&) (eq_op sop1_eqType (Obj.magic o1) (Obj.magic o2))
         (pexpr_beq e3 e4)
     | _ -> false)
  | Papp2 (o1, e11, e12) ->
    (match e2 with
     | Papp2 (o2, e21, e22) ->
       (&&)
         ((&&) (eq_op sop2_eqType (Obj.magic o1) (Obj.magic o2))
           (pexpr_beq e11 e21)) (pexpr_beq e12 e22)
     | _ -> false)
  | PappN (o1, es1) ->
    (match e2 with
     | PappN (o2, es2) ->
       (&&) (eq_op opN_eqType (Obj.magic o1) (Obj.magic o2))
         (all2 pexpr_beq es1 es2)
     | _ -> false)
  | Pif (t1, b1, e11, e12) ->
    (match e2 with
     | Pif (t2, b2, e21, e22) ->
       (&&)
         ((&&)
           ((&&) (eq_op stype_eqType (Obj.magic t1) (Obj.magic t2))
             (pexpr_beq b1 b2)) (pexpr_beq e11 e21)) (pexpr_beq e12 e22)
     | _ -> false)

(** val pexpr_eq_axiom : pexpr Equality.axiom **)

let pexpr_eq_axiom __top_assumption_ =
  let _evar_0_ = fun n1 __top_assumption_0 ->
    let _evar_0_ = fun n2 ->
      iffP (eq_op coq_Z_eqType n1 n2)
        (if eq_op coq_Z_eqType n1 n2 then ReflectT else ReflectF)
    in
    let _evar_0_0 = fun _ -> ReflectF in
    let _evar_0_1 = fun _ -> ReflectF in
    let _evar_0_2 = fun _ -> ReflectF in
    let _evar_0_3 = fun _ _ _ _ -> ReflectF in
    let _evar_0_4 = fun _ _ _ _ _ -> ReflectF in
    let _evar_0_5 = fun _ _ _ -> ReflectF in
    let _evar_0_6 = fun _ _ -> ReflectF in
    let _evar_0_7 = fun _ _ _ -> ReflectF in
    let _evar_0_8 = fun _ _ -> ReflectF in
    let _evar_0_9 = fun _ _ _ _ -> ReflectF in
    (match __top_assumption_0 with
     | Pconst z -> Obj.magic _evar_0_ z
     | Pbool b -> _evar_0_0 b
     | Parr_init p -> _evar_0_1 p
     | Pvar g -> _evar_0_2 g
     | Pget (a, w, g, p) -> _evar_0_3 a w g p
     | Psub (a, w, p, g, p0) -> _evar_0_4 a w p g p0
     | Pload (w, v, p) -> _evar_0_5 w v p
     | Papp1 (s, p) -> _evar_0_6 s p
     | Papp2 (s, p, p0) -> _evar_0_7 s p p0
     | PappN (o, l) -> _evar_0_8 o l
     | Pif (s, p, p0, p1) -> _evar_0_9 s p p0 p1)
  in
  let _evar_0_0 = fun b1 __top_assumption_0 ->
    let _evar_0_0 = fun _ -> ReflectF in
    let _evar_0_1 = fun b2 ->
      iffP (eq_op bool_eqType b1 b2)
        (if eq_op bool_eqType b1 b2 then ReflectT else ReflectF)
    in
    let _evar_0_2 = fun _ -> ReflectF in
    let _evar_0_3 = fun _ -> ReflectF in
    let _evar_0_4 = fun _ _ _ _ -> ReflectF in
    let _evar_0_5 = fun _ _ _ _ _ -> ReflectF in
    let _evar_0_6 = fun _ _ _ -> ReflectF in
    let _evar_0_7 = fun _ _ -> ReflectF in
    let _evar_0_8 = fun _ _ _ -> ReflectF in
    let _evar_0_9 = fun _ _ -> ReflectF in
    let _evar_0_10 = fun _ _ _ _ -> ReflectF in
    (match __top_assumption_0 with
     | Pconst z -> _evar_0_0 z
     | Pbool b -> Obj.magic _evar_0_1 b
     | Parr_init p -> _evar_0_2 p
     | Pvar g -> _evar_0_3 g
     | Pget (a, w, g, p) -> _evar_0_4 a w g p
     | Psub (a, w, p, g, p0) -> _evar_0_5 a w p g p0
     | Pload (w, v, p) -> _evar_0_6 w v p
     | Papp1 (s, p) -> _evar_0_7 s p
     | Papp2 (s, p, p0) -> _evar_0_8 s p p0
     | PappN (o, l) -> _evar_0_9 o l
     | Pif (s, p, p0, p1) -> _evar_0_10 s p p0 p1)
  in
  let _evar_0_1 = fun n1 __top_assumption_0 ->
    let _evar_0_1 = fun _ -> ReflectF in
    let _evar_0_2 = fun _ -> ReflectF in
    let _evar_0_3 = fun n2 ->
      iffP (eq_op pos_eqType n1 n2)
        (if eq_op pos_eqType n1 n2 then ReflectT else ReflectF)
    in
    let _evar_0_4 = fun _ -> ReflectF in
    let _evar_0_5 = fun _ _ _ _ -> ReflectF in
    let _evar_0_6 = fun _ _ _ _ _ -> ReflectF in
    let _evar_0_7 = fun _ _ _ -> ReflectF in
    let _evar_0_8 = fun _ _ -> ReflectF in
    let _evar_0_9 = fun _ _ _ -> ReflectF in
    let _evar_0_10 = fun _ _ -> ReflectF in
    let _evar_0_11 = fun _ _ _ _ -> ReflectF in
    (match __top_assumption_0 with
     | Pconst z -> _evar_0_1 z
     | Pbool b -> _evar_0_2 b
     | Parr_init p -> Obj.magic _evar_0_3 p
     | Pvar g -> _evar_0_4 g
     | Pget (a, w, g, p) -> _evar_0_5 a w g p
     | Psub (a, w, p, g, p0) -> _evar_0_6 a w p g p0
     | Pload (w, v, p) -> _evar_0_7 w v p
     | Papp1 (s, p) -> _evar_0_8 s p
     | Papp2 (s, p, p0) -> _evar_0_9 s p p0
     | PappN (o, l) -> _evar_0_10 o l
     | Pif (s, p, p0, p1) -> _evar_0_11 s p p0 p1)
  in
  let _evar_0_2 = fun x1 __top_assumption_0 ->
    let _evar_0_2 = fun _ -> ReflectF in
    let _evar_0_3 = fun _ -> ReflectF in
    let _evar_0_4 = fun _ -> ReflectF in
    let _evar_0_5 = fun x2 ->
      iffP (eq_op gvar_eqType x1 x2)
        (if eq_op gvar_eqType x1 x2 then ReflectT else ReflectF)
    in
    let _evar_0_6 = fun _ _ _ _ -> ReflectF in
    let _evar_0_7 = fun _ _ _ _ _ -> ReflectF in
    let _evar_0_8 = fun _ _ _ -> ReflectF in
    let _evar_0_9 = fun _ _ -> ReflectF in
    let _evar_0_10 = fun _ _ _ -> ReflectF in
    let _evar_0_11 = fun _ _ -> ReflectF in
    let _evar_0_12 = fun _ _ _ _ -> ReflectF in
    (match __top_assumption_0 with
     | Pconst z -> _evar_0_2 z
     | Pbool b -> _evar_0_3 b
     | Parr_init p -> _evar_0_4 p
     | Pvar g -> Obj.magic _evar_0_5 g
     | Pget (a, w, g, p) -> _evar_0_6 a w g p
     | Psub (a, w, p, g, p0) -> _evar_0_7 a w p g p0
     | Pload (w, v, p) -> _evar_0_8 w v p
     | Papp1 (s, p) -> _evar_0_9 s p
     | Papp2 (s, p, p0) -> _evar_0_10 s p p0
     | PappN (o, l) -> _evar_0_11 o l
     | Pif (s, p, p0, p1) -> _evar_0_12 s p p0 p1)
  in
  let _evar_0_3 = fun aa1 w1 x1 e1 __top_assumption_0 ->
    let _evar_0_3 = fun _ -> ReflectF in
    let _evar_0_4 = fun _ -> ReflectF in
    let _evar_0_5 = fun _ -> ReflectF in
    let _evar_0_6 = fun _ -> ReflectF in
    let _evar_0_7 = fun aa2 w2 x2 e2 ->
      iffP
        ((&&)
          ((&&)
            ((&&) (eq_op arr_access_eqType aa1 aa2)
              (eq_op wsize_eqType w1 w2)) (eq_op gvar_eqType x1 x2))
          (pexpr_beq e1 e2))
        (if (&&)
              ((&&)
                ((&&) (eq_op arr_access_eqType aa1 aa2)
                  (eq_op wsize_eqType w1 w2)) (eq_op gvar_eqType x1 x2))
              (pexpr_beq e1 e2)
         then ReflectT
         else ReflectF)
    in
    let _evar_0_8 = fun _ _ _ _ _ -> ReflectF in
    let _evar_0_9 = fun _ _ _ -> ReflectF in
    let _evar_0_10 = fun _ _ -> ReflectF in
    let _evar_0_11 = fun _ _ _ -> ReflectF in
    let _evar_0_12 = fun _ _ -> ReflectF in
    let _evar_0_13 = fun _ _ _ _ -> ReflectF in
    (match __top_assumption_0 with
     | Pconst z -> _evar_0_3 z
     | Pbool b -> _evar_0_4 b
     | Parr_init p -> _evar_0_5 p
     | Pvar g -> _evar_0_6 g
     | Pget (a, w, g, p) -> Obj.magic _evar_0_7 a w g p
     | Psub (a, w, p, g, p0) -> _evar_0_8 a w p g p0
     | Pload (w, v, p) -> _evar_0_9 w v p
     | Papp1 (s, p) -> _evar_0_10 s p
     | Papp2 (s, p, p0) -> _evar_0_11 s p p0
     | PappN (o, l) -> _evar_0_12 o l
     | Pif (s, p, p0, p1) -> _evar_0_13 s p p0 p1)
  in
  let _evar_0_4 = fun aa1 w1 x1 e1 len1 __top_assumption_0 ->
    let _evar_0_4 = fun _ -> ReflectF in
    let _evar_0_5 = fun _ -> ReflectF in
    let _evar_0_6 = fun _ -> ReflectF in
    let _evar_0_7 = fun _ -> ReflectF in
    let _evar_0_8 = fun _ _ _ _ -> ReflectF in
    let _evar_0_9 = fun aa2 w2 x2 e2 len2 ->
      iffP
        ((&&)
          ((&&)
            ((&&)
              ((&&) (eq_op arr_access_eqType aa1 aa2)
                (eq_op wsize_eqType w1 w2)) (eq_op pos_eqType x1 x2))
            (eq_op gvar_eqType e1 e2)) (pexpr_beq len1 len2))
        (if (&&)
              ((&&)
                ((&&)
                  ((&&) (eq_op arr_access_eqType aa1 aa2)
                    (eq_op wsize_eqType w1 w2)) (eq_op pos_eqType x1 x2))
                (eq_op gvar_eqType e1 e2)) (pexpr_beq len1 len2)
         then ReflectT
         else ReflectF)
    in
    let _evar_0_10 = fun _ _ _ -> ReflectF in
    let _evar_0_11 = fun _ _ -> ReflectF in
    let _evar_0_12 = fun _ _ _ -> ReflectF in
    let _evar_0_13 = fun _ _ -> ReflectF in
    let _evar_0_14 = fun _ _ _ _ -> ReflectF in
    (match __top_assumption_0 with
     | Pconst z -> _evar_0_4 z
     | Pbool b -> _evar_0_5 b
     | Parr_init p -> _evar_0_6 p
     | Pvar g -> _evar_0_7 g
     | Pget (a, w, g, p) -> _evar_0_8 a w g p
     | Psub (a, w, p, g, p0) -> Obj.magic _evar_0_9 a w p g p0
     | Pload (w, v, p) -> _evar_0_10 w v p
     | Papp1 (s, p) -> _evar_0_11 s p
     | Papp2 (s, p, p0) -> _evar_0_12 s p p0
     | PappN (o, l) -> _evar_0_13 o l
     | Pif (s, p, p0, p1) -> _evar_0_14 s p p0 p1)
  in
  let _evar_0_5 = fun w1 x1 e1 __top_assumption_0 ->
    let _evar_0_5 = fun _ -> ReflectF in
    let _evar_0_6 = fun _ -> ReflectF in
    let _evar_0_7 = fun _ -> ReflectF in
    let _evar_0_8 = fun _ -> ReflectF in
    let _evar_0_9 = fun _ _ _ _ -> ReflectF in
    let _evar_0_10 = fun _ _ _ _ _ -> ReflectF in
    let _evar_0_11 = fun w2 x2 e2 ->
      iffP
        ((&&) ((&&) (eq_op wsize_eqType w1 w2) (eq_op var_i_eqType x1 x2))
          (pexpr_beq e1 e2))
        (if (&&) ((&&) (eq_op wsize_eqType w1 w2) (eq_op var_i_eqType x1 x2))
              (pexpr_beq e1 e2)
         then ReflectT
         else ReflectF)
    in
    let _evar_0_12 = fun _ _ -> ReflectF in
    let _evar_0_13 = fun _ _ _ -> ReflectF in
    let _evar_0_14 = fun _ _ -> ReflectF in
    let _evar_0_15 = fun _ _ _ _ -> ReflectF in
    (match __top_assumption_0 with
     | Pconst z -> _evar_0_5 z
     | Pbool b -> _evar_0_6 b
     | Parr_init p -> _evar_0_7 p
     | Pvar g -> _evar_0_8 g
     | Pget (a, w, g, p) -> _evar_0_9 a w g p
     | Psub (a, w, p, g, p0) -> _evar_0_10 a w p g p0
     | Pload (w, v, p) -> Obj.magic _evar_0_11 w v p
     | Papp1 (s, p) -> _evar_0_12 s p
     | Papp2 (s, p, p0) -> _evar_0_13 s p p0
     | PappN (o, l) -> _evar_0_14 o l
     | Pif (s, p, p0, p1) -> _evar_0_15 s p p0 p1)
  in
  let _evar_0_6 = fun o1 e1 __top_assumption_0 ->
    let _evar_0_6 = fun _ -> ReflectF in
    let _evar_0_7 = fun _ -> ReflectF in
    let _evar_0_8 = fun _ -> ReflectF in
    let _evar_0_9 = fun _ -> ReflectF in
    let _evar_0_10 = fun _ _ _ _ -> ReflectF in
    let _evar_0_11 = fun _ _ _ _ _ -> ReflectF in
    let _evar_0_12 = fun _ _ _ -> ReflectF in
    let _evar_0_13 = fun o2 e2 ->
      iffP ((&&) (eq_op sop1_eqType o1 o2) (pexpr_beq e1 e2))
        (if (&&) (eq_op sop1_eqType o1 o2) (pexpr_beq e1 e2)
         then ReflectT
         else ReflectF)
    in
    let _evar_0_14 = fun _ _ _ -> ReflectF in
    let _evar_0_15 = fun _ _ -> ReflectF in
    let _evar_0_16 = fun _ _ _ _ -> ReflectF in
    (match __top_assumption_0 with
     | Pconst z -> _evar_0_6 z
     | Pbool b -> _evar_0_7 b
     | Parr_init p -> _evar_0_8 p
     | Pvar g -> _evar_0_9 g
     | Pget (a, w, g, p) -> _evar_0_10 a w g p
     | Psub (a, w, p, g, p0) -> _evar_0_11 a w p g p0
     | Pload (w, v, p) -> _evar_0_12 w v p
     | Papp1 (s, p) -> Obj.magic _evar_0_13 s p
     | Papp2 (s, p, p0) -> _evar_0_14 s p p0
     | PappN (o, l) -> _evar_0_15 o l
     | Pif (s, p, p0, p1) -> _evar_0_16 s p p0 p1)
  in
  let _evar_0_7 = fun o1 e11 e12 __top_assumption_0 ->
    let _evar_0_7 = fun _ -> ReflectF in
    let _evar_0_8 = fun _ -> ReflectF in
    let _evar_0_9 = fun _ -> ReflectF in
    let _evar_0_10 = fun _ -> ReflectF in
    let _evar_0_11 = fun _ _ _ _ -> ReflectF in
    let _evar_0_12 = fun _ _ _ _ _ -> ReflectF in
    let _evar_0_13 = fun _ _ _ -> ReflectF in
    let _evar_0_14 = fun _ _ -> ReflectF in
    let _evar_0_15 = fun o2 e21 e22 ->
      iffP
        ((&&) ((&&) (eq_op sop2_eqType o1 o2) (pexpr_beq e11 e21))
          (pexpr_beq e12 e22))
        (if (&&) ((&&) (eq_op sop2_eqType o1 o2) (pexpr_beq e11 e21))
              (pexpr_beq e12 e22)
         then ReflectT
         else ReflectF)
    in
    let _evar_0_16 = fun _ _ -> ReflectF in
    let _evar_0_17 = fun _ _ _ _ -> ReflectF in
    (match __top_assumption_0 with
     | Pconst z -> _evar_0_7 z
     | Pbool b -> _evar_0_8 b
     | Parr_init p -> _evar_0_9 p
     | Pvar g -> _evar_0_10 g
     | Pget (a, w, g, p) -> _evar_0_11 a w g p
     | Psub (a, w, p, g, p0) -> _evar_0_12 a w p g p0
     | Pload (w, v, p) -> _evar_0_13 w v p
     | Papp1 (s, p) -> _evar_0_14 s p
     | Papp2 (s, p, p0) -> Obj.magic _evar_0_15 s p p0
     | PappN (o, l) -> _evar_0_16 o l
     | Pif (s, p, p0, p1) -> _evar_0_17 s p p0 p1)
  in
  let _evar_0_8 = fun o1 es1 __top_assumption_0 ->
    let _evar_0_8 = fun _ -> ReflectF in
    let _evar_0_9 = fun _ -> ReflectF in
    let _evar_0_10 = fun _ -> ReflectF in
    let _evar_0_11 = fun _ -> ReflectF in
    let _evar_0_12 = fun _ _ _ _ -> ReflectF in
    let _evar_0_13 = fun _ _ _ _ _ -> ReflectF in
    let _evar_0_14 = fun _ _ _ -> ReflectF in
    let _evar_0_15 = fun _ _ -> ReflectF in
    let _evar_0_16 = fun _ _ _ -> ReflectF in
    let _evar_0_17 = fun o2 es2 ->
      iffP ((&&) (eq_op opN_eqType o1 o2) (all2 pexpr_beq es1 es2))
        (if (&&) (eq_op opN_eqType o1 o2) (all2 pexpr_beq es1 es2)
         then ReflectT
         else ReflectF)
    in
    let _evar_0_18 = fun _ _ _ _ -> ReflectF in
    (match __top_assumption_0 with
     | Pconst z -> _evar_0_8 z
     | Pbool b -> _evar_0_9 b
     | Parr_init p -> _evar_0_10 p
     | Pvar g -> _evar_0_11 g
     | Pget (a, w, g, p) -> _evar_0_12 a w g p
     | Psub (a, w, p, g, p0) -> _evar_0_13 a w p g p0
     | Pload (w, v, p) -> _evar_0_14 w v p
     | Papp1 (s, p) -> _evar_0_15 s p
     | Papp2 (s, p, p0) -> _evar_0_16 s p p0
     | PappN (o, l) -> Obj.magic _evar_0_17 o l
     | Pif (s, p, p0, p1) -> _evar_0_18 s p p0 p1)
  in
  let _evar_0_9 = fun st1 t1 e11 e12 __top_assumption_0 ->
    let _evar_0_9 = fun _ -> ReflectF in
    let _evar_0_10 = fun _ -> ReflectF in
    let _evar_0_11 = fun _ -> ReflectF in
    let _evar_0_12 = fun _ -> ReflectF in
    let _evar_0_13 = fun _ _ _ _ -> ReflectF in
    let _evar_0_14 = fun _ _ _ _ _ -> ReflectF in
    let _evar_0_15 = fun _ _ _ -> ReflectF in
    let _evar_0_16 = fun _ _ -> ReflectF in
    let _evar_0_17 = fun _ _ _ -> ReflectF in
    let _evar_0_18 = fun _ _ -> ReflectF in
    let _evar_0_19 = fun st2 t2 e21 e22 ->
      iffP
        ((&&)
          ((&&) ((&&) (eq_op stype_eqType st1 st2) (pexpr_beq t1 t2))
            (pexpr_beq e11 e21)) (pexpr_beq e12 e22))
        (if (&&)
              ((&&) ((&&) (eq_op stype_eqType st1 st2) (pexpr_beq t1 t2))
                (pexpr_beq e11 e21)) (pexpr_beq e12 e22)
         then ReflectT
         else ReflectF)
    in
    (match __top_assumption_0 with
     | Pconst z -> _evar_0_9 z
     | Pbool b -> _evar_0_10 b
     | Parr_init p -> _evar_0_11 p
     | Pvar g -> _evar_0_12 g
     | Pget (a, w, g, p) -> _evar_0_13 a w g p
     | Psub (a, w, p, g, p0) -> _evar_0_14 a w p g p0
     | Pload (w, v, p) -> _evar_0_15 w v p
     | Papp1 (s, p) -> _evar_0_16 s p
     | Papp2 (s, p, p0) -> _evar_0_17 s p p0
     | PappN (o, l) -> _evar_0_18 o l
     | Pif (s, p, p0, p1) -> Obj.magic _evar_0_19 s p p0 p1)
  in
  (match __top_assumption_ with
   | Pconst z -> Obj.magic _evar_0_ z
   | Pbool b -> Obj.magic _evar_0_0 b
   | Parr_init p -> Obj.magic _evar_0_1 p
   | Pvar g -> Obj.magic _evar_0_2 g
   | Pget (a, w, g, p) -> Obj.magic _evar_0_3 a w g p
   | Psub (a, w, p, g, p0) -> Obj.magic _evar_0_4 a w p g p0
   | Pload (w, v, p) -> Obj.magic _evar_0_5 w v p
   | Papp1 (s, p) -> Obj.magic _evar_0_6 s p
   | Papp2 (s, p, p0) -> Obj.magic _evar_0_7 s p p0
   | PappN (o, l) -> Obj.magic _evar_0_8 o l
   | Pif (s, p, p0, p1) -> Obj.magic _evar_0_9 s p p0 p1)

(** val pexpr_eqMixin : pexpr Equality.mixin_of **)

let pexpr_eqMixin =
  { Equality.op = pexpr_beq; Equality.mixin_of__1 = pexpr_eq_axiom }

(** val pexpr_eqType : Equality.coq_type **)

let pexpr_eqType =
  Obj.magic pexpr_eqMixin

type lval =
| Lnone of var_info * stype
| Lvar of var_i
| Lmem of wsize * var_i * pexpr
| Laset of arr_access * wsize * var_i * pexpr
| Lasub of arr_access * wsize * positive * var_i * pexpr

(** val lval_beq : lval -> lval -> bool **)

let lval_beq x1 x2 =
  match x1 with
  | Lnone (i1, t1) ->
    (match x2 with
     | Lnone (i2, t2) ->
       (&&) (eq_op pos_eqType (Obj.magic i1) (Obj.magic i2))
         (eq_op stype_eqType (Obj.magic t1) (Obj.magic t2))
     | _ -> false)
  | Lvar x3 ->
    (match x2 with
     | Lvar x4 -> eq_op var_i_eqType (Obj.magic x3) (Obj.magic x4)
     | _ -> false)
  | Lmem (w1, x3, e1) ->
    (match x2 with
     | Lmem (w2, x4, e2) ->
       (&&)
         ((&&) (eq_op wsize_eqType (Obj.magic w1) (Obj.magic w2))
           (eq_op var_i_eqType (Obj.magic x3) (Obj.magic x4)))
         (eq_op pexpr_eqType (Obj.magic e1) (Obj.magic e2))
     | _ -> false)
  | Laset (aa1, w1, x3, e1) ->
    (match x2 with
     | Laset (aa2, w2, x4, e2) ->
       (&&)
         ((&&)
           ((&&) (eq_op arr_access_eqType (Obj.magic aa1) (Obj.magic aa2))
             (eq_op wsize_eqType (Obj.magic w1) (Obj.magic w2)))
           (eq_op var_i_eqType (Obj.magic x3) (Obj.magic x4)))
         (eq_op pexpr_eqType (Obj.magic e1) (Obj.magic e2))
     | _ -> false)
  | Lasub (aa1, w1, len1, x3, e1) ->
    (match x2 with
     | Lasub (aa2, w2, len2, x4, e2) ->
       (&&)
         ((&&)
           ((&&)
             ((&&) (eq_op arr_access_eqType (Obj.magic aa1) (Obj.magic aa2))
               (eq_op wsize_eqType (Obj.magic w1) (Obj.magic w2)))
             (eq_op pos_eqType (Obj.magic len1) (Obj.magic len2)))
           (eq_op var_i_eqType (Obj.magic x3) (Obj.magic x4)))
         (eq_op pexpr_eqType (Obj.magic e1) (Obj.magic e2))
     | _ -> false)

(** val lval_eq_axiom : lval Equality.axiom **)

let lval_eq_axiom _top_assumption_ =
  let _evar_0_ = fun i1 t1 __top_assumption_ ->
    let _evar_0_ = fun i2 t2 ->
      iffP ((&&) (eq_op pos_eqType i1 i2) (eq_op stype_eqType t1 t2))
        (if (&&) (eq_op pos_eqType i1 i2) (eq_op stype_eqType t1 t2)
         then ReflectT
         else ReflectF)
    in
    let _evar_0_0 = fun _ -> ReflectF in
    let _evar_0_1 = fun _ _ _ -> ReflectF in
    let _evar_0_2 = fun _ _ _ _ -> ReflectF in
    let _evar_0_3 = fun _ _ _ _ _ -> ReflectF in
    (match __top_assumption_ with
     | Lnone (h, h0) -> Obj.magic _evar_0_ h h0
     | Lvar h -> _evar_0_0 h
     | Lmem (h, h0, h1) -> _evar_0_1 h h0 h1
     | Laset (h, h0, h1, h2) -> _evar_0_2 h h0 h1 h2
     | Lasub (h, h0, h1, h2, h3) -> _evar_0_3 h h0 h1 h2 h3)
  in
  let _evar_0_0 = fun x1 __top_assumption_ ->
    let _evar_0_0 = fun _ _ -> ReflectF in
    let _evar_0_1 = fun x2 ->
      iffP (eq_op var_i_eqType x1 x2)
        (if eq_op var_i_eqType x1 x2 then ReflectT else ReflectF)
    in
    let _evar_0_2 = fun _ _ _ -> ReflectF in
    let _evar_0_3 = fun _ _ _ _ -> ReflectF in
    let _evar_0_4 = fun _ _ _ _ _ -> ReflectF in
    (match __top_assumption_ with
     | Lnone (h, h0) -> _evar_0_0 h h0
     | Lvar h -> Obj.magic _evar_0_1 h
     | Lmem (h, h0, h1) -> _evar_0_2 h h0 h1
     | Laset (h, h0, h1, h2) -> _evar_0_3 h h0 h1 h2
     | Lasub (h, h0, h1, h2, h3) -> _evar_0_4 h h0 h1 h2 h3)
  in
  let _evar_0_1 = fun w1 x1 e1 __top_assumption_ ->
    let _evar_0_1 = fun _ _ -> ReflectF in
    let _evar_0_2 = fun _ -> ReflectF in
    let _evar_0_3 = fun w2 x2 e2 ->
      iffP
        ((&&) ((&&) (eq_op wsize_eqType w1 w2) (eq_op var_i_eqType x1 x2))
          (eq_op pexpr_eqType e1 e2))
        (if (&&) ((&&) (eq_op wsize_eqType w1 w2) (eq_op var_i_eqType x1 x2))
              (eq_op pexpr_eqType e1 e2)
         then ReflectT
         else ReflectF)
    in
    let _evar_0_4 = fun _ _ _ _ -> ReflectF in
    let _evar_0_5 = fun _ _ _ _ _ -> ReflectF in
    (match __top_assumption_ with
     | Lnone (h, h0) -> _evar_0_1 h h0
     | Lvar h -> _evar_0_2 h
     | Lmem (h, h0, h1) -> Obj.magic _evar_0_3 h h0 h1
     | Laset (h, h0, h1, h2) -> _evar_0_4 h h0 h1 h2
     | Lasub (h, h0, h1, h2, h3) -> _evar_0_5 h h0 h1 h2 h3)
  in
  let _evar_0_2 = fun aa1 w1 x1 e1 __top_assumption_ ->
    let _evar_0_2 = fun _ _ -> ReflectF in
    let _evar_0_3 = fun _ -> ReflectF in
    let _evar_0_4 = fun _ _ _ -> ReflectF in
    let _evar_0_5 = fun aa2 w2 x2 e2 ->
      iffP
        ((&&)
          ((&&)
            ((&&) (eq_op arr_access_eqType aa1 aa2)
              (eq_op wsize_eqType w1 w2)) (eq_op var_i_eqType x1 x2))
          (eq_op pexpr_eqType e1 e2))
        (if (&&)
              ((&&)
                ((&&) (eq_op arr_access_eqType aa1 aa2)
                  (eq_op wsize_eqType w1 w2)) (eq_op var_i_eqType x1 x2))
              (eq_op pexpr_eqType e1 e2)
         then ReflectT
         else ReflectF)
    in
    let _evar_0_6 = fun _ _ _ _ _ -> ReflectF in
    (match __top_assumption_ with
     | Lnone (h, h0) -> _evar_0_2 h h0
     | Lvar h -> _evar_0_3 h
     | Lmem (h, h0, h1) -> _evar_0_4 h h0 h1
     | Laset (h, h0, h1, h2) -> Obj.magic _evar_0_5 h h0 h1 h2
     | Lasub (h, h0, h1, h2, h3) -> _evar_0_6 h h0 h1 h2 h3)
  in
  let _evar_0_3 = fun aa1 w1 len1 x1 e1 __top_assumption_ ->
    let _evar_0_3 = fun _ _ -> ReflectF in
    let _evar_0_4 = fun _ -> ReflectF in
    let _evar_0_5 = fun _ _ _ -> ReflectF in
    let _evar_0_6 = fun _ _ _ _ -> ReflectF in
    let _evar_0_7 = fun aa2 w2 len2 x2 e2 ->
      iffP
        ((&&)
          ((&&)
            ((&&)
              ((&&) (eq_op arr_access_eqType aa1 aa2)
                (eq_op wsize_eqType w1 w2)) (eq_op pos_eqType len1 len2))
            (eq_op var_i_eqType x1 x2)) (eq_op pexpr_eqType e1 e2))
        (if (&&)
              ((&&)
                ((&&)
                  ((&&) (eq_op arr_access_eqType aa1 aa2)
                    (eq_op wsize_eqType w1 w2)) (eq_op pos_eqType len1 len2))
                (eq_op var_i_eqType x1 x2)) (eq_op pexpr_eqType e1 e2)
         then ReflectT
         else ReflectF)
    in
    (match __top_assumption_ with
     | Lnone (h, h0) -> _evar_0_3 h h0
     | Lvar h -> _evar_0_4 h
     | Lmem (h, h0, h1) -> _evar_0_5 h h0 h1
     | Laset (h, h0, h1, h2) -> _evar_0_6 h h0 h1 h2
     | Lasub (h, h0, h1, h2, h3) -> Obj.magic _evar_0_7 h h0 h1 h2 h3)
  in
  (match _top_assumption_ with
   | Lnone (h, h0) -> Obj.magic _evar_0_ h h0
   | Lvar h -> Obj.magic _evar_0_0 h
   | Lmem (h, h0, h1) -> Obj.magic _evar_0_1 h h0 h1
   | Laset (h, h0, h1, h2) -> Obj.magic _evar_0_2 h h0 h1 h2
   | Lasub (h, h0, h1, h2, h3) -> Obj.magic _evar_0_3 h h0 h1 h2 h3)

(** val lval_eqMixin : lval Equality.mixin_of **)

let lval_eqMixin =
  { Equality.op = lval_beq; Equality.mixin_of__1 = lval_eq_axiom }

(** val lval_eqType : Equality.coq_type **)

let lval_eqType =
  Obj.magic lval_eqMixin

type dir =
| UpTo
| DownTo

(** val dir_beq : dir -> dir -> bool **)

let dir_beq x y =
  match x with
  | UpTo -> (match y with
             | UpTo -> true
             | DownTo -> false)
  | DownTo -> (match y with
               | UpTo -> false
               | DownTo -> true)

(** val dir_eq_dec : dir -> dir -> bool **)

let dir_eq_dec x y =
  let b = dir_beq x y in if b then true else false

(** val dir_eq_axiom : dir Equality.axiom **)

let dir_eq_axiom x y =
  iffP (dir_beq x y) (if dir_beq x y then ReflectT else ReflectF)

(** val dir_eqMixin : dir Equality.mixin_of **)

let dir_eqMixin =
  { Equality.op = dir_beq; Equality.mixin_of__1 = dir_eq_axiom }

(** val dir_eqType : Equality.coq_type **)

let dir_eqType =
  Obj.magic dir_eqMixin

type range = (dir * pexpr) * pexpr

(** val wrange : dir -> coq_Z -> coq_Z -> coq_Z list **)

let wrange d n1 n2 =
  let n = Z.to_nat (Z.sub n2 n1) in
  (match d with
   | UpTo -> map (fun i -> Z.add n1 (Z.of_nat i)) (iota O n)
   | DownTo -> map (fun i -> Z.sub n2 (Z.of_nat i)) (iota O n))

type instr_info = positive

type assgn_tag =
| AT_none
| AT_keep
| AT_rename
| AT_inline
| AT_phinode

(** val assgn_tag_beq : assgn_tag -> assgn_tag -> bool **)

let assgn_tag_beq x y =
  match x with
  | AT_none -> (match y with
                | AT_none -> true
                | _ -> false)
  | AT_keep -> (match y with
                | AT_keep -> true
                | _ -> false)
  | AT_rename -> (match y with
                  | AT_rename -> true
                  | _ -> false)
  | AT_inline -> (match y with
                  | AT_inline -> true
                  | _ -> false)
  | AT_phinode -> (match y with
                   | AT_phinode -> true
                   | _ -> false)

(** val assgn_tag_eq_dec : assgn_tag -> assgn_tag -> bool **)

let assgn_tag_eq_dec x y =
  let b = assgn_tag_beq x y in if b then true else false

(** val assgn_tag_eq_axiom : assgn_tag Equality.axiom **)

let assgn_tag_eq_axiom x y =
  iffP (assgn_tag_beq x y) (if assgn_tag_beq x y then ReflectT else ReflectF)

(** val assgn_tag_eqMixin : assgn_tag Equality.mixin_of **)

let assgn_tag_eqMixin =
  { Equality.op = assgn_tag_beq; Equality.mixin_of__1 = assgn_tag_eq_axiom }

(** val assgn_tag_eqType : Equality.coq_type **)

let assgn_tag_eqType =
  Obj.magic assgn_tag_eqMixin

type inline_info =
| InlineFun
| DoNotInline

(** val inline_info_beq : inline_info -> inline_info -> bool **)

let inline_info_beq x y =
  match x with
  | InlineFun -> (match y with
                  | InlineFun -> true
                  | DoNotInline -> false)
  | DoNotInline -> (match y with
                    | InlineFun -> false
                    | DoNotInline -> true)

(** val inline_info_eq_dec : inline_info -> inline_info -> bool **)

let inline_info_eq_dec x y =
  let b = inline_info_beq x y in if b then true else false

(** val inline_info_eq_axiom : inline_info Equality.axiom **)

let inline_info_eq_axiom x y =
  iffP (inline_info_beq x y)
    (if inline_info_beq x y then ReflectT else ReflectF)

(** val inline_info_eqMixin : inline_info Equality.mixin_of **)

let inline_info_eqMixin =
  { Equality.op = inline_info_beq; Equality.mixin_of__1 =
    inline_info_eq_axiom }

(** val inline_info_eqType : Equality.coq_type **)

let inline_info_eqType =
  Obj.magic inline_info_eqMixin

type align =
| Align
| NoAlign

(** val align_beq : align -> align -> bool **)

let align_beq x y =
  match x with
  | Align -> (match y with
              | Align -> true
              | NoAlign -> false)
  | NoAlign -> (match y with
                | Align -> false
                | NoAlign -> true)

(** val align_eq_dec : align -> align -> bool **)

let align_eq_dec x y =
  let b = align_beq x y in if b then true else false

(** val align_eq_axiom : align Equality.axiom **)

let align_eq_axiom x y =
  iffP (align_beq x y) (if align_beq x y then ReflectT else ReflectF)

(** val align_eqMixin : align Equality.mixin_of **)

let align_eqMixin =
  { Equality.op = align_beq; Equality.mixin_of__1 = align_eq_axiom }

(** val align_eqType : Equality.coq_type **)

let align_eqType =
  Obj.magic align_eqMixin

type 'asm_op instr_r =
| Cassgn of lval * assgn_tag * stype * pexpr
| Copn of lval list * assgn_tag * 'asm_op sopn * pexpr list
| Cif of pexpr * 'asm_op instr list * 'asm_op instr list
| Cfor of var_i * range * 'asm_op instr list
| Cwhile of align * 'asm_op instr list * pexpr * 'asm_op instr list
| Ccall of inline_info * lval list * funname * pexpr list
and 'asm_op instr =
| MkI of instr_info * 'asm_op instr_r

(** val instr_r_beq : 'a1 asmOp -> 'a1 instr_r -> 'a1 instr_r -> bool **)

let instr_r_beq asmop =
  let rec instr_r_beq0 i1 i2 =
    match i1 with
    | Cassgn (x1, tag1, ty1, e1) ->
      (match i2 with
       | Cassgn (x2, tag2, ty2, e2) ->
         (&&)
           ((&&)
             ((&&) (eq_op assgn_tag_eqType (Obj.magic tag1) (Obj.magic tag2))
               (eq_op stype_eqType (Obj.magic ty1) (Obj.magic ty2)))
             (eq_op lval_eqType (Obj.magic x1) (Obj.magic x2)))
           (eq_op pexpr_eqType (Obj.magic e1) (Obj.magic e2))
       | _ -> false)
    | Copn (x1, tag1, o1, e1) ->
      (match i2 with
       | Copn (x2, tag2, o2, e2) ->
         (&&)
           ((&&)
             ((&&)
               (eq_op (seq_eqType lval_eqType) (Obj.magic x1) (Obj.magic x2))
               (eq_op assgn_tag_eqType (Obj.magic tag1) (Obj.magic tag2)))
             (eq_op (sopn_eqType asmop) (Obj.magic o1) (Obj.magic o2)))
           (eq_op (seq_eqType pexpr_eqType) (Obj.magic e1) (Obj.magic e2))
       | _ -> false)
    | Cif (e1, c11, c12) ->
      (match i2 with
       | Cif (e2, c21, c22) ->
         (&&)
           ((&&) (eq_op pexpr_eqType (Obj.magic e1) (Obj.magic e2))
             (all2 instr_beq0 c11 c21)) (all2 instr_beq0 c12 c22)
       | _ -> false)
    | Cfor (i3, r, c1) ->
      let (p, hi1) = r in
      let (dir1, lo1) = p in
      (match i2 with
       | Cfor (i4, r0, c2) ->
         let (p0, hi2) = r0 in
         let (dir2, lo2) = p0 in
         (&&)
           ((&&)
             ((&&)
               ((&&) (eq_op var_i_eqType (Obj.magic i3) (Obj.magic i4))
                 (eq_op dir_eqType (Obj.magic dir1) (Obj.magic dir2)))
               (eq_op pexpr_eqType (Obj.magic lo1) (Obj.magic lo2)))
             (eq_op pexpr_eqType (Obj.magic hi1) (Obj.magic hi2)))
           (all2 instr_beq0 c1 c2)
       | _ -> false)
    | Cwhile (a1, c1, e1, c1') ->
      (match i2 with
       | Cwhile (a2, c2, e2, c2') ->
         (&&)
           ((&&)
             ((&&) (eq_op align_eqType (Obj.magic a1) (Obj.magic a2))
               (all2 instr_beq0 c1 c2))
             (eq_op pexpr_eqType (Obj.magic e1) (Obj.magic e2)))
           (all2 instr_beq0 c1' c2')
       | _ -> false)
    | Ccall (ii1, x1, f1, arg1) ->
      (match i2 with
       | Ccall (ii2, x2, f2, arg2) ->
         (&&)
           ((&&)
             ((&&) (eq_op inline_info_eqType (Obj.magic ii1) (Obj.magic ii2))
               (eq_op (seq_eqType lval_eqType) (Obj.magic x1) (Obj.magic x2)))
             (eq_op pos_eqType (Obj.magic f1) (Obj.magic f2)))
           (eq_op (seq_eqType pexpr_eqType) (Obj.magic arg1) (Obj.magic arg2))
       | _ -> false)
  and instr_beq0 i1 i2 =
    let MkI (if1, i3) = i1 in
    let MkI (if2, i4) = i2 in
    (&&) (eq_op pos_eqType (Obj.magic if1) (Obj.magic if2))
      (instr_r_beq0 i3 i4)
  in instr_r_beq0

(** val instr_beq : 'a1 asmOp -> 'a1 instr -> 'a1 instr -> bool **)

let instr_beq asmop =
  let rec instr_r_beq0 i1 i2 =
    match i1 with
    | Cassgn (x1, tag1, ty1, e1) ->
      (match i2 with
       | Cassgn (x2, tag2, ty2, e2) ->
         (&&)
           ((&&)
             ((&&) (eq_op assgn_tag_eqType (Obj.magic tag1) (Obj.magic tag2))
               (eq_op stype_eqType (Obj.magic ty1) (Obj.magic ty2)))
             (eq_op lval_eqType (Obj.magic x1) (Obj.magic x2)))
           (eq_op pexpr_eqType (Obj.magic e1) (Obj.magic e2))
       | _ -> false)
    | Copn (x1, tag1, o1, e1) ->
      (match i2 with
       | Copn (x2, tag2, o2, e2) ->
         (&&)
           ((&&)
             ((&&)
               (eq_op (seq_eqType lval_eqType) (Obj.magic x1) (Obj.magic x2))
               (eq_op assgn_tag_eqType (Obj.magic tag1) (Obj.magic tag2)))
             (eq_op (sopn_eqType asmop) (Obj.magic o1) (Obj.magic o2)))
           (eq_op (seq_eqType pexpr_eqType) (Obj.magic e1) (Obj.magic e2))
       | _ -> false)
    | Cif (e1, c11, c12) ->
      (match i2 with
       | Cif (e2, c21, c22) ->
         (&&)
           ((&&) (eq_op pexpr_eqType (Obj.magic e1) (Obj.magic e2))
             (all2 instr_beq0 c11 c21)) (all2 instr_beq0 c12 c22)
       | _ -> false)
    | Cfor (i3, r, c1) ->
      let (p, hi1) = r in
      let (dir1, lo1) = p in
      (match i2 with
       | Cfor (i4, r0, c2) ->
         let (p0, hi2) = r0 in
         let (dir2, lo2) = p0 in
         (&&)
           ((&&)
             ((&&)
               ((&&) (eq_op var_i_eqType (Obj.magic i3) (Obj.magic i4))
                 (eq_op dir_eqType (Obj.magic dir1) (Obj.magic dir2)))
               (eq_op pexpr_eqType (Obj.magic lo1) (Obj.magic lo2)))
             (eq_op pexpr_eqType (Obj.magic hi1) (Obj.magic hi2)))
           (all2 instr_beq0 c1 c2)
       | _ -> false)
    | Cwhile (a1, c1, e1, c1') ->
      (match i2 with
       | Cwhile (a2, c2, e2, c2') ->
         (&&)
           ((&&)
             ((&&) (eq_op align_eqType (Obj.magic a1) (Obj.magic a2))
               (all2 instr_beq0 c1 c2))
             (eq_op pexpr_eqType (Obj.magic e1) (Obj.magic e2)))
           (all2 instr_beq0 c1' c2')
       | _ -> false)
    | Ccall (ii1, x1, f1, arg1) ->
      (match i2 with
       | Ccall (ii2, x2, f2, arg2) ->
         (&&)
           ((&&)
             ((&&) (eq_op inline_info_eqType (Obj.magic ii1) (Obj.magic ii2))
               (eq_op (seq_eqType lval_eqType) (Obj.magic x1) (Obj.magic x2)))
             (eq_op pos_eqType (Obj.magic f1) (Obj.magic f2)))
           (eq_op (seq_eqType pexpr_eqType) (Obj.magic arg1) (Obj.magic arg2))
       | _ -> false)
  and instr_beq0 i1 i2 =
    let MkI (if1, i3) = i1 in
    let MkI (if2, i4) = i2 in
    (&&) (eq_op pos_eqType (Obj.magic if1) (Obj.magic if2))
      (instr_r_beq0 i3 i4)
  in instr_beq0

(** val instr_eq_axiom_ :
    'a1 asmOp -> ('a1 instr_r -> 'a1 instr_r -> reflect) -> 'a1 instr
    Equality.axiom **)

let instr_eq_axiom_ asmop _ __top_assumption_ =
  let _evar_0_ = fun ii1 ir1 __top_assumption_0 ->
    let _evar_0_ = fun ii2 ir2 ->
      iffP ((&&) (eq_op pos_eqType ii1 ii2) (instr_r_beq asmop ir1 ir2))
        (andP (eq_op pos_eqType ii1 ii2) (instr_r_beq asmop ir1 ir2))
    in
    let MkI (i, i0) = __top_assumption_0 in Obj.magic _evar_0_ i i0
  in
  let MkI (i, i0) = __top_assumption_ in Obj.magic _evar_0_ i i0

(** val instr_r_eq_axiom : 'a1 asmOp -> 'a1 instr_r Equality.axiom **)

let rec instr_r_eq_axiom asmop __top_assumption_ =
  let _evar_0_ = fun x1 t1 ty1 e1 __top_assumption_0 ->
    let _evar_0_ = fun x2 t2 ty2 e2 ->
      iffP
        ((&&)
          ((&&)
            ((&&) (eq_op assgn_tag_eqType t1 t2) (eq_op stype_eqType ty1 ty2))
            (eq_op lval_eqType x1 x2)) (eq_op pexpr_eqType e1 e2))
        (if (&&)
              ((&&)
                ((&&) (eq_op assgn_tag_eqType t1 t2)
                  (eq_op stype_eqType ty1 ty2)) (eq_op lval_eqType x1 x2))
              (eq_op pexpr_eqType e1 e2)
         then ReflectT
         else ReflectF)
    in
    let _evar_0_0 = fun _ _ _ _ -> ReflectF in
    let _evar_0_1 = fun _ _ _ -> ReflectF in
    let _evar_0_2 = fun _ __top_assumption_1 ->
      let _evar_0_2 = fun __top_assumption_2 ->
        let _evar_0_2 = fun _ _ _ _ -> ReflectF in
        let (a, b) = __top_assumption_2 in _evar_0_2 a b
      in
      let (a, b) = __top_assumption_1 in _evar_0_2 a b
    in
    let _evar_0_3 = fun _ _ _ _ -> ReflectF in
    let _evar_0_4 = fun _ _ _ _ -> ReflectF in
    (match __top_assumption_0 with
     | Cassgn (l, a, s, p) -> Obj.magic _evar_0_ l a s p
     | Copn (l, a, s, l0) -> _evar_0_0 l a s l0
     | Cif (p, l, l0) -> _evar_0_1 p l l0
     | Cfor (v, r, l) -> _evar_0_2 v r l
     | Cwhile (a, l, p, l0) -> _evar_0_3 a l p l0
     | Ccall (i, l, f, l0) -> _evar_0_4 i l f l0)
  in
  let _evar_0_0 = fun x1 t1 o1 e1 __top_assumption_0 ->
    let _evar_0_0 = fun _ _ _ _ -> ReflectF in
    let _evar_0_1 = fun x2 t2 o2 e2 ->
      iffP
        ((&&)
          ((&&)
            ((&&) (eq_op (seq_eqType lval_eqType) x1 x2)
              (eq_op assgn_tag_eqType t1 t2))
            (eq_op (sopn_eqType asmop) o1 o2))
          (eq_op (seq_eqType pexpr_eqType) e1 e2))
        (if (&&)
              ((&&)
                ((&&) (eq_op (seq_eqType lval_eqType) x1 x2)
                  (eq_op assgn_tag_eqType t1 t2))
                (eq_op (sopn_eqType asmop) o1 o2))
              (eq_op (seq_eqType pexpr_eqType) e1 e2)
         then ReflectT
         else ReflectF)
    in
    let _evar_0_2 = fun _ _ _ -> ReflectF in
    let _evar_0_3 = fun _ __top_assumption_1 ->
      let _evar_0_3 = fun __top_assumption_2 ->
        let _evar_0_3 = fun _ _ _ _ -> ReflectF in
        let (a, b) = __top_assumption_2 in _evar_0_3 a b
      in
      let (a, b) = __top_assumption_1 in _evar_0_3 a b
    in
    let _evar_0_4 = fun _ _ _ _ -> ReflectF in
    let _evar_0_5 = fun _ _ _ _ -> ReflectF in
    (match __top_assumption_0 with
     | Cassgn (l, a, s, p) -> _evar_0_0 l a s p
     | Copn (l, a, s, l0) -> Obj.magic _evar_0_1 l a s l0
     | Cif (p, l, l0) -> _evar_0_2 p l l0
     | Cfor (v, r, l) -> _evar_0_3 v r l
     | Cwhile (a, l, p, l0) -> _evar_0_4 a l p l0
     | Ccall (i, l, f, l0) -> _evar_0_5 i l f l0)
  in
  let _evar_0_1 = fun e1 c11 c12 __top_assumption_0 ->
    let _evar_0_1 = fun _ _ _ _ -> ReflectF in
    let _evar_0_2 = fun _ _ _ _ -> ReflectF in
    let _evar_0_3 = fun e2 c21 c22 ->
      ssr_have
        (reflect_all2_eqb (instr_beq asmop)
          (instr_eq_axiom_ asmop (instr_r_eq_axiom asmop))) (fun _ ->
        iffP
          ((&&)
            ((&&) (eq_op pexpr_eqType e1 e2) (all2 (instr_beq asmop) c11 c21))
            (all2 (instr_beq asmop) c12 c22))
          (if (&&)
                ((&&) (eq_op pexpr_eqType e1 e2)
                  (all2 (instr_beq asmop) c11 c21))
                (all2 (instr_beq asmop) c12 c22)
           then ReflectT
           else ReflectF))
    in
    let _evar_0_4 = fun _ __top_assumption_1 ->
      let _evar_0_4 = fun __top_assumption_2 ->
        let _evar_0_4 = fun _ _ _ _ -> ReflectF in
        let (a, b) = __top_assumption_2 in _evar_0_4 a b
      in
      let (a, b) = __top_assumption_1 in _evar_0_4 a b
    in
    let _evar_0_5 = fun _ _ _ _ -> ReflectF in
    let _evar_0_6 = fun _ _ _ _ -> ReflectF in
    (match __top_assumption_0 with
     | Cassgn (l, a, s, p) -> _evar_0_1 l a s p
     | Copn (l, a, s, l0) -> _evar_0_2 l a s l0
     | Cif (p, l, l0) -> Obj.magic _evar_0_3 p l l0
     | Cfor (v, r, l) -> _evar_0_4 v r l
     | Cwhile (a, l, p, l0) -> _evar_0_5 a l p l0
     | Ccall (i, l, f, l0) -> _evar_0_6 i l f l0)
  in
  let _evar_0_2 = fun x1 __top_assumption_0 ->
    let _evar_0_2 = fun __top_assumption_1 ->
      let _evar_0_2 = fun dir1 lo1 hi1 c1 __top_assumption_2 ->
        let _evar_0_2 = fun _ _ _ _ -> ReflectF in
        let _evar_0_3 = fun _ _ _ _ -> ReflectF in
        let _evar_0_4 = fun _ _ _ -> ReflectF in
        let _evar_0_5 = fun x2 __top_assumption_3 ->
          let _evar_0_5 = fun __top_assumption_4 ->
            let _evar_0_5 = fun dir2 lo2 hi2 c2 ->
              ssr_have
                (reflect_all2_eqb (instr_beq asmop)
                  (instr_eq_axiom_ asmop (instr_r_eq_axiom asmop))) (fun _ ->
                iffP
                  ((&&)
                    ((&&)
                      ((&&)
                        ((&&) (eq_op var_i_eqType x1 x2)
                          (eq_op dir_eqType dir1 dir2))
                        (eq_op pexpr_eqType lo1 lo2))
                      (eq_op pexpr_eqType hi1 hi2))
                    (all2 (instr_beq asmop) c1 c2))
                  (if (&&)
                        ((&&)
                          ((&&)
                            ((&&) (eq_op var_i_eqType x1 x2)
                              (eq_op dir_eqType dir1 dir2))
                            (eq_op pexpr_eqType lo1 lo2))
                          (eq_op pexpr_eqType hi1 hi2))
                        (all2 (instr_beq asmop) c1 c2)
                   then ReflectT
                   else ReflectF))
            in
            let (a, b) = __top_assumption_4 in _evar_0_5 a b
          in
          let (a, b) = __top_assumption_3 in _evar_0_5 a b
        in
        let _evar_0_6 = fun _ _ _ _ -> ReflectF in
        let _evar_0_7 = fun _ _ _ _ -> ReflectF in
        (match __top_assumption_2 with
         | Cassgn (l, a, s, p) -> _evar_0_2 l a s p
         | Copn (l, a, s, l0) -> _evar_0_3 l a s l0
         | Cif (p, l, l0) -> _evar_0_4 p l l0
         | Cfor (v, r, l) -> Obj.magic _evar_0_5 v r l
         | Cwhile (a, l, p, l0) -> _evar_0_6 a l p l0
         | Ccall (i, l, f, l0) -> _evar_0_7 i l f l0)
      in
      let (a, b) = __top_assumption_1 in _evar_0_2 a b
    in
    let (a, b) = __top_assumption_0 in _evar_0_2 a b
  in
  let _evar_0_3 = fun a1 c1 e1 c1' __top_assumption_0 ->
    let _evar_0_3 = fun _ _ _ _ -> ReflectF in
    let _evar_0_4 = fun _ _ _ _ -> ReflectF in
    let _evar_0_5 = fun _ _ _ -> ReflectF in
    let _evar_0_6 = fun _ __top_assumption_1 ->
      let _evar_0_6 = fun __top_assumption_2 ->
        let _evar_0_6 = fun _ _ _ _ -> ReflectF in
        let (a, b) = __top_assumption_2 in _evar_0_6 a b
      in
      let (a, b) = __top_assumption_1 in _evar_0_6 a b
    in
    let _evar_0_7 = fun a2 c2 e2 c2' ->
      ssr_have
        (reflect_all2_eqb (instr_beq asmop)
          (instr_eq_axiom_ asmop (instr_r_eq_axiom asmop))) (fun _ ->
        iffP
          ((&&)
            ((&&)
              ((&&) (eq_op align_eqType a1 a2) (all2 (instr_beq asmop) c1 c2))
              (eq_op pexpr_eqType e1 e2)) (all2 (instr_beq asmop) c1' c2'))
          (if (&&)
                ((&&)
                  ((&&) (eq_op align_eqType a1 a2)
                    (all2 (instr_beq asmop) c1 c2))
                  (eq_op pexpr_eqType e1 e2)) (all2 (instr_beq asmop) c1' c2')
           then ReflectT
           else ReflectF))
    in
    let _evar_0_8 = fun _ _ _ _ -> ReflectF in
    (match __top_assumption_0 with
     | Cassgn (l, a, s, p) -> _evar_0_3 l a s p
     | Copn (l, a, s, l0) -> _evar_0_4 l a s l0
     | Cif (p, l, l0) -> _evar_0_5 p l l0
     | Cfor (v, r, l) -> _evar_0_6 v r l
     | Cwhile (a, l, p, l0) -> Obj.magic _evar_0_7 a l p l0
     | Ccall (i, l, f, l0) -> _evar_0_8 i l f l0)
  in
  let _evar_0_4 = fun ii1 x1 f1 arg1 __top_assumption_0 ->
    let _evar_0_4 = fun _ _ _ _ -> ReflectF in
    let _evar_0_5 = fun _ _ _ _ -> ReflectF in
    let _evar_0_6 = fun _ _ _ -> ReflectF in
    let _evar_0_7 = fun _ __top_assumption_1 ->
      let _evar_0_7 = fun __top_assumption_2 ->
        let _evar_0_7 = fun _ _ _ _ -> ReflectF in
        let (a, b) = __top_assumption_2 in _evar_0_7 a b
      in
      let (a, b) = __top_assumption_1 in _evar_0_7 a b
    in
    let _evar_0_8 = fun _ _ _ _ -> ReflectF in
    let _evar_0_9 = fun ii2 x2 f2 arg2 ->
      iffP
        ((&&)
          ((&&)
            ((&&) (eq_op inline_info_eqType ii1 ii2)
              (eq_op (seq_eqType lval_eqType) x1 x2))
            (eq_op pos_eqType f1 f2))
          (eq_op (seq_eqType pexpr_eqType) arg1 arg2))
        (if (&&)
              ((&&)
                ((&&) (eq_op inline_info_eqType ii1 ii2)
                  (eq_op (seq_eqType lval_eqType) x1 x2))
                (eq_op pos_eqType f1 f2))
              (eq_op (seq_eqType pexpr_eqType) arg1 arg2)
         then ReflectT
         else ReflectF)
    in
    (match __top_assumption_0 with
     | Cassgn (l, a, s, p) -> _evar_0_4 l a s p
     | Copn (l, a, s, l0) -> _evar_0_5 l a s l0
     | Cif (p, l, l0) -> _evar_0_6 p l l0
     | Cfor (v, r, l) -> _evar_0_7 v r l
     | Cwhile (a, l, p, l0) -> _evar_0_8 a l p l0
     | Ccall (i, l, f, l0) -> Obj.magic _evar_0_9 i l f l0)
  in
  (match __top_assumption_ with
   | Cassgn (l, a, s, p) -> Obj.magic _evar_0_ l a s p
   | Copn (l, a, s, l0) -> Obj.magic _evar_0_0 l a s l0
   | Cif (p, l, l0) -> Obj.magic _evar_0_1 p l l0
   | Cfor (v, r, l) -> Obj.magic _evar_0_2 v r l
   | Cwhile (a, l, p, l0) -> Obj.magic _evar_0_3 a l p l0
   | Ccall (i, l, f, l0) -> Obj.magic _evar_0_4 i l f l0)

(** val instr_r_eqMixin : 'a1 asmOp -> 'a1 instr_r Equality.mixin_of **)

let instr_r_eqMixin asmop =
  { Equality.op = (instr_r_beq asmop); Equality.mixin_of__1 =
    (instr_r_eq_axiom asmop) }

(** val instr_r_eqType : 'a1 asmOp -> Equality.coq_type **)

let instr_r_eqType asmop =
  Obj.magic instr_r_eqMixin asmop

(** val instr_eq_axiom : 'a1 asmOp -> 'a1 instr Equality.axiom **)

let instr_eq_axiom asmop =
  instr_eq_axiom_ asmop (instr_r_eq_axiom asmop)

(** val instr_eqMixin : 'a1 asmOp -> 'a1 instr Equality.mixin_of **)

let instr_eqMixin asmop =
  { Equality.op = (instr_beq asmop); Equality.mixin_of__1 =
    (instr_eq_axiom asmop) }

(** val instr_eqType : 'a1 asmOp -> Equality.coq_type **)

let instr_eqType asmop =
  Obj.magic instr_eqMixin asmop

type fun_info = positive

type progT =
| Build_progT

type extra_prog_t = __

type extra_val_t = __

(** val extra_fun_t : Equality.coq_type -> progT -> Equality.coq_type **)

let extra_fun_t eft _ =
  eft

type ('asm_op, 'extra_fun_t) _fundef = { f_info : fun_info;
                                         f_tyin : stype list;
                                         f_params : var_i list;
                                         f_body : 'asm_op instr list;
                                         f_tyout : stype list;
                                         f_res : var_i list;
                                         f_extra : 'extra_fun_t }

(** val f_info : 'a1 asmOp -> ('a1, 'a2) _fundef -> fun_info **)

let f_info _ x =
  x.f_info

(** val f_tyin : 'a1 asmOp -> ('a1, 'a2) _fundef -> stype list **)

let f_tyin _ x =
  x.f_tyin

(** val f_params : 'a1 asmOp -> ('a1, 'a2) _fundef -> var_i list **)

let f_params _ x =
  x.f_params

(** val f_body : 'a1 asmOp -> ('a1, 'a2) _fundef -> 'a1 instr list **)

let f_body _ x =
  x.f_body

(** val f_tyout : 'a1 asmOp -> ('a1, 'a2) _fundef -> stype list **)

let f_tyout _ x =
  x.f_tyout

(** val f_res : 'a1 asmOp -> ('a1, 'a2) _fundef -> var_i list **)

let f_res _ x =
  x.f_res

(** val f_extra : 'a1 asmOp -> ('a1, 'a2) _fundef -> 'a2 **)

let f_extra _ x =
  x.f_extra

type ('asm_op, 'extra_fun_t) _fun_decl =
  funname * ('asm_op, 'extra_fun_t) _fundef

type ('asm_op, 'extra_fun_t, 'extra_prog_t) _prog = { p_funcs : ('asm_op,
                                                                'extra_fun_t)
                                                                _fun_decl list;
                                                      p_globs : glob_decl list;
                                                      p_extra : 'extra_prog_t }

(** val p_funcs :
    'a1 asmOp -> ('a1, 'a2, 'a3) _prog -> ('a1, 'a2) _fun_decl list **)

let p_funcs _ x =
  x.p_funcs

(** val p_globs : 'a1 asmOp -> ('a1, 'a2, 'a3) _prog -> glob_decl list **)

let p_globs _ x =
  x.p_globs

(** val p_extra : 'a1 asmOp -> ('a1, 'a2, 'a3) _prog -> 'a3 **)

let p_extra _ x =
  x.p_extra

type 'asm_op fundef = ('asm_op, Equality.sort) _fundef

type function_signature = stype list * stype list

(** val signature_of_fundef :
    'a1 asmOp -> Equality.coq_type -> progT -> 'a1 fundef ->
    function_signature **)

let signature_of_fundef _ _ _ fd =
  (fd.f_tyin, fd.f_tyout)

type 'asm_op fun_decl = funname * 'asm_op fundef

type 'asm_op prog = ('asm_op, Equality.sort, extra_prog_t) _prog

(** val fundef_beq :
    'a1 asmOp -> Equality.coq_type -> progT -> 'a1 fundef -> 'a1 fundef ->
    bool **)

let fundef_beq asmop eft pT fd1 fd2 =
  let { f_info = ii1; f_tyin = tin1; f_params = x1; f_body = c1; f_tyout =
    tout1; f_res = r1; f_extra = e1 } = fd1
  in
  let { f_info = ii2; f_tyin = tin2; f_params = x2; f_body = c2; f_tyout =
    tout2; f_res = r2; f_extra = e2 } = fd2
  in
  (&&)
    ((&&)
      ((&&)
        ((&&)
          ((&&)
            ((&&) (eq_op pos_eqType (Obj.magic ii1) (Obj.magic ii2))
              (eq_op (seq_eqType stype_eqType) (Obj.magic tin1)
                (Obj.magic tin2)))
            (eq_op (seq_eqType var_i_eqType) (Obj.magic x1) (Obj.magic x2)))
          (eq_op (seq_eqType (instr_eqType asmop)) (Obj.magic c1)
            (Obj.magic c2)))
        (eq_op (seq_eqType stype_eqType) (Obj.magic tout1) (Obj.magic tout2)))
      (eq_op (seq_eqType var_i_eqType) (Obj.magic r1) (Obj.magic r2)))
    (eq_op (extra_fun_t eft pT) e1 e2)

(** val fundef_eq_axiom :
    'a1 asmOp -> Equality.coq_type -> progT -> 'a1 fundef Equality.axiom **)

let fundef_eq_axiom asmop eft pT __top_assumption_ =
  let _evar_0_ = fun i1 tin1 p1 c1 tout1 r1 e1 __top_assumption_0 ->
    let _evar_0_ = fun i2 tin2 p2 c2 tout2 r2 e2 ->
      iffP
        ((&&)
          ((&&)
            ((&&)
              ((&&)
                ((&&)
                  ((&&) (eq_op pos_eqType i1 i2)
                    (eq_op (seq_eqType stype_eqType) tin1 tin2))
                  (eq_op (seq_eqType var_i_eqType) p1 p2))
                (eq_op (seq_eqType (instr_eqType asmop)) c1 c2))
              (eq_op (seq_eqType stype_eqType) tout1 tout2))
            (eq_op (seq_eqType var_i_eqType) r1 r2))
          (eq_op (extra_fun_t eft pT) e1 e2))
        (if (&&)
              ((&&)
                ((&&)
                  ((&&)
                    ((&&)
                      ((&&) (eq_op pos_eqType i1 i2)
                        (eq_op (seq_eqType stype_eqType) tin1 tin2))
                      (eq_op (seq_eqType var_i_eqType) p1 p2))
                    (eq_op (seq_eqType (instr_eqType asmop)) c1 c2))
                  (eq_op (seq_eqType stype_eqType) tout1 tout2))
                (eq_op (seq_eqType var_i_eqType) r1 r2))
              (eq_op (extra_fun_t eft pT) e1 e2)
         then ReflectT
         else ReflectF)
    in
    let { f_info = f_info0; f_tyin = f_tyin0; f_params = f_params0; f_body =
      f_body0; f_tyout = f_tyout0; f_res = f_res0; f_extra = f_extra0 } =
      __top_assumption_0
    in
    Obj.magic _evar_0_ f_info0 f_tyin0 f_params0 f_body0 f_tyout0 f_res0
      f_extra0
  in
  let { f_info = f_info0; f_tyin = f_tyin0; f_params = f_params0; f_body =
    f_body0; f_tyout = f_tyout0; f_res = f_res0; f_extra = f_extra0 } =
    __top_assumption_
  in
  Obj.magic _evar_0_ f_info0 f_tyin0 f_params0 f_body0 f_tyout0 f_res0
    f_extra0

(** val fundef_eqMixin :
    'a1 asmOp -> Equality.coq_type -> progT -> 'a1 fundef Equality.mixin_of **)

let fundef_eqMixin asmop eft pT =
  { Equality.op = (fundef_beq asmop eft pT); Equality.mixin_of__1 =
    (fundef_eq_axiom asmop eft pT) }

(** val fundef_eqType :
    'a1 asmOp -> Equality.coq_type -> progT -> Equality.coq_type **)

let fundef_eqType asmop eft pT =
  Obj.magic fundef_eqMixin asmop eft pT

(** val coq_Build_prog :
    'a1 asmOp -> Equality.coq_type -> progT -> ('a1, Equality.sort) _fun_decl
    list -> glob_decl list -> extra_prog_t -> 'a1 prog **)

let coq_Build_prog _ _ _ p_funcs0 p_globs0 p_extra0 =
  { p_funcs = p_funcs0; p_globs = p_globs0; p_extra = p_extra0 }

(** val progUnit : progT **)

let progUnit =
  Build_progT

type 'asm_op ufundef = 'asm_op fundef

type 'asm_op ufun_decl = 'asm_op fun_decl

type 'asm_op ufun_decls = 'asm_op fun_decl list

type 'asm_op uprog = 'asm_op prog

type 'asm_op _ufundef = ('asm_op, unit) _fundef

type 'asm_op _ufun_decl = ('asm_op, unit) _fun_decl

type 'asm_op _ufun_decls = ('asm_op, unit) _fun_decl list

type 'asm_op _uprog = ('asm_op, unit, unit) _prog

(** val to_uprog : 'a1 asmOp -> 'a1 _uprog -> 'a1 uprog **)

let to_uprog _ p =
  Obj.magic p

type saved_stack =
| SavedStackNone
| SavedStackReg of Var.var
| SavedStackStk of coq_Z

(** val saved_stack_beq : saved_stack -> saved_stack -> bool **)

let saved_stack_beq x y =
  match x with
  | SavedStackNone -> (match y with
                       | SavedStackNone -> true
                       | _ -> false)
  | SavedStackReg v1 ->
    (match y with
     | SavedStackReg v2 -> eq_op Var.var_eqType (Obj.magic v1) (Obj.magic v2)
     | _ -> false)
  | SavedStackStk z1 ->
    (match y with
     | SavedStackStk z2 -> eq_op coq_Z_eqType (Obj.magic z1) (Obj.magic z2)
     | _ -> false)

(** val saved_stack_eq_axiom : saved_stack Equality.axiom **)

let saved_stack_eq_axiom __top_assumption_ =
  let _evar_0_ = fun __top_assumption_0 ->
    let _evar_0_ = ReflectT in
    let _evar_0_0 = fun _ -> ReflectF in
    let _evar_0_1 = fun _ -> ReflectF in
    (match __top_assumption_0 with
     | SavedStackNone -> _evar_0_
     | SavedStackReg v -> _evar_0_0 v
     | SavedStackStk z -> _evar_0_1 z)
  in
  let _evar_0_0 = fun v1 __top_assumption_0 ->
    let _evar_0_0 = ReflectF in
    let _evar_0_1 = fun v2 ->
      iffP (eq_op Var.var_eqType v1 v2) (eqP Var.var_eqType v1 v2)
    in
    let _evar_0_2 = fun _ -> ReflectF in
    (match __top_assumption_0 with
     | SavedStackNone -> _evar_0_0
     | SavedStackReg v -> Obj.magic _evar_0_1 v
     | SavedStackStk z -> _evar_0_2 z)
  in
  let _evar_0_1 = fun z1 __top_assumption_0 ->
    let _evar_0_1 = ReflectF in
    let _evar_0_2 = fun _ -> ReflectF in
    let _evar_0_3 = fun z2 ->
      iffP (eq_op coq_Z_eqType z1 z2) (eqP coq_Z_eqType z1 z2)
    in
    (match __top_assumption_0 with
     | SavedStackNone -> _evar_0_1
     | SavedStackReg v -> _evar_0_2 v
     | SavedStackStk z -> Obj.magic _evar_0_3 z)
  in
  (match __top_assumption_ with
   | SavedStackNone -> _evar_0_
   | SavedStackReg v -> Obj.magic _evar_0_0 v
   | SavedStackStk z -> Obj.magic _evar_0_1 z)

(** val saved_stack_eqMixin : saved_stack Equality.mixin_of **)

let saved_stack_eqMixin =
  { Equality.op = saved_stack_beq; Equality.mixin_of__1 =
    saved_stack_eq_axiom }

(** val saved_stack_eqType : Equality.coq_type **)

let saved_stack_eqType =
  Obj.magic saved_stack_eqMixin

type return_address_location =
| RAnone
| RAreg of Var.var
| RAstack of coq_Z

(** val return_address_location_beq :
    return_address_location -> return_address_location -> bool **)

let return_address_location_beq r1 r2 =
  match r1 with
  | RAnone -> (match r2 with
               | RAnone -> true
               | _ -> false)
  | RAreg x1 ->
    (match r2 with
     | RAreg x2 -> eq_op Var.var_eqType (Obj.magic x1) (Obj.magic x2)
     | _ -> false)
  | RAstack z1 ->
    (match r2 with
     | RAstack z2 -> eq_op coq_Z_eqType (Obj.magic z1) (Obj.magic z2)
     | _ -> false)

(** val return_address_location_eq_axiom :
    return_address_location Equality.axiom **)

let return_address_location_eq_axiom _top_assumption_ =
  let _evar_0_ = fun __top_assumption_ ->
    let _evar_0_ = ReflectT in
    let _evar_0_0 = fun _ -> ReflectF in
    let _evar_0_1 = fun _ -> ReflectF in
    (match __top_assumption_ with
     | RAnone -> _evar_0_
     | RAreg v -> _evar_0_0 v
     | RAstack z -> _evar_0_1 z)
  in
  let _evar_0_0 = fun x1 __top_assumption_ ->
    let _evar_0_0 = ReflectF in
    let _evar_0_1 = fun x2 ->
      iffP (eq_op Var.var_eqType x1 x2) (eqP Var.var_eqType x1 x2)
    in
    let _evar_0_2 = fun _ -> ReflectF in
    (match __top_assumption_ with
     | RAnone -> _evar_0_0
     | RAreg v -> Obj.magic _evar_0_1 v
     | RAstack z -> _evar_0_2 z)
  in
  let _evar_0_1 = fun z1 __top_assumption_ ->
    let _evar_0_1 = ReflectF in
    let _evar_0_2 = fun _ -> ReflectF in
    let _evar_0_3 = fun z2 ->
      iffP (eq_op coq_Z_eqType z1 z2) (eqP coq_Z_eqType z1 z2)
    in
    (match __top_assumption_ with
     | RAnone -> _evar_0_1
     | RAreg v -> _evar_0_2 v
     | RAstack z -> Obj.magic _evar_0_3 z)
  in
  (match _top_assumption_ with
   | RAnone -> _evar_0_
   | RAreg v -> Obj.magic _evar_0_0 v
   | RAstack z -> Obj.magic _evar_0_1 z)

(** val return_address_location_eqMixin :
    return_address_location Equality.mixin_of **)

let return_address_location_eqMixin =
  { Equality.op = return_address_location_beq; Equality.mixin_of__1 =
    return_address_location_eq_axiom }

(** val return_address_location_eqType : Equality.coq_type **)

let return_address_location_eqType =
  Obj.magic return_address_location_eqMixin

type stk_fun_extra = { sf_align : wsize; sf_stk_sz : coq_Z;
                       sf_stk_extra_sz : coq_Z; sf_stk_max : coq_Z;
                       sf_to_save : (Var.var * coq_Z) list;
                       sf_save_stack : saved_stack;
                       sf_return_address : return_address_location }

(** val sf_align : stk_fun_extra -> wsize **)

let sf_align s =
  s.sf_align

(** val sf_stk_sz : stk_fun_extra -> coq_Z **)

let sf_stk_sz s =
  s.sf_stk_sz

(** val sf_stk_extra_sz : stk_fun_extra -> coq_Z **)

let sf_stk_extra_sz s =
  s.sf_stk_extra_sz

(** val sf_stk_max : stk_fun_extra -> coq_Z **)

let sf_stk_max s =
  s.sf_stk_max

(** val sf_to_save : stk_fun_extra -> (Var.var * coq_Z) list **)

let sf_to_save s =
  s.sf_to_save

(** val sf_save_stack : stk_fun_extra -> saved_stack **)

let sf_save_stack s =
  s.sf_save_stack

(** val sf_return_address : stk_fun_extra -> return_address_location **)

let sf_return_address s =
  s.sf_return_address

(** val sfe_beq : stk_fun_extra -> stk_fun_extra -> bool **)

let sfe_beq e1 e2 =
  (&&)
    ((&&)
      ((&&)
        ((&&)
          ((&&)
            ((&&)
              (eq_op wsize_eqType (Obj.magic e1.sf_align)
                (Obj.magic e2.sf_align))
              (eq_op coq_Z_eqType (Obj.magic e1.sf_stk_sz)
                (Obj.magic e2.sf_stk_sz)))
            (eq_op coq_Z_eqType (Obj.magic e1.sf_stk_max)
              (Obj.magic e2.sf_stk_max)))
          (eq_op coq_Z_eqType (Obj.magic e1.sf_stk_extra_sz)
            (Obj.magic e2.sf_stk_extra_sz)))
        (eq_op (seq_eqType (prod_eqType Var.var_eqType coq_Z_eqType))
          (Obj.magic e1.sf_to_save) (Obj.magic e2.sf_to_save)))
      (eq_op saved_stack_eqType (Obj.magic e1.sf_save_stack)
        (Obj.magic e2.sf_save_stack)))
    (eq_op return_address_location_eqType (Obj.magic e1.sf_return_address)
      (Obj.magic e2.sf_return_address))

(** val sfe_eq_axiom : stk_fun_extra Equality.axiom **)

let sfe_eq_axiom _top_assumption_ =
  let _evar_0_ = fun a b c d e f g __top_assumption_ ->
    let _evar_0_ = fun a' b' c' d' e' f' g' ->
      equivP
        ((&&)
          ((&&)
            ((&&)
              ((&&)
                ((&&)
                  ((&&) (eq_op wsize_eqType (Obj.magic a) (Obj.magic a'))
                    (eq_op coq_Z_eqType (Obj.magic b) (Obj.magic b')))
                  (eq_op coq_Z_eqType (Obj.magic d) (Obj.magic d')))
                (eq_op coq_Z_eqType (Obj.magic c) (Obj.magic c')))
              (eq_op (seq_eqType (prod_eqType Var.var_eqType coq_Z_eqType))
                (Obj.magic e) (Obj.magic e')))
            (eq_op saved_stack_eqType (Obj.magic f) (Obj.magic f')))
          (eq_op return_address_location_eqType (Obj.magic g) (Obj.magic g')))
        (andP
          ((&&)
            ((&&)
              ((&&)
                ((&&)
                  ((&&) (eq_op wsize_eqType (Obj.magic a) (Obj.magic a'))
                    (eq_op coq_Z_eqType (Obj.magic b) (Obj.magic b')))
                  (eq_op coq_Z_eqType (Obj.magic d) (Obj.magic d')))
                (eq_op coq_Z_eqType (Obj.magic c) (Obj.magic c')))
              (eq_op (seq_eqType (prod_eqType Var.var_eqType coq_Z_eqType))
                (Obj.magic e) (Obj.magic e')))
            (eq_op saved_stack_eqType (Obj.magic f) (Obj.magic f')))
          (eq_op return_address_location_eqType (Obj.magic g) (Obj.magic g')))
    in
    let { sf_align = sf_align0; sf_stk_sz = sf_stk_sz0; sf_stk_extra_sz =
      sf_stk_extra_sz0; sf_stk_max = sf_stk_max0; sf_to_save = sf_to_save0;
      sf_save_stack = sf_save_stack0; sf_return_address =
      sf_return_address0 } = __top_assumption_
    in
    _evar_0_ sf_align0 sf_stk_sz0 sf_stk_extra_sz0 sf_stk_max0 sf_to_save0
      sf_save_stack0 sf_return_address0
  in
  let { sf_align = sf_align0; sf_stk_sz = sf_stk_sz0; sf_stk_extra_sz =
    sf_stk_extra_sz0; sf_stk_max = sf_stk_max0; sf_to_save = sf_to_save0;
    sf_save_stack = sf_save_stack0; sf_return_address =
    sf_return_address0 } = _top_assumption_
  in
  _evar_0_ sf_align0 sf_stk_sz0 sf_stk_extra_sz0 sf_stk_max0 sf_to_save0
    sf_save_stack0 sf_return_address0

(** val sfe_eqMixin : stk_fun_extra Equality.mixin_of **)

let sfe_eqMixin =
  { Equality.op = sfe_beq; Equality.mixin_of__1 = sfe_eq_axiom }

(** val sfe_eqType : Equality.coq_type **)

let sfe_eqType =
  Obj.magic sfe_eqMixin

type sprog_extra = { sp_rsp : Equality.sort; sp_rip : Equality.sort;
                     sp_globs : GRing.ComRing.sort list }

(** val sp_rsp : sprog_extra -> Equality.sort **)

let sp_rsp s =
  s.sp_rsp

(** val sp_rip : sprog_extra -> Equality.sort **)

let sp_rip s =
  s.sp_rip

(** val sp_globs : sprog_extra -> GRing.ComRing.sort list **)

let sp_globs s =
  s.sp_globs

(** val progStack : coq_PointerData -> progT **)

let progStack _ =
  Build_progT

type 'asm_op sfundef = 'asm_op fundef

type 'asm_op sfun_decl = 'asm_op fun_decl

type 'asm_op sfun_decls = 'asm_op fun_decl list

type 'asm_op sprog = 'asm_op prog

type 'asm_op _sfundef = ('asm_op, stk_fun_extra) _fundef

type 'asm_op _sfun_decl = ('asm_op, stk_fun_extra) _fun_decl

type 'asm_op _sfun_decls = ('asm_op, stk_fun_extra) _fun_decl list

type 'asm_op _sprog = ('asm_op, stk_fun_extra, sprog_extra) _prog

(** val to_sprog : coq_PointerData -> 'a1 asmOp -> 'a1 _sprog -> 'a1 sprog **)

let to_sprog _ _ p =
  Obj.magic p

(** val with_body :
    'a1 asmOp -> ('a1, 'a2) _fundef -> 'a1 instr list -> ('a1, 'a2) _fundef **)

let with_body _ fd body =
  { f_info = fd.f_info; f_tyin = fd.f_tyin; f_params = fd.f_params; f_body =
    body; f_tyout = fd.f_tyout; f_res = fd.f_res; f_extra = fd.f_extra }

(** val swith_extra :
    coq_PointerData -> 'a1 asmOp -> coq_PointerData -> 'a1 ufundef ->
    Equality.sort -> 'a1 sfundef **)

let swith_extra _ _ _ fd f_extra0 =
  { f_info = fd.f_info; f_tyin = fd.f_tyin; f_params = fd.f_params; f_body =
    fd.f_body; f_tyout = fd.f_tyout; f_res = fd.f_res; f_extra = f_extra0 }

(** val is_const : pexpr -> coq_Z option **)

let is_const = function
| Pconst n -> Some n
| _ -> None

(** val is_bool : pexpr -> bool option **)

let is_bool = function
| Pbool b -> Some b
| _ -> None

(** val cast_w : wsize -> pexpr -> pexpr **)

let cast_w ws x =
  Papp1 ((Oword_of_int ws), x)

(** val cast_ptr : coq_PointerData -> pexpr -> pexpr **)

let cast_ptr pd =
  cast_w (coq_Uptr pd)

(** val cast_const : coq_PointerData -> coq_Z -> pexpr **)

let cast_const pd z =
  cast_ptr pd (Pconst z)

(** val wconst : wsize -> GRing.ComRing.sort -> pexpr **)

let wconst sz n =
  Papp1 ((Oword_of_int sz), (Pconst (wunsigned sz n)))

(** val is_wconst : wsize -> pexpr -> GRing.ComRing.sort option **)

let is_wconst sz = function
| Papp1 (s, e0) ->
  (match s with
   | Oword_of_int sz' ->
     if cmp_le wsize_cmp sz sz'
     then Option.bind (fun n -> Some (zero_extend sz sz' (wrepr sz' n)))
            (is_const e0)
     else None
   | _ -> None)
| _ -> None

(** val is_wconst_of_size : Equality.sort -> pexpr -> coq_Z option **)

let is_wconst_of_size sz = function
| Papp1 (s, p) ->
  (match s with
   | Oword_of_int sz' ->
     (match p with
      | Pconst z ->
        if eq_op wsize_eqType (Obj.magic sz') sz then Some z else None
      | _ -> None)
   | _ -> None)
| _ -> None

(** val vrv_rec : Sv.t -> lval -> Sv.t **)

let vrv_rec s = function
| Lvar x -> Sv.add (Obj.magic x.v_var) s
| Laset (_, _, x, _) -> Sv.add (Obj.magic x.v_var) s
| Lasub (_, _, _, x, _) -> Sv.add (Obj.magic x.v_var) s
| _ -> s

(** val vrvs_rec : Sv.t -> lval list -> Sv.t **)

let vrvs_rec s rv =
  foldl vrv_rec s rv

(** val vrv : lval -> Sv.t **)

let vrv =
  vrv_rec Sv.empty

(** val vrvs : lval list -> Sv.t **)

let vrvs =
  vrvs_rec Sv.empty

(** val lv_write_mem : lval -> bool **)

let lv_write_mem = function
| Lmem (_, _, _) -> true
| _ -> false

(** val write_i_rec : 'a1 asmOp -> Sv.t -> 'a1 instr_r -> Sv.t **)

let write_i_rec _ =
  let rec write_i_rec0 s = function
  | Cassgn (x, _, _, _) -> vrv_rec s x
  | Copn (xs, _, _, _) -> vrvs_rec s xs
  | Cif (_, c1, c2) -> foldl write_I_rec0 (foldl write_I_rec0 s c2) c1
  | Cfor (x, _, c) -> foldl write_I_rec0 (Sv.add (Obj.magic x.v_var) s) c
  | Cwhile (_, c, _, c') -> foldl write_I_rec0 (foldl write_I_rec0 s c') c
  | Ccall (_, x, _, _) -> vrvs_rec s x
  and write_I_rec0 s = function
  | MkI (_, i0) -> write_i_rec0 s i0
  in write_i_rec0

(** val write_I_rec : 'a1 asmOp -> Sv.t -> 'a1 instr -> Sv.t **)

let write_I_rec _ =
  let rec write_i_rec0 s = function
  | Cassgn (x, _, _, _) -> vrv_rec s x
  | Copn (xs, _, _, _) -> vrvs_rec s xs
  | Cif (_, c1, c2) -> foldl write_I_rec0 (foldl write_I_rec0 s c2) c1
  | Cfor (x, _, c) -> foldl write_I_rec0 (Sv.add (Obj.magic x.v_var) s) c
  | Cwhile (_, c, _, c') -> foldl write_I_rec0 (foldl write_I_rec0 s c') c
  | Ccall (_, x, _, _) -> vrvs_rec s x
  and write_I_rec0 s = function
  | MkI (_, i0) -> write_i_rec0 s i0
  in write_I_rec0

(** val write_i : 'a1 asmOp -> 'a1 instr_r -> Sv.t **)

let write_i asmop i =
  write_i_rec asmop Sv.empty i

(** val write_I : 'a1 asmOp -> 'a1 instr -> Sv.t **)

let write_I asmop i =
  write_I_rec asmop Sv.empty i

(** val write_c_rec : 'a1 asmOp -> Sv.t -> 'a1 instr list -> Sv.t **)

let write_c_rec asmop s c =
  foldl (write_I_rec asmop) s c

(** val write_c : 'a1 asmOp -> 'a1 instr list -> Sv.t **)

let write_c asmop c =
  write_c_rec asmop Sv.empty c

(** val read_gvar : gvar -> Sv.t **)

let read_gvar x =
  if is_lvar x then Sv.singleton (Obj.magic x.gv.v_var) else Sv.empty

(** val read_e_rec : Sv.t -> pexpr -> Sv.t **)

let rec read_e_rec s = function
| Pvar x -> Sv.union (read_gvar x) s
| Pget (_, _, x, e0) -> read_e_rec (Sv.union (read_gvar x) s) e0
| Psub (_, _, _, x, e0) -> read_e_rec (Sv.union (read_gvar x) s) e0
| Pload (_, x, e0) -> read_e_rec (Sv.add (Obj.magic x.v_var) s) e0
| Papp1 (_, e0) -> read_e_rec s e0
| Papp2 (_, e1, e2) -> read_e_rec (read_e_rec s e2) e1
| PappN (_, es) -> foldl read_e_rec s es
| Pif (_, t0, e1, e2) -> read_e_rec (read_e_rec (read_e_rec s e2) e1) t0
| _ -> s

(** val read_e : pexpr -> Sv.t **)

let read_e =
  read_e_rec Sv.empty

(** val read_es_rec : Sv.t -> pexpr list -> Sv.t **)

let read_es_rec =
  foldl read_e_rec

(** val read_es : pexpr list -> Sv.t **)

let read_es =
  read_es_rec Sv.empty

(** val read_rv_rec : Sv.t -> lval -> Sv.t **)

let read_rv_rec s = function
| Lmem (_, x, e) -> read_e_rec (Sv.add (Obj.magic x.v_var) s) e
| Laset (_, _, x, e) -> read_e_rec (Sv.add (Obj.magic x.v_var) s) e
| Lasub (_, _, _, x, e) -> read_e_rec (Sv.add (Obj.magic x.v_var) s) e
| _ -> s

(** val read_rv : lval -> Sv.t **)

let read_rv =
  read_rv_rec Sv.empty

(** val read_rvs_rec : Sv.t -> lval list -> Sv.t **)

let read_rvs_rec =
  foldl read_rv_rec

(** val read_rvs : lval list -> Sv.t **)

let read_rvs =
  read_rvs_rec Sv.empty

(** val read_i_rec : 'a1 asmOp -> Sv.t -> 'a1 instr_r -> Sv.t **)

let read_i_rec _ =
  let rec read_i_rec0 s = function
  | Cassgn (x, _, _, e) -> read_rv_rec (read_e_rec s e) x
  | Copn (xs, _, _, es) -> read_es_rec (read_rvs_rec s xs) es
  | Cif (b, c1, c2) ->
    let s0 = foldl read_I_rec0 s c1 in
    let s1 = foldl read_I_rec0 s0 c2 in read_e_rec s1 b
  | Cfor (_, r, c) ->
    let (p, e2) = r in
    let (_, e1) = p in
    let s0 = foldl read_I_rec0 s c in read_e_rec (read_e_rec s0 e2) e1
  | Cwhile (_, c, e, c') ->
    let s0 = foldl read_I_rec0 s c in
    let s1 = foldl read_I_rec0 s0 c' in read_e_rec s1 e
  | Ccall (_, xs, _, es) -> read_es_rec (read_rvs_rec s xs) es
  and read_I_rec0 s = function
  | MkI (_, i0) -> read_i_rec0 s i0
  in read_i_rec0

(** val read_I_rec : 'a1 asmOp -> Sv.t -> 'a1 instr -> Sv.t **)

let read_I_rec _ =
  let rec read_i_rec0 s = function
  | Cassgn (x, _, _, e) -> read_rv_rec (read_e_rec s e) x
  | Copn (xs, _, _, es) -> read_es_rec (read_rvs_rec s xs) es
  | Cif (b, c1, c2) ->
    let s0 = foldl read_I_rec0 s c1 in
    let s1 = foldl read_I_rec0 s0 c2 in read_e_rec s1 b
  | Cfor (_, r, c) ->
    let (p, e2) = r in
    let (_, e1) = p in
    let s0 = foldl read_I_rec0 s c in read_e_rec (read_e_rec s0 e2) e1
  | Cwhile (_, c, e, c') ->
    let s0 = foldl read_I_rec0 s c in
    let s1 = foldl read_I_rec0 s0 c' in read_e_rec s1 e
  | Ccall (_, xs, _, es) -> read_es_rec (read_rvs_rec s xs) es
  and read_I_rec0 s = function
  | MkI (_, i0) -> read_i_rec0 s i0
  in read_I_rec0

(** val read_c_rec : 'a1 asmOp -> Sv.t -> 'a1 instr list -> Sv.t **)

let read_c_rec asmop =
  foldl (read_I_rec asmop)

(** val read_i : 'a1 asmOp -> 'a1 instr_r -> Sv.t **)

let read_i asmop =
  read_i_rec asmop Sv.empty

(** val read_I : 'a1 asmOp -> 'a1 instr -> Sv.t **)

let read_I asmop =
  read_I_rec asmop Sv.empty

(** val read_c : 'a1 asmOp -> 'a1 instr list -> Sv.t **)

let read_c asmop =
  read_c_rec asmop Sv.empty

(** val vars_I : 'a1 asmOp -> 'a1 instr -> Sv.t **)

let vars_I asmop i =
  Sv.union (read_I asmop i) (write_I asmop i)

(** val vars_c : 'a1 asmOp -> 'a1 instr list -> Sv.t **)

let vars_c asmop c =
  Sv.union (read_c asmop c) (write_c asmop c)

(** val vars_lval : lval -> Sv.t **)

let vars_lval l =
  Sv.union (read_rv l) (vrv l)

(** val vars_lvals : lval list -> Sv.t **)

let vars_lvals ls =
  Sv.union (read_rvs ls) (vrvs ls)

(** val vars_l : var_i list -> Sv.t **)

let rec vars_l = function
| [] -> Sv.empty
| h :: q -> Sv.add (Obj.magic h.v_var) (vars_l q)

(** val vars_fd :
    'a1 asmOp -> Equality.coq_type -> progT -> 'a1 fundef -> Sv.t **)

let vars_fd asmop _ _ fd =
  Sv.union (vars_l fd.f_params)
    (Sv.union (vars_l fd.f_res) (vars_c asmop fd.f_body))

(** val vars_p :
    'a1 asmOp -> Equality.coq_type -> progT -> 'a1 fun_decl list -> Sv.t **)

let vars_p asmop eft pT p =
  foldr (fun f x -> let (_, fd) = f in Sv.union x (vars_fd asmop eft pT fd))
    Sv.empty p

(** val eq_gvar : gvar -> gvar -> bool **)

let eq_gvar x x' =
  (&&) (eq_op v_scope_eqType (Obj.magic x.gs) (Obj.magic x'.gs))
    (eq_op Var.var_eqType (Obj.magic x.gv.v_var) (Obj.magic x'.gv.v_var))

(** val eq_expr : pexpr -> pexpr -> bool **)

let rec eq_expr e e' =
  match e with
  | Pconst z ->
    (match e' with
     | Pconst z' -> eq_op coq_Z_eqType (Obj.magic z) (Obj.magic z')
     | _ -> false)
  | Pbool b ->
    (match e' with
     | Pbool b' -> eq_op bool_eqType (Obj.magic b) (Obj.magic b')
     | _ -> false)
  | Parr_init n ->
    (match e' with
     | Parr_init n' -> eq_op pos_eqType (Obj.magic n) (Obj.magic n')
     | _ -> false)
  | Pvar x -> (match e' with
               | Pvar x' -> eq_gvar x x'
               | _ -> false)
  | Pget (aa, w, x, e0) ->
    (match e' with
     | Pget (aa', w', x', e'0) ->
       (&&)
         ((&&)
           ((&&) (eq_op arr_access_eqType (Obj.magic aa) (Obj.magic aa'))
             (eq_op wsize_eqType (Obj.magic w) (Obj.magic w')))
           (eq_gvar x x')) (eq_expr e0 e'0)
     | _ -> false)
  | Psub (aa, w, len, x, e0) ->
    (match e' with
     | Psub (aa', w', len', x', e'0) ->
       (&&)
         ((&&)
           ((&&)
             ((&&) (eq_op arr_access_eqType (Obj.magic aa) (Obj.magic aa'))
               (eq_op wsize_eqType (Obj.magic w) (Obj.magic w')))
             (eq_op pos_eqType (Obj.magic len) (Obj.magic len')))
           (eq_gvar x x')) (eq_expr e0 e'0)
     | _ -> false)
  | Pload (w, x, e0) ->
    (match e' with
     | Pload (w', x', e'0) ->
       (&&)
         ((&&) (eq_op wsize_eqType (Obj.magic w) (Obj.magic w'))
           (eq_op Var.var_eqType (Obj.magic x.v_var) (Obj.magic x'.v_var)))
         (eq_expr e0 e'0)
     | _ -> false)
  | Papp1 (o, e0) ->
    (match e' with
     | Papp1 (o', e'0) ->
       (&&) (eq_op sop1_eqType (Obj.magic o) (Obj.magic o')) (eq_expr e0 e'0)
     | _ -> false)
  | Papp2 (o, e1, e2) ->
    (match e' with
     | Papp2 (o', e1', e2') ->
       (&&)
         ((&&) (eq_op sop2_eqType (Obj.magic o) (Obj.magic o'))
           (eq_expr e1 e1')) (eq_expr e2 e2')
     | _ -> false)
  | PappN (o, es) ->
    (match e' with
     | PappN (o', es') ->
       (&&) (eq_op opN_eqType (Obj.magic o) (Obj.magic o'))
         (all2 eq_expr es es')
     | _ -> false)
  | Pif (t0, e0, e1, e2) ->
    (match e' with
     | Pif (t', e'0, e1', e2') ->
       (&&)
         ((&&)
           ((&&) (eq_op stype_eqType (Obj.magic t0) (Obj.magic t'))
             (eq_expr e0 e'0)) (eq_expr e1 e1')) (eq_expr e2 e2')
     | _ -> false)

(** val eq_lval : lval -> lval -> bool **)

let eq_lval x x' =
  match x with
  | Lnone (_, ty) ->
    (match x' with
     | Lnone (_, ty') -> eq_op stype_eqType (Obj.magic ty) (Obj.magic ty')
     | _ -> false)
  | Lvar v ->
    (match x' with
     | Lvar v' ->
       eq_op Var.var_eqType (Obj.magic v.v_var) (Obj.magic v'.v_var)
     | _ -> false)
  | Lmem (w, v, e) ->
    (match x' with
     | Lmem (w', v', e') ->
       (&&)
         ((&&) (eq_op wsize_eqType (Obj.magic w) (Obj.magic w'))
           (eq_op Var.var_eqType (Obj.magic v.v_var) (Obj.magic v'.v_var)))
         (eq_expr e e')
     | _ -> false)
  | Laset (aa, w, v, e) ->
    (match x' with
     | Laset (aa', w', v', e') ->
       (&&)
         ((&&)
           ((&&) (eq_op arr_access_eqType (Obj.magic aa) (Obj.magic aa'))
             (eq_op wsize_eqType (Obj.magic w) (Obj.magic w')))
           (eq_op Var.var_eqType (Obj.magic v.v_var) (Obj.magic v'.v_var)))
         (eq_expr e e')
     | _ -> false)
  | Lasub (aa, w, len, v, e) ->
    (match x' with
     | Lasub (aa', w', len', v', e') ->
       (&&)
         ((&&)
           ((&&)
             ((&&) (eq_op arr_access_eqType (Obj.magic aa) (Obj.magic aa'))
               (eq_op wsize_eqType (Obj.magic w) (Obj.magic w')))
             (eq_op pos_eqType (Obj.magic len) (Obj.magic len')))
           (eq_op Var.var_eqType (Obj.magic v.v_var) (Obj.magic v'.v_var)))
         (eq_expr e e')
     | _ -> false)
