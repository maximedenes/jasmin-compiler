open Datatypes
open Compiler_util
open Eqtype
open Expr
open Global
open Seq
open Sopn
open Type
open Utils0
open Var0

module E :
 sig
  val pass : char list

  val make_ref_error : instr_info -> char list -> pp_error_loc
 end

val with_var :
  'a1 asmOp -> (glob_decl list -> Var.var -> Equality.sort) ->
  Equality.coq_type -> progT -> 'a1 prog -> var_i -> Var.var -> var_i

val is_reg_ptr_expr :
  'a1 asmOp -> (Var.var -> bool) -> (glob_decl list -> Var.var ->
  Equality.sort) -> Equality.coq_type -> progT -> 'a1 prog -> Var.var ->
  pexpr -> var_i option

val is_reg_ptr_lval :
  'a1 asmOp -> (Var.var -> bool) -> (glob_decl list -> Var.var ->
  Equality.sort) -> Equality.coq_type -> progT -> 'a1 prog -> Var.var -> lval
  -> var_i option

val make_prologue :
  'a1 asmOp -> (Var.var -> bool) -> (glob_decl list -> Var.var ->
  Equality.sort) -> Equality.coq_type -> progT -> 'a1 prog -> instr_info ->
  Sv.t -> var_i list -> stype list -> pexpr list -> (pp_error_loc, 'a1 instr
  list * pexpr list) result

type pseudo_instr =
| PI_lv of lval
| PI_i of lval * stype * var_i

val make_pseudo_epilogue :
  'a1 asmOp -> (Var.var -> bool) -> (glob_decl list -> Var.var ->
  Equality.sort) -> Equality.coq_type -> progT -> 'a1 prog -> instr_info ->
  Sv.t -> var_i list -> stype list -> lval list -> (pp_error_loc,
  pseudo_instr list) result

val mk_ep_i : 'a1 asmOp -> instr_info -> lval -> stype -> var_i -> 'a1 instr

val noload : pexpr -> bool

val wf_lv : lval -> bool

val swapable :
  'a1 asmOp -> instr_info -> pseudo_instr list -> (pp_error_loc, lval
  list * 'a1 instr list) result

val make_epilogue :
  'a1 asmOp -> (Var.var -> bool) -> (glob_decl list -> Var.var ->
  Equality.sort) -> Equality.coq_type -> progT -> 'a1 prog -> instr_info ->
  Sv.t -> var_i list -> stype list -> lval list -> (pp_error_loc, lval
  list * 'a1 instr list) result

val update_c :
  'a1 asmOp -> ('a1 instr -> 'a1 instr list cexec) -> 'a1 instr list ->
  (pp_error_loc, 'a1 instr list) result

val update_i :
  'a1 asmOp -> (Var.var -> bool) -> (glob_decl list -> Var.var ->
  Equality.sort) -> Equality.coq_type -> progT -> 'a1 prog -> (funname ->
  ((var_i list * stype list) * var_i list) * stype list) -> Sv.t -> 'a1 instr
  -> 'a1 instr list cexec

val update_fd :
  'a1 asmOp -> (Var.var -> bool) -> (glob_decl list -> Var.var ->
  Equality.sort) -> Equality.coq_type -> progT -> 'a1 prog -> (funname ->
  ((var_i list * stype list) * var_i list) * stype list) -> 'a1 fundef ->
  (pp_error_loc, ('a1, Equality.sort) _fundef) result

val get_sig :
  'a1 asmOp -> Equality.coq_type -> progT -> 'a1 prog -> funname -> ((var_i
  list * stype list) * var_i list) * stype list

val makereference_prog :
  'a1 asmOp -> (Var.var -> bool) -> (glob_decl list -> Var.var ->
  Equality.sort) -> Equality.coq_type -> progT -> 'a1 prog -> 'a1 prog cexec
