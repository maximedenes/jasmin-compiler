open BinInt
open BinNums
open List0
open Arch_decl
open Arch_extra
open Expr
open Linearization
open Memory_model
open Seq
open Sopn
open Type
open Utils0
open Word0
open Wsize
open X86_decl
open X86_extra
open X86_instr_decl

(** val x86_allocate_stack_frame :
    var_i -> coq_Z -> (lval list * x86_extended_op sopn) * pexpr list **)

let x86_allocate_stack_frame rspi sz =
  let rspg = { gv = rspi; gs = Slocal } in
  let p = Papp2 ((Oadd (Op_w (coq_Uptr (arch_pd x86_decl)))), (Pvar rspg),
    (cast_const (arch_pd x86_decl) sz))
  in
  ((((Lvar rspi) :: []), (coq_Ox86 (LEA (coq_Uptr (arch_pd x86_decl))))),
  (p :: []))

(** val x86_free_stack_frame :
    var_i -> coq_Z -> (lval list * x86_extended_op sopn) * pexpr list **)

let x86_free_stack_frame rspi sz =
  let rspg = { gv = rspi; gs = Slocal } in
  let p = Papp2 ((Osub (Op_w (coq_Uptr (arch_pd x86_decl)))), (Pvar rspg),
    (cast_const (arch_pd x86_decl) sz))
  in
  ((((Lvar rspi) :: []), (coq_Ox86 (LEA (coq_Uptr (arch_pd x86_decl))))),
  (p :: []))

(** val x86_ensure_rsp_alignment :
    var_i -> wsize -> (lval list * x86_extended_op sopn) * pexpr list **)

let x86_ensure_rsp_alignment rspi al =
  let to_lvar = fun x -> Lvar { v_var = (to_var Coq_sbool x86_rflag_toS x);
    v_info = Coq_xH }
  in
  let eflags = List0.map to_lvar (OF :: (CF :: (SF :: (PF :: (ZF :: []))))) in
  let p0 = Pvar { gv = rspi; gs = Slocal } in
  let p1 = Papp1 ((Oword_of_int (coq_Uptr (arch_pd x86_decl))), (Pconst
    (Z.opp (wsize_size al))))
  in
  (((cat eflags ((Lvar rspi) :: [])),
  (coq_Ox86 (AND (coq_Uptr (arch_pd x86_decl))))), (p0 :: (p1 :: [])))

(** val x86_lassign :
    lval -> wsize -> pexpr -> (lval list * x86_extended_op sopn) * pexpr list **)

let x86_lassign x ws e =
  let op = if cmp_le wsize_cmp ws U64 then MOV ws else VMOVDQU ws in
  (((x :: []), (coq_Ox86 op)), (e :: []))

(** val x86_linearization_params :
    (register, xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op
    linearization_params **)

let x86_linearization_params =
  { lp_tmp = (Obj.magic ('R'::('A'::('X'::[])))); lp_allocate_stack_frame =
    x86_allocate_stack_frame; lp_free_stack_frame = x86_free_stack_frame;
    lp_ensure_rsp_alignment = x86_ensure_rsp_alignment; lp_lassign =
    x86_lassign }
