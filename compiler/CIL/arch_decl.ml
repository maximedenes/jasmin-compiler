open BinNums
open Bool
open Datatypes
open Eqtype
open Fintype
open Label
open Memory_model
open Sem_type
open Seq
open SsrZ
open Ssralg
open Ssrbool
open Ssrnat
open Strings
open Type
open Utils0
open Word0
open Wsize

type __ = Obj.t
let __ = let rec f _ = Obj.repr f in Obj.repr f

type 't coq_ToString = { category : char list; _finC : 't finTypeC;
                         to_string : ('t -> char list);
                         strings : (char list * 't) list }

(** val rtype : stype -> 'a1 coq_ToString -> stype **)

let rtype t _ =
  t

type ('reg, 'xreg, 'rflag, 'cond) arch_decl = { reg_size : wsize;
                                                xreg_size : wsize;
                                                cond_eqC : 'cond eqTypeC;
                                                toS_r : 'reg coq_ToString;
                                                toS_x : 'xreg coq_ToString;
                                                toS_f : 'rflag coq_ToString }

(** val arch_pd : ('a1, 'a2, 'a3, 'a4) arch_decl -> coq_PointerData **)

let arch_pd h =
  h.reg_size

type ('reg, 'xreg, 'rflag, 'cond) reg_t = 'reg

type ('reg, 'xreg, 'rflag, 'cond) xreg_t = 'xreg

type ('reg, 'xreg, 'rflag, 'cond) rflag_t = 'rflag

type ('reg, 'xreg, 'rflag, 'cond) cond_t = 'cond

type ('reg, 'xreg, 'rflag, 'cond) reg_address = { ad_disp : GRing.ComRing.sort;
                                                  ad_base : ('reg, 'xreg,
                                                            'rflag, 'cond)
                                                            reg_t option;
                                                  ad_scale : nat;
                                                  ad_offset : ('reg, 'xreg,
                                                              'rflag, 'cond)
                                                              reg_t option }

type ('reg, 'xreg, 'rflag, 'cond) address =
| Areg of ('reg, 'xreg, 'rflag, 'cond) reg_address
| Arip of GRing.ComRing.sort

(** val oeq_reg :
    ('a1, 'a2, 'a3, 'a4) arch_decl -> ('a1, 'a2, 'a3, 'a4) reg_t option ->
    ('a1, 'a2, 'a3, 'a4) reg_t option -> bool **)

let oeq_reg arch x y =
  eq_op (option_eqType (ceqT_eqType arch.toS_r._finC._eqC)) (Obj.magic x)
    (Obj.magic y)

(** val reg_address_beq :
    ('a1, 'a2, 'a3, 'a4) arch_decl -> ('a1, 'a2, 'a3, 'a4) reg_address ->
    ('a1, 'a2, 'a3, 'a4) reg_address -> bool **)

let reg_address_beq arch addr1 addr2 =
  let { ad_disp = d1; ad_base = b1; ad_scale = s1; ad_offset = o1 } = addr1 in
  let { ad_disp = d2; ad_base = b2; ad_scale = s2; ad_offset = o2 } = addr2 in
  (&&) (eq_op (GRing.ComRing.eqType (word (coq_Uptr (arch_pd arch)))) d1 d2)
    ((&&) (oeq_reg arch b1 b2)
      ((&&) (eq_op nat_eqType (Obj.magic s1) (Obj.magic s2))
        (oeq_reg arch o1 o2)))

(** val reg_address_eq_axiom :
    ('a1, 'a2, 'a3, 'a4) arch_decl -> ('a1, 'a2, 'a3, 'a4) reg_address
    Equality.axiom **)

let reg_address_eq_axiom arch _top_assumption_ =
  let _evar_0_ = fun d1 b1 s1 o1 __top_assumption_ ->
    let _evar_0_ = fun d2 b2 s2 o2 ->
      iffP
        (reg_address_beq arch { ad_disp = d1; ad_base = b1; ad_scale = s1;
          ad_offset = o1 } { ad_disp = d2; ad_base = b2; ad_scale = s2;
          ad_offset = o2 })
        (if reg_address_beq arch { ad_disp = d1; ad_base = b1; ad_scale = s1;
              ad_offset = o1 } { ad_disp = d2; ad_base = b2; ad_scale = s2;
              ad_offset = o2 }
         then ReflectT
         else ReflectF)
    in
    let { ad_disp = ad_disp0; ad_base = ad_base0; ad_scale = ad_scale0;
      ad_offset = ad_offset0 } = __top_assumption_
    in
    _evar_0_ ad_disp0 ad_base0 ad_scale0 ad_offset0
  in
  let { ad_disp = ad_disp0; ad_base = ad_base0; ad_scale = ad_scale0;
    ad_offset = ad_offset0 } = _top_assumption_
  in
  _evar_0_ ad_disp0 ad_base0 ad_scale0 ad_offset0

(** val reg_address_eqMixin :
    ('a1, 'a2, 'a3, 'a4) arch_decl -> ('a1, 'a2, 'a3, 'a4) reg_address
    Equality.mixin_of **)

let reg_address_eqMixin arch =
  { Equality.op = (reg_address_beq arch); Equality.mixin_of__1 =
    (reg_address_eq_axiom arch) }

(** val reg_address_eqType :
    ('a1, 'a2, 'a3, 'a4) arch_decl -> Equality.coq_type **)

let reg_address_eqType arch =
  Obj.magic reg_address_eqMixin arch

(** val address_beq :
    ('a1, 'a2, 'a3, 'a4) arch_decl -> ('a1, 'a2, 'a3, 'a4) address -> ('a1,
    'a2, 'a3, 'a4) address -> bool **)

let address_beq arch addr1 addr2 =
  match addr1 with
  | Areg ra1 ->
    (match addr2 with
     | Areg ra2 ->
       eq_op (reg_address_eqType arch) (Obj.magic ra1) (Obj.magic ra2)
     | Arip _ -> false)
  | Arip p1 ->
    (match addr2 with
     | Areg _ -> false
     | Arip p2 ->
       eq_op (GRing.ComRing.eqType (word (coq_Uptr (arch_pd arch)))) p1 p2)

(** val address_eq_axiom :
    ('a1, 'a2, 'a3, 'a4) arch_decl -> ('a1, 'a2, 'a3, 'a4) address
    Equality.axiom **)

let address_eq_axiom arch _top_assumption_ =
  let _evar_0_ = fun _r_ __top_assumption_ ->
    let _evar_0_ = fun _r1_ ->
      reflect_inj (reg_address_eqType arch) (Obj.magic (fun x -> Areg x)) _r_
        _r1_ (eqP (reg_address_eqType arch) _r_ _r1_)
    in
    let _evar_0_0 = fun _ -> ReflectF in
    (match __top_assumption_ with
     | Areg r -> Obj.magic _evar_0_ r
     | Arip s -> _evar_0_0 s)
  in
  let _evar_0_0 = fun _s_ __top_assumption_ ->
    let _evar_0_0 = fun _ -> ReflectF in
    let _evar_0_1 = fun _s1_ ->
      reflect_inj (GRing.ComRing.eqType (word arch.reg_size)) (fun x -> Arip
        x) _s_ _s1_ (eqP (GRing.ComRing.eqType (word arch.reg_size)) _s_ _s1_)
    in
    (match __top_assumption_ with
     | Areg r -> _evar_0_0 r
     | Arip s -> _evar_0_1 s)
  in
  (match _top_assumption_ with
   | Areg r -> Obj.magic _evar_0_ r
   | Arip s -> _evar_0_0 s)

(** val address_eqMixin :
    ('a1, 'a2, 'a3, 'a4) arch_decl -> ('a1, 'a2, 'a3, 'a4) address
    Equality.mixin_of **)

let address_eqMixin arch =
  { Equality.op = (address_beq arch); Equality.mixin_of__1 =
    (address_eq_axiom arch) }

(** val address_eqType :
    ('a1, 'a2, 'a3, 'a4) arch_decl -> Equality.coq_type **)

let address_eqType arch =
  Obj.magic address_eqMixin arch

(** val rflags : ('a1, 'a2, 'a3, 'a4) arch_decl -> 'a3 list **)

let rflags arch =
  Obj.magic enum_mem (cfinT_finType arch.toS_f._finC)
    (mem predPredType
      (Obj.magic PredOfSimpl.coerce (coq_SimplPred (fun _ -> true))))

type ('reg, 'xreg, 'rflag, 'cond) asm_arg =
| Condt of ('reg, 'xreg, 'rflag, 'cond) cond_t
| Imm of wsize * GRing.ComRing.sort
| Reg of ('reg, 'xreg, 'rflag, 'cond) reg_t
| Addr of ('reg, 'xreg, 'rflag, 'cond) address
| XReg of ('reg, 'xreg, 'rflag, 'cond) xreg_t

type ('reg, 'xreg, 'rflag, 'cond) asm_args =
  ('reg, 'xreg, 'rflag, 'cond) asm_arg list

(** val asm_arg_beq :
    ('a1, 'a2, 'a3, 'a4) arch_decl -> ('a1, 'a2, 'a3, 'a4) asm_arg -> ('a1,
    'a2, 'a3, 'a4) asm_arg -> bool **)

let asm_arg_beq arch a1 a2 =
  match a1 with
  | Condt t1 ->
    (match a2 with
     | Condt t2 ->
       eq_op (ceqT_eqType arch.cond_eqC) (Obj.magic t1) (Obj.magic t2)
     | _ -> false)
  | Imm (sz1, w1) ->
    (match a2 with
     | Imm (sz2, w2) ->
       (&&) (eq_op wsize_eqType (Obj.magic sz1) (Obj.magic sz2))
         (eq_op coq_Z_eqType (Obj.magic wunsigned sz1 w1)
           (Obj.magic wunsigned sz2 w2))
     | _ -> false)
  | Reg r1 ->
    (match a2 with
     | Reg r2 ->
       eq_op (ceqT_eqType arch.toS_r._finC._eqC) (Obj.magic r1) (Obj.magic r2)
     | _ -> false)
  | Addr a3 ->
    (match a2 with
     | Addr a4 -> eq_op (address_eqType arch) (Obj.magic a3) (Obj.magic a4)
     | _ -> false)
  | XReg r1 ->
    (match a2 with
     | XReg r2 ->
       eq_op (ceqT_eqType arch.toS_x._finC._eqC) (Obj.magic r1) (Obj.magic r2)
     | _ -> false)

(** val asm_arg_eq_axiom :
    ('a1, 'a2, 'a3, 'a4) arch_decl -> ('a1, 'a2, 'a3, 'a4) asm_arg
    Equality.axiom **)

let asm_arg_eq_axiom arch _top_assumption_ =
  let _evar_0_ = fun t1 __top_assumption_ ->
    let _evar_0_ = fun t2 ->
      reflect_inj (ceqT_eqType arch.cond_eqC) (fun x -> Condt x) t1 t2
        (eqP (ceqT_eqType arch.cond_eqC) t1 t2)
    in
    let _evar_0_0 = fun _ _ -> ReflectF in
    let _evar_0_1 = fun _ -> ReflectF in
    let _evar_0_2 = fun _ -> ReflectF in
    let _evar_0_3 = fun _ -> ReflectF in
    (match __top_assumption_ with
     | Condt c -> _evar_0_ c
     | Imm (ws, s) -> _evar_0_0 ws s
     | Reg r -> _evar_0_1 r
     | Addr a -> _evar_0_2 a
     | XReg x -> _evar_0_3 x)
  in
  let _evar_0_0 = fun sz1 w1 __top_assumption_ ->
    let _evar_0_0 = fun _ -> ReflectF in
    let _evar_0_1 = fun sz2 w2 ->
      iffP
        ((&&) (eq_op wsize_eqType (Obj.magic sz1) (Obj.magic sz2))
          (eq_op coq_Z_eqType (Obj.magic wunsigned sz1 w1)
            (Obj.magic wunsigned sz2 w2)))
        (if (&&) (eq_op wsize_eqType (Obj.magic sz1) (Obj.magic sz2))
              (eq_op coq_Z_eqType (Obj.magic wunsigned sz1 w1)
                (Obj.magic wunsigned sz2 w2))
         then ReflectT
         else ReflectF)
    in
    let _evar_0_2 = fun _ -> ReflectF in
    let _evar_0_3 = fun _ -> ReflectF in
    let _evar_0_4 = fun _ -> ReflectF in
    (match __top_assumption_ with
     | Condt c -> _evar_0_0 c
     | Imm (ws, s) -> _evar_0_1 ws s
     | Reg r -> _evar_0_2 r
     | Addr a -> _evar_0_3 a
     | XReg x -> _evar_0_4 x)
  in
  let _evar_0_1 = fun r1 __top_assumption_ ->
    let _evar_0_1 = fun _ -> ReflectF in
    let _evar_0_2 = fun _ _ -> ReflectF in
    let _evar_0_3 = fun r2 ->
      reflect_inj (ceqT_eqType arch.toS_r._finC._eqC) (fun x -> Reg x) r1 r2
        (eqP (ceqT_eqType arch.toS_r._finC._eqC) r1 r2)
    in
    let _evar_0_4 = fun _ -> ReflectF in
    let _evar_0_5 = fun _ -> ReflectF in
    (match __top_assumption_ with
     | Condt c -> _evar_0_1 c
     | Imm (ws, s) -> _evar_0_2 ws s
     | Reg r -> _evar_0_3 r
     | Addr a -> _evar_0_4 a
     | XReg x -> _evar_0_5 x)
  in
  let _evar_0_2 = fun a1 __top_assumption_ ->
    let _evar_0_2 = fun _ -> ReflectF in
    let _evar_0_3 = fun _ _ -> ReflectF in
    let _evar_0_4 = fun _ -> ReflectF in
    let _evar_0_5 = fun a2 ->
      reflect_inj (address_eqType arch) (Obj.magic (fun x -> Addr x)) a1 a2
        (eqP (address_eqType arch) a1 a2)
    in
    let _evar_0_6 = fun _ -> ReflectF in
    (match __top_assumption_ with
     | Condt c -> _evar_0_2 c
     | Imm (ws, s) -> _evar_0_3 ws s
     | Reg r -> _evar_0_4 r
     | Addr a -> Obj.magic _evar_0_5 a
     | XReg x -> _evar_0_6 x)
  in
  let _evar_0_3 = fun xr1 __top_assumption_ ->
    let _evar_0_3 = fun _ -> ReflectF in
    let _evar_0_4 = fun _ _ -> ReflectF in
    let _evar_0_5 = fun _ -> ReflectF in
    let _evar_0_6 = fun _ -> ReflectF in
    let _evar_0_7 = fun xr2 ->
      reflect_inj (ceqT_eqType arch.toS_x._finC._eqC) (fun x -> XReg x) xr1
        xr2 (eqP (ceqT_eqType arch.toS_x._finC._eqC) xr1 xr2)
    in
    (match __top_assumption_ with
     | Condt c -> _evar_0_3 c
     | Imm (ws, s) -> _evar_0_4 ws s
     | Reg r -> _evar_0_5 r
     | Addr a -> _evar_0_6 a
     | XReg x -> _evar_0_7 x)
  in
  (match _top_assumption_ with
   | Condt c -> Obj.magic _evar_0_ c
   | Imm (ws, s) -> _evar_0_0 ws s
   | Reg r -> Obj.magic _evar_0_1 r
   | Addr a -> Obj.magic _evar_0_2 a
   | XReg x -> Obj.magic _evar_0_3 x)

(** val asm_arg_eqMixin :
    ('a1, 'a2, 'a3, 'a4) arch_decl -> ('a1, 'a2, 'a3, 'a4) asm_arg
    Equality.mixin_of **)

let asm_arg_eqMixin arch =
  { Equality.op = (asm_arg_beq arch); Equality.mixin_of__1 =
    (asm_arg_eq_axiom arch) }

(** val asm_arg_eqType :
    ('a1, 'a2, 'a3, 'a4) arch_decl -> Equality.coq_type **)

let asm_arg_eqType arch =
  Obj.magic asm_arg_eqMixin arch

type msb_flag =
| MSB_CLEAR
| MSB_MERGE

(** val msb_flag_beq : msb_flag -> msb_flag -> bool **)

let msb_flag_beq x y =
  match x with
  | MSB_CLEAR -> (match y with
                  | MSB_CLEAR -> true
                  | MSB_MERGE -> false)
  | MSB_MERGE -> (match y with
                  | MSB_CLEAR -> false
                  | MSB_MERGE -> true)

(** val msb_flag_eq_axiom : msb_flag Equality.axiom **)

let msb_flag_eq_axiom x y =
  iffP (msb_flag_beq x y) (if msb_flag_beq x y then ReflectT else ReflectF)

(** val msb_flag_eqMixin : msb_flag Equality.mixin_of **)

let msb_flag_eqMixin =
  { Equality.op = msb_flag_beq; Equality.mixin_of__1 = msb_flag_eq_axiom }

(** val msb_flag_eqType : Equality.coq_type **)

let msb_flag_eqType =
  Obj.magic msb_flag_eqMixin

type ('reg, 'xreg, 'rflag, 'cond) implicit_arg =
| IArflag of ('reg, 'xreg, 'rflag, 'cond) rflag_t
| IAreg of ('reg, 'xreg, 'rflag, 'cond) reg_t

type addr_kind =
| AK_compute
| AK_mem

type ('reg, 'xreg, 'rflag, 'cond) arg_desc =
| ADImplicit of ('reg, 'xreg, 'rflag, 'cond) implicit_arg
| ADExplicit of addr_kind * nat * ('reg, 'xreg, 'rflag, 'cond) reg_t option

(** val coq_F :
    ('a1, 'a2, 'a3, 'a4) arch_decl -> ('a1, 'a2, 'a3, 'a4) rflag_t -> ('a1,
    'a2, 'a3, 'a4) arg_desc **)

let coq_F _ f =
  ADImplicit (IArflag f)

(** val coq_R :
    ('a1, 'a2, 'a3, 'a4) arch_decl -> ('a1, 'a2, 'a3, 'a4) reg_t -> ('a1,
    'a2, 'a3, 'a4) arg_desc **)

let coq_R _ r =
  ADImplicit (IAreg r)

(** val coq_E :
    ('a1, 'a2, 'a3, 'a4) arch_decl -> nat -> ('a1, 'a2, 'a3, 'a4) arg_desc **)

let coq_E _ n =
  ADExplicit (AK_mem, n, None)

(** val coq_Ec :
    ('a1, 'a2, 'a3, 'a4) arch_decl -> nat -> ('a1, 'a2, 'a3, 'a4) arg_desc **)

let coq_Ec _ n =
  ADExplicit (AK_compute, n, None)

(** val coq_Ef :
    ('a1, 'a2, 'a3, 'a4) arch_decl -> nat -> ('a1, 'a2, 'a3, 'a4) reg_t ->
    ('a1, 'a2, 'a3, 'a4) arg_desc **)

let coq_Ef _ n r =
  ADExplicit (AK_mem, n, (Some r))

(** val check_oreg :
    ('a1, 'a2, 'a3, 'a4) arch_decl -> Equality.sort option -> ('a1, 'a2, 'a3,
    'a4) asm_arg -> bool **)

let check_oreg arch or0 ai =
  match or0 with
  | Some r ->
    (match ai with
     | Imm (_, _) -> true
     | Reg r' -> eq_op (ceqT_eqType arch.toS_r._finC._eqC) r (Obj.magic r')
     | _ -> false)
  | None -> true

type arg_kind =
| CAcond
| CAreg
| CAxmm
| CAmem of bool
| CAimm of wsize

(** val arg_kind_beq : arg_kind -> arg_kind -> bool **)

let arg_kind_beq x y =
  match x with
  | CAcond -> (match y with
               | CAcond -> true
               | _ -> false)
  | CAreg -> (match y with
              | CAreg -> true
              | _ -> false)
  | CAxmm -> (match y with
              | CAxmm -> true
              | _ -> false)
  | CAmem x0 ->
    (match y with
     | CAmem x1 -> internal_bool_beq x0 x1
     | _ -> false)
  | CAimm x0 -> (match y with
                 | CAimm x1 -> wsize_beq x0 x1
                 | _ -> false)

(** val arg_kind_eq_axiom : arg_kind Equality.axiom **)

let arg_kind_eq_axiom x y =
  iffP (arg_kind_beq x y) (if arg_kind_beq x y then ReflectT else ReflectF)

(** val arg_kind_eqMixin : arg_kind Equality.mixin_of **)

let arg_kind_eqMixin =
  { Equality.op = arg_kind_beq; Equality.mixin_of__1 = arg_kind_eq_axiom }

(** val arg_kind_eqType : Equality.coq_type **)

let arg_kind_eqType =
  Obj.magic arg_kind_eqMixin

type arg_kinds = arg_kind list

type args_kinds = arg_kinds list

type i_args_kinds = args_kinds list

type ('reg, 'xreg, 'rflag, 'cond) pp_asm_op_ext =
| PP_error
| PP_name
| PP_iname of wsize
| PP_iname2 of char list * wsize * wsize
| PP_viname of velem * bool
| PP_viname2 of velem * velem
| PP_ct of ('reg, 'xreg, 'rflag, 'cond) asm_arg

type ('reg, 'xreg, 'rflag, 'cond) pp_asm_op = { pp_aop_name : char list;
                                                pp_aop_ext : ('reg, 'xreg,
                                                             'rflag, 'cond)
                                                             pp_asm_op_ext;
                                                pp_aop_args : (wsize * ('reg,
                                                              'xreg, 'rflag,
                                                              'cond) asm_arg)
                                                              list }

type ('reg, 'xreg, 'rflag, 'cond) instr_desc_t = { id_msb_flag : msb_flag;
                                                   id_tin : stype list;
                                                   id_in : ('reg, 'xreg,
                                                           'rflag, 'cond)
                                                           arg_desc list;
                                                   id_tout : stype list;
                                                   id_out : ('reg, 'xreg,
                                                            'rflag, 'cond)
                                                            arg_desc list;
                                                   id_semi : sem_tuple exec
                                                             sem_prod;
                                                   id_args_kinds : i_args_kinds;
                                                   id_nargs : nat;
                                                   id_str_jas : (unit ->
                                                                char list);
                                                   id_safe : safe_cond list;
                                                   id_wsize : wsize;
                                                   id_pp_asm : (('reg, 'xreg,
                                                               'rflag, 'cond)
                                                               asm_args ->
                                                               ('reg, 'xreg,
                                                               'rflag, 'cond)
                                                               pp_asm_op) }

type 'asm_op prim_constructor =
| PrimP of wsize * (wsize -> 'asm_op)
| PrimM of 'asm_op
| PrimV of (velem -> wsize -> 'asm_op)
| PrimSV of (signedness -> velem -> wsize -> 'asm_op)
| PrimX of (wsize -> wsize -> 'asm_op)
| PrimVV of (velem -> wsize -> velem -> wsize -> 'asm_op)

type ('reg, 'xreg, 'rflag, 'cond, 'asm_op) asm_op_decl = { _eqT : 'asm_op
                                                                  eqTypeC;
                                                           instr_desc_op : 
                                                           ('asm_op -> ('reg,
                                                           'xreg, 'rflag,
                                                           'cond)
                                                           instr_desc_t);
                                                           prim_string : 
                                                           (char list * 'asm_op
                                                           prim_constructor)
                                                           list }

type ('reg, 'xreg, 'rflag, 'cond, 'asm_op) asm_op_t' = 'asm_op

type ('reg, 'xreg, 'rflag, 'cond, 'asm_op) asm_op_msb_t =
  wsize option * 'asm_op

(** val extend_size : wsize -> stype -> stype **)

let extend_size ws t = match t with
| Coq_sword ws' ->
  if cmp_le wsize_cmp ws' ws then Coq_sword ws else Coq_sword ws'
| _ -> t

(** val wextend_size : wsize -> stype -> sem_ot -> sem_ot **)

let wextend_size ws t x =
  match t with
  | Coq_sword ws' ->
    if cmp_le wsize_cmp ws' ws then zero_extend ws ws' x else x
  | _ -> x

(** val extend_tuple : wsize -> stype list -> sem_tuple -> sem_tuple **)

let rec extend_tuple ws id_tout0 t =
  match id_tout0 with
  | [] -> Obj.magic ()
  | t0 :: ts ->
    let rec_ = extend_tuple ws ts in
    (match ts with
     | [] -> wextend_size ws t0 t
     | _ :: _ ->
       Obj.magic ((wextend_size ws t0 (fst (Obj.magic t))),
         (rec_ (snd (Obj.magic t)))))

(** val apply_lprod : ('a1 -> 'a2) -> __ list -> 'a1 lprod -> 'a2 lprod **)

let rec apply_lprod f ts a =
  match ts with
  | [] -> Obj.magic f a
  | _ :: ts' -> Obj.magic (fun x -> apply_lprod f ts' (Obj.magic a x))

(** val is_not_CAmem : arg_kind -> bool **)

let is_not_CAmem = function
| CAmem _ -> false
| _ -> true

(** val exclude_mem_args_kinds :
    ('a1, 'a2, 'a3, 'a4) arch_decl -> ('a1, 'a2, 'a3, 'a4) arg_desc ->
    args_kinds -> args_kinds **)

let exclude_mem_args_kinds _ d cond =
  match d with
  | ADImplicit _ -> cond
  | ADExplicit (_, i, _) ->
    mapi (fun k c ->
      if eq_op nat_eqType (Obj.magic k) (Obj.magic i)
      then filter is_not_CAmem c
      else c) cond

(** val exclude_mem_i_args_kinds :
    ('a1, 'a2, 'a3, 'a4) arch_decl -> ('a1, 'a2, 'a3, 'a4) arg_desc ->
    i_args_kinds -> i_args_kinds **)

let exclude_mem_i_args_kinds arch d cond =
  map (exclude_mem_args_kinds arch d) cond

(** val exclude_mem_aux :
    ('a1, 'a2, 'a3, 'a4) arch_decl -> i_args_kinds -> ('a1, 'a2, 'a3, 'a4)
    arg_desc list -> i_args_kinds **)

let exclude_mem_aux arch cond d =
  foldl (fun cond0 d0 -> exclude_mem_i_args_kinds arch d0 cond0) cond d

(** val exclude_mem :
    ('a1, 'a2, 'a3, 'a4) arch_decl -> i_args_kinds -> ('a1, 'a2, 'a3, 'a4)
    arg_desc list -> i_args_kinds **)

let exclude_mem arch cond d =
  filter (fun c ->
    negb
      (in_mem (Obj.magic [])
        (mem (seq_predType (seq_eqType arg_kind_eqType)) (Obj.magic c))))
    (exclude_mem_aux arch cond d)

(** val instr_desc :
    ('a1, 'a2, 'a3, 'a4) arch_decl -> ('a1, 'a2, 'a3, 'a4, 'a5) asm_op_decl
    -> ('a1, 'a2, 'a3, 'a4, 'a5) asm_op_msb_t -> ('a1, 'a2, 'a3, 'a4)
    instr_desc_t **)

let instr_desc arch asm_op_d = function
| (ws, o0) ->
  let d = asm_op_d.instr_desc_op o0 in
  (match ws with
   | Some ws0 ->
     if eq_op msb_flag_eqType (Obj.magic d.id_msb_flag) (Obj.magic MSB_CLEAR)
     then { id_msb_flag = d.id_msb_flag; id_tin = d.id_tin; id_in = d.id_in;
            id_tout = (map (extend_size ws0) d.id_tout); id_out = d.id_out;
            id_semi =
            (apply_lprod (Result.map (extend_tuple ws0 d.id_tout))
              (map (Obj.magic __) d.id_tin) d.id_semi); id_args_kinds =
            (exclude_mem arch d.id_args_kinds d.id_out); id_nargs =
            d.id_nargs; id_str_jas = d.id_str_jas; id_safe = d.id_safe;
            id_wsize = d.id_wsize; id_pp_asm = d.id_pp_asm }
     else d
   | None -> d)

type ('reg, 'xreg, 'rflag, 'cond, 'asm_op) asm_i =
| ALIGN
| LABEL of label
| STORELABEL of ('reg, 'xreg, 'rflag, 'cond) reg_t * label
| JMP of remote_label
| JMPI of ('reg, 'xreg, 'rflag, 'cond) asm_arg
| Jcc of label * ('reg, 'xreg, 'rflag, 'cond) cond_t
| JAL of ('reg, 'xreg, 'rflag, 'cond) reg_t * remote_label
| CALL of remote_label
| POPPC
| AsmOp of ('reg, 'xreg, 'rflag, 'cond, 'asm_op) asm_op_t'
   * ('reg, 'xreg, 'rflag, 'cond) asm_args

type ('reg, 'xreg, 'rflag, 'cond, 'asm_op) asm_code =
  ('reg, 'xreg, 'rflag, 'cond, 'asm_op) asm_i list

type ('reg, 'xreg, 'rflag, 'cond) asm_typed_reg =
| ARReg of ('reg, 'xreg, 'rflag, 'cond) reg_t
| AXReg of ('reg, 'xreg, 'rflag, 'cond) xreg_t
| ABReg of ('reg, 'xreg, 'rflag, 'cond) rflag_t

type ('reg, 'xreg, 'rflag, 'cond, 'asm_op) asm_fundef = { asm_fd_align : 
                                                          wsize;
                                                          asm_fd_arg : 
                                                          ('reg, 'xreg,
                                                          'rflag, 'cond)
                                                          asm_typed_reg list;
                                                          asm_fd_body : 
                                                          ('reg, 'xreg,
                                                          'rflag, 'cond,
                                                          'asm_op) asm_code;
                                                          asm_fd_res : 
                                                          ('reg, 'xreg,
                                                          'rflag, 'cond)
                                                          asm_typed_reg list;
                                                          asm_fd_export : 
                                                          bool;
                                                          asm_fd_total_stack : 
                                                          coq_Z }

type ('reg, 'xreg, 'rflag, 'cond, 'asm_op) asm_prog = { asm_globs : GRing.ComRing.sort
                                                                    list;
                                                        asm_funcs : (funname * ('reg,
                                                                    'xreg,
                                                                    'rflag,
                                                                    'cond,
                                                                    'asm_op)
                                                                    asm_fundef)
                                                                    list }

type ('reg, 'xreg, 'rflag, 'cond, 'asm_op) asm = { _arch_decl : ('reg, 'xreg,
                                                                'rflag,
                                                                'cond)
                                                                arch_decl;
                                                   _asm_op_decl : ('reg,
                                                                  'xreg,
                                                                  'rflag,
                                                                  'cond,
                                                                  'asm_op)
                                                                  asm_op_decl;
                                                   eval_cond : ((('reg,
                                                               'xreg, 'rflag,
                                                               'cond) rflag_t
                                                               -> bool exec)
                                                               -> ('reg,
                                                               'xreg, 'rflag,
                                                               'cond) cond_t
                                                               -> bool exec);
                                                   stack_pointer_register : 
                                                   ('reg, 'xreg, 'rflag,
                                                   'cond) reg_t }
