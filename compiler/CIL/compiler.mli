open BinNums
open Bool
open Datatypes
open Allocation
open Arch_decl
open Arch_extra
open Array_copy
open Array_expansion
open Array_init
open Compiler_util
open Constant_prop
open Dead_calls
open Dead_code
open Eqtype
open Expr
open Global
open Inline
open Linear
open Linearization
open Lowering
open MakeReferenceArguments
open Merge_varmaps
open Propagate_inline
open Remove_globals
open Seq
open Sopn
open Ssralg
open Ssrbool
open Stack_alloc
open Tunneling
open Type
open Unrolling
open Utils0
open Var0
open Wsize
open X86_decl
open X86_extra
open X86_gen
open X86_instr_decl
open X86_linearization
open X86_sem
open X86_stack_alloc

val mov_ofs :
  lval -> assgn_tag -> vptr_kind -> pexpr -> coq_Z -> (register,
  xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op instr_r
  option

val var_tmp : Var.var

val lparams :
  (register, xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op
  linearization_params

val unroll1 :
  ((register, xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op
  asm_op_t -> bool) -> (register, xmm_register, rflag, condt, x86_op,
  x86_extra_op) extended_op uprog -> (register, xmm_register, rflag, condt,
  x86_op, x86_extra_op) extended_op uprog cexec

val unroll :
  ((register, xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op
  asm_op_t -> bool) -> nat -> (register, xmm_register, rflag, condt, x86_op,
  x86_extra_op) extended_op uprog -> (pp_error_loc, (register, xmm_register,
  rflag, condt, x86_op, x86_extra_op) extended_op uprog) result

val unroll_loop :
  ((register, xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op
  asm_op_t -> bool) -> (register, xmm_register, rflag, condt, x86_op,
  x86_extra_op) extended_op prog -> (pp_error_loc, (register, xmm_register,
  rflag, condt, x86_op, x86_extra_op) extended_op uprog) result

type compiler_step =
| Typing
| ParamsExpansion
| ArrayCopy
| AddArrInit
| Inlining
| RemoveUnusedFunction
| Unrolling
| Splitting
| Renaming
| RemovePhiNodes
| DeadCode_Renaming
| RemoveArrInit
| RegArrayExpansion
| RemoveGlobal
| MakeRefArguments
| LowerInstruction
| PropagateInline
| StackAllocation
| RemoveReturn
| RegAllocation
| DeadCode_RegAllocation
| Linearization
| Tunneling
| Assembly

val compiler_step_list : compiler_step list

val compiler_step_beq : compiler_step -> compiler_step -> bool

val compiler_step_eq_dec : compiler_step -> compiler_step -> bool

val compiler_step_eq_axiom : compiler_step Equality.axiom

val compiler_step_eqMixin : compiler_step Equality.mixin_of

val compiler_step_eqType : Equality.coq_type

type stack_alloc_oracles = { ao_globals : GRing.ComRing.sort list;
                             ao_global_alloc : ((Var.var * wsize) * coq_Z)
                                               list;
                             ao_stack_alloc : (funname -> stk_alloc_oracle_t) }

val ao_globals : stack_alloc_oracles -> GRing.ComRing.sort list

val ao_global_alloc : stack_alloc_oracles -> ((Var.var * wsize) * coq_Z) list

val ao_stack_alloc : stack_alloc_oracles -> funname -> stk_alloc_oracle_t

type compiler_params = { rename_fd : (instr_info -> funname -> (register,
                                     xmm_register, rflag, condt, x86_op,
                                     x86_extra_op) extended_op _ufundef ->
                                     (register, xmm_register, rflag, condt,
                                     x86_op, x86_extra_op) extended_op
                                     _ufundef);
                         expand_fd : (funname -> (register, xmm_register,
                                     rflag, condt, x86_op, x86_extra_op)
                                     extended_op _ufundef -> expand_info);
                         split_live_ranges_fd : (funname -> (register,
                                                xmm_register, rflag, condt,
                                                x86_op, x86_extra_op)
                                                extended_op _ufundef ->
                                                (register, xmm_register,
                                                rflag, condt, x86_op,
                                                x86_extra_op) extended_op
                                                _ufundef);
                         renaming_fd : (funname -> (register, xmm_register,
                                       rflag, condt, x86_op, x86_extra_op)
                                       extended_op _ufundef -> (register,
                                       xmm_register, rflag, condt, x86_op,
                                       x86_extra_op) extended_op _ufundef);
                         remove_phi_nodes_fd : (funname -> (register,
                                               xmm_register, rflag, condt,
                                               x86_op, x86_extra_op)
                                               extended_op _ufundef ->
                                               (register, xmm_register,
                                               rflag, condt, x86_op,
                                               x86_extra_op) extended_op
                                               _ufundef);
                         lowering_vars : fresh_vars;
                         inline_var : (Var.var -> bool);
                         is_var_in_memory : (var_i -> bool);
                         stack_register_symbol : Equality.sort;
                         global_static_data_symbol : Equality.sort;
                         stackalloc : ((register, xmm_register, rflag, condt,
                                      x86_op, x86_extra_op) extended_op
                                      _uprog -> stack_alloc_oracles);
                         removereturn : ((register, xmm_register, rflag,
                                        condt, x86_op, x86_extra_op)
                                        extended_op _sprog -> funname -> bool
                                        list option);
                         regalloc : ((register, xmm_register, rflag, condt,
                                    x86_op, x86_extra_op) extended_op
                                    _sfun_decl list -> (register,
                                    xmm_register, rflag, condt, x86_op,
                                    x86_extra_op) extended_op _sfun_decl list);
                         extra_free_registers : (instr_info -> Var.var option);
                         print_uprog : (compiler_step -> (register,
                                       xmm_register, rflag, condt, x86_op,
                                       x86_extra_op) extended_op _uprog ->
                                       (register, xmm_register, rflag, condt,
                                       x86_op, x86_extra_op) extended_op
                                       _uprog);
                         print_sprog : (compiler_step -> (register,
                                       xmm_register, rflag, condt, x86_op,
                                       x86_extra_op) extended_op _sprog ->
                                       (register, xmm_register, rflag, condt,
                                       x86_op, x86_extra_op) extended_op
                                       _sprog);
                         print_linear : (compiler_step -> (register,
                                        xmm_register, rflag, condt, x86_op,
                                        x86_extra_op) extended_op lprog ->
                                        (register, xmm_register, rflag,
                                        condt, x86_op, x86_extra_op)
                                        extended_op lprog);
                         warning : (instr_info -> warning_msg -> instr_info);
                         lowering_opt : lowering_options;
                         is_glob : (Var.var -> bool);
                         fresh_id : (glob_decl list -> Var.var ->
                                    Equality.sort);
                         fresh_counter : Equality.sort;
                         is_reg_ptr : (Var.var -> bool);
                         is_ptr : (Var.var -> bool);
                         is_reg_array : (Var.var -> bool) }

val rename_fd :
  compiler_params -> instr_info -> funname -> (register, xmm_register, rflag,
  condt, x86_op, x86_extra_op) extended_op _ufundef -> (register,
  xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op _ufundef

val expand_fd :
  compiler_params -> funname -> (register, xmm_register, rflag, condt,
  x86_op, x86_extra_op) extended_op _ufundef -> expand_info

val split_live_ranges_fd :
  compiler_params -> funname -> (register, xmm_register, rflag, condt,
  x86_op, x86_extra_op) extended_op _ufundef -> (register, xmm_register,
  rflag, condt, x86_op, x86_extra_op) extended_op _ufundef

val renaming_fd :
  compiler_params -> funname -> (register, xmm_register, rflag, condt,
  x86_op, x86_extra_op) extended_op _ufundef -> (register, xmm_register,
  rflag, condt, x86_op, x86_extra_op) extended_op _ufundef

val remove_phi_nodes_fd :
  compiler_params -> funname -> (register, xmm_register, rflag, condt,
  x86_op, x86_extra_op) extended_op _ufundef -> (register, xmm_register,
  rflag, condt, x86_op, x86_extra_op) extended_op _ufundef

val lowering_vars : compiler_params -> fresh_vars

val inline_var : compiler_params -> Var.var -> bool

val is_var_in_memory : compiler_params -> var_i -> bool

val stack_register_symbol : compiler_params -> Equality.sort

val global_static_data_symbol : compiler_params -> Equality.sort

val stackalloc :
  compiler_params -> (register, xmm_register, rflag, condt, x86_op,
  x86_extra_op) extended_op _uprog -> stack_alloc_oracles

val removereturn :
  compiler_params -> (register, xmm_register, rflag, condt, x86_op,
  x86_extra_op) extended_op _sprog -> funname -> bool list option

val regalloc :
  compiler_params -> (register, xmm_register, rflag, condt, x86_op,
  x86_extra_op) extended_op _sfun_decl list -> (register, xmm_register,
  rflag, condt, x86_op, x86_extra_op) extended_op _sfun_decl list

val extra_free_registers : compiler_params -> instr_info -> Var.var option

val print_uprog :
  compiler_params -> compiler_step -> (register, xmm_register, rflag, condt,
  x86_op, x86_extra_op) extended_op _uprog -> (register, xmm_register, rflag,
  condt, x86_op, x86_extra_op) extended_op _uprog

val print_sprog :
  compiler_params -> compiler_step -> (register, xmm_register, rflag, condt,
  x86_op, x86_extra_op) extended_op _sprog -> (register, xmm_register, rflag,
  condt, x86_op, x86_extra_op) extended_op _sprog

val print_linear :
  compiler_params -> compiler_step -> (register, xmm_register, rflag, condt,
  x86_op, x86_extra_op) extended_op lprog -> (register, xmm_register, rflag,
  condt, x86_op, x86_extra_op) extended_op lprog

val warning : compiler_params -> instr_info -> warning_msg -> instr_info

val lowering_opt : compiler_params -> lowering_options

val is_glob : compiler_params -> Var.var -> bool

val fresh_id : compiler_params -> glob_decl list -> Var.var -> Equality.sort

val fresh_counter : compiler_params -> Equality.sort

val is_reg_ptr : compiler_params -> Var.var -> bool

val is_ptr : compiler_params -> Var.var -> bool

val is_reg_array : compiler_params -> Var.var -> bool

type architecture_params =
  (register, xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op
  asm_op_t -> bool
  (* singleton inductive, whose constructor was mk_aparams *)

val is_move_op :
  architecture_params -> (register, xmm_register, rflag, condt, x86_op,
  x86_extra_op) extended_op asm_op_t -> bool

val split_live_ranges_prog :
  compiler_params -> (register, xmm_register, rflag, condt, x86_op,
  x86_extra_op) extended_op _uprog -> (register, xmm_register, rflag, condt,
  x86_op, x86_extra_op) extended_op _uprog

val renaming_prog :
  compiler_params -> (register, xmm_register, rflag, condt, x86_op,
  x86_extra_op) extended_op _uprog -> (register, xmm_register, rflag, condt,
  x86_op, x86_extra_op) extended_op _uprog

val remove_phi_nodes_prog :
  compiler_params -> (register, xmm_register, rflag, condt, x86_op,
  x86_extra_op) extended_op _uprog -> (register, xmm_register, rflag, condt,
  x86_op, x86_extra_op) extended_op _uprog

val check_removereturn :
  funname list -> (funname -> bool list option) -> (pp_error_loc, unit) result

val allNone : 'a1 option list -> bool

val check_no_ptr :
  Equality.sort list -> (funname -> stk_alloc_oracle_t) -> unit cexec

val compiler_first_part :
  compiler_params -> architecture_params -> funname list -> (register,
  xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op prog ->
  (register, xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op
  uprog cexec

val compiler_third_part :
  compiler_params -> architecture_params -> funname list -> (register,
  xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op sprog ->
  (register, xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op
  sprog cexec

val compiler_front_end :
  compiler_params -> architecture_params -> funname list -> funname list ->
  (register, xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op
  prog -> (register, xmm_register, rflag, condt, x86_op, x86_extra_op)
  extended_op sprog cexec

val check_export :
  Equality.sort list -> (register, xmm_register, rflag, condt, x86_op,
  x86_extra_op) extended_op sprog -> unit cexec

val compiler_back_end :
  compiler_params -> Sv.t -> Equality.sort list -> (register, xmm_register,
  rflag, condt, x86_op, x86_extra_op) extended_op sprog -> (pp_error_loc,
  (register, xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op
  lprog) result

val compiler_back_end_to_x86 :
  compiler_params -> funname list -> (register, xmm_register, rflag, condt,
  x86_op, x86_extra_op) extended_op sprog -> (pp_error_loc, (register,
  xmm_register, rflag, condt, x86_op) asm_prog) result

val compile_prog_to_x86 :
  compiler_params -> architecture_params -> funname list -> funname list ->
  (register, xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op
  prog -> x86_prog cexec
