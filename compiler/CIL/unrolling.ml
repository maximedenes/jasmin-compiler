open List0
open Compiler_util
open Eqtype
open Expr
open Seq
open Sopn
open Var0

(** val unroll_cmd :
    'a1 asmOp -> ('a1 instr -> 'a1 instr list) -> 'a1 instr list -> 'a1 instr
    list **)

let unroll_cmd _ unroll_i0 c =
  fold_right (fun i c' -> cat (unroll_i0 i) c') [] c

(** val assgn : 'a1 asmOp -> instr_info -> var_i -> pexpr -> 'a1 instr **)

let assgn _ ii x e =
  MkI (ii, (Cassgn ((Lvar x), AT_inline, (Var.vtype x.v_var), e)))

(** val unroll_i : 'a1 asmOp -> 'a1 instr -> 'a1 instr list **)

let rec unroll_i asmop i = match i with
| MkI (ii, ir) ->
  (match ir with
   | Cif (b, c1, c2) ->
     (MkI (ii, (Cif (b, (unroll_cmd asmop (unroll_i asmop) c1),
       (unroll_cmd asmop (unroll_i asmop) c2))))) :: []
   | Cfor (i0, r, c) ->
     let (p, hi) = r in
     let (dir, low) = p in
     let c' = unroll_cmd asmop (unroll_i asmop) c in
     (match is_const low with
      | Some vlo ->
        (match is_const hi with
         | Some vhi ->
           let l = wrange dir vlo vhi in
           let cs = map (fun n -> (assgn asmop ii i0 (Pconst n)) :: c') l in
           flatten cs
         | None -> (MkI (ii, (Cfor (i0, ((dir, low), hi), c')))) :: [])
      | None -> (MkI (ii, (Cfor (i0, ((dir, low), hi), c')))) :: [])
   | Cwhile (a, c, e, c') ->
     (MkI (ii, (Cwhile (a, (unroll_cmd asmop (unroll_i asmop) c), e,
       (unroll_cmd asmop (unroll_i asmop) c'))))) :: []
   | _ -> i :: [])

(** val unroll_fun :
    'a1 asmOp -> Equality.coq_type -> progT -> 'a1 fundef -> ('a1,
    Equality.sort) _fundef **)

let unroll_fun asmop _ _ f =
  let { f_info = ii; f_tyin = si; f_params = p; f_body = c; f_tyout = so;
    f_res = r; f_extra = ev } = f
  in
  { f_info = ii; f_tyin = si; f_params = p; f_body =
  (unroll_cmd asmop (unroll_i asmop) c); f_tyout = so; f_res = r; f_extra =
  ev }

(** val unroll_prog :
    'a1 asmOp -> Equality.coq_type -> progT -> 'a1 prog -> 'a1 prog **)

let unroll_prog asmop t pT p =
  map_prog asmop t pT (unroll_fun asmop t pT) p
