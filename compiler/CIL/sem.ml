open BinInt
open BinNums
open Datatypes
open Div
open Eqtype
open Expr
open Global
open Low_memory
open Memory_model
open Sem_type
open Sopn
open SsrZ
open Ssralg
open Ssrbool
open Ssreflect
open Type
open Utils0
open Values
open Var0
open Warray_
open Word0
open Wsize
open Xseq

type __ = Obj.t
let __ = let rec f _ = Obj.repr f in Obj.repr f

(** val undef_addr : stype -> sem_t exec **)

let undef_addr = function
| Coq_sarr n -> Ok (Obj.magic WArray.empty n)
| _ -> undef_error

(** val vmap0 : sem_t exec Fv.t **)

let vmap0 =
  Fv.empty (fun x -> undef_addr (Var.vtype x))

(** val on_vu : ('a1 -> 'a2) -> 'a2 exec -> 'a1 exec -> 'a2 exec **)

let on_vu fv fu = function
| Ok v0 -> Ok (fv v0)
| Error e -> (match e with
              | ErrAddrUndef -> fu
              | _ -> Error e)

(** val on_vuP :
    ('a1 -> 'a2) -> 'a2 exec -> 'a1 exec -> 'a2 -> ('a1 -> __ -> __ -> 'a3)
    -> (__ -> __ -> 'a3) -> 'a3 **)

let on_vuP _ _ v _ x x0 =
  let _evar_0_ = fun a hfv _ -> hfv a __ __ in
  let _evar_0_0 = fun __top_assumption_ ->
    let _evar_0_0 = fun _ _ -> assert false (* absurd case *) in
    let _evar_0_1 = fun _ hfu -> hfu __ in
    let _evar_0_2 = fun _ _ -> assert false (* absurd case *) in
    let _evar_0_3 = fun _ _ -> assert false (* absurd case *) in
    let _evar_0_4 = fun _ _ -> assert false (* absurd case *) in
    (match __top_assumption_ with
     | ErrOob -> (fun hfv hfu _ -> _evar_0_0 hfv hfu)
     | ErrAddrUndef -> _evar_0_1
     | ErrAddrInvalid -> (fun hfv hfu _ -> _evar_0_2 hfv hfu)
     | ErrStack -> (fun hfv hfu _ -> _evar_0_3 hfv hfu)
     | ErrType -> (fun hfv hfu _ -> _evar_0_4 hfv hfu))
  in
  (match v with
   | Ok a -> _evar_0_ a x x0
   | Error e -> _evar_0_0 e x x0 __)

(** val get_var : sem_t exec Fv.t -> Var.var -> value exec **)

let get_var m x =
  on_vu (to_val (Var.vtype x)) undef_error (Fv.get m x)

(** val set_var :
    sem_t exec Fv.t -> Var.var -> value -> sem_t exec Fv.t exec **)

let set_var m x v =
  on_vu (fun v0 -> Fv.set m x (Ok v0))
    (if is_sbool (Obj.magic Var.vtype x)
     then Ok (Fv.set m x (undef_addr (Var.vtype x)))
     else type_error) (of_val (Var.vtype x) v)

(** val set_varP :
    sem_t exec Fv.t -> sem_t exec Fv.t -> Var.var -> value -> (sem_t -> __ ->
    __ -> 'a1) -> (__ -> __ -> __ -> 'a1) -> 'a1 **)

let set_varP m m' x v h1 h2 =
  on_vuP (fun v0 -> Fv.set m x (Ok v0))
    (if is_sbool (Obj.magic Var.vtype x)
     then Ok (Fv.set m x (undef_addr (Var.vtype x)))
     else type_error) (of_val (Var.vtype x) v) m' h1
    (let _evar_0_ = fun _ _ _ -> h2 __ __ __ in
     let _evar_0_0 = fun _ _ _ -> assert false (* absurd case *) in
     (match ifPn (is_sbool (Obj.magic Var.vtype x)) (Ok
              (Fv.set m x (undef_addr (Var.vtype x)))) type_error with
      | IfSpecTrue -> _evar_0_ __
      | IfSpecFalse -> _evar_0_0 __))

(** val zlsl : coq_Z -> coq_Z -> coq_Z **)

let zlsl x i =
  if Z.leb Z0 i
  then Z.mul x (Z.pow (Zpos (Coq_xO Coq_xH)) i)
  else Z.div x (Z.pow (Zpos (Coq_xO Coq_xH)) (Z.opp i))

(** val zasr : coq_Z -> coq_Z -> coq_Z **)

let zasr x i =
  zlsl x (Z.opp i)

(** val sem_shift :
    (wsize -> GRing.ComRing.sort -> coq_Z -> GRing.ComRing.sort) -> wsize ->
    GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort **)

let sem_shift shift s v i =
  let i0 = wunsigned U8 (wand U8 i (x86_shift_mask s)) in shift s v i0

(** val sem_shr :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort **)

let sem_shr s =
  sem_shift wshr s

(** val sem_sar :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort **)

let sem_sar s =
  sem_shift wsar s

(** val sem_shl :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort **)

let sem_shl s =
  sem_shift wshl s

(** val sem_vadd :
    velem -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort ->
    GRing.ComRing.sort **)

let sem_vadd ve ws =
  lift2_vec (wsize_of_velem ve)
    (GRing.add (GRing.ComRing.zmodType (word (wsize_of_velem ve)))) ws

(** val sem_vsub :
    velem -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort ->
    GRing.ComRing.sort **)

let sem_vsub ve ws =
  lift2_vec (wsize_of_velem ve) (fun x y ->
    GRing.add (GRing.ComRing.zmodType (word (wsize_of_velem ve))) x
      (GRing.opp (GRing.ComRing.zmodType (word (wsize_of_velem ve))) y)) ws

(** val sem_vmul :
    velem -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort ->
    GRing.ComRing.sort **)

let sem_vmul ve ws =
  lift2_vec (wsize_of_velem ve)
    (GRing.mul (GRing.ComRing.ringType (word (wsize_of_velem ve)))) ws

(** val sem_vshr :
    velem -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort ->
    GRing.ComRing.sort **)

let sem_vshr ve ws v i =
  lift1_vec (wsize_of_velem ve) (fun x ->
    wshr (wsize_of_velem ve) x (wunsigned U8 i)) ws v

(** val sem_vsar :
    velem -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort ->
    GRing.ComRing.sort **)

let sem_vsar ve ws v i =
  lift1_vec (wsize_of_velem ve) (fun x ->
    wsar (wsize_of_velem ve) x (wunsigned U8 i)) ws v

(** val sem_vshl :
    velem -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort ->
    GRing.ComRing.sort **)

let sem_vshl ve ws v i =
  lift1_vec (wsize_of_velem ve) (fun x ->
    wshl (wsize_of_velem ve) x (wunsigned U8 i)) ws v

(** val sem_sop1_typed : sop1 -> sem_t -> sem_t **)

let sem_sop1_typed = function
| Oword_of_int sz -> Obj.magic wrepr sz
| Oint_of_word sz -> Obj.magic wunsigned sz
| Osignext (szo, szi) -> sign_extend szo szi
| Ozeroext (szo, szi) -> zero_extend szo szi
| Onot -> Obj.magic negb
| Olnot sz -> wnot sz
| Oneg o0 ->
  (match o0 with
   | Op_int -> Obj.magic Z.opp
   | Op_w sz -> GRing.opp (GRing.ComRing.zmodType (word sz)))

(** val sem_sop1 : sop1 -> value -> value exec **)

let sem_sop1 o v =
  match of_val (fst (type_of_op1 o)) v with
  | Ok x -> Ok (to_val (snd (type_of_op1 o)) (sem_sop1_typed o x))
  | Error s -> Error s

(** val signed : 'a1 -> 'a1 -> signedness -> 'a1 **)

let signed fu fs = function
| Signed -> fs
| Unsigned -> fu

(** val mk_sem_divmod :
    wsize -> (GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort)
    -> GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort exec **)

let mk_sem_divmod sz o w1 w2 =
  if (||)
       (eq_op (GRing.ComRing.eqType (word sz)) w2
         (GRing.zero (GRing.ComRing.zmodType (word sz))))
       ((&&)
         (eq_op coq_Z_eqType (Obj.magic wsigned sz w1)
           (Obj.magic wmin_signed sz))
         (eq_op (GRing.ComRing.eqType (word sz)) w2
           (GRing.opp
             (GRing.Ring.zmodType (GRing.ComRing.ringType (word sz)))
             (GRing.one (GRing.ComRing.ringType (word sz))))))
  then type_error
  else Ok (o w1 w2)

(** val mk_sem_sop2 : ('a1 -> 'a2 -> 'a3) -> 'a1 -> 'a2 -> 'a3 exec **)

let mk_sem_sop2 o v1 v2 =
  Ok (o v1 v2)

(** val sem_sop2_typed : sop2 -> sem_t -> sem_t -> sem_t exec **)

let sem_sop2_typed = function
| Obeq ->
  mk_sem_sop2
    (Obj.magic eq_op
      (Equality.clone bool_eqType (Obj.magic bool_eqMixin) (fun x -> x)))
| Oand -> mk_sem_sop2 (Obj.magic (&&))
| Oor -> mk_sem_sop2 (Obj.magic (||))
| Oadd o0 ->
  (match o0 with
   | Op_int -> mk_sem_sop2 (Obj.magic Z.add)
   | Op_w s -> mk_sem_sop2 (GRing.add (GRing.ComRing.zmodType (word s))))
| Omul o0 ->
  (match o0 with
   | Op_int -> mk_sem_sop2 (Obj.magic Z.mul)
   | Op_w s -> mk_sem_sop2 (GRing.mul (GRing.ComRing.ringType (word s))))
| Osub o0 ->
  (match o0 with
   | Op_int -> mk_sem_sop2 (Obj.magic Z.sub)
   | Op_w s ->
     mk_sem_sop2 (fun x y ->
       GRing.add (GRing.ComRing.zmodType (word s)) x
         (GRing.opp (GRing.ComRing.zmodType (word s)) y)))
| Odiv c ->
  (match c with
   | Cmp_int -> mk_sem_sop2 (Obj.magic Z.div)
   | Cmp_w (u, s) -> mk_sem_divmod s (signed (wdiv s) (wdivi s) u))
| Omod c ->
  (match c with
   | Cmp_int -> mk_sem_sop2 (Obj.magic Z.modulo)
   | Cmp_w (u, s) -> mk_sem_divmod s (signed (wmod s) (wmodi s) u))
| Oland s -> mk_sem_sop2 (wand s)
| Olor s -> mk_sem_sop2 (wor s)
| Olxor s -> mk_sem_sop2 (wxor s)
| Olsr s -> mk_sem_sop2 (sem_shr s)
| Olsl o0 ->
  (match o0 with
   | Op_int -> mk_sem_sop2 (Obj.magic zlsl)
   | Op_w s -> mk_sem_sop2 (sem_shl s))
| Oasr o0 ->
  (match o0 with
   | Op_int -> mk_sem_sop2 (Obj.magic zasr)
   | Op_w s -> mk_sem_sop2 (sem_sar s))
| Oeq o0 ->
  (match o0 with
   | Op_int -> mk_sem_sop2 (Obj.magic Z.eqb)
   | Op_w s -> mk_sem_sop2 (Obj.magic eq_op (GRing.ComRing.eqType (word s))))
| Oneq o0 ->
  (match o0 with
   | Op_int ->
     mk_sem_sop2 (fun x y ->
       Obj.magic negb (Z.eqb (Obj.magic x) (Obj.magic y)))
   | Op_w s ->
     mk_sem_sop2 (fun x y ->
       Obj.magic negb (eq_op (GRing.ComRing.eqType (word s)) x y)))
| Olt c ->
  (match c with
   | Cmp_int -> mk_sem_sop2 (Obj.magic Z.ltb)
   | Cmp_w (u, s) -> mk_sem_sop2 (Obj.magic wlt s u))
| Ole c ->
  (match c with
   | Cmp_int -> mk_sem_sop2 (Obj.magic Z.leb)
   | Cmp_w (u, s) -> mk_sem_sop2 (Obj.magic wle s u))
| Ogt c ->
  (match c with
   | Cmp_int -> mk_sem_sop2 (Obj.magic Z.gtb)
   | Cmp_w (u, s) -> mk_sem_sop2 (fun x y -> Obj.magic wlt s u y x))
| Oge c ->
  (match c with
   | Cmp_int -> mk_sem_sop2 (Obj.magic Z.geb)
   | Cmp_w (u, s) -> mk_sem_sop2 (fun x y -> Obj.magic wle s u y x))
| Ovadd (ve, ws) -> mk_sem_sop2 (sem_vadd ve ws)
| Ovsub (ve, ws) -> mk_sem_sop2 (sem_vsub ve ws)
| Ovmul (ve, ws) -> mk_sem_sop2 (sem_vmul ve ws)
| Ovlsr (ve, ws) -> mk_sem_sop2 (sem_vshr ve ws)
| Ovlsl (ve, ws) -> mk_sem_sop2 (sem_vshl ve ws)
| Ovasr (ve, ws) -> mk_sem_sop2 (sem_vsar ve ws)

(** val sem_sop2 : sop2 -> value -> value -> value exec **)

let sem_sop2 o v1 v2 =
  match of_val (fst (fst (type_of_op2 o))) v1 with
  | Ok x ->
    (match of_val (snd (fst (type_of_op2 o))) v2 with
     | Ok x0 ->
       (match sem_sop2_typed o x x0 with
        | Ok x1 -> Ok (to_val (snd (type_of_op2 o)) x1)
        | Error s -> Error s)
     | Error s -> Error s)
  | Error s -> Error s

(** val neg_f : bool -> bool -> bool **)

let neg_f n f =
  if n then negb f else f

(** val sem_cfc :
    bool -> combine_flags_core -> bool -> bool -> bool -> bool -> bool exec **)

let sem_cfc n o oF cF sF zF =
  let r =
    match o with
    | CFC_O -> oF
    | CFC_B -> cF
    | CFC_E -> zF
    | CFC_S -> sF
    | CFC_L -> negb (eq_op bool_eqType (Obj.magic oF) (Obj.magic sF))
    | CFC_BE -> (||) cF zF
    | CFC_LE ->
      (||) (negb (eq_op bool_eqType (Obj.magic oF) (Obj.magic sF))) zF
  in
  Ok (neg_f n r)

(** val sem_combine_flags : combine_flags -> bool exec sem_prod **)

let sem_combine_flags o =
  let c = cf_tbl o in Obj.magic sem_cfc (fst c) (snd c)

(** val sem_opN_typed : opN -> sem_t exec sem_prod **)

let sem_opN_typed = function
| Opack (sz, pe) ->
  curry Coq_sint (divn (nat_of_wsize sz) (nat_of_pelem pe)) (fun vs -> Ok
    (wpack sz (nat_of_pelem pe) (Obj.magic vs)))
| Ocombine_flags o0 -> sem_combine_flags o0

(** val sem_opN : opN -> values -> value exec **)

let sem_opN op vs =
  match app_sopn (fst (type_of_opN op)) (sem_opN_typed op) vs with
  | Ok x -> Ok (to_val (snd (type_of_opN op)) x)
  | Error s -> Error s

type estate = { emem : Memory.mem; evm : sem_t exec Fv.t }

(** val emem : coq_PointerData -> estate -> Memory.mem **)

let emem _ e =
  e.emem

(** val evm : coq_PointerData -> estate -> sem_t exec Fv.t **)

let evm _ e =
  e.evm

(** val get_global_value : glob_decl list -> Var.var -> glob_value option **)

let get_global_value gd g =
  assoc Var.var_eqType (Obj.magic gd) (Obj.magic g)

(** val gv2val : glob_value -> value **)

let gv2val = function
| Gword (ws, w) -> Vword (ws, w)
| Garr (p, a) -> Varr (p, a)

(** val get_global : glob_decl list -> Var.var -> value exec **)

let get_global gd g =
  match get_global_value gd g with
  | Some ga ->
    let v = gv2val ga in
    if eq_op stype_eqType (Obj.magic type_of_val v) (Obj.magic Var.vtype g)
    then Ok v
    else type_error
  | None -> type_error

(** val get_gvar : glob_decl list -> sem_t exec Fv.t -> gvar -> value exec **)

let get_gvar gd vm x =
  if is_lvar x then get_var vm x.gv.v_var else get_global gd x.gv.v_var

(** val on_arr_var :
    value exec -> (positive -> WArray.array -> 'a1 exec) -> (error, 'a1)
    result **)

let on_arr_var v f =
  match v with
  | Ok x -> (match x with
             | Varr (n, t0) -> f n t0
             | _ -> type_error)
  | Error s -> Error s

(** val on_arr_varP :
    coq_PointerData -> (positive -> WArray.array -> 'a1 exec) -> 'a1 ->
    estate -> Var.var -> (positive -> WArray.array -> __ -> __ -> __ -> 'a2)
    -> 'a2 **)

let on_arr_varP _ f v s x h =
  rbindP (get_var s.evm x) (fun v0 ->
    match v0 with
    | Varr (n, t0) -> f n t0
    | _ -> type_error) v (fun vx _ ->
    ssr_have __ (fun _ ->
      let _evar_0_ = fun _ -> assert false (* absurd case *) in
      let _evar_0_0 = fun _ -> assert false (* absurd case *) in
      let _evar_0_1 = fun len t0 -> h len t0 __ in
      let _evar_0_2 = fun _ _ -> assert false (* absurd case *) in
      let _evar_0_3 = fun _ -> assert false (* absurd case *) in
      (match vx with
       | Vbool b -> (fun _ -> _evar_0_ b)
       | Vint z -> (fun _ -> _evar_0_0 z)
       | Varr (len, a) -> _evar_0_1 len a __
       | Vword (s0, s1) -> (fun _ -> _evar_0_2 s0 s1)
       | Vundef t0 -> (fun _ -> _evar_0_3 t0))))

(** val on_arr_gvarP :
    (positive -> WArray.array -> 'a1 exec) -> 'a1 -> glob_decl list -> sem_t
    exec Fv.t -> gvar -> (positive -> WArray.array -> __ -> __ -> __ -> 'a2)
    -> 'a2 **)

let on_arr_gvarP f v gd s x h =
  rbindP (get_gvar gd s x) (fun v0 ->
    match v0 with
    | Varr (n, t0) -> f n t0
    | _ -> type_error) v (fun vx _ ->
    ssr_have __ (fun _ ->
      let _evar_0_ = fun _ -> assert false (* absurd case *) in
      let _evar_0_0 = fun _ -> assert false (* absurd case *) in
      let _evar_0_1 = fun len t0 -> h len t0 __ in
      let _evar_0_2 = fun _ _ -> assert false (* absurd case *) in
      let _evar_0_3 = fun _ -> assert false (* absurd case *) in
      (match vx with
       | Vbool b -> (fun _ -> _evar_0_ b)
       | Vint z -> (fun _ -> _evar_0_0 z)
       | Varr (len, a) -> _evar_0_1 len a __
       | Vword (s0, s1) -> (fun _ -> _evar_0_2 s0 s1)
       | Vundef t0 -> (fun _ -> _evar_0_3 t0))))

(** val is_defined : value -> bool **)

let is_defined = function
| Vundef _ -> false
| _ -> true

(** val sem_pexpr :
    coq_PointerData -> glob_decl list -> estate -> pexpr -> value exec **)

let rec sem_pexpr pd gd s = function
| Pconst z -> Ok (Vint z)
| Pbool b -> Ok (Vbool b)
| Parr_init n -> Ok (Varr (n, (WArray.empty n)))
| Pvar v -> get_gvar gd s.evm v
| Pget (aa, ws, x, e0) ->
  on_arr_var (get_gvar gd s.evm x) (fun n t0 ->
    match match sem_pexpr pd gd s e0 with
          | Ok x0 -> to_int x0
          | Error s0 -> Error s0 with
    | Ok x0 ->
      (match WArray.get n aa ws t0 x0 with
       | Ok x1 -> Ok (Vword (ws, x1))
       | Error s0 -> Error s0)
    | Error s0 -> Error s0)
| Psub (aa, ws, len, x, e0) ->
  on_arr_var (get_gvar gd s.evm x) (fun n t0 ->
    match match sem_pexpr pd gd s e0 with
          | Ok x0 -> to_int x0
          | Error s0 -> Error s0 with
    | Ok x0 ->
      (match WArray.get_sub n aa ws len t0 x0 with
       | Ok x1 -> Ok (Varr ((Z.to_pos (arr_size ws len)), x1))
       | Error s0 -> Error s0)
    | Error s0 -> Error s0)
| Pload (sz, x, e0) ->
  (match match get_var s.evm x.v_var with
         | Ok x0 -> to_word (coq_Uptr pd) x0
         | Error s0 -> Error s0 with
   | Ok x0 ->
     (match match sem_pexpr pd gd s e0 with
            | Ok x1 -> to_word (coq_Uptr pd) x1
            | Error s0 -> Error s0 with
      | Ok x1 ->
        (match CoreMem.read
                 (GRing.Zmodule.eqType
                   (GRing.ComRing.zmodType (word (coq_Uptr pd))))
                 (coq_Pointer pd) (Memory.coq_CM pd) s.emem
                 (GRing.add (GRing.ComRing.zmodType (word (coq_Uptr pd))) x0
                   x1) sz with
         | Ok x2 -> Ok (to_val (Coq_sword sz) x2)
         | Error s0 -> Error s0)
      | Error s0 -> Error s0)
   | Error s0 -> Error s0)
| Papp1 (o, e1) ->
  (match sem_pexpr pd gd s e1 with
   | Ok x -> sem_sop1 o x
   | Error s0 -> Error s0)
| Papp2 (o, e1, e2) ->
  (match sem_pexpr pd gd s e1 with
   | Ok x ->
     (match sem_pexpr pd gd s e2 with
      | Ok x0 -> sem_sop2 o x x0
      | Error s0 -> Error s0)
   | Error s0 -> Error s0)
| PappN (op, es) ->
  (match mapM (sem_pexpr pd gd s) es with
   | Ok x -> sem_opN op x
   | Error s0 -> Error s0)
| Pif (t0, e0, e1, e2) ->
  (match match sem_pexpr pd gd s e0 with
         | Ok x -> to_bool x
         | Error s0 -> Error s0 with
   | Ok x ->
     (match match sem_pexpr pd gd s e1 with
            | Ok x0 -> truncate_val t0 x0
            | Error s0 -> Error s0 with
      | Ok x0 ->
        (match match sem_pexpr pd gd s e2 with
               | Ok x1 -> truncate_val t0 x1
               | Error s0 -> Error s0 with
         | Ok x1 -> Ok (if x then x0 else x1)
         | Error s0 -> Error s0)
      | Error s0 -> Error s0)
   | Error s0 -> Error s0)

(** val sem_pexprs :
    coq_PointerData -> glob_decl list -> estate -> pexpr list -> (error,
    value list) result **)

let sem_pexprs pd gd s =
  mapM (sem_pexpr pd gd s)

(** val write_var :
    coq_PointerData -> var_i -> value -> estate -> estate exec **)

let write_var _ x v s =
  match set_var s.evm x.v_var v with
  | Ok x0 -> Ok { emem = s.emem; evm = x0 }
  | Error s0 -> Error s0

(** val write_vars :
    coq_PointerData -> var_i list -> value list -> estate -> (error, estate)
    result **)

let write_vars pd xs vs s =
  fold2 ErrType (write_var pd) xs vs s

(** val write_none :
    coq_PointerData -> estate -> Equality.sort -> value -> estate exec **)

let write_none _ s ty v =
  on_vu (fun _ -> s) (if is_sbool ty then Ok s else type_error)
    (of_val (Obj.magic ty) v)

(** val write_lval :
    coq_PointerData -> glob_decl list -> lval -> value -> estate -> estate
    exec **)

let write_lval pd gd l v s =
  match l with
  | Lnone (_, ty) -> write_none pd s (Obj.magic ty) v
  | Lvar x -> write_var pd x v s
  | Lmem (sz, x, e) ->
    (match match get_var s.evm x.v_var with
           | Ok x0 -> to_word (coq_Uptr pd) x0
           | Error s0 -> Error s0 with
     | Ok x0 ->
       (match match sem_pexpr pd gd s e with
              | Ok x1 -> to_word (coq_Uptr pd) x1
              | Error s0 -> Error s0 with
        | Ok x1 ->
          let p =
            GRing.add (GRing.ComRing.zmodType (word (coq_Uptr pd))) x0 x1
          in
          (match to_word sz v with
           | Ok x2 ->
             (match CoreMem.write
                      (GRing.Zmodule.eqType
                        (GRing.ComRing.zmodType (word (coq_Uptr pd))))
                      (coq_Pointer pd) (Memory.coq_CM pd) s.emem p sz x2 with
              | Ok x3 -> Ok { emem = x3; evm = s.evm }
              | Error s0 -> Error s0)
           | Error s0 -> Error s0)
        | Error s0 -> Error s0)
     | Error s0 -> Error s0)
  | Laset (aa, ws, x, i) ->
    on_arr_var (get_var s.evm x.v_var) (fun n t0 ->
      match match sem_pexpr pd gd s i with
            | Ok x0 -> to_int x0
            | Error s0 -> Error s0 with
      | Ok x0 ->
        (match to_word ws v with
         | Ok x1 ->
           (match WArray.set n ws t0 aa x0 x1 with
            | Ok x2 -> write_var pd x (to_val (Coq_sarr n) (Obj.magic x2)) s
            | Error s0 -> Error s0)
         | Error s0 -> Error s0)
      | Error s0 -> Error s0)
  | Lasub (aa, ws, len, x, i) ->
    on_arr_var (get_var s.evm x.v_var) (fun n t0 ->
      match match sem_pexpr pd gd s i with
            | Ok x0 -> to_int x0
            | Error s0 -> Error s0 with
      | Ok x0 ->
        (match to_arr (Z.to_pos (arr_size ws len)) v with
         | Ok x1 ->
           (match WArray.set_sub n aa ws len t0 x0 x1 with
            | Ok x2 -> write_var pd x (to_val (Coq_sarr n) (Obj.magic x2)) s
            | Error s0 -> Error s0)
         | Error s0 -> Error s0)
      | Error s0 -> Error s0)

(** val write_lvals :
    coq_PointerData -> glob_decl list -> estate -> lval list -> value list ->
    (error, estate) result **)

let write_lvals pd gd s xs vs =
  fold2 ErrType (write_lval pd gd) xs vs s

(** val is_word : wsize -> value -> unit exec **)

let is_word _ = function
| Vword (_, _) -> Ok ()
| Vundef t0 -> (match t0 with
                | Coq_sword _ -> Ok ()
                | _ -> type_error)
| _ -> type_error

(** val exec_sopn : 'a1 asmOp -> 'a1 sopn -> values -> values exec **)

let exec_sopn asmop o vs =
  let semi = sopn_sem asmop o in
  (match app_sopn (get_instr_desc asmop o).tin semi vs with
   | Ok x -> Ok (list_ltuple (get_instr_desc asmop o).tout x)
   | Error s -> Error s)

(** val sem_range :
    'a1 asmOp -> coq_PointerData -> 'a1 uprog -> estate -> range -> (error,
    coq_Z list) result **)

let sem_range _ pd p s = function
| (p0, pe2) ->
  let (d, pe1) = p0 in
  (match match sem_pexpr pd p.p_globs s pe1 with
         | Ok x -> to_int x
         | Error s0 -> Error s0 with
   | Ok x ->
     (match match sem_pexpr pd p.p_globs s pe2 with
            | Ok x0 -> to_int x0
            | Error s0 -> Error s0 with
      | Ok x0 -> Ok (wrange d x x0)
      | Error s0 -> Error s0)
   | Error s0 -> Error s0)

(** val sem_sopn :
    'a1 asmOp -> coq_PointerData -> glob_decl list -> 'a1 sopn -> estate ->
    lval list -> pexpr list -> (error, estate) result **)

let sem_sopn asmop pd gd o m lvs args =
  match match sem_pexprs pd gd m args with
        | Ok x -> exec_sopn asmop o x
        | Error s -> Error s with
  | Ok x -> write_lvals pd gd m lvs x
  | Error s -> Error s
