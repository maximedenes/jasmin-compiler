open BinInt
open BinNums
open Datatypes
open Eqtype
open Sem_type
open Ssralg
open Type
open Utils0
open Warray_
open Word0
open Wsize

type value =
| Vbool of bool
| Vint of coq_Z
| Varr of positive * WArray.array
| Vword of wsize * GRing.ComRing.sort
| Vundef of stype

val undef_b : value

type values = value list

val to_bool : value -> (error, bool) result

val to_int : value -> (error, coq_Z) result

val truncate_word :
  wsize -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort exec

val to_word : wsize -> value -> GRing.ComRing.sort exec

val to_arr : positive -> value -> WArray.array exec

val type_of_val : value -> stype

val of_val : stype -> value -> sem_t exec

val to_val : stype -> sem_t -> value

val truncate_val : stype -> value -> value exec

val oto_val : stype -> sem_ot -> value

val subtype : stype -> stype -> bool

val list_ltuple : stype list -> sem_tuple -> values

val app_sopn : stype list -> 'a1 exec sem_prod -> values -> 'a1 exec
