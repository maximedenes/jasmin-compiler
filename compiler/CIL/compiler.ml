open BinNums
open Bool
open Datatypes
open Allocation
open Arch_decl
open Arch_extra
open Array_copy
open Array_expansion
open Array_init
open Compiler_util
open Constant_prop
open Dead_calls
open Dead_code
open Eqtype
open Expr
open Global
open Inline
open Linear
open Linearization
open Lowering
open MakeReferenceArguments
open Merge_varmaps
open Propagate_inline
open Remove_globals
open Seq
open Sopn
open Ssralg
open Ssrbool
open Stack_alloc
open Tunneling
open Type
open Unrolling
open Utils0
open Var0
open Wsize
open X86_decl
open X86_extra
open X86_gen
open X86_instr_decl
open X86_linearization
open X86_sem
open X86_stack_alloc

(** val mov_ofs :
    lval -> assgn_tag -> vptr_kind -> pexpr -> coq_Z -> (register,
    xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op instr_r
    option **)

let mov_ofs =
  x86_mov_ofs

(** val var_tmp : Var.var **)

let var_tmp =
  to_var (Coq_sword U64) x86_reg_toS RAX

(** val lparams :
    (register, xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op
    linearization_params **)

let lparams =
  x86_linearization_params

(** val unroll1 :
    ((register, xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op
    asm_op_t -> bool) -> (register, xmm_register, rflag, condt, x86_op,
    x86_extra_op) extended_op uprog -> (register, xmm_register, rflag, condt,
    x86_op, x86_extra_op) extended_op uprog cexec **)

let unroll1 is_move_op0 p =
  let p0 =
    unroll_prog (asm_opI x86_extra)
      (Equality.clone unit_eqType (Obj.magic unit_eqMixin) (fun x -> x))
      progUnit p
  in
  let p1 =
    const_prop_prog (asm_opI x86_extra)
      (Equality.clone unit_eqType (Obj.magic unit_eqMixin) (fun x -> x))
      progUnit p0
  in
  dead_code_prog (asm_opI x86_extra) is_move_op0
    (Equality.clone unit_eqType (Obj.magic unit_eqMixin) (fun x -> x))
    progUnit p1 false

(** val unroll :
    ((register, xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op
    asm_op_t -> bool) -> nat -> (register, xmm_register, rflag, condt,
    x86_op, x86_extra_op) extended_op uprog -> (pp_error_loc, (register,
    xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op uprog)
    result **)

let rec unroll is_move_op0 n p =
  match n with
  | O ->
    Error
      (loop_iterator
        ('u'::('n'::('r'::('o'::('l'::('l'::('i'::('n'::('g'::[]))))))))))
  | S n0 ->
    (match unroll1 is_move_op0 p with
     | Ok x ->
       if eq_op
            (seq_eqType
              (prod_eqType pos_eqType
                (fundef_eqType (asm_opI x86_extra)
                  (Equality.clone unit_eqType (Obj.magic unit_eqMixin)
                    (fun x0 -> x0)) progUnit))) (Obj.magic p.p_funcs)
            (Obj.magic x.p_funcs)
       then Ok p
       else unroll is_move_op0 n0 x
     | Error s -> Error s)

(** val unroll_loop :
    ((register, xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op
    asm_op_t -> bool) -> (register, xmm_register, rflag, condt, x86_op,
    x86_extra_op) extended_op prog -> (pp_error_loc, (register, xmm_register,
    rflag, condt, x86_op, x86_extra_op) extended_op uprog) result **)

let unroll_loop is_move_op0 p =
  unroll is_move_op0 Loop.nb p

type compiler_step =
| Typing
| ParamsExpansion
| ArrayCopy
| AddArrInit
| Inlining
| RemoveUnusedFunction
| Unrolling
| Splitting
| Renaming
| RemovePhiNodes
| DeadCode_Renaming
| RemoveArrInit
| RegArrayExpansion
| RemoveGlobal
| MakeRefArguments
| LowerInstruction
| PropagateInline
| StackAllocation
| RemoveReturn
| RegAllocation
| DeadCode_RegAllocation
| Linearization
| Tunneling
| Assembly

(** val compiler_step_list : compiler_step list **)

let compiler_step_list =
  Typing :: (ParamsExpansion :: (ArrayCopy :: (AddArrInit :: (Inlining :: (RemoveUnusedFunction :: (Unrolling :: (Splitting :: (Renaming :: (RemovePhiNodes :: (DeadCode_Renaming :: (RemoveArrInit :: (RegArrayExpansion :: (RemoveGlobal :: (MakeRefArguments :: (LowerInstruction :: (PropagateInline :: (StackAllocation :: (RemoveReturn :: (RegAllocation :: (DeadCode_RegAllocation :: (Linearization :: (Tunneling :: (Assembly :: [])))))))))))))))))))))))

(** val compiler_step_beq : compiler_step -> compiler_step -> bool **)

let compiler_step_beq x y =
  match x with
  | Typing -> (match y with
               | Typing -> true
               | _ -> false)
  | ParamsExpansion -> (match y with
                        | ParamsExpansion -> true
                        | _ -> false)
  | ArrayCopy -> (match y with
                  | ArrayCopy -> true
                  | _ -> false)
  | AddArrInit -> (match y with
                   | AddArrInit -> true
                   | _ -> false)
  | Inlining -> (match y with
                 | Inlining -> true
                 | _ -> false)
  | RemoveUnusedFunction ->
    (match y with
     | RemoveUnusedFunction -> true
     | _ -> false)
  | Unrolling -> (match y with
                  | Unrolling -> true
                  | _ -> false)
  | Splitting -> (match y with
                  | Splitting -> true
                  | _ -> false)
  | Renaming -> (match y with
                 | Renaming -> true
                 | _ -> false)
  | RemovePhiNodes -> (match y with
                       | RemovePhiNodes -> true
                       | _ -> false)
  | DeadCode_Renaming -> (match y with
                          | DeadCode_Renaming -> true
                          | _ -> false)
  | RemoveArrInit -> (match y with
                      | RemoveArrInit -> true
                      | _ -> false)
  | RegArrayExpansion -> (match y with
                          | RegArrayExpansion -> true
                          | _ -> false)
  | RemoveGlobal -> (match y with
                     | RemoveGlobal -> true
                     | _ -> false)
  | MakeRefArguments -> (match y with
                         | MakeRefArguments -> true
                         | _ -> false)
  | LowerInstruction -> (match y with
                         | LowerInstruction -> true
                         | _ -> false)
  | PropagateInline -> (match y with
                        | PropagateInline -> true
                        | _ -> false)
  | StackAllocation -> (match y with
                        | StackAllocation -> true
                        | _ -> false)
  | RemoveReturn -> (match y with
                     | RemoveReturn -> true
                     | _ -> false)
  | RegAllocation -> (match y with
                      | RegAllocation -> true
                      | _ -> false)
  | DeadCode_RegAllocation ->
    (match y with
     | DeadCode_RegAllocation -> true
     | _ -> false)
  | Linearization -> (match y with
                      | Linearization -> true
                      | _ -> false)
  | Tunneling -> (match y with
                  | Tunneling -> true
                  | _ -> false)
  | Assembly -> (match y with
                 | Assembly -> true
                 | _ -> false)

(** val compiler_step_eq_dec : compiler_step -> compiler_step -> bool **)

let compiler_step_eq_dec x y =
  let b = compiler_step_beq x y in if b then true else false

(** val compiler_step_eq_axiom : compiler_step Equality.axiom **)

let compiler_step_eq_axiom x y =
  iffP (compiler_step_beq x y)
    (if compiler_step_beq x y then ReflectT else ReflectF)

(** val compiler_step_eqMixin : compiler_step Equality.mixin_of **)

let compiler_step_eqMixin =
  { Equality.op = compiler_step_beq; Equality.mixin_of__1 =
    compiler_step_eq_axiom }

(** val compiler_step_eqType : Equality.coq_type **)

let compiler_step_eqType =
  Obj.magic compiler_step_eqMixin

type stack_alloc_oracles = { ao_globals : GRing.ComRing.sort list;
                             ao_global_alloc : ((Var.var * wsize) * coq_Z)
                                               list;
                             ao_stack_alloc : (funname -> stk_alloc_oracle_t) }

(** val ao_globals : stack_alloc_oracles -> GRing.ComRing.sort list **)

let ao_globals s =
  s.ao_globals

(** val ao_global_alloc :
    stack_alloc_oracles -> ((Var.var * wsize) * coq_Z) list **)

let ao_global_alloc s =
  s.ao_global_alloc

(** val ao_stack_alloc :
    stack_alloc_oracles -> funname -> stk_alloc_oracle_t **)

let ao_stack_alloc s =
  s.ao_stack_alloc

type compiler_params = { rename_fd : (instr_info -> funname -> (register,
                                     xmm_register, rflag, condt, x86_op,
                                     x86_extra_op) extended_op _ufundef ->
                                     (register, xmm_register, rflag, condt,
                                     x86_op, x86_extra_op) extended_op
                                     _ufundef);
                         expand_fd : (funname -> (register, xmm_register,
                                     rflag, condt, x86_op, x86_extra_op)
                                     extended_op _ufundef -> expand_info);
                         split_live_ranges_fd : (funname -> (register,
                                                xmm_register, rflag, condt,
                                                x86_op, x86_extra_op)
                                                extended_op _ufundef ->
                                                (register, xmm_register,
                                                rflag, condt, x86_op,
                                                x86_extra_op) extended_op
                                                _ufundef);
                         renaming_fd : (funname -> (register, xmm_register,
                                       rflag, condt, x86_op, x86_extra_op)
                                       extended_op _ufundef -> (register,
                                       xmm_register, rflag, condt, x86_op,
                                       x86_extra_op) extended_op _ufundef);
                         remove_phi_nodes_fd : (funname -> (register,
                                               xmm_register, rflag, condt,
                                               x86_op, x86_extra_op)
                                               extended_op _ufundef ->
                                               (register, xmm_register,
                                               rflag, condt, x86_op,
                                               x86_extra_op) extended_op
                                               _ufundef);
                         lowering_vars : fresh_vars;
                         inline_var : (Var.var -> bool);
                         is_var_in_memory : (var_i -> bool);
                         stack_register_symbol : Equality.sort;
                         global_static_data_symbol : Equality.sort;
                         stackalloc : ((register, xmm_register, rflag, condt,
                                      x86_op, x86_extra_op) extended_op
                                      _uprog -> stack_alloc_oracles);
                         removereturn : ((register, xmm_register, rflag,
                                        condt, x86_op, x86_extra_op)
                                        extended_op _sprog -> funname -> bool
                                        list option);
                         regalloc : ((register, xmm_register, rflag, condt,
                                    x86_op, x86_extra_op) extended_op
                                    _sfun_decl list -> (register,
                                    xmm_register, rflag, condt, x86_op,
                                    x86_extra_op) extended_op _sfun_decl list);
                         extra_free_registers : (instr_info -> Var.var option);
                         print_uprog : (compiler_step -> (register,
                                       xmm_register, rflag, condt, x86_op,
                                       x86_extra_op) extended_op _uprog ->
                                       (register, xmm_register, rflag, condt,
                                       x86_op, x86_extra_op) extended_op
                                       _uprog);
                         print_sprog : (compiler_step -> (register,
                                       xmm_register, rflag, condt, x86_op,
                                       x86_extra_op) extended_op _sprog ->
                                       (register, xmm_register, rflag, condt,
                                       x86_op, x86_extra_op) extended_op
                                       _sprog);
                         print_linear : (compiler_step -> (register,
                                        xmm_register, rflag, condt, x86_op,
                                        x86_extra_op) extended_op lprog ->
                                        (register, xmm_register, rflag,
                                        condt, x86_op, x86_extra_op)
                                        extended_op lprog);
                         warning : (instr_info -> warning_msg -> instr_info);
                         lowering_opt : lowering_options;
                         is_glob : (Var.var -> bool);
                         fresh_id : (glob_decl list -> Var.var ->
                                    Equality.sort);
                         fresh_counter : Equality.sort;
                         is_reg_ptr : (Var.var -> bool);
                         is_ptr : (Var.var -> bool);
                         is_reg_array : (Var.var -> bool) }

(** val rename_fd :
    compiler_params -> instr_info -> funname -> (register, xmm_register,
    rflag, condt, x86_op, x86_extra_op) extended_op _ufundef -> (register,
    xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op _ufundef **)

let rename_fd c =
  c.rename_fd

(** val expand_fd :
    compiler_params -> funname -> (register, xmm_register, rflag, condt,
    x86_op, x86_extra_op) extended_op _ufundef -> expand_info **)

let expand_fd c =
  c.expand_fd

(** val split_live_ranges_fd :
    compiler_params -> funname -> (register, xmm_register, rflag, condt,
    x86_op, x86_extra_op) extended_op _ufundef -> (register, xmm_register,
    rflag, condt, x86_op, x86_extra_op) extended_op _ufundef **)

let split_live_ranges_fd c =
  c.split_live_ranges_fd

(** val renaming_fd :
    compiler_params -> funname -> (register, xmm_register, rflag, condt,
    x86_op, x86_extra_op) extended_op _ufundef -> (register, xmm_register,
    rflag, condt, x86_op, x86_extra_op) extended_op _ufundef **)

let renaming_fd c =
  c.renaming_fd

(** val remove_phi_nodes_fd :
    compiler_params -> funname -> (register, xmm_register, rflag, condt,
    x86_op, x86_extra_op) extended_op _ufundef -> (register, xmm_register,
    rflag, condt, x86_op, x86_extra_op) extended_op _ufundef **)

let remove_phi_nodes_fd c =
  c.remove_phi_nodes_fd

(** val lowering_vars : compiler_params -> fresh_vars **)

let lowering_vars c =
  c.lowering_vars

(** val inline_var : compiler_params -> Var.var -> bool **)

let inline_var c =
  c.inline_var

(** val is_var_in_memory : compiler_params -> var_i -> bool **)

let is_var_in_memory c =
  c.is_var_in_memory

(** val stack_register_symbol : compiler_params -> Equality.sort **)

let stack_register_symbol c =
  c.stack_register_symbol

(** val global_static_data_symbol : compiler_params -> Equality.sort **)

let global_static_data_symbol c =
  c.global_static_data_symbol

(** val stackalloc :
    compiler_params -> (register, xmm_register, rflag, condt, x86_op,
    x86_extra_op) extended_op _uprog -> stack_alloc_oracles **)

let stackalloc c =
  c.stackalloc

(** val removereturn :
    compiler_params -> (register, xmm_register, rflag, condt, x86_op,
    x86_extra_op) extended_op _sprog -> funname -> bool list option **)

let removereturn c =
  c.removereturn

(** val regalloc :
    compiler_params -> (register, xmm_register, rflag, condt, x86_op,
    x86_extra_op) extended_op _sfun_decl list -> (register, xmm_register,
    rflag, condt, x86_op, x86_extra_op) extended_op _sfun_decl list **)

let regalloc c =
  c.regalloc

(** val extra_free_registers :
    compiler_params -> instr_info -> Var.var option **)

let extra_free_registers c =
  c.extra_free_registers

(** val print_uprog :
    compiler_params -> compiler_step -> (register, xmm_register, rflag,
    condt, x86_op, x86_extra_op) extended_op _uprog -> (register,
    xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op _uprog **)

let print_uprog c =
  c.print_uprog

(** val print_sprog :
    compiler_params -> compiler_step -> (register, xmm_register, rflag,
    condt, x86_op, x86_extra_op) extended_op _sprog -> (register,
    xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op _sprog **)

let print_sprog c =
  c.print_sprog

(** val print_linear :
    compiler_params -> compiler_step -> (register, xmm_register, rflag,
    condt, x86_op, x86_extra_op) extended_op lprog -> (register,
    xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op lprog **)

let print_linear c =
  c.print_linear

(** val warning :
    compiler_params -> instr_info -> warning_msg -> instr_info **)

let warning c =
  c.warning

(** val lowering_opt : compiler_params -> lowering_options **)

let lowering_opt c =
  c.lowering_opt

(** val is_glob : compiler_params -> Var.var -> bool **)

let is_glob c =
  c.is_glob

(** val fresh_id :
    compiler_params -> glob_decl list -> Var.var -> Equality.sort **)

let fresh_id c =
  c.fresh_id

(** val fresh_counter : compiler_params -> Equality.sort **)

let fresh_counter c =
  c.fresh_counter

(** val is_reg_ptr : compiler_params -> Var.var -> bool **)

let is_reg_ptr c =
  c.is_reg_ptr

(** val is_ptr : compiler_params -> Var.var -> bool **)

let is_ptr c =
  c.is_ptr

(** val is_reg_array : compiler_params -> Var.var -> bool **)

let is_reg_array c =
  c.is_reg_array

type architecture_params =
  (register, xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op
  asm_op_t -> bool
  (* singleton inductive, whose constructor was mk_aparams *)

(** val is_move_op :
    architecture_params -> (register, xmm_register, rflag, condt, x86_op,
    x86_extra_op) extended_op asm_op_t -> bool **)

let is_move_op a =
  a

(** val split_live_ranges_prog :
    compiler_params -> (register, xmm_register, rflag, condt, x86_op,
    x86_extra_op) extended_op _uprog -> (register, xmm_register, rflag,
    condt, x86_op, x86_extra_op) extended_op _uprog **)

let split_live_ranges_prog cp p =
  Obj.magic map_prog_name (asm_opI x86_extra) unit_eqMixin progUnit
    cp.split_live_ranges_fd p

(** val renaming_prog :
    compiler_params -> (register, xmm_register, rflag, condt, x86_op,
    x86_extra_op) extended_op _uprog -> (register, xmm_register, rflag,
    condt, x86_op, x86_extra_op) extended_op _uprog **)

let renaming_prog cp p =
  Obj.magic map_prog_name (asm_opI x86_extra) unit_eqMixin progUnit
    cp.renaming_fd p

(** val remove_phi_nodes_prog :
    compiler_params -> (register, xmm_register, rflag, condt, x86_op,
    x86_extra_op) extended_op _uprog -> (register, xmm_register, rflag,
    condt, x86_op, x86_extra_op) extended_op _uprog **)

let remove_phi_nodes_prog cp p =
  Obj.magic map_prog_name (asm_opI x86_extra) unit_eqMixin progUnit
    cp.remove_phi_nodes_fd p

(** val check_removereturn :
    funname list -> (funname -> bool list option) -> (pp_error_loc, unit)
    result **)

let check_removereturn entries remove_return =
  if eq_op (seq_eqType (seq_eqType bool_eqType))
       (Obj.magic pmap remove_return entries) (Obj.magic [])
  then Ok ()
  else Error
         (pp_internal_error_s
           ('r'::('e'::('m'::('o'::('v'::('e'::(' '::('r'::('e'::('t'::('u'::('r'::('n'::[])))))))))))))
           ('S'::('i'::('g'::('n'::('a'::('t'::('u'::('r'::('e'::(' '::('o'::('f'::(' '::('s'::('o'::('m'::('e'::(' '::('e'::('x'::('p'::('o'::('r'::('t'::(' '::('f'::('u'::('n'::('c'::('t'::('i'::('o'::('n'::('s'::(' '::('a'::('r'::('e'::(' '::('m'::('o'::('d'::('i'::('f'::('i'::('e'::('d'::[]))))))))))))))))))))))))))))))))))))))))))))))))

(** val allNone : 'a1 option list -> bool **)

let allNone m =
  all (fun a -> match a with
                | Some _ -> false
                | None -> true) m

(** val check_no_ptr :
    Equality.sort list -> (funname -> stk_alloc_oracle_t) -> unit cexec **)

let check_no_ptr entries ao =
  allM pos_eqType (fun fn ->
    if allNone (Obj.magic ao fn).sao_params
    then if allNone (Obj.magic ao fn).sao_return
         then Ok ()
         else Error
                (pp_at_fn (Obj.magic fn)
                  (Stack_alloc.E.stk_error_no_var
                    ('e'::('x'::('p'::('o'::('r'::('t'::(' '::('f'::('u'::('n'::('c'::('t'::('i'::('o'::('n'::('s'::(' '::('d'::('o'::('n'::('\226'::('\128'::('\153'::('t'::(' '::('s'::('u'::('p'::('p'::('o'::('r'::('t'::(' '::('\226'::('\128'::('\156'::('p'::('t'::('r'::('\226'::('\128'::('\157'::(' '::('r'::('e'::('t'::('u'::('r'::('n'::(' '::('v'::('a'::('l'::('u'::('e'::('s'::[]))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
    else let s =
           pp_at_fn (Obj.magic fn)
             (Stack_alloc.E.stk_error_no_var
               ('e'::('x'::('p'::('o'::('r'::('t'::(' '::('f'::('u'::('n'::('c'::('t'::('i'::('o'::('n'::('s'::(' '::('d'::('o'::('n'::('\226'::('\128'::('\153'::('t'::(' '::('s'::('u'::('p'::('p'::('o'::('r'::('t'::(' '::('\226'::('\128'::('\156'::('p'::('t'::('r'::('\226'::('\128'::('\157'::(' '::('a'::('r'::('g'::('u'::('m'::('e'::('n'::('t'::('s'::[])))))))))))))))))))))))))))))))))))))))))))))))))))))
         in
         Error s) entries

(** val compiler_first_part :
    compiler_params -> architecture_params -> funname list -> (register,
    xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op prog ->
    (register, xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op
    uprog cexec **)

let compiler_first_part cparams aparams to_keep p =
  match array_copy_prog (asm_opI x86_extra) cparams.fresh_counter
          (Equality.clone unit_eqType (Obj.magic unit_eqMixin) (fun x -> x))
          progUnit p with
  | Ok x ->
    let p0 = cparams.print_uprog ArrayCopy (Obj.magic x) in
    let p1 =
      add_init_prog (asm_opI x86_extra) cparams.is_ptr
        (Equality.clone unit_eqType (Obj.magic unit_eqMixin) (fun x0 -> x0))
        progUnit (Obj.magic p0)
    in
    let p2 = cparams.print_uprog AddArrInit (Obj.magic p1) in
    (match inline_prog_err (arch_pd x86_decl) (asm_opI x86_extra)
             cparams.inline_var (Obj.magic cparams.rename_fd) (Obj.magic p2) with
     | Ok x0 ->
       let p3 = cparams.print_uprog Inlining (Obj.magic x0) in
       (match dead_calls_err_seq (asm_opI x86_extra)
                (Equality.clone unit_eqType (Obj.magic unit_eqMixin)
                  (fun x1 -> x1)) progUnit to_keep (Obj.magic p3) with
        | Ok x1 ->
          let p4 = cparams.print_uprog RemoveUnusedFunction (Obj.magic x1) in
          (match unroll (is_move_op aparams) Loop.nb (Obj.magic p4) with
           | Ok x2 ->
             let p5 = cparams.print_uprog Unrolling (Obj.magic x2) in
             let pv = split_live_ranges_prog cparams p5 in
             let pv0 = cparams.print_uprog Splitting pv in
             let pv1 = renaming_prog cparams pv0 in
             let pv2 = cparams.print_uprog Renaming pv1 in
             let pv3 = remove_phi_nodes_prog cparams pv2 in
             let pv4 = cparams.print_uprog RemovePhiNodes pv3 in
             (match CheckAllocRegU.check_prog (arch_pd x86_decl)
                      (asm_opI x86_extra) (Obj.magic p5).p_extra
                      (Obj.magic p5).p_funcs (Obj.magic pv4).p_extra
                      (Obj.magic pv4).p_funcs with
              | Ok _ ->
                (match dead_code_prog (asm_opI x86_extra)
                         (is_move_op aparams)
                         (Equality.clone unit_eqType (Obj.magic unit_eqMixin)
                           (fun x3 -> x3)) progUnit (Obj.magic pv4) false with
                 | Ok x3 ->
                   let pv5 =
                     cparams.print_uprog DeadCode_Renaming (Obj.magic x3)
                   in
                   let pr =
                     remove_init_prog (asm_opI x86_extra)
                       cparams.is_reg_array
                       (Equality.clone unit_eqType (Obj.magic unit_eqMixin)
                         (fun x4 -> x4)) progUnit (Obj.magic pv5)
                   in
                   let pr0 = cparams.print_uprog RemoveArrInit (Obj.magic pr)
                   in
                   (match expand_prog (asm_opI x86_extra)
                            (Obj.magic cparams.expand_fd) (Obj.magic pr0) with
                    | Ok x4 ->
                      let pe =
                        cparams.print_uprog RegArrayExpansion (Obj.magic x4)
                      in
                      (match remove_glob_prog (asm_opI x86_extra)
                               cparams.is_glob cparams.fresh_id (Obj.magic pe) with
                       | Ok x5 ->
                         let pg =
                           cparams.print_uprog RemoveGlobal (Obj.magic x5)
                         in
                         (match makereference_prog (asm_opI x86_extra)
                                  cparams.is_reg_ptr cparams.fresh_id
                                  (Equality.clone unit_eqType
                                    (Obj.magic unit_eqMixin) (fun x6 -> x6))
                                  progUnit (Obj.magic pg) with
                          | Ok x6 ->
                            let pa =
                              cparams.print_uprog MakeRefArguments
                                (Obj.magic x6)
                            in
                            if fvars_correct cparams.lowering_vars
                                 (Obj.magic unit_eqMixin) progUnit
                                 (Obj.magic pa).p_funcs
                            then let pl =
                                   lower_prog cparams.lowering_opt
                                     cparams.warning cparams.lowering_vars
                                     (Equality.clone unit_eqType
                                       (Obj.magic unit_eqMixin) (fun x7 ->
                                       x7)) progUnit cparams.is_var_in_memory
                                     (Obj.magic pa)
                                 in
                                 let pl0 =
                                   cparams.print_uprog LowerInstruction
                                     (Obj.magic pl)
                                 in
                                 (match pi_prog (asm_opI x86_extra)
                                          (Equality.clone unit_eqType
                                            (Obj.magic unit_eqMixin)
                                            (fun x7 -> x7)) progUnit
                                          (Obj.magic pl0) with
                                  | Ok x7 ->
                                    let pp =
                                      cparams.print_uprog PropagateInline
                                        (Obj.magic x7)
                                    in
                                    Ok (Obj.magic pp)
                                  | Error s -> Error s)
                            else let s =
                                   pp_internal_error_s
                                     ('l'::('o'::('w'::('e'::('r'::('i'::('n'::('g'::[]))))))))
                                     ('l'::('o'::('w'::('e'::('r'::('i'::('n'::('g'::(' '::('c'::('h'::('e'::('c'::('k'::(' '::('f'::('a'::('i'::('l'::('s'::[]))))))))))))))))))))
                                 in
                                 Error s
                          | Error s -> Error s)
                       | Error s -> Error s)
                    | Error s -> Error s)
                 | Error s -> Error s)
              | Error s -> Error s)
           | Error s -> Error s)
        | Error s -> Error s)
     | Error s -> Error s)
  | Error s -> Error s

(** val compiler_third_part :
    compiler_params -> architecture_params -> funname list -> (register,
    xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op sprog ->
    (register, xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op
    sprog cexec **)

let compiler_third_part cparams aparams entries ps =
  let rminfo = cparams.removereturn (Obj.magic ps) in
  (match check_removereturn entries rminfo with
   | Ok _ ->
     (match dead_code_prog_tokeep (asm_opI x86_extra) (is_move_op aparams)
              false rminfo
              (Equality.clone sfe_eqType (Obj.magic sfe_eqMixin) (fun x -> x))
              (progStack (arch_pd x86_decl)) ps with
      | Ok x ->
        let pr = cparams.print_sprog RemoveReturn (Obj.magic x) in
        let pa = { p_funcs = (cparams.regalloc pr.p_funcs); p_globs =
          pr.p_globs; p_extra = pr.p_extra }
        in
        let pa0 = cparams.print_sprog RegAllocation pa in
        (match CheckAllocRegS.check_prog (arch_pd x86_decl)
                 (asm_opI x86_extra) (Obj.magic pr).p_extra
                 (Obj.magic pr).p_funcs (Obj.magic pa0).p_extra
                 (Obj.magic pa0).p_funcs with
         | Ok _ ->
           (match dead_code_prog (asm_opI x86_extra) (is_move_op aparams)
                    (Equality.clone sfe_eqType (Obj.magic sfe_eqMixin)
                      (fun x0 -> x0)) (progStack (arch_pd x86_decl))
                    (Obj.magic pa0) true with
            | Ok x0 ->
              let pd =
                cparams.print_sprog DeadCode_RegAllocation (Obj.magic x0)
              in
              Ok (Obj.magic pd)
            | Error s -> Error s)
         | Error s -> Error s)
      | Error s -> Error s)
   | Error s -> Error s)

(** val compiler_front_end :
    compiler_params -> architecture_params -> funname list -> funname list ->
    (register, xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op
    prog -> (register, xmm_register, rflag, condt, x86_op, x86_extra_op)
    extended_op sprog cexec **)

let compiler_front_end cparams aparams entries subroutines p =
  match compiler_first_part cparams aparams (cat entries subroutines) p with
  | Ok x ->
    let ao = cparams.stackalloc (Obj.magic x) in
    (match check_no_ptr (Obj.magic entries) ao.ao_stack_alloc with
     | Ok _ ->
       (match alloc_prog (arch_pd x86_decl) (asm_opI x86_extra) true mov_ofs
                cparams.global_static_data_symbol
                cparams.stack_register_symbol ao.ao_globals
                ao.ao_global_alloc ao.ao_stack_alloc (Obj.magic x) with
        | Ok x0 ->
          let ps = cparams.print_sprog StackAllocation x0 in
          (match compiler_third_part cparams aparams entries (Obj.magic ps) with
           | Ok x1 -> Ok x1
           | Error s -> Error s)
        | Error s -> Error s)
     | Error s -> Error s)
  | Error s -> Error s

(** val check_export :
    Equality.sort list -> (register, xmm_register, rflag, condt, x86_op,
    x86_extra_op) extended_op sprog -> unit cexec **)

let check_export entries p =
  allM pos_eqType (fun fn ->
    match get_fundef p.p_funcs (Obj.magic fn) with
    | Some fd ->
      if eq_op return_address_location_eqType
           (Obj.magic (Obj.magic fd).f_extra.sf_return_address)
           (Obj.magic RAnone)
      then Ok ()
      else Error
             (pp_at_fn (Obj.magic fn)
               (Merge_varmaps.E.gen_error true None (PPEstring
                 ('e'::('x'::('p'::('o'::('r'::('t'::(' '::('f'::('u'::('n'::('c'::('t'::('i'::('o'::('n'::(' '::('e'::('x'::('p'::('e'::('c'::('t'::('s'::(' '::('a'::(' '::('r'::('e'::('t'::('u'::('r'::('n'::(' '::('a'::('d'::('d'::('r'::('e'::('s'::('s'::[])))))))))))))))))))))))))))))))))))))))))))
    | None ->
      Error
        (pp_at_fn (Obj.magic fn)
          (Merge_varmaps.E.gen_error true None (PPEstring
            ('u'::('n'::('k'::('n'::('o'::('w'::('n'::(' '::('e'::('x'::('p'::('o'::('r'::('t'::(' '::('f'::('u'::('n'::('c'::('t'::('i'::('o'::('n'::[])))))))))))))))))))))))))))
    entries

(** val compiler_back_end :
    compiler_params -> Sv.t -> Equality.sort list -> (register, xmm_register,
    rflag, condt, x86_op, x86_extra_op) extended_op sprog -> (pp_error_loc,
    (register, xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op
    lprog) result **)

let compiler_back_end cparams callee_saved entries pd =
  match check_export entries pd with
  | Ok _ ->
    (match check x86_extra pd cparams.extra_free_registers var_tmp
             callee_saved with
     | Ok _ ->
       (match linear_prog (arch_pd x86_decl) (asm_opI x86_extra) pd
                cparams.extra_free_registers lparams with
        | Ok x ->
          let pl = cparams.print_linear Linearization x in
          (match tunnel_program (asm_opI x86_extra) pl with
           | Ok x0 -> let pl0 = cparams.print_linear Tunneling x0 in Ok pl0
           | Error s -> Error s)
        | Error s -> Error s)
     | Error s -> Error s)
  | Error s -> Error s

(** val compiler_back_end_to_x86 :
    compiler_params -> funname list -> (register, xmm_register, rflag, condt,
    x86_op, x86_extra_op) extended_op sprog -> (pp_error_loc, (register,
    xmm_register, rflag, condt, x86_op) asm_prog) result **)

let compiler_back_end_to_x86 cparams entries p =
  let callee_saved =
    sv_of_list (to_var (Coq_sword U64) x86_reg_toS) x86_callee_saved
  in
  (match compiler_back_end cparams callee_saved (Obj.magic entries) p with
   | Ok x -> assemble_prog x
   | Error s -> Error s)

(** val compile_prog_to_x86 :
    compiler_params -> architecture_params -> funname list -> funname list ->
    (register, xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op
    prog -> x86_prog cexec **)

let compile_prog_to_x86 cparams aparams entries subroutines p =
  match compiler_front_end cparams aparams entries subroutines p with
  | Ok x -> compiler_back_end_to_x86 cparams entries x
  | Error s -> Error s
