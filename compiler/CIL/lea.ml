open Eqtype
open Expr
open Memory_model
open Ssralg
open Utils0
open Word0
open Wsize

type lea = { lea_disp : GRing.ComRing.sort; lea_base : var_i option;
             lea_scale : GRing.ComRing.sort; lea_offset : var_i option }

(** val lea_const : coq_PointerData -> GRing.ComRing.sort -> lea **)

let lea_const pd z =
  { lea_disp = z; lea_base = None; lea_scale =
    (GRing.one (GRing.ComRing.ringType (word (coq_Uptr pd)))); lea_offset =
    None }

(** val lea_var : coq_PointerData -> var_i -> lea **)

let lea_var pd x =
  { lea_disp = (GRing.zero (GRing.ComRing.zmodType (word (coq_Uptr pd))));
    lea_base = (Some x); lea_scale =
    (GRing.one (GRing.ComRing.ringType (word (coq_Uptr pd)))); lea_offset =
    None }

(** val mkLea :
    coq_PointerData -> GRing.ComRing.sort -> var_i option -> Equality.sort ->
    var_i option -> lea **)

let mkLea pd d b sc o =
  if eq_op
       (GRing.Zmodule.eqType (GRing.ComRing.zmodType (word (coq_Uptr pd))))
       sc (GRing.zero (GRing.ComRing.zmodType (word (coq_Uptr pd))))
  then { lea_disp = d; lea_base = b; lea_scale =
         (GRing.one (GRing.ComRing.ringType (word (coq_Uptr pd))));
         lea_offset = None }
  else { lea_disp = d; lea_base = b; lea_scale = sc; lea_offset = o }

(** val lea_mul : coq_PointerData -> lea -> lea -> lea option **)

let lea_mul pd l1 l2 =
  let { lea_disp = d1; lea_base = b1; lea_scale = sc1; lea_offset = o1 } = l1
  in
  let { lea_disp = d2; lea_base = b2; lea_scale = sc2; lea_offset = o2 } = l2
  in
  let d = GRing.mul (GRing.ComRing.ringType (word (coq_Uptr pd))) d1 d2 in
  (match b1 with
   | Some _ ->
     (match o1 with
      | Some _ -> None
      | None ->
        (match b2 with
         | Some _ -> None
         | None ->
           (match o2 with
            | Some _ -> None
            | None -> Some (mkLea pd d None d2 b1))))
   | None ->
     (match o1 with
      | Some _ ->
        (match b2 with
         | Some _ -> None
         | None ->
           (match o2 with
            | Some _ -> None
            | None ->
              Some
                (mkLea pd d None
                  (GRing.mul (GRing.ComRing.ringType (word (coq_Uptr pd))) d2
                    sc1) o1)))
      | None ->
        (match b2 with
         | Some _ ->
           (match o2 with
            | Some _ -> None
            | None -> Some (mkLea pd d None d1 b2))
         | None ->
           (match o2 with
            | Some _ ->
              Some
                (mkLea pd d None
                  (GRing.mul (GRing.ComRing.ringType (word (coq_Uptr pd))) d1
                    sc2) o2)
            | None -> Some (lea_const pd d)))))

(** val lea_add : coq_PointerData -> lea -> lea -> lea option **)

let lea_add pd l1 l2 =
  let { lea_disp = d1; lea_base = b1; lea_scale = sc1; lea_offset = o1 } = l1
  in
  let { lea_disp = d2; lea_base = b2; lea_scale = sc2; lea_offset = o2 } = l2
  in
  let disp = GRing.add (GRing.ComRing.zmodType (word (coq_Uptr pd))) d1 d2 in
  (match b1 with
   | Some _ ->
     (match o1 with
      | Some _ ->
        (match b2 with
         | Some _ -> None
         | None ->
           (match o2 with
            | Some _ -> None
            | None -> Some (mkLea pd disp b1 sc1 o1)))
      | None ->
        (match b2 with
         | Some _ ->
           (match o2 with
            | Some _ -> None
            | None ->
              Some
                (mkLea pd disp b1
                  (GRing.one (GRing.ComRing.ringType (word (coq_Uptr pd))))
                  b2))
         | None ->
           (match o2 with
            | Some _ -> Some (mkLea pd disp b1 sc2 o2)
            | None -> Some (mkLea pd disp b1 sc1 o1))))
   | None ->
     (match o1 with
      | Some _ ->
        (match b2 with
         | Some _ ->
           (match o2 with
            | Some _ -> None
            | None -> Some (mkLea pd disp b2 sc1 o1))
         | None ->
           (match o2 with
            | Some _ ->
              if eq_op (GRing.ComRing.eqType (word (coq_Uptr pd))) sc1
                   (GRing.one (GRing.ComRing.ringType (word (coq_Uptr pd))))
              then Some (mkLea pd disp o1 sc2 o2)
              else if eq_op (GRing.ComRing.eqType (word (coq_Uptr pd))) sc2
                        (GRing.one
                          (GRing.ComRing.ringType (word (coq_Uptr pd))))
                   then Some (mkLea pd disp o2 sc1 o1)
                   else None
            | None -> Some (mkLea pd disp b1 sc1 o1)))
      | None -> Some (mkLea pd disp b2 sc2 o2)))

(** val lea_sub : coq_PointerData -> lea -> lea -> lea option **)

let lea_sub pd l1 l2 =
  let { lea_disp = d1; lea_base = b1; lea_scale = sc1; lea_offset = o1 } = l1
  in
  let { lea_disp = d2; lea_base = b2; lea_scale = _; lea_offset = o2 } = l2 in
  let disp =
    GRing.add (GRing.ComRing.zmodType (word (coq_Uptr pd))) d1
      (GRing.opp (GRing.ComRing.zmodType (word (coq_Uptr pd))) d2)
  in
  (match b2 with
   | Some _ -> None
   | None ->
     (match o2 with
      | Some _ -> None
      | None -> Some (mkLea pd disp b1 sc1 o1)))

(** val mk_lea_rec : coq_PointerData -> wsize -> pexpr -> lea option **)

let rec mk_lea_rec pd sz = function
| Pvar x -> if is_lvar x then Some (lea_var pd x.gv) else None
| Papp1 (s, p) ->
  (match s with
   | Oword_of_int sz' ->
     (match p with
      | Pconst z ->
        Some (lea_const pd (sign_extend (coq_Uptr pd) sz' (wrepr sz' z)))
      | _ -> None)
   | _ -> None)
| Papp2 (s, e1, e2) ->
  (match s with
   | Oadd o ->
     (match o with
      | Op_int -> None
      | Op_w _ ->
        (match mk_lea_rec pd sz e1 with
         | Some l1 ->
           (match mk_lea_rec pd sz e2 with
            | Some l2 -> lea_add pd l1 l2
            | None -> None)
         | None -> None))
   | Omul o ->
     (match o with
      | Op_int -> None
      | Op_w _ ->
        (match mk_lea_rec pd sz e1 with
         | Some l1 ->
           (match mk_lea_rec pd sz e2 with
            | Some l2 -> lea_mul pd l1 l2
            | None -> None)
         | None -> None))
   | Osub o ->
     (match o with
      | Op_int -> None
      | Op_w _ ->
        (match mk_lea_rec pd sz e1 with
         | Some l1 ->
           (match mk_lea_rec pd sz e2 with
            | Some l2 -> lea_sub pd l1 l2
            | None -> None)
         | None -> None))
   | _ -> None)
| _ -> None

(** val push_cast_sz : wsize -> pexpr -> pexpr **)

let rec push_cast_sz sz e = match e with
| Papp1 (s, e1) ->
  (match s with
   | Oint_of_word sz' ->
     if cmp_le wsize_cmp sz sz' then e1 else Papp1 ((Oword_of_int sz), e)
   | _ -> Papp1 ((Oword_of_int sz), e))
| Papp2 (s, e1, e2) ->
  (match s with
   | Oadd o ->
     (match o with
      | Op_int ->
        Papp2 ((Oadd (Op_w sz)), (push_cast_sz sz e1), (push_cast_sz sz e2))
      | Op_w _ -> Papp1 ((Oword_of_int sz), e))
   | Omul o ->
     (match o with
      | Op_int ->
        Papp2 ((Omul (Op_w sz)), (push_cast_sz sz e1), (push_cast_sz sz e2))
      | Op_w _ -> Papp1 ((Oword_of_int sz), e))
   | Osub o ->
     (match o with
      | Op_int ->
        Papp2 ((Osub (Op_w sz)), (push_cast_sz sz e1), (push_cast_sz sz e2))
      | Op_w _ -> Papp1 ((Oword_of_int sz), e))
   | _ -> Papp1 ((Oword_of_int sz), e))
| _ -> Papp1 ((Oword_of_int sz), e)

(** val push_cast : pexpr -> pexpr **)

let rec push_cast e = match e with
| Papp1 (o, e1) ->
  (match o with
   | Oword_of_int sz -> push_cast_sz sz (push_cast e1)
   | _ -> Papp1 (o, (push_cast e1)))
| Papp2 (o, e1, e2) -> Papp2 (o, (push_cast e1), (push_cast e2))
| _ -> e

(** val mk_lea : coq_PointerData -> wsize -> pexpr -> lea option **)

let mk_lea pd sz e =
  mk_lea_rec pd sz (push_cast e)
