open Arch_decl
open Arch_extra
open Expr
open Memory_model
open Type
open Var0

val magic_variables :
  ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> ('a1, 'a2, 'a3, 'a4, 'a5, 'a6)
  extended_op sprog -> Sv.t

val savedstackreg : saved_stack -> Sv.t

val saved_stack_vm :
  ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> (('a1, 'a2, 'a3, 'a4, 'a5, 'a6)
  extended_op, stk_fun_extra) _fundef -> Sv.t

val sv_of_flags : ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> 'a3 list -> Sv.t

val ra_vm :
  ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> stk_fun_extra -> Var.var -> Sv.t

val ra_undef :
  ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> (('a1, 'a2, 'a3, 'a4, 'a5, 'a6)
  extended_op, stk_fun_extra) _fundef -> Var.var -> Sv.t
