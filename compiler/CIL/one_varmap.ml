open Arch_decl
open Arch_extra
open Expr
open Memory_model
open Type
open Var0

(** val magic_variables :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> ('a1, 'a2, 'a3, 'a4, 'a5,
    'a6) extended_op sprog -> Sv.t **)

let magic_variables asm_e p =
  let vgd = { Var.vtype = (Coq_sword
    (coq_Uptr (arch_pd asm_e._asm._arch_decl))); Var.vname =
    (Obj.magic p).p_extra.sp_rip }
  in
  let vrsp = { Var.vtype = (Coq_sword
    (coq_Uptr (arch_pd asm_e._asm._arch_decl))); Var.vname =
    (Obj.magic p).p_extra.sp_rsp }
  in
  Sv.add (Obj.magic vgd) (Sv.singleton (Obj.magic vrsp))

(** val savedstackreg : saved_stack -> Sv.t **)

let savedstackreg = function
| SavedStackReg r -> Sv.singleton (Obj.magic r)
| _ -> Sv.empty

(** val saved_stack_vm :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> (('a1, 'a2, 'a3, 'a4, 'a5,
    'a6) extended_op, stk_fun_extra) _fundef -> Sv.t **)

let saved_stack_vm _ fd =
  savedstackreg fd.f_extra.sf_save_stack

(** val sv_of_flags :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> 'a3 list -> Sv.t **)

let sv_of_flags asm_e =
  sv_of_list (to_var Coq_sbool asm_e._asm._arch_decl.toS_f)

(** val ra_vm :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> stk_fun_extra -> Var.var ->
    Sv.t **)

let ra_vm asm_e e x =
  match e.sf_return_address with
  | RAnone ->
    Sv.add (Obj.magic x) (sv_of_flags asm_e (rflags asm_e._asm._arch_decl))
  | RAreg ra -> Sv.singleton (Obj.magic ra)
  | RAstack _ -> Sv.empty

(** val ra_undef :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> (('a1, 'a2, 'a3, 'a4, 'a5,
    'a6) extended_op, stk_fun_extra) _fundef -> Var.var -> Sv.t **)

let ra_undef asm_e fd x =
  Sv.union (ra_vm asm_e fd.f_extra x) (saved_stack_vm asm_e fd)
