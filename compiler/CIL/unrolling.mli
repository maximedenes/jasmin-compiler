open List0
open Compiler_util
open Eqtype
open Expr
open Seq
open Sopn
open Var0

val unroll_cmd :
  'a1 asmOp -> ('a1 instr -> 'a1 instr list) -> 'a1 instr list -> 'a1 instr
  list

val assgn : 'a1 asmOp -> instr_info -> var_i -> pexpr -> 'a1 instr

val unroll_i : 'a1 asmOp -> 'a1 instr -> 'a1 instr list

val unroll_fun :
  'a1 asmOp -> Equality.coq_type -> progT -> 'a1 fundef -> ('a1,
  Equality.sort) _fundef

val unroll_prog :
  'a1 asmOp -> Equality.coq_type -> progT -> 'a1 prog -> 'a1 prog
