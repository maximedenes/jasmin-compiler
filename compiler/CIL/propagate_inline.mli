open Datatypes
open Compiler_util
open Constant_prop
open Eqtype
open Expr
open Seq
open Sopn
open Utils0
open Var0

val use_mem : pexpr -> bool

type pi_cel = { pi_def : pexpr; pi_fv : Sv.t; pi_m : bool }

type pimap = pi_cel Mvar.t

val piempty : pimap

val remove : pimap -> Var.var -> pi_cel Mvar.t

val remove_m : pimap -> pi_cel Mvar.t

val set : pimap -> Var.var -> pexpr -> pimap

val merge : pimap -> pimap -> pi_cel Mvar.t

val incl : pimap -> pimap -> bool

val sbneq : pexpr -> pexpr -> pexpr

val lower_cfc : combine_flags_core -> pexpr list -> pexpr option

val scfc : combine_flags -> pexpr list -> pexpr

val pi_e : pimap -> pexpr -> pexpr

val pi_es : pimap -> pexpr list -> pexpr list

val pi_lv : pimap -> lval -> pimap * lval

val map_fold : ('a1 -> 'a2 -> 'a1 * 'a3) -> 'a1 -> 'a2 list -> 'a1 * 'a3 list

val pi_lvs : pimap -> lval list -> pimap * lval list

val set_lv : pimap -> lval -> Equality.sort -> pexpr -> pimap

module E :
 sig
  val pass : char list

  val ii_loop_iterator : instr_info -> pp_error_loc
 end

val pi_c :
  'a1 asmOp -> (pimap -> 'a1 instr -> (pimap * 'a1 instr) cexec) -> pimap ->
  'a1 instr list -> (pp_error_loc, pimap * 'a1 instr list) result

val loop_for :
  'a1 asmOp -> (pimap -> 'a1 instr -> (pimap * 'a1 instr) cexec) ->
  instr_info -> Var.var -> 'a1 instr list -> nat -> pimap -> (pp_error_loc,
  pimap * 'a1 instr list) result

val loop_while :
  'a1 asmOp -> (pimap -> 'a1 instr -> (pimap * 'a1 instr) cexec) ->
  instr_info -> 'a1 instr list -> pexpr -> 'a1 instr list -> nat -> pimap ->
  (pp_error_loc, ((pimap * 'a1 instr list) * pexpr) * 'a1 instr list) result

val pi_i :
  'a1 asmOp -> pimap -> 'a1 instr -> (pp_error_loc, pimap * 'a1 instr) result

val pi_fun :
  'a1 asmOp -> Equality.coq_type -> progT -> 'a1 fundef -> (pp_error_loc,
  ('a1, Equality.sort) _fundef) result

val pi_prog :
  'a1 asmOp -> Equality.coq_type -> progT -> 'a1 prog -> (pp_error_loc, ('a1,
  Equality.sort, extra_prog_t) _prog) result
