open BinInt
open BinNums
open List0
open Arch_decl
open Arch_extra
open Expr
open Linearization
open Memory_model
open Seq
open Sopn
open Type
open Utils0
open Word0
open Wsize
open X86_decl
open X86_extra
open X86_instr_decl

val x86_allocate_stack_frame :
  var_i -> coq_Z -> (lval list * x86_extended_op sopn) * pexpr list

val x86_free_stack_frame :
  var_i -> coq_Z -> (lval list * x86_extended_op sopn) * pexpr list

val x86_ensure_rsp_alignment :
  var_i -> wsize -> (lval list * x86_extended_op sopn) * pexpr list

val x86_lassign :
  lval -> wsize -> pexpr -> (lval list * x86_extended_op sopn) * pexpr list

val x86_linearization_params :
  (register, xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op
  linearization_params
