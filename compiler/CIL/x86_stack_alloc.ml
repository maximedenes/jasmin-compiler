open BinNums
open Arch_decl
open Arch_extra
open Eqtype
open Expr
open Memory_model
open SsrZ
open Stack_alloc
open X86_decl
open X86_extra
open X86_instr_decl

(** val lea_ptr :
    lval -> assgn_tag -> pexpr -> coq_Z -> (register, xmm_register, rflag,
    condt, x86_op, x86_extra_op) extended_op instr_r **)

let lea_ptr x tag y ofs =
  Copn ((x :: []), tag, (coq_Ox86 (LEA (coq_Uptr (arch_pd x86_decl)))),
    ((add (arch_pd x86_decl) y (cast_const (arch_pd x86_decl) ofs)) :: []))

(** val mov_ptr : lval -> assgn_tag -> pexpr -> x86_extended_op instr_r **)

let mov_ptr x tag y =
  Copn ((x :: []), tag, (coq_Ox86 (MOV (coq_Uptr (arch_pd x86_decl)))),
    (y :: []))

type mov_kind =
| MK_LEA
| MK_MOV

(** val mk_mov : vptr_kind -> mov_kind **)

let mk_mov = function
| VKglob _ -> MK_LEA
| VKptr p ->
  (match p with
   | Pdirect (_, _, _, _, v0) ->
     (match v0 with
      | Slocal -> MK_MOV
      | Sglob -> MK_LEA)
   | _ -> MK_MOV)

(** val x86_mov_ofs :
    lval -> assgn_tag -> vptr_kind -> pexpr -> coq_Z -> (register,
    xmm_register, rflag, condt, x86_op, x86_extra_op) extended_op instr_r
    option **)

let x86_mov_ofs x tag vpk y ofs =
  let addr =
    match mk_mov vpk with
    | MK_LEA -> lea_ptr x tag y ofs
    | MK_MOV ->
      if eq_op coq_Z_eqType (Obj.magic ofs) (Obj.magic Z0)
      then mov_ptr x tag y
      else lea_ptr x tag y ofs
  in
  Some addr
