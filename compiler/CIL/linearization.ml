open BinInt
open BinNums
open BinPos
open Datatypes
open List0
open Compiler_util
open Constant_prop
open Eqtype
open Expr
open Label
open Linear
open Memory_model
open Seq
open Sopn
open SsrZ
open Ssralg
open Ssrint
open Ssrnat
open Type
open Utils0
open Var0
open Word0
open Wsize

module E =
 struct
  (** val pass_name : char list **)

  let pass_name =
    'l'::('i'::('n'::('e'::('a'::('r'::('i'::('z'::('a'::('t'::('i'::('o'::('n'::[]))))))))))))

  (** val gen_error :
      bool -> instr_info option -> char list -> pp_error_loc **)

  let gen_error internal ii msg =
    { pel_msg = (PPEstring msg); pel_fn = None; pel_fi = None; pel_ii = ii;
      pel_vi = None; pel_pass = (Some pass_name); pel_internal = internal }

  (** val ii_error : instr_info -> char list -> pp_error_loc **)

  let ii_error ii msg =
    gen_error false (Some ii) msg

  (** val error : char list -> pp_error_loc **)

  let error msg =
    gen_error false None msg

  (** val internal_error : char list -> pp_error_loc **)

  let internal_error msg =
    gen_error true None msg
 end

(** val stack_frame_allocation_size : stk_fun_extra -> coq_Z **)

let stack_frame_allocation_size e =
  round_ws e.sf_align (Z.add e.sf_stk_sz e.sf_stk_extra_sz)

(** val check_c :
    'a1 asmOp -> ('a1 instr -> unit cexec) -> 'a1 instr list -> unit cexec **)

let rec check_c asmop check_i0 = function
| [] -> Ok ()
| i :: c0 ->
  (match check_c asmop check_i0 c0 with
   | Ok _ -> check_i0 i
   | Error s -> Error s)

(** val check_i :
    coq_PointerData -> 'a1 asmOp -> 'a1 sprog -> (instr_info -> Var.var
    option) -> funname -> wsize -> 'a1 instr -> unit cexec **)

let rec check_i pd asmop p extra_free_registers this stack_align = function
| MkI (ii, ir) ->
  (match ir with
   | Cassgn (_, _, ty, _) ->
     (match ty with
      | Coq_sword _ -> Ok ()
      | _ ->
        Error
          (E.ii_error ii
            ('a'::('s'::('s'::('i'::('g'::('n'::(' '::('n'::('o'::('t'::(' '::('a'::(' '::('w'::('o'::('r'::('d'::[])))))))))))))))))))
   | Copn (_, _, _, _) -> Ok ()
   | Cif (_, c1, c2) ->
     (match check_c asmop
              (check_i pd asmop p extra_free_registers this stack_align) c1 with
      | Ok _ ->
        check_c asmop
          (check_i pd asmop p extra_free_registers this stack_align) c2
      | Error s -> Error s)
   | Cfor (_, _, _) ->
     Error
       (E.ii_error ii
         ('f'::('o'::('r'::(' '::('f'::('o'::('u'::('n'::('d'::(' '::('i'::('n'::(' '::('l'::('i'::('n'::('e'::('a'::('r'::[]))))))))))))))))))))
   | Cwhile (_, c, e, c') ->
     if eq_op pexpr_eqType (Obj.magic e) (Obj.magic (Pbool false))
     then check_c asmop
            (check_i pd asmop p extra_free_registers this stack_align) c
     else (match check_c asmop
                   (check_i pd asmop p extra_free_registers this stack_align)
                   c with
           | Ok _ ->
             check_c asmop
               (check_i pd asmop p extra_free_registers this stack_align) c'
           | Error s -> Error s)
   | Ccall (_, _, fn, _) ->
     if negb (eq_op pos_eqType (Obj.magic fn) (Obj.magic this))
     then (match get_fundef p.p_funcs fn with
           | Some fd ->
             let e = fd.f_extra in
             if match (Obj.magic e).sf_return_address with
                | RAnone -> false
                | RAreg _ -> true
                | RAstack _ ->
                  negb
                    (eq_op (option_eqType Var.var_eqType)
                      (Obj.magic extra_free_registers ii) (Obj.magic None))
             then if cmp_le wsize_cmp (Obj.magic e).sf_align stack_align
                  then Ok ()
                  else let s =
                         E.ii_error ii
                           ('c'::('a'::('l'::('l'::('e'::('r'::(' '::('n'::('e'::('e'::('d'::(' '::('a'::('l'::('i'::('g'::('n'::('m'::('e'::('n'::('t'::(' '::('g'::('r'::('e'::('a'::('t'::('e'::('r'::(' '::('t'::('h'::('a'::('n'::(' '::('c'::('a'::('l'::('l'::('e'::('e'::[])))))))))))))))))))))))))))))))))))))))))
                       in
                       Error s
             else let s =
                    E.ii_error ii
                      ('('::('o'::('n'::('e'::('_'::('v'::('a'::('r'::('m'::('a'::('p'::(')'::(' '::('n'::('o'::('w'::('h'::('e'::('r'::('e'::(' '::('t'::('o'::(' '::('s'::('t'::('o'::('r'::('e'::(' '::('t'::('h'::('e'::(' '::('r'::('e'::('t'::('u'::('r'::('n'::(' '::('a'::('d'::('d'::('r'::('e'::('s'::('s'::[]))))))))))))))))))))))))))))))))))))))))))))))))
                  in
                  Error s
           | None ->
             Error
               (E.ii_error ii
                 ('c'::('a'::('l'::('l'::(' '::('t'::('o'::(' '::('u'::('n'::('k'::('n'::('o'::('w'::('n'::(' '::('f'::('u'::('n'::('c'::('t'::('i'::('o'::('n'::[]))))))))))))))))))))))))))
     else let s =
            E.ii_error ii
              ('c'::('a'::('l'::('l'::(' '::('t'::('o'::(' '::('s'::('e'::('l'::('f'::[]))))))))))))
          in
          Error s)

(** val all_disjoint_aligned_between :
    coq_PointerData -> coq_Z -> coq_Z -> wsize -> 'a1 list -> ('a1 ->
    (coq_Z * wsize) cexec) -> unit cexec **)

let all_disjoint_aligned_between pd lo hi al m slot =
  match foldM (fun a base ->
          match slot a with
          | Ok x ->
            let (ofs, ws) = x in
            if Z.leb base ofs
            then if cmp_le wsize_cmp ws al
                 then if is_align (GRing.ComRing.eqType (word (coq_Uptr pd)))
                           (coq_Pointer pd) (wrepr (coq_Uptr pd) ofs) ws
                      then Ok (Z.add ofs (wsize_size ws))
                      else let s =
                             E.error
                               ('t'::('o'::('-'::('s'::('a'::('v'::('e'::(':'::(' '::('b'::('a'::('d'::(' '::('s'::('l'::('o'::('t'::(' '::('a'::('l'::('i'::('g'::('n'::('e'::('m'::('e'::('n'::('t'::[]))))))))))))))))))))))))))))
                           in
                           Error s
                 else let s =
                        E.error
                          ('t'::('o'::('-'::('s'::('a'::('v'::('e'::(':'::(' '::('b'::('a'::('d'::(' '::('f'::('r'::('a'::('m'::('e'::(' '::('a'::('l'::('i'::('g'::('n'::('e'::('m'::('e'::('n'::('t'::[])))))))))))))))))))))))))))))
                      in
                      Error s
            else let s =
                   E.error
                     ('t'::('o'::('-'::('s'::('a'::('v'::('e'::(':'::(' '::('o'::('v'::('e'::('r'::('l'::('a'::('p'::[]))))))))))))))))
                 in
                 Error s
          | Error s -> Error s) lo m with
  | Ok x ->
    if Z.leb x hi
    then Ok ()
    else Error
           (E.error
             ('t'::('o'::('-'::('s'::('a'::('v'::('e'::(':'::(' '::('o'::('v'::('e'::('r'::('f'::('l'::('o'::('w'::(' '::('i'::('n'::(' '::('t'::('h'::('e'::(' '::('s'::('t'::('a'::('c'::('k'::(' '::('f'::('r'::('a'::('m'::('e'::[])))))))))))))))))))))))))))))))))))))
  | Error s -> Error s

(** val check_to_save : coq_PointerData -> stk_fun_extra -> unit cexec **)

let check_to_save pd e =
  match e.sf_return_address with
  | RAnone ->
    all_disjoint_aligned_between pd
      (match e.sf_save_stack with
       | SavedStackStk ofs -> Z.add ofs (wsize_size (coq_Uptr pd))
       | _ -> e.sf_stk_sz) (Z.add e.sf_stk_sz e.sf_stk_extra_sz)
      (coq_Uptr pd) e.sf_to_save (fun pat ->
      let (x, ofs) = pat in
      (match is_word_type (Var.vtype x) with
       | Some ws -> Ok (ofs, ws)
       | None ->
         Error
           (E.error
             ('t'::('o'::('-'::('s'::('a'::('v'::('e'::(':'::(' '::('n'::('o'::('t'::(' '::('a'::(' '::('w'::('o'::('r'::('d'::[]))))))))))))))))))))))
  | _ -> Ok ()

(** val linear_c :
    'a1 asmOp -> ('a1 instr -> label -> 'a1 lcmd -> label * 'a1 lcmd) -> 'a1
    instr list -> label -> 'a1 lcmd -> label * 'a1 lcmd **)

let rec linear_c asmop linear_i0 c lbl lc =
  match c with
  | [] -> (lbl, lc)
  | i :: c0 ->
    let (lbl0, lc0) = linear_c asmop linear_i0 c0 lbl lc in
    linear_i0 i lbl0 lc0

(** val next_lbl : positive -> positive **)

let next_lbl lbl =
  Pos.add lbl Coq_xH

(** val add_align :
    'a1 asmOp -> instr_info -> align -> 'a1 lcmd -> 'a1 linstr list **)

let add_align _ ii a lc =
  match a with
  | Align -> { li_ii = ii; li_i = Lalign } :: lc
  | NoAlign -> lc

(** val align :
    'a1 asmOp -> instr_info -> align -> (label * 'a1 lcmd) -> label * 'a1 lcmd **)

let align asmop ii a p =
  ((fst p), (add_align asmop ii a (snd p)))

type 'asm_op linearization_params = { lp_tmp : Equality.sort;
                                      lp_allocate_stack_frame : (var_i ->
                                                                coq_Z ->
                                                                (lval
                                                                list * 'asm_op
                                                                sopn) * pexpr
                                                                list);
                                      lp_free_stack_frame : (var_i -> coq_Z
                                                            -> (lval
                                                            list * 'asm_op
                                                            sopn) * pexpr
                                                            list);
                                      lp_ensure_rsp_alignment : (var_i ->
                                                                wsize ->
                                                                (lval
                                                                list * 'asm_op
                                                                sopn) * pexpr
                                                                list);
                                      lp_lassign : (lval -> wsize -> pexpr ->
                                                   (lval list * 'asm_op
                                                   sopn) * pexpr list) }

(** val check_fd :
    coq_PointerData -> 'a1 asmOp -> 'a1 sprog -> (instr_info -> Var.var
    option) -> 'a1 linearization_params -> funname -> 'a1 sfundef ->
    (pp_error_loc, unit) result **)

let check_fd pd asmop p extra_free_registers =
  let check_stack_ofs = fun e ofs ws ->
    (&&) (Z.leb e.sf_stk_sz ofs)
      ((&&)
        (Z.leb (Z.add ofs (wsize_size ws))
          (Z.add e.sf_stk_sz e.sf_stk_extra_sz))
        ((&&) (cmp_le wsize_cmp ws e.sf_align)
          (is_align (GRing.ComRing.eqType (word (coq_Uptr pd)))
            (coq_Pointer pd) (wrepr (coq_Uptr pd) ofs) ws)))
  in
  (fun lp ->
  let var_tmp = { Var.vtype = (Coq_sword (coq_Uptr pd)); Var.vname =
    lp.lp_tmp }
  in
  (fun fn fd ->
  let e = fd.f_extra in
  let stack_align = (Obj.magic e).sf_align in
  (match check_c asmop
           (check_i pd asmop p extra_free_registers fn stack_align) fd.f_body with
   | Ok _ ->
     (match check_to_save pd (Obj.magic e) with
      | Ok _ ->
        if (&&) (Z.leb Z0 (Obj.magic e).sf_stk_sz)
             ((&&) (Z.leb Z0 (Obj.magic e).sf_stk_extra_sz)
               (Z.ltb (stack_frame_allocation_size (Obj.magic e))
                 (wbase (coq_Uptr pd))))
        then if match (Obj.magic e).sf_return_address with
                | RAnone -> true
                | RAreg ra ->
                  eq_op stype_eqType (Obj.magic Var.vtype ra)
                    (Obj.magic (Coq_sword (coq_Uptr pd)))
                | RAstack ofs -> Obj.magic check_stack_ofs e ofs (coq_Uptr pd)
             then if (||)
                       (negb
                         (eq_op return_address_location_eqType
                           (Obj.magic (Obj.magic e).sf_return_address)
                           (Obj.magic RAnone)))
                       (match (Obj.magic e).sf_save_stack with
                        | SavedStackNone ->
                          (&&)
                            (eq_op
                              (seq_eqType
                                (prod_eqType Var.var_eqType coq_Z_eqType))
                              (Obj.magic (Obj.magic e).sf_to_save)
                              (Obj.magic []))
                            ((&&)
                              (eq_op wsize_eqType (Obj.magic stack_align)
                                (Obj.magic U8))
                              ((&&)
                                (eq_op coq_Z_eqType
                                  (Obj.magic (Obj.magic e).sf_stk_sz)
                                  (Obj.magic int_to_Z (Posz O)))
                                (eq_op coq_Z_eqType
                                  (Obj.magic (Obj.magic e).sf_stk_extra_sz)
                                  (Obj.magic int_to_Z (Posz O)))))
                        | SavedStackReg r ->
                          (&&)
                            (eq_op stype_eqType (Obj.magic Var.vtype r)
                              (Obj.magic (Coq_sword (coq_Uptr pd))))
                            (eq_op
                              (seq_eqType
                                (prod_eqType Var.var_eqType coq_Z_eqType))
                              (Obj.magic (Obj.magic e).sf_to_save)
                              (Obj.magic []))
                        | SavedStackStk ofs ->
                          (&&)
                            (Obj.magic check_stack_ofs e ofs (coq_Uptr pd))
                            (negb
                              (Sv.mem (Obj.magic var_tmp)
                                (sv_of_list fst (Obj.magic e).sf_to_save))))
                  then Ok ()
                  else let s =
                         E.error
                           ('b'::('a'::('d'::(' '::('s'::('a'::('v'::('e'::('-'::('s'::('t'::('a'::('c'::('k'::[]))))))))))))))
                       in
                       Error s
             else let s =
                    E.error
                      ('b'::('a'::('d'::(' '::('r'::('e'::('t'::('u'::('r'::('n'::('-'::('a'::('d'::('d'::('r'::('e'::('s'::('s'::[]))))))))))))))))))
                  in
                  Error s
        else let s =
               E.error
                 ('b'::('a'::('d'::(' '::('s'::('t'::('a'::('c'::('k'::(' '::('s'::('i'::('z'::('e'::[]))))))))))))))
             in
             Error s
      | Error s -> Error s)
   | Error s -> Error s)))

(** val check_prog :
    coq_PointerData -> 'a1 asmOp -> 'a1 sprog -> (instr_info -> Var.var
    option) -> 'a1 linearization_params -> (pp_error_loc, unit) result **)

let check_prog pd asmop p extra_free_registers lp =
  match map_cfprog_name_gen (fun x -> x.f_info)
          (check_fd pd asmop p extra_free_registers lp) p.p_funcs with
  | Ok _ -> Ok ()
  | Error s -> Error s

(** val lassign :
    'a1 asmOp -> 'a1 linearization_params -> instr_info -> lval -> wsize ->
    pexpr -> 'a1 linstr **)

let lassign _ lp ii x ws e =
  let args = lp.lp_lassign x ws e in
  { li_ii = ii; li_i = (Lopn ((fst (fst args)), (snd (fst args)),
  (snd args))) }

(** val lmove :
    'a1 asmOp -> 'a1 linearization_params -> instr_info -> var_i -> wsize ->
    gvar -> 'a1 linstr **)

let lmove asmop lp ii rd ws r0 =
  lassign asmop lp ii (Lvar rd) ws (Pvar r0)

(** val lload :
    coq_PointerData -> 'a1 asmOp -> 'a1 linearization_params -> instr_info ->
    var_i -> wsize -> var_i -> coq_Z -> 'a1 linstr **)

let lload pd asmop lp ii rd ws r0 ofs =
  lassign asmop lp ii (Lvar rd) ws (Pload (ws, r0, (cast_const pd ofs)))

(** val lstore :
    coq_PointerData -> 'a1 asmOp -> 'a1 linearization_params -> instr_info ->
    var_i -> coq_Z -> wsize -> gvar -> 'a1 linstr **)

let lstore pd asmop lp ii rd ofs ws r0 =
  lassign asmop lp ii (Lmem (ws, rd, (cast_const pd ofs))) ws (Pvar r0)

(** val allocate_stack_frame :
    coq_PointerData -> 'a1 asmOp -> 'a1 sprog -> 'a1 linearization_params ->
    bool -> instr_info -> coq_Z -> 'a1 lcmd **)

let allocate_stack_frame pd _ p lp =
  let rsp = { Var.vtype = (Coq_sword (coq_Uptr pd)); Var.vname =
    (Obj.magic p).p_extra.sp_rsp }
  in
  let rspi = { v_var = rsp; v_info = Coq_xH } in
  (fun free ii sz ->
  if eq_op coq_Z_eqType (Obj.magic sz) (Obj.magic Z0)
  then []
  else let args =
         if free
         then lp.lp_allocate_stack_frame rspi sz
         else lp.lp_free_stack_frame rspi sz
       in
       { li_ii = ii; li_i = (Lopn ((fst (fst args)), (snd (fst args)),
       (snd args))) } :: [])

(** val ensure_rsp_alignment :
    coq_PointerData -> 'a1 asmOp -> 'a1 sprog -> 'a1 linearization_params ->
    instr_info -> wsize -> 'a1 linstr **)

let ensure_rsp_alignment pd _ p lp =
  let rsp = { Var.vtype = (Coq_sword (coq_Uptr pd)); Var.vname =
    (Obj.magic p).p_extra.sp_rsp }
  in
  let rspi = { v_var = rsp; v_info = Coq_xH } in
  (fun ii al ->
  let args = lp.lp_ensure_rsp_alignment rspi al in
  { li_ii = ii; li_i = (Lopn ((fst (fst args)), (snd (fst args)),
  (snd args))) })

(** val push_to_save :
    coq_PointerData -> 'a1 asmOp -> 'a1 sprog -> 'a1 linearization_params ->
    instr_info -> (Var.var * coq_Z) list -> 'a1 lcmd **)

let push_to_save pd asmop p lp =
  let rsp = { Var.vtype = (Coq_sword (coq_Uptr pd)); Var.vname =
    (Obj.magic p).p_extra.sp_rsp }
  in
  let rspi = { v_var = rsp; v_info = Coq_xH } in
  (fun ii to_save ->
  let mkli = fun pat ->
    let (x, ofs) = pat in
    (match is_word_type (Var.vtype x) with
     | Some ws ->
       lstore pd asmop lp ii rspi ofs ws { gv = { v_var = x; v_info =
         Coq_xH }; gs = Slocal }
     | None -> { li_ii = ii; li_i = Lalign })
  in
  List0.map mkli to_save)

(** val pop_to_save :
    coq_PointerData -> 'a1 asmOp -> 'a1 sprog -> 'a1 linearization_params ->
    instr_info -> (Var.var * coq_Z) list -> 'a1 lcmd **)

let pop_to_save pd asmop p lp =
  let rsp = { Var.vtype = (Coq_sword (coq_Uptr pd)); Var.vname =
    (Obj.magic p).p_extra.sp_rsp }
  in
  let rspi = { v_var = rsp; v_info = Coq_xH } in
  (fun ii to_save ->
  let mkli = fun pat ->
    let (x, ofs) = pat in
    (match is_word_type (Var.vtype x) with
     | Some ws ->
       lload pd asmop lp ii { v_var = x; v_info = Coq_xH } ws rspi ofs
     | None -> { li_ii = ii; li_i = Lalign })
  in
  List0.map mkli to_save)

(** val linear_i :
    coq_PointerData -> 'a1 asmOp -> 'a1 sprog -> (instr_info -> Var.var
    option) -> 'a1 linearization_params -> funname -> 'a1 instr -> label ->
    'a1 lcmd -> label * 'a1 lcmd **)

let linear_i pd asmop p extra_free_registers lp fn =
  let rsp = { Var.vtype = (Coq_sword (coq_Uptr pd)); Var.vname =
    (Obj.magic p).p_extra.sp_rsp }
  in
  let rspi = { v_var = rsp; v_info = Coq_xH } in
  let rec linear_i0 i lbl lc =
    let MkI (ii, ir) = i in
    (match ir with
     | Cassgn (x, _, ty, e) ->
       let lc' =
         match ty with
         | Coq_sword sz -> (lassign asmop lp ii x sz e) :: lc
         | _ -> lc
       in
       (lbl, lc')
     | Copn (xs, _, o, es) ->
       (lbl, ({ li_ii = ii; li_i = (Lopn (xs, o, es)) } :: lc))
     | Cif (e, c1, c2) ->
       (match c1 with
        | [] ->
          let lbl0 = next_lbl lbl in
          let (lbl1, lc0) =
            linear_c asmop linear_i0 c2 lbl0 ({ li_ii = ii; li_i = (Llabel
              lbl) } :: lc)
          in
          (lbl1, ({ li_ii = ii; li_i = (Lcond (e, lbl)) } :: lc0))
        | _ :: _ ->
          (match c2 with
           | [] ->
             let lbl0 = next_lbl lbl in
             let (lbl1, lc0) =
               linear_c asmop linear_i0 c1 lbl0 ({ li_ii = ii; li_i = (Llabel
                 lbl) } :: lc)
             in
             (lbl1, ({ li_ii = ii; li_i = (Lcond ((snot e), lbl)) } :: lc0))
           | _ :: _ ->
             let l2 = next_lbl lbl in
             let lbl0 = next_lbl l2 in
             let (lbl1, lc0) =
               let (lbl1, lc0) =
                 let (lbl1, lc0) =
                   linear_c asmop linear_i0 c1 lbl0 ({ li_ii = ii; li_i =
                     (Llabel l2) } :: lc)
                 in
                 let lc1 = { li_ii = ii; li_i = (Llabel lbl) } :: lc0 in
                 (lbl1, ({ li_ii = ii; li_i = (Lgoto (fn, l2)) } :: lc1))
               in
               linear_c asmop linear_i0 c2 lbl1 lc0
             in
             (lbl1, ({ li_ii = ii; li_i = (Lcond (e, lbl)) } :: lc0))))
     | Cfor (_, _, _) -> (lbl, lc)
     | Cwhile (a, c, e, c') ->
       (match is_bool e with
        | Some b ->
          if b
          then let lbl0 = next_lbl lbl in
               align asmop ii a
                 (let (lbl1, lc0) =
                    let (lbl1, lc0) =
                      linear_c asmop linear_i0 c' lbl0 ({ li_ii = ii; li_i =
                        (Lgoto (fn, lbl)) } :: lc)
                    in
                    linear_c asmop linear_i0 c lbl1 lc0
                  in
                  (lbl1, ({ li_ii = ii; li_i = (Llabel lbl) } :: lc0)))
          else linear_c asmop linear_i0 c lbl lc
        | None ->
          (match c' with
           | [] ->
             let lbl0 = next_lbl lbl in
             align asmop ii a
               (let (lbl1, lc0) =
                  linear_c asmop linear_i0 c lbl0 ({ li_ii = ii; li_i =
                    (Lcond (e, lbl)) } :: lc)
                in
                (lbl1, ({ li_ii = ii; li_i = (Llabel lbl) } :: lc0)))
           | _ :: _ ->
             let l2 = next_lbl lbl in
             let lbl0 = next_lbl l2 in
             let (lbl1, lc0) =
               align asmop ii a
                 (let (lbl1, lc0) =
                    let (lbl1, lc0) =
                      linear_c asmop linear_i0 c lbl0 ({ li_ii = ii; li_i =
                        (Lcond (e, l2)) } :: lc)
                    in
                    let lc1 = { li_ii = ii; li_i = (Llabel lbl) } :: lc0 in
                    linear_c asmop linear_i0 c' lbl1 lc1
                  in
                  (lbl1, ({ li_ii = ii; li_i = (Llabel l2) } :: lc0)))
             in
             (lbl1, ({ li_ii = ii; li_i = (Lgoto (fn, lbl)) } :: lc0))))
     | Ccall (_, _, fn', _) ->
       (match get_fundef p.p_funcs fn' with
        | Some fd ->
          let e = fd.f_extra in
          let ra = (Obj.magic e).sf_return_address in
          if eq_op return_address_location_eqType (Obj.magic ra)
               (Obj.magic RAnone)
          then (lbl, lc)
          else let sz = stack_frame_allocation_size (Obj.magic e) in
               let before = allocate_stack_frame pd asmop p lp false ii sz in
               let after = allocate_stack_frame pd asmop p lp true ii sz in
               let lbl0 = next_lbl lbl in
               let lcall = (fn',
                 (if eq_op pos_eqType (Obj.magic fn') (Obj.magic fn)
                  then lbl
                  else Coq_xH))
               in
               (match (Obj.magic e).sf_return_address with
                | RAnone -> (lbl0, lc)
                | RAreg ra0 ->
                  (lbl0,
                    (cat before ({ li_ii = ii; li_i = (LstoreLabel ((Lvar
                      { v_var = ra0; v_info = Coq_xH }),
                      lbl)) } :: ({ li_ii = ii; li_i = (Lgoto
                      lcall) } :: ({ li_ii = ii; li_i = (Llabel
                      lbl) } :: (cat after lc))))))
                | RAstack z ->
                  (match extra_free_registers ii with
                   | Some ra0 ->
                     let glob_ra = { gv = { v_var = ra0; v_info = Coq_xH };
                       gs = Slocal }
                     in
                     (lbl0,
                     (cat before ({ li_ii = ii; li_i = (LstoreLabel ((Lvar
                       { v_var = ra0; v_info = Coq_xH }),
                       lbl)) } :: ((lstore pd asmop lp ii rspi z
                                     (coq_Uptr pd) glob_ra) :: ({ li_ii = ii;
                       li_i = (Lgoto lcall) } :: ({ li_ii = ii; li_i =
                       (Llabel lbl) } :: (cat after lc)))))))
                   | None -> (lbl0, lc)))
        | None -> (lbl, lc)))
  in linear_i0

(** val linear_body :
    coq_PointerData -> 'a1 asmOp -> 'a1 sprog -> (instr_info -> Var.var
    option) -> 'a1 linearization_params -> funname -> stk_fun_extra -> 'a1
    instr list -> 'a1 lcmd **)

let linear_body pd asmop p extra_free_registers lp fn =
  let rsp = { Var.vtype = (Coq_sword (coq_Uptr pd)); Var.vname =
    (Obj.magic p).p_extra.sp_rsp }
  in
  let rspi = { v_var = rsp; v_info = Coq_xH } in
  let rspg = { gv = rspi; gs = Slocal } in
  let var_tmp = { Var.vtype = (Coq_sword (coq_Uptr pd)); Var.vname =
    lp.lp_tmp }
  in
  (fun e body ->
  let (p0, lbl) =
    match e.sf_return_address with
    | RAnone ->
      (match e.sf_save_stack with
       | SavedStackNone -> (([], []), Coq_xH)
       | SavedStackReg x ->
         let r = { v_var = x; v_info = Coq_xH } in
         ((((lmove asmop lp Coq_xH rspi (coq_Uptr pd) { gv = r; gs = Slocal }) :: []),
         ((lmove asmop lp Coq_xH r (coq_Uptr pd) rspg) :: (cat
                                                            (allocate_stack_frame
                                                              pd asmop p lp
                                                              false Coq_xH
                                                              (Z.add
                                                                e.sf_stk_sz
                                                                e.sf_stk_extra_sz))
                                                            ((ensure_rsp_alignment
                                                               pd asmop p lp
                                                               Coq_xH
                                                               e.sf_align) :: [])))),
         Coq_xH)
       | SavedStackStk ofs ->
         let tmp = { v_var = var_tmp; v_info = Coq_xH } in
         (((cat (pop_to_save pd asmop p lp Coq_xH e.sf_to_save)
             ((lload pd asmop lp Coq_xH rspi (coq_Uptr pd) rspi ofs) :: [])),
         ((lmove asmop lp Coq_xH tmp (coq_Uptr pd) rspg) :: (cat
                                                              (allocate_stack_frame
                                                                pd asmop p lp
                                                                false Coq_xH
                                                                (Z.add
                                                                  e.sf_stk_sz
                                                                  e.sf_stk_extra_sz))
                                                              ((ensure_rsp_alignment
                                                                 pd asmop p
                                                                 lp Coq_xH
                                                                 e.sf_align) :: (
                                                              (lstore pd
                                                                asmop lp
                                                                Coq_xH rspi
                                                                ofs
                                                                (coq_Uptr pd)
                                                                { gv = tmp;
                                                                gs = Slocal }) :: 
                                                              (push_to_save
                                                                pd asmop p lp
                                                                Coq_xH
                                                                e.sf_to_save)))))),
         Coq_xH))
    | RAreg r ->
      ((({ li_ii = Coq_xH; li_i = (Ligoto (Pvar { gv = { v_var = r; v_info =
        Coq_xH }; gs = Slocal })) } :: []), ({ li_ii = Coq_xH; li_i = (Llabel
        Coq_xH) } :: [])), (Coq_xO Coq_xH))
    | RAstack z ->
      ((({ li_ii = Coq_xH; li_i = (Ligoto (Pload ((coq_Uptr pd), rspi,
        (cast_const pd z)))) } :: []), ({ li_ii = Coq_xH; li_i = (Llabel
        Coq_xH) } :: [])), (Coq_xO Coq_xH))
  in
  let (tail, head) = p0 in
  let fd' =
    linear_c asmop (linear_i pd asmop p extra_free_registers lp fn) body lbl
      tail
  in
  cat head (snd fd'))

(** val linear_fd :
    coq_PointerData -> 'a1 asmOp -> 'a1 sprog -> (instr_info -> Var.var
    option) -> 'a1 linearization_params -> funname -> 'a1 sfundef -> 'a1
    lfundef **)

let linear_fd pd asmop p extra_free_registers lp fn fd =
  let e = fd.f_extra in
  let is_export =
    eq_op return_address_location_eqType
      (Obj.magic (Obj.magic e).sf_return_address) (Obj.magic RAnone)
  in
  let res = if is_export then fd.f_res else [] in
  { lfd_info = fd.f_info; lfd_align = (Obj.magic e).sf_align; lfd_tyin =
  fd.f_tyin; lfd_arg = fd.f_params; lfd_body =
  (linear_body pd asmop p extra_free_registers lp fn (Obj.magic e) fd.f_body);
  lfd_tyout = fd.f_tyout; lfd_res = res; lfd_export = is_export;
  lfd_callee_saved =
  (if is_export then map fst (Obj.magic e).sf_to_save else []);
  lfd_total_stack = (Obj.magic e).sf_stk_max }

(** val linear_prog :
    coq_PointerData -> 'a1 asmOp -> 'a1 sprog -> (instr_info -> Var.var
    option) -> 'a1 linearization_params -> 'a1 lprog cexec **)

let linear_prog pd asmop p extra_free_registers lparams =
  match check_prog pd asmop p extra_free_registers lparams with
  | Ok _ ->
    if eq_op nat_eqType (Obj.magic size p.p_globs) (Obj.magic O)
    then let funcs =
           map (fun pat ->
             let (f, fd) = pat in
             (f, (linear_fd pd asmop p extra_free_registers lparams f fd)))
             p.p_funcs
         in
         Ok { lp_rip = (Obj.magic p).p_extra.sp_rip; lp_rsp =
         (Obj.magic p).p_extra.sp_rsp; lp_globs =
         (Obj.magic p).p_extra.sp_globs; lp_funcs = funcs }
    else let s =
           E.internal_error
             ('i'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('p'::('_'::('g'::('l'::('o'::('b'::('s'::[])))))))))))))))
         in
         Error s
  | Error s -> Error s
