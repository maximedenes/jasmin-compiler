open BinNums
open Sumbool
open Eqtype
open Expr
open Low_memory
open Memory_model
open Sem
open Ssralg
open Type
open Utils0
open Values
open Var0
open Warray_
open Word0
open Wsize

type __ = Obj.t

type pword = { pw_size : wsize; pw_word : GRing.ComRing.sort }

type psem_t = __

val pword_of_word : wsize -> GRing.ComRing.sort -> pword

val to_pword : wsize -> value -> pword exec

val pof_val : stype -> value -> psem_t exec

val wextend_type : Equality.sort -> Equality.sort -> bool

val pundef_addr : stype -> psem_t exec

val vmap0 : psem_t exec Fv.t

val set_var : psem_t exec Fv.t -> Var.var -> value -> psem_t exec Fv.t exec

type estate = { emem : Memory.mem; evm : psem_t exec Fv.t }

val with_vm : coq_PointerData -> estate -> psem_t exec Fv.t -> estate

val write_var : coq_PointerData -> var_i -> value -> estate -> estate exec

val write_vars :
  coq_PointerData -> var_i list -> value list -> estate -> (error, estate)
  result

type semCallParams = { init_state : (Equality.sort -> extra_prog_t ->
                                    extra_val_t -> estate -> estate exec);
                       finalize : (Equality.sort -> Memory.mem -> Memory.mem) }

val sCP_unit : coq_PointerData -> semCallParams

val init_stk_state :
  coq_PointerData -> stk_fun_extra -> sprog_extra -> GRing.ComRing.sort ->
  estate -> (error, estate) result

val finalize_stk_mem :
  coq_PointerData -> stk_fun_extra -> Memory.mem -> Memory.mem

val sCP_stack : coq_PointerData -> semCallParams
