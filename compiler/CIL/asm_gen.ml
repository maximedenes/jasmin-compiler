open BinNums
open Datatypes
open String0
open Arch_decl
open Arch_extra
open Compiler_util
open Eqtype
open Expr
open Lea
open Memory_model
open Oseq
open Seq
open Sopn
open Ssralg
open Ssrnat
open Type
open Utils0
open Var0
open Word0
open Wsize

module E =
 struct
  (** val pass_name : char list **)

  let pass_name =
    'a'::('s'::('m'::('g'::('e'::('n'::[])))))

  (** val gen_error :
      bool -> instr_info option -> var_info option -> pp_error -> pp_error_loc **)

  let gen_error internal ii vi msg =
    { pel_msg = msg; pel_fn = None; pel_fi = None; pel_ii = ii; pel_vi = vi;
      pel_pass = (Some pass_name); pel_internal = internal }

  (** val internal_error : instr_info -> char list -> pp_error_loc **)

  let internal_error ii msg =
    gen_error true (Some ii) None (PPEstring msg)

  (** val error : instr_info -> pp_error -> pp_error_loc **)

  let error ii msg =
    gen_error false (Some ii) None msg

  (** val verror :
      bool -> char list -> instr_info -> var_i -> pp_error_loc **)

  let verror internal msg ii v =
    gen_error internal (Some ii) (Some v.v_info)
      (pp_box ((PPEstring msg) :: ((PPEstring (':'::[])) :: ((PPEvar
        v.v_var) :: []))))

  (** val invalid_name : char list -> instr_info -> var_i -> pp_error_loc **)

  let invalid_name category0 ii v =
    verror true
      (append ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::[]))))))))
        (append category0 (' '::('n'::('a'::('m'::('e'::[]))))))) ii v

  (** val invalid_ty : char list -> instr_info -> var_i -> pp_error_loc **)

  let invalid_ty category0 ii v =
    verror true
      (append ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::[]))))))))
        (append category0 (' '::('t'::('y'::('p'::('e'::[]))))))) ii v

  (** val berror : instr_info -> pexpr -> char list -> pp_error_loc **)

  let berror ii e msg =
    gen_error false (Some ii) None
      (pp_vbox
        ((pp_box ((PPEstring
           ('n'::('o'::('t'::(' '::('a'::('b'::('l'::('e'::(' '::('t'::('o'::(' '::('c'::('o'::('m'::('p'::('i'::('l'::('e'::(' '::('t'::('h'::('e'::(' '::('c'::('o'::('n'::('d'::('i'::('t'::('i'::('o'::('n'::[])))))))))))))))))))))))))))))))))) :: ((PPEexpr
           e) :: []))) :: ((PPEstring msg) :: [])))

  (** val werror : instr_info -> pexpr -> char list -> pp_error_loc **)

  let werror ii e msg =
    gen_error false (Some ii) None
      (pp_vbox
        ((pp_box ((PPEstring
           ('i'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('p'::('e'::('x'::('p'::('r'::(' '::('f'::('o'::('r'::(' '::('o'::('p'::('r'::('d'::[]))))))))))))))))))))))) :: ((PPEexpr
           e) :: []))) :: ((PPEstring msg) :: [])))
 end

(** val of_var_e :
    stype -> 'a1 coq_ToString -> instr_info -> var_i -> (pp_error_loc, 'a1)
    result **)

let of_var_e t tS ii v =
  match of_var t tS v.v_var with
  | Some r -> Ok r
  | None ->
    if eq_op stype_eqType (Obj.magic Var.vtype v.v_var) (Obj.magic rtype t tS)
    then Error (E.invalid_ty tS.category ii v)
    else Error (E.invalid_name tS.category ii v)

(** val to_reg :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> Var.var -> ('a1, 'a2, 'a3,
    'a4) reg_t option **)

let to_reg asm_e =
  of_var (Coq_sword asm_e._asm._arch_decl.reg_size)
    asm_e._asm._arch_decl.toS_r

(** val to_xreg :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> Var.var -> ('a1, 'a2, 'a3,
    'a4) xreg_t option **)

let to_xreg asm_e =
  of_var (Coq_sword asm_e._asm._arch_decl.xreg_size)
    asm_e._asm._arch_decl.toS_x

(** val scale_of_z' :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> instr_info ->
    GRing.ComRing.sort -> nat cexec **)

let scale_of_z' asm_e ii z =
  match wunsigned (coq_Uptr (arch_pd asm_e._asm._arch_decl)) z with
  | Zpos p ->
    (match p with
     | Coq_xI _ ->
       Error
         (E.error ii (PPEstring
           ('i'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('s'::('c'::('a'::('l'::('e'::[])))))))))))))))
     | Coq_xO p0 ->
       (match p0 with
        | Coq_xI _ ->
          Error
            (E.error ii (PPEstring
              ('i'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('s'::('c'::('a'::('l'::('e'::[])))))))))))))))
        | Coq_xO p1 ->
          (match p1 with
           | Coq_xI _ ->
             Error
               (E.error ii (PPEstring
                 ('i'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('s'::('c'::('a'::('l'::('e'::[])))))))))))))))
           | Coq_xO p2 ->
             (match p2 with
              | Coq_xH -> Ok (S (S (S O)))
              | _ ->
                Error
                  (E.error ii (PPEstring
                    ('i'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('s'::('c'::('a'::('l'::('e'::[]))))))))))))))))
           | Coq_xH -> Ok (S (S O)))
        | Coq_xH -> Ok (S O))
     | Coq_xH -> Ok O)
  | _ ->
    Error
      (E.error ii (PPEstring
        ('i'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('s'::('c'::('a'::('l'::('e'::[])))))))))))))))

(** val reg_of_ovar :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> instr_info -> var_i option ->
    ('a1, 'a2, 'a3, 'a4) reg_t option cexec **)

let reg_of_ovar asm_e ii = function
| Some x0 ->
  (match of_var_e (Coq_sword asm_e._asm._arch_decl.reg_size)
           asm_e._asm._arch_decl.toS_r ii x0 with
   | Ok x1 -> Ok (Some x1)
   | Error s -> Error s)
| None -> Ok None

(** val assemble_lea :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> instr_info -> lea ->
    (pp_error_loc, ('a1, 'a2, 'a3, 'a4) address) result **)

let assemble_lea asm_e ii lea0 =
  match reg_of_ovar asm_e ii lea0.lea_base with
  | Ok x ->
    (match reg_of_ovar asm_e ii lea0.lea_offset with
     | Ok x0 ->
       (match scale_of_z' asm_e ii lea0.lea_scale with
        | Ok x1 ->
          Ok (Areg { ad_disp = lea0.lea_disp; ad_base = x; ad_scale = x1;
            ad_offset = x0 })
        | Error s -> Error s)
     | Error s -> Error s)
  | Error s -> Error s

(** val addr_of_pexpr :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> Var.var -> instr_info ->
    wsize -> pexpr -> (pp_error_loc, ('a1, 'a2, 'a3, 'a4) address) result **)

let addr_of_pexpr asm_e rip ii sz e =
  if cmp_le wsize_cmp sz (coq_Uptr (arch_pd asm_e._asm._arch_decl))
  then (match mk_lea (arch_pd asm_e._asm._arch_decl) sz e with
        | Some lea0 ->
          (match lea0.lea_base with
           | Some r ->
             if eq_op Var.var_eqType (Obj.magic r.v_var) (Obj.magic rip)
             then if eq_op (option_eqType var_i_eqType)
                       (Obj.magic lea0.lea_offset) (Obj.magic None)
                  then Ok (Arip lea0.lea_disp)
                  else let s =
                         E.error ii
                           (pp_box ((PPEstring
                             ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('g'::('l'::('o'::('b'::('a'::('l'::(' '::('a'::('d'::('d'::('r'::('e'::('s'::('s'::(' '::(':'::[]))))))))))))))))))))))))) :: ((PPEexpr
                             e) :: [])))
                       in
                       Error s
             else assemble_lea asm_e ii lea0
           | None -> assemble_lea asm_e ii lea0)
        | None ->
          Error
            (E.error ii
              (pp_box ((PPEstring
                ('n'::('o'::('t'::(' '::('a'::('b'::('l'::('e'::(' '::('t'::('o'::(' '::('a'::('s'::('s'::('e'::('m'::('b'::('l'::('e'::(' '::('a'::('d'::('d'::('r'::('e'::('s'::('s'::(' '::(':'::[]))))))))))))))))))))))))))))))) :: ((PPEexpr
                e) :: [])))))
  else let s =
         E.error ii (PPEstring
           ('B'::('a'::('d'::(' '::('t'::('y'::('p'::('e'::(' '::('f'::('o'::('r'::(' '::('a'::('d'::('d'::('r'::('e'::('s'::('s'::[])))))))))))))))))))))
       in
       Error s

(** val addr_of_xpexpr :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> Var.var -> instr_info ->
    wsize -> var_i -> pexpr -> (pp_error_loc, ('a1, 'a2, 'a3, 'a4) address)
    result **)

let addr_of_xpexpr asm_e rip ii sz v e =
  addr_of_pexpr asm_e rip ii sz (Papp2 ((Oadd (Op_w sz)), (coq_Plvar v), e))

(** val xreg_of_var :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> instr_info -> var_i -> ('a1,
    'a2, 'a3, 'a4) asm_arg cexec **)

let xreg_of_var asm_e ii x =
  match to_xreg asm_e x.v_var with
  | Some r -> Ok (XReg r)
  | None ->
    (match to_reg asm_e x.v_var with
     | Some r -> Ok (Reg r)
     | None ->
       Error
         (E.verror false
           ('N'::('o'::('t'::(' '::('a'::(' '::('('::('x'::(')'::('r'::('e'::('g'::('i'::('s'::('t'::('e'::('r'::[])))))))))))))))))
           ii x))

(** val assemble_word_load :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> Var.var -> instr_info ->
    wsize -> pexpr -> (pp_error_loc, ('a1, 'a2, 'a3, 'a4) asm_arg) result **)

let assemble_word_load asm_e rip ii sz e = match e with
| Pvar x ->
  if is_lvar x
  then let x0 = x.gv in xreg_of_var asm_e ii x0
  else let s =
         E.internal_error ii
           ('G'::('l'::('o'::('b'::('a'::('l'::(' '::('v'::('a'::('r'::('i'::('a'::('b'::('l'::('e'::('s'::(' '::('r'::('e'::('m'::('a'::('i'::('n'::[])))))))))))))))))))))))
       in
       Error s
| Pload (sz', v, e') ->
  if eq_op wsize_eqType (Obj.magic sz) (Obj.magic sz')
  then (match addr_of_xpexpr asm_e rip ii
                (coq_Uptr (arch_pd asm_e._asm._arch_decl)) v e' with
        | Ok x -> Ok (Addr x)
        | Error s -> Error s)
  else let s =
         E.werror ii e
           ('i'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('L'::('o'::('a'::('d'::(' '::('s'::('i'::('z'::('e'::[])))))))))))))))))
       in
       Error s
| Papp1 (s, p) ->
  (match s with
   | Oword_of_int sz' ->
     (match p with
      | Pconst z ->
        let w = wrepr sz' z in
        let w1 = sign_extend sz sz' w in
        let w2 = wrepr sz z in
        if eq_op (GRing.ComRing.eqType (word sz)) w1 w2
        then Ok (Imm (sz', w))
        else let s0 =
               E.werror ii e
                 ('o'::('u'::('t'::(' '::('o'::('f'::(' '::('b'::('o'::('u'::('n'::('d'::(' '::('c'::('o'::('n'::('s'::('t'::('a'::('n'::('t'::[])))))))))))))))))))))
             in
             Error s0
      | _ ->
        Error
          (E.werror ii e
            ('i'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('p'::('e'::('x'::('p'::('r'::(' '::('f'::('o'::('r'::(' '::('w'::('o'::('r'::('d'::[]))))))))))))))))))))))))
   | _ ->
     Error
       (E.werror ii e
         ('i'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('p'::('e'::('x'::('p'::('r'::(' '::('f'::('o'::('r'::(' '::('w'::('o'::('r'::('d'::[]))))))))))))))))))))))))
| _ ->
  Error
    (E.werror ii e
      ('i'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('p'::('e'::('x'::('p'::('r'::(' '::('f'::('o'::('r'::(' '::('w'::('o'::('r'::('d'::[])))))))))))))))))))))))

(** val assemble_word :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> addr_kind -> Var.var ->
    instr_info -> wsize -> pexpr -> (pp_error_loc, ('a1, 'a2, 'a3, 'a4)
    asm_arg) result **)

let assemble_word asm_e k rip ii sz e =
  match k with
  | AK_compute ->
    (match addr_of_pexpr asm_e rip ii sz e with
     | Ok x -> Ok (Addr x)
     | Error s -> Error s)
  | AK_mem -> assemble_word_load asm_e rip ii sz e

(** val arg_of_pexpr :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> (instr_info -> pexpr -> ('a1,
    'a2, 'a3, 'a4) cond_t cexec) -> addr_kind -> Var.var -> instr_info ->
    stype -> pexpr -> (pp_error_loc, ('a1, 'a2, 'a3, 'a4) asm_arg) result **)

let arg_of_pexpr asm_e assemble_cond k rip ii ty e =
  match ty with
  | Coq_sbool ->
    (match assemble_cond ii e with
     | Ok x -> Ok (Condt x)
     | Error s -> Error s)
  | Coq_sint ->
    Error
      (E.werror ii e
        ('n'::('o'::('t'::(' '::('a'::('b'::('l'::('e'::(' '::('t'::('o'::(' '::('a'::('s'::('s'::('e'::('m'::('b'::('l'::('e'::(' '::('a'::('n'::(' '::('e'::('x'::('p'::('r'::('e'::('s'::('s'::('i'::('o'::('n'::(' '::('o'::('f'::(' '::('t'::('y'::('p'::('e'::(' '::('i'::('n'::('t'::[])))))))))))))))))))))))))))))))))))))))))))))))
  | Coq_sarr _ ->
    Error
      (E.werror ii e
        ('n'::('o'::('t'::(' '::('a'::('b'::('l'::('e'::(' '::('t'::('o'::(' '::('a'::('s'::('s'::('e'::('m'::('b'::('l'::('e'::(' '::('a'::('n'::(' '::('e'::('x'::('p'::('r'::('e'::('s'::('s'::('i'::('o'::('n'::(' '::('o'::('f'::(' '::('t'::('y'::('p'::('e'::(' '::('a'::('r'::('r'::('a'::('y'::(' '::('_'::[])))))))))))))))))))))))))))))))))))))))))))))))))))
  | Coq_sword sz -> assemble_word asm_e k rip ii sz e

(** val pexpr_of_lval : instr_info -> lval -> pexpr cexec **)

let pexpr_of_lval ii = function
| Lnone (_, _) ->
  Error
    (E.internal_error ii
      ('_'::(' '::('l'::('v'::('a'::('l'::(' '::('r'::('e'::('m'::('a'::('i'::('n'::('s'::[])))))))))))))))
| Lvar x -> Ok (coq_Plvar x)
| Lmem (s, x, e) -> Ok (Pload (s, x, e))
| Laset (_, _, _, _) ->
  Error
    (E.internal_error ii
      ('L'::('a'::('s'::('e'::('t'::(' '::('l'::('v'::('a'::('l'::(' '::('r'::('e'::('m'::('a'::('i'::('n'::('s'::[])))))))))))))))))))
| Lasub (_, _, _, _, _) ->
  Error
    (E.internal_error ii
      ('L'::('a'::('s'::('u'::('b'::(' '::('l'::('v'::('a'::('l'::(' '::('r'::('e'::('m'::('a'::('i'::('n'::('s'::[])))))))))))))))))))

type 't nmap = nat -> 't option

(** val nget : 'a1 nmap -> nat -> 'a1 option **)

let nget m =
  m

(** val nset : 'a1 nmap -> nat -> 'a1 -> Equality.sort -> 'a1 option **)

let nset m n t x =
  if eq_op nat_eqType x (Obj.magic n) then Some t else nget m (Obj.magic x)

(** val nempty : nat -> 'a1 option **)

let nempty _ =
  None

(** val var_of_implicit :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> ('a1, 'a2, 'a3, 'a4)
    Arch_decl.implicit_arg -> Var.var **)

let var_of_implicit asm_e = function
| Arch_decl.IArflag f -> to_var Coq_sbool asm_e._asm._arch_decl.toS_f f
| Arch_decl.IAreg r ->
  to_var (Coq_sword asm_e._asm._arch_decl.reg_size)
    asm_e._asm._arch_decl.toS_r r

(** val compile_arg :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> (instr_info -> pexpr -> ('a1,
    'a2, 'a3, 'a4) cond_t cexec) -> Var.var -> instr_info -> ((('a1, 'a2,
    'a3, 'a4) Arch_decl.arg_desc * stype) * pexpr) -> ('a1, 'a2, 'a3, 'a4)
    asm_arg nmap -> ('a1, 'a2, 'a3, 'a4) asm_arg nmap cexec **)

let compile_arg asm_e assemble_cond rip ii ade m =
  let ad = fst ade in
  let e = snd ade in
  (match fst ad with
   | Arch_decl.ADImplicit i ->
     if eq_expr
          (coq_Plvar { v_var = (var_of_implicit asm_e i); v_info = Coq_xH }) e
     then Ok m
     else let s =
            E.internal_error ii
              ('('::('c'::('o'::('m'::('p'::('i'::('l'::('e'::('_'::('a'::('r'::('g'::(')'::(' '::('b'::('a'::('d'::(' '::('i'::('m'::('p'::('l'::('i'::('c'::('i'::('t'::(' '::('r'::('e'::('g'::('i'::('s'::('t'::('e'::('r'::[])))))))))))))))))))))))))))))))))))
          in
          Error s
   | Arch_decl.ADExplicit (k, n, o) ->
     (match arg_of_pexpr asm_e assemble_cond k rip ii (snd ad) e with
      | Ok x ->
        if check_oreg asm_e._asm._arch_decl (Obj.magic o) x
        then (match nget m n with
              | Some a' ->
                if eq_op (asm_arg_eqType asm_e._asm._arch_decl) (Obj.magic x)
                     (Obj.magic a')
                then Ok m
                else Error
                       (E.internal_error ii
                         ('('::('c'::('o'::('m'::('p'::('i'::('l'::('e'::('_'::('a'::('r'::('g'::(')'::(' '::('n'::('o'::('t'::(' '::('c'::('o'::('m'::('p'::('a'::('t'::('i'::('b'::('l'::('e'::(' '::('a'::('s'::('m'::('_'::('a'::('r'::('g'::[])))))))))))))))))))))))))))))))))))))
              | None -> Ok (Obj.magic nset m n x))
        else let s =
               E.internal_error ii
                 ('('::('c'::('o'::('m'::('p'::('i'::('l'::('e'::('_'::('a'::('r'::('g'::(')'::(' '::('b'::('a'::('d'::(' '::('f'::('o'::('r'::('c'::('e'::('d'::(' '::('r'::('e'::('g'::('i'::('s'::('t'::('e'::('r'::[])))))))))))))))))))))))))))))))))
             in
             Error s
      | Error s -> Error s))

(** val compile_args :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> (instr_info -> pexpr -> ('a1,
    'a2, 'a3, 'a4) cond_t cexec) -> Var.var -> instr_info -> (('a1, 'a2, 'a3,
    'a4) Arch_decl.arg_desc * stype) list -> pexpr list -> ('a1, 'a2, 'a3,
    'a4) asm_arg nmap -> (pp_error_loc, ('a1, 'a2, 'a3, 'a4) asm_arg nmap)
    result **)

let compile_args asm_e assemble_cond rip ii adts es m =
  foldM (compile_arg asm_e assemble_cond rip ii) m (zip adts es)

(** val compat_imm :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> stype -> Equality.sort ->
    Equality.sort -> bool **)

let compat_imm asm_e ty a' a =
  (||) (eq_op (asm_arg_eqType asm_e._asm._arch_decl) a a')
    (match ty with
     | Coq_sword sz ->
       (match Obj.magic a with
        | Imm (sz1, w1) ->
          (match Obj.magic a' with
           | Imm (sz2, w2) ->
             eq_op (GRing.ComRing.eqType (word sz)) (sign_extend sz sz1 w1)
               (sign_extend sz sz2 w2)
           | _ -> false)
        | _ -> false)
     | _ -> false)

(** val check_sopn_arg :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> (instr_info -> pexpr -> ('a1,
    'a2, 'a3, 'a4) cond_t cexec) -> Var.var -> instr_info -> ('a1, 'a2, 'a3,
    'a4) asm_arg list -> pexpr -> (('a1, 'a2, 'a3, 'a4)
    Arch_decl.arg_desc * stype) -> bool **)

let check_sopn_arg asm_e assemble_cond rip ii loargs x adt =
  match fst adt with
  | Arch_decl.ADImplicit i ->
    eq_expr x
      (coq_Plvar { v_var = (var_of_implicit asm_e i); v_info = Coq_xH })
  | Arch_decl.ADExplicit (k, n, o) ->
    (match onth loargs n with
     | Some a ->
       (match arg_of_pexpr asm_e assemble_cond k rip ii (snd adt) x with
        | Ok a' ->
          (&&) (compat_imm asm_e (snd adt) (Obj.magic a) (Obj.magic a'))
            (check_oreg asm_e._asm._arch_decl (Obj.magic o) a)
        | Error _ -> false)
     | None -> false)

(** val check_sopn_dest :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> (instr_info -> pexpr -> ('a1,
    'a2, 'a3, 'a4) cond_t cexec) -> Var.var -> instr_info -> ('a1, 'a2, 'a3,
    'a4) asm_arg list -> pexpr -> (('a1, 'a2, 'a3, 'a4)
    Arch_decl.arg_desc * stype) -> bool **)

let check_sopn_dest asm_e assemble_cond rip ii loargs x adt =
  match fst adt with
  | Arch_decl.ADImplicit i ->
    eq_expr x
      (coq_Plvar { v_var = (var_of_implicit asm_e i); v_info = Coq_xH })
  | Arch_decl.ADExplicit (_, n, o) ->
    (match onth loargs n with
     | Some a ->
       (match arg_of_pexpr asm_e assemble_cond AK_mem rip ii (snd adt) x with
        | Ok a' ->
          (&&)
            (eq_op (asm_arg_eqType asm_e._asm._arch_decl) (Obj.magic a)
              (Obj.magic a'))
            (check_oreg asm_e._asm._arch_decl (Obj.magic o) a)
        | Error _ -> false)
     | None -> false)

(** val assemble_asm_op_aux :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> (instr_info -> pexpr -> ('a1,
    'a2, 'a3, 'a4) cond_t cexec) -> Var.var -> instr_info -> ('a1, 'a2, 'a3,
    'a4, 'a5) asm_op_msb_t -> lval list -> pexpr list -> (pp_error_loc, ('a1,
    'a2, 'a3, 'a4) asm_arg list) result **)

let assemble_asm_op_aux asm_e assemble_cond rip ii op outx inx =
  let id = instr_desc asm_e._asm._arch_decl asm_e._asm._asm_op_decl op in
  (match compile_args asm_e assemble_cond rip ii (zip id.id_in id.id_tin) inx
           nempty with
   | Ok x ->
     (match mapM (pexpr_of_lval ii) outx with
      | Ok x0 ->
        (match compile_args asm_e assemble_cond rip ii
                 (zip id.id_out id.id_tout) x0 x with
         | Ok x1 ->
           (match omap (nget x1) (iota O id.id_nargs) with
            | Some asm_args0 -> Ok asm_args0
            | None ->
              Error
                (E.internal_error ii
                  ('c'::('o'::('m'::('p'::('i'::('l'::('e'::('_'::('a'::('r'::('g'::(' '::(':'::(' '::('a'::('s'::('s'::('e'::('r'::('t'::(' '::('f'::('a'::('l'::('s'::('e'::(' '::('n'::('g'::('e'::('t'::[])))))))))))))))))))))))))))))))))
         | Error s -> Error s)
      | Error s -> Error s)
   | Error s -> Error s)

(** val check_sopn_args :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> (instr_info -> pexpr -> ('a1,
    'a2, 'a3, 'a4) cond_t cexec) -> Var.var -> instr_info -> ('a1, 'a2, 'a3,
    'a4) asm_arg list -> pexpr list -> (('a1, 'a2, 'a3, 'a4)
    Arch_decl.arg_desc * stype) list -> bool **)

let check_sopn_args asm_e assemble_cond rip ii loargs xs adt =
  all2 (check_sopn_arg asm_e assemble_cond rip ii loargs) xs adt

(** val check_sopn_dests :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> (instr_info -> pexpr -> ('a1,
    'a2, 'a3, 'a4) cond_t cexec) -> Var.var -> instr_info -> ('a1, 'a2, 'a3,
    'a4) asm_arg list -> lval list -> (('a1, 'a2, 'a3, 'a4)
    Arch_decl.arg_desc * stype) list -> bool **)

let check_sopn_dests asm_e assemble_cond rip ii loargs outx adt =
  match mapM (pexpr_of_lval ii) outx with
  | Ok eoutx ->
    all2 (check_sopn_dest asm_e assemble_cond rip ii loargs) eoutx adt
  | Error _ -> false

(** val check_arg_kind_no_imm :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> ('a1, 'a2, 'a3, 'a4) asm_arg
    -> arg_kind -> bool **)

let check_arg_kind_no_imm _ a cond =
  match a with
  | Condt _ -> (match cond with
                | CAcond -> true
                | _ -> false)
  | Imm (_, _) -> (match cond with
                   | CAimm _ -> true
                   | _ -> false)
  | Reg _ -> (match cond with
              | CAreg -> true
              | _ -> false)
  | Addr _ -> (match cond with
               | CAmem _ -> true
               | _ -> false)
  | XReg _ -> (match cond with
               | CAxmm -> true
               | _ -> false)

(** val filter_arg_kinds_no_imm :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> ('a1, 'a2, 'a3, 'a4) asm_arg
    -> arg_kinds -> (unit, arg_kinds) result **)

let filter_arg_kinds_no_imm asm_e a cond =
  let cond' = filter (check_arg_kind_no_imm asm_e a) cond in
  (match cond' with
   | [] -> Error ()
   | _ :: _ -> Ok cond')

(** val filter_args_kinds_no_imm :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> ('a1, 'a2, 'a3, 'a4) asm_args
    -> args_kinds -> args_kinds option **)

let filter_args_kinds_no_imm asm_e args cond =
  match mapM2 () (fun a c -> filter_arg_kinds_no_imm asm_e a c) args cond with
  | Ok cond0 -> Some cond0
  | Error _ -> None

(** val filter_i_args_kinds_no_imm :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> i_args_kinds -> ('a1, 'a2,
    'a3, 'a4) asm_args -> i_args_kinds **)

let filter_i_args_kinds_no_imm asm_e cond a =
  pmap (filter_args_kinds_no_imm asm_e a) cond

(** val enforce_imm_arg_kind :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> ('a1, 'a2, 'a3, 'a4) asm_arg
    -> arg_kind -> ('a1, 'a2, 'a3, 'a4) asm_arg option **)

let enforce_imm_arg_kind _ a cond =
  match a with
  | Condt _ -> (match cond with
                | CAcond -> Some a
                | _ -> None)
  | Imm (sz, w) ->
    (match cond with
     | CAimm sz' ->
       let w1 = zero_extend sz' sz w in
       let w2 = sign_extend sz sz' w1 in
       if eq_op (GRing.ComRing.eqType (word sz)) w w2
       then Some (Imm (sz', w1))
       else None
     | _ -> None)
  | Reg _ -> (match cond with
              | CAreg -> Some a
              | _ -> None)
  | Addr _ -> (match cond with
               | CAmem _ -> Some a
               | _ -> None)
  | XReg _ -> (match cond with
               | CAxmm -> Some a
               | _ -> None)

(** val enforce_imm_arg_kinds :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> ('a1, 'a2, 'a3, 'a4) asm_arg
    -> arg_kinds -> ('a1, 'a2, 'a3, 'a4) asm_arg option **)

let enforce_imm_arg_kinds asm_e a cond =
  find_map arg_kind_eqType (Obj.magic enforce_imm_arg_kind asm_e a)
    (Obj.magic cond)

(** val enforce_imm_args_kinds :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> ('a1, 'a2, 'a3, 'a4) asm_args
    -> args_kinds -> ('a1, 'a2, 'a3, 'a4) asm_args option **)

let enforce_imm_args_kinds asm_e args cond =
  match mapM2 () (fun a c -> o2r () (enforce_imm_arg_kinds asm_e a c)) args
          cond with
  | Ok args0 -> Some args0
  | Error _ -> None

(** val enforce_imm_i_args_kinds :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> i_args_kinds -> ('a1, 'a2,
    'a3, 'a4) asm_args -> ('a1, 'a2, 'a3, 'a4) asm_args option **)

let enforce_imm_i_args_kinds asm_e cond a =
  find_map (seq_eqType (seq_eqType arg_kind_eqType))
    (Obj.magic enforce_imm_args_kinds asm_e a) (Obj.magic cond)

(** val pp_arg_kind : arg_kind -> pp_error **)

let pp_arg_kind = function
| CAcond -> PPEstring ('c'::('o'::('n'::('d'::[]))))
| CAreg -> PPEstring ('r'::('e'::('g'::[])))
| CAxmm -> PPEstring ('x'::('r'::('e'::('g'::[]))))
| CAmem b ->
  pp_nobox ((PPEstring
    ('m'::('e'::('m'::(' '::('('::('g'::('l'::('o'::('b'::(' '::[]))))))))))) :: ((PPEstring
    (if b then [] else 'n'::('o'::('t'::(' '::[]))))) :: ((PPEstring
    ('a'::('l'::('l'::('o'::('w'::('e'::('d'::(')'::[]))))))))) :: [])))
| CAimm ws ->
  pp_nobox ((PPEstring ('i'::('m'::('m'::(' '::[]))))) :: ((PPEstring
    (string_of_wsize ws)) :: []))

(** val pp_list :
    pp_error -> ('a1 -> pp_error) -> 'a1 list -> pp_error list **)

let rec pp_list sep pp = function
| [] -> []
| x :: xs0 ->
  (match xs0 with
   | [] -> (pp x) :: []
   | _ :: _ -> (pp x) :: (sep :: (pp_list sep pp xs0)))

(** val pp_arg_kinds : arg_kind list -> pp_error **)

let pp_arg_kinds cond =
  pp_box
    ((pp_nobox ((PPEstring
       ('['::[])) :: (cat
                       (pp_list
                         (pp_nobox ((PPEstring
                           (';'::[])) :: (PPEbreak :: []))) pp_arg_kind cond)
                       ((PPEstring (']'::[])) :: [])))) :: [])

(** val pp_args_kinds : arg_kind list list -> pp_error **)

let pp_args_kinds cond =
  pp_box
    ((pp_nobox ((PPEstring
       ('['::[])) :: (cat
                       (pp_list
                         (pp_nobox ((PPEstring
                           (';'::[])) :: (PPEbreak :: []))) pp_arg_kinds cond)
                       ((PPEstring (']'::[])) :: [])))) :: [])

(** val pp_i_args_kinds : arg_kind list list list -> pp_error **)

let pp_i_args_kinds cond =
  pp_vbox ((pp_nobox (pp_list PPEbreak pp_args_kinds cond)) :: [])

(** val assemble_asm_op :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> (instr_info -> pexpr -> ('a1,
    'a2, 'a3, 'a4) cond_t cexec) -> Var.var -> instr_info -> ('a1, 'a2, 'a3,
    'a4, 'a5) asm_op_msb_t -> lval list -> pexpr list -> (pp_error_loc,
    'a5 * ('a1, 'a2, 'a3, 'a4) asm_arg list) result **)

let assemble_asm_op asm_e assemble_cond rip ii op outx inx =
  let id = instr_desc asm_e._asm._arch_decl asm_e._asm._asm_op_decl op in
  (match assemble_asm_op_aux asm_e assemble_cond rip ii op outx inx with
   | Ok x ->
     let s = id.id_str_jas () in
     let args_kinds0 = filter_i_args_kinds_no_imm asm_e id.id_args_kinds x in
     if negb
          (eq_op (seq_eqType (seq_eqType (seq_eqType arg_kind_eqType)))
            (Obj.magic args_kinds0) (Obj.magic []))
     then (match enforce_imm_i_args_kinds asm_e args_kinds0 x with
           | Some asm_args0 ->
             if (&&)
                  (check_sopn_args asm_e assemble_cond rip ii asm_args0 inx
                    (zip id.id_in id.id_tin))
                  (check_sopn_dests asm_e assemble_cond rip ii asm_args0 outx
                    (zip id.id_out id.id_tout))
             then Ok ((snd op), asm_args0)
             else let s0 =
                    E.internal_error ii
                      ('a'::('s'::('s'::('e'::('m'::('b'::('l'::('e'::('_'::('a'::('s'::('m'::('_'::('o'::('p'::('n'::(':'::(' '::('c'::('a'::('n'::('n'::('o'::('t'::(' '::('c'::('h'::('e'::('c'::('k'::[]))))))))))))))))))))))))))))))
                  in
                  Error s0
           | None ->
             let s0 =
               E.error ii
                 (pp_nobox
                   ((pp_box ((PPEstring
                      ('i'::('n'::('s'::('t'::('r'::('u'::('c'::('t'::('i'::('o'::('n'::[])))))))))))) :: ((PPEstring
                      s) :: ((PPEstring
                      ('i'::('s'::(' '::('g'::('i'::('v'::('e'::('n'::(' '::('a'::('t'::(' '::('l'::('e'::('a'::('s'::('t'::(' '::('o'::('n'::('e'::(' '::('t'::('o'::('o'::(' '::('l'::('a'::('r'::('g'::('e'::(' '::('i'::('m'::('m'::('e'::('d'::('i'::('a'::('t'::('e'::(' '::('a'::('s'::(' '::('a'::('n'::(' '::('a'::('r'::('g'::('u'::('m'::('e'::('n'::('t'::('.'::[])))))))))))))))))))))))))))))))))))))))))))))))))))))))))) :: [])))) :: (PPEbreak :: (
                   (pp_vbox ((PPEstring
                     ('A'::('l'::('l'::('o'::('w'::('e'::('d'::(' '::('a'::('r'::('g'::('s'::(' '::('c'::('o'::('m'::('p'::('a'::('t'::('i'::('b'::('l'::('e'::(' '::('w'::('i'::('t'::('h'::(' '::('t'::('h'::('e'::(' '::('i'::('n'::('p'::('u'::('t'::(' '::('('::('e'::('x'::('c'::('e'::('p'::('t'::(' '::('o'::('n'::(' '::('i'::('m'::('m'::('e'::('d'::('i'::('a'::('t'::('e'::(' '::('s'::('i'::('z'::('e'::('s'::(')'::(' '::('a'::('r'::('e'::(':'::[])))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))) :: (
                     (pp_nobox ((PPEstring
                       (' '::(' '::[]))) :: ((pp_vbox
                                               ((pp_i_args_kinds args_kinds0) :: [])) :: []))) :: ((PPEstring
                     ('A'::('l'::('l'::(' '::('a'::('l'::('l'::('o'::('w'::('e'::('d'::(' '::('a'::('r'::('g'::('s'::(' '::('('::('r'::('e'::('g'::('a'::('r'::('d'::('l'::('e'::('s'::('s'::(' '::('o'::('f'::(' '::('t'::('h'::('e'::(' '::('i'::('n'::('p'::('u'::('t'::(')'::(' '::('a'::('r'::('e'::(':'::[])))))))))))))))))))))))))))))))))))))))))))))))) :: (
                     (pp_nobox ((PPEstring
                       (' '::(' '::[]))) :: ((pp_vbox
                                               ((pp_i_args_kinds
                                                  id.id_args_kinds) :: [])) :: []))) :: []))))) :: []))))
             in
             Error s0)
     else let s0 =
            E.error ii
              (pp_nobox
                ((pp_box ((PPEstring
                   ('i'::('n'::('s'::('t'::('r'::('u'::('c'::('t'::('i'::('o'::('n'::[])))))))))))) :: ((PPEstring
                   s) :: ((PPEstring
                   ('i'::('s'::(' '::('g'::('i'::('v'::('e'::('n'::(' '::('i'::('n'::('c'::('o'::('m'::('p'::('a'::('t'::('i'::('b'::('l'::('e'::(' '::('a'::('r'::('g'::('s'::('.'::[])))))))))))))))))))))))))))) :: [])))) :: (PPEbreak :: (
                (pp_vbox ((PPEstring
                  ('A'::('l'::('l'::('o'::('w'::('e'::('d'::(' '::('a'::('r'::('g'::('s'::(' '::('a'::('r'::('e'::(':'::[])))))))))))))))))) :: (
                  (pp_nobox ((PPEstring
                    (' '::(' '::[]))) :: ((pp_i_args_kinds id.id_args_kinds) :: []))) :: []))) :: []))))
          in
          Error s0
   | Error s -> Error s)

(** val assemble_sopn :
    ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> (instr_info -> pexpr -> ('a1,
    'a2, 'a3, 'a4) cond_t cexec) -> Var.var -> instr_info -> ('a1, 'a2, 'a3,
    'a4, 'a5, 'a6) extended_op sopn -> lval list -> pexpr list ->
    (pp_error_loc, 'a5 * ('a1, 'a2, 'a3, 'a4) asm_arg list) result **)

let assemble_sopn asm_e assemble_cond rip ii op outx inx =
  match op with
  | Oasm a ->
    (match a with
     | BaseOp op0 -> assemble_asm_op asm_e assemble_cond rip ii op0 outx inx
     | ExtOp op0 ->
       (match asm_e.to_asm ii op0 outx inx with
        | Ok x ->
          let (y, inx0) = x in
          let (op1, outx0) = y in
          assemble_asm_op asm_e assemble_cond rip ii op1 outx0 inx0
        | Error s -> Error s))
  | _ ->
    Error
      (E.internal_error ii
        ('a'::('s'::('s'::('e'::('m'::('b'::('l'::('e'::('_'::('s'::('o'::('p'::('n'::(' '::(':'::(' '::('i'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('o'::('p'::[])))))))))))))))))))))))))))
