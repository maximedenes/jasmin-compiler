open BinInt
open BinNums
open Datatypes
open Div
open Eqtype
open Expr
open Global
open Low_memory
open Memory_model
open Sem_type
open Sopn
open SsrZ
open Ssralg
open Ssrbool
open Ssreflect
open Type
open Utils0
open Values
open Var0
open Warray_
open Word0
open Wsize
open Xseq

type __ = Obj.t

val undef_addr : stype -> sem_t exec

val vmap0 : sem_t exec Fv.t

val on_vu : ('a1 -> 'a2) -> 'a2 exec -> 'a1 exec -> 'a2 exec

val on_vuP :
  ('a1 -> 'a2) -> 'a2 exec -> 'a1 exec -> 'a2 -> ('a1 -> __ -> __ -> 'a3) ->
  (__ -> __ -> 'a3) -> 'a3

val get_var : sem_t exec Fv.t -> Var.var -> value exec

val set_var : sem_t exec Fv.t -> Var.var -> value -> sem_t exec Fv.t exec

val set_varP :
  sem_t exec Fv.t -> sem_t exec Fv.t -> Var.var -> value -> (sem_t -> __ ->
  __ -> 'a1) -> (__ -> __ -> __ -> 'a1) -> 'a1

val zlsl : coq_Z -> coq_Z -> coq_Z

val zasr : coq_Z -> coq_Z -> coq_Z

val sem_shift :
  (wsize -> GRing.ComRing.sort -> coq_Z -> GRing.ComRing.sort) -> wsize ->
  GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort

val sem_shr :
  wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort

val sem_sar :
  wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort

val sem_shl :
  wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort

val sem_vadd :
  velem -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort ->
  GRing.ComRing.sort

val sem_vsub :
  velem -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort ->
  GRing.ComRing.sort

val sem_vmul :
  velem -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort ->
  GRing.ComRing.sort

val sem_vshr :
  velem -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort ->
  GRing.ComRing.sort

val sem_vsar :
  velem -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort ->
  GRing.ComRing.sort

val sem_vshl :
  velem -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort ->
  GRing.ComRing.sort

val sem_sop1_typed : sop1 -> sem_t -> sem_t

val sem_sop1 : sop1 -> value -> value exec

val signed : 'a1 -> 'a1 -> signedness -> 'a1

val mk_sem_divmod :
  wsize -> (GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort)
  -> GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort exec

val mk_sem_sop2 : ('a1 -> 'a2 -> 'a3) -> 'a1 -> 'a2 -> 'a3 exec

val sem_sop2_typed : sop2 -> sem_t -> sem_t -> sem_t exec

val sem_sop2 : sop2 -> value -> value -> value exec

val neg_f : bool -> bool -> bool

val sem_cfc :
  bool -> combine_flags_core -> bool -> bool -> bool -> bool -> bool exec

val sem_combine_flags : combine_flags -> bool exec sem_prod

val sem_opN_typed : opN -> sem_t exec sem_prod

val sem_opN : opN -> values -> value exec

type estate = { emem : Memory.mem; evm : sem_t exec Fv.t }

val emem : coq_PointerData -> estate -> Memory.mem

val evm : coq_PointerData -> estate -> sem_t exec Fv.t

val get_global_value : glob_decl list -> Var.var -> glob_value option

val gv2val : glob_value -> value

val get_global : glob_decl list -> Var.var -> value exec

val get_gvar : glob_decl list -> sem_t exec Fv.t -> gvar -> value exec

val on_arr_var :
  value exec -> (positive -> WArray.array -> 'a1 exec) -> (error, 'a1) result

val on_arr_varP :
  coq_PointerData -> (positive -> WArray.array -> 'a1 exec) -> 'a1 -> estate
  -> Var.var -> (positive -> WArray.array -> __ -> __ -> __ -> 'a2) -> 'a2

val on_arr_gvarP :
  (positive -> WArray.array -> 'a1 exec) -> 'a1 -> glob_decl list -> sem_t
  exec Fv.t -> gvar -> (positive -> WArray.array -> __ -> __ -> __ -> 'a2) ->
  'a2

val is_defined : value -> bool

val sem_pexpr :
  coq_PointerData -> glob_decl list -> estate -> pexpr -> value exec

val sem_pexprs :
  coq_PointerData -> glob_decl list -> estate -> pexpr list -> (error, value
  list) result

val write_var : coq_PointerData -> var_i -> value -> estate -> estate exec

val write_vars :
  coq_PointerData -> var_i list -> value list -> estate -> (error, estate)
  result

val write_none :
  coq_PointerData -> estate -> Equality.sort -> value -> estate exec

val write_lval :
  coq_PointerData -> glob_decl list -> lval -> value -> estate -> estate exec

val write_lvals :
  coq_PointerData -> glob_decl list -> estate -> lval list -> value list ->
  (error, estate) result

val is_word : wsize -> value -> unit exec

val exec_sopn : 'a1 asmOp -> 'a1 sopn -> values -> values exec

val sem_range :
  'a1 asmOp -> coq_PointerData -> 'a1 uprog -> estate -> range -> (error,
  coq_Z list) result

val sem_sopn :
  'a1 asmOp -> coq_PointerData -> glob_decl list -> 'a1 sopn -> estate ->
  lval list -> pexpr list -> (error, estate) result
