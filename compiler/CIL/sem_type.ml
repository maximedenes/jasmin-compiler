open Datatypes
open Type
open Utils0

type __ = Obj.t

type sem_t = __

type 'tr sem_prod = 'tr lprod

type sem_ot = __

type sem_tuple = ltuple

(** val curry : stype -> nat -> (sem_t list -> 'a1) -> 'a1 sem_prod **)

let curry _ n f =
  let rec loop = function
  | O -> Obj.magic f
  | S n' -> (fun acc -> Obj.magic (fun a -> loop n' (a :: acc)))
  in loop n []
