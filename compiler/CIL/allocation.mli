open BinNums
open Bool
open Datatypes
open Compiler_util
open Eqtype
open Expr
open Memory_model
open Psem
open Seq
open Sopn
open SsrZ
open Ssrbool
open Ssrfun
open Type
open Utils0
open Var0
open Warray_
open Wsize

module E :
 sig
  val pass_name : char list

  val gen_error : bool -> instr_info option -> char list -> pp_error_loc

  val error : char list -> pp_error_loc

  val loop_iterator : pp_error_loc

  val fold2 : pp_error_loc
 end

module type CheckB =
 sig
  module M :
   sig
    type t

    val empty : t

    val merge : t -> t -> t

    val incl : t -> t -> bool
   end

  val check_e : pexpr -> pexpr -> M.t -> M.t cexec

  val check_lval : (stype * pexpr) option -> lval -> lval -> M.t -> M.t cexec
 end

module type CheckBE =
 sig
  module M :
   sig
    type t

    val empty : t

    val merge : t -> t -> t

    val incl : t -> t -> bool
   end

  val check_e : pexpr -> pexpr -> M.t -> M.t cexec

  val check_lval : (stype * pexpr) option -> lval -> lval -> M.t -> M.t cexec

  val eft : coq_PointerData -> Equality.coq_type

  val pT : coq_PointerData -> progT

  val sCP : coq_PointerData -> semCallParams

  val init_alloc :
    coq_PointerData -> Equality.sort -> extra_prog_t -> Equality.sort ->
    extra_prog_t -> M.t cexec
 end

module CheckBU :
 functor (C:CheckB) ->
 sig
  module M :
   sig
    type t = C.M.t

    val empty : t

    val merge : t -> t -> t

    val incl : t -> t -> bool
   end

  val check_e : pexpr -> pexpr -> M.t -> M.t cexec

  val check_lval : (stype * pexpr) option -> lval -> lval -> M.t -> M.t cexec

  val eft : coq_PointerData -> Equality.coq_type

  val pT : coq_PointerData -> progT

  val sCP : coq_PointerData -> semCallParams

  val init_alloc :
    coq_PointerData -> Equality.sort -> extra_prog_t -> Equality.sort ->
    extra_prog_t -> M.t cexec
 end

val alloc_error : char list -> pp_error_loc

module CheckBS :
 functor (C:CheckB) ->
 sig
  module M :
   sig
    type t = C.M.t

    val empty : t

    val merge : t -> t -> t

    val incl : t -> t -> bool
   end

  val check_e : pexpr -> pexpr -> M.t -> M.t cexec

  val check_lval : (stype * pexpr) option -> lval -> lval -> M.t -> M.t cexec

  val eft : coq_PointerData -> Equality.coq_type

  val pT : coq_PointerData -> progT

  val sCP : coq_PointerData -> semCallParams

  val check_lvals :
    lval list -> lval list -> M.t -> (pp_error_loc, M.t) result

  val check_vars :
    var_i list -> var_i list -> M.t -> (pp_error_loc, M.t) result

  val error1 : pp_error_loc

  val error2 : pp_error_loc

  val error3 : pp_error_loc

  val init_alloc :
    coq_PointerData -> Equality.sort -> extra_prog_t -> Equality.sort ->
    extra_prog_t -> M.t cexec
 end

module MakeCheckAlloc :
 functor (C:CheckBE) ->
 sig
  val loop :
    (C.M.t -> C.M.t cexec) -> nat -> C.M.t -> (pp_error_loc, C.M.t) result

  val loop2 :
    (C.M.t -> (C.M.t * C.M.t) cexec) -> nat -> C.M.t -> (pp_error_loc, C.M.t)
    result

  val check_es :
    pexpr list -> pexpr list -> C.M.t -> (pp_error_loc, C.M.t) result

  val check_lvals :
    lval list -> lval list -> C.M.t -> (pp_error_loc, C.M.t) result

  val check_var : var_i -> var_i -> C.M.t -> C.M.t cexec

  val check_vars :
    var_i list -> var_i list -> C.M.t -> (pp_error_loc, C.M.t) result

  val check_i :
    'a1 asmOp -> 'a1 instr_r -> 'a1 instr_r -> C.M.t -> (pp_error_loc, C.M.t)
    result

  val check_I :
    'a1 asmOp -> 'a1 instr -> 'a1 instr -> C.M.t -> (pp_error_loc, C.M.t)
    result

  val check_cmd :
    'a1 asmOp -> 'a1 instr list -> 'a1 instr list -> C.M.t -> (pp_error_loc,
    C.M.t) result

  val check_fundef :
    coq_PointerData -> 'a1 asmOp -> extra_prog_t -> extra_prog_t ->
    (funname * 'a1 fundef) -> (funname * 'a1 fundef) -> unit -> unit cexec

  val check_prog_error : pp_error_loc

  val check_prog :
    coq_PointerData -> 'a1 asmOp -> extra_prog_t -> (funname * 'a1 fundef)
    list -> extra_prog_t -> (funname * 'a1 fundef) list -> (pp_error_loc,
    unit) result
 end

module CBAreg :
 sig
  module M :
   sig
    module Mv :
     sig
      val oget : Sv.t Mvar.t -> Equality.sort -> Sv.t

      type t_ = { mvar : Var.var Mvar.t; mid : Sv.t Mvar.t }

      val mvar : t_ -> Var.var Mvar.t

      val mid : t_ -> Sv.t Mvar.t

      type t = t_

      val get : t -> Var.var -> Var.var option

      val rm_id : t -> Equality.sort -> Var.var Mvar.t

      val ms_upd :
        Sv.t Mvar.t -> (Sv.t -> Sv.t) -> Equality.sort -> Sv.t Mvar.Map.t

      val rm_x : t -> Equality.sort -> Sv.t Mvar.Map.t

      val remove : t -> Equality.sort -> t_

      val set : t -> Equality.sort -> Equality.sort -> t_

      val add : t_ -> Equality.sort -> Var.var -> t_

      val empty : t_
     end

    val bool_dec : bool -> bool

    val v_wextendty : Var.var -> Var.var -> bool

    val v_wextendtyP : Var.var -> Var.var -> bool

    type t_ = { mv : Mv.t; mset : Sv.t }

    val mv : t_ -> Mv.t

    val mset : t_ -> Sv.t

    type t = t_

    val get : t -> Var.var -> Var.var option

    val set : t_ -> Var.var -> Var.var -> t_

    val add : t_ -> Var.var -> Var.var -> t_

    val addc : t_ -> Var.var -> Var.var -> t_

    val empty_s : Sv.t -> t_

    val empty : t_

    val merge_aux : t_ -> t_ -> Equality.sort Mvar.t

    val merge : t_ -> t_ -> t_

    val remove : t_ -> Equality.sort -> t_

    val incl : t_ -> t_ -> bool

    val inclP : t -> t_ -> reflect
   end

  val cerr_varalloc : Var.var -> Var.var -> char list -> pp_error_loc

  val check_v : var_i -> var_i -> M.t -> M.t cexec

  val error_e : pp_error_loc

  val check_gv : gvar -> gvar -> M.t -> M.t cexec

  val check_e : pexpr -> pexpr -> M.t -> M.t cexec

  val check_var : Var.var -> Var.var -> M.t_ -> M.t cexec

  val check_varc : var_i -> var_i -> M.t_ -> M.t cexec

  val is_Pvar : (stype * pexpr) option -> (stype * var_i) option

  val error_lv : pp_error_loc

  val check_lval : (stype * pexpr) option -> lval -> lval -> M.t -> M.t cexec
 end

module CBAregU :
 sig
  module M :
   sig
    type t = CBAreg.M.t

    val empty : t

    val merge : t -> t -> t

    val incl : t -> t -> bool
   end

  val check_e : pexpr -> pexpr -> M.t -> M.t cexec

  val check_lval : (stype * pexpr) option -> lval -> lval -> M.t -> M.t cexec

  val eft : coq_PointerData -> Equality.coq_type

  val pT : coq_PointerData -> progT

  val sCP : coq_PointerData -> semCallParams

  val init_alloc :
    coq_PointerData -> Equality.sort -> extra_prog_t -> Equality.sort ->
    extra_prog_t -> M.t cexec
 end

module CheckAllocRegU :
 sig
  val loop :
    (CBAregU.M.t -> CBAregU.M.t cexec) -> nat -> CBAregU.M.t ->
    (pp_error_loc, CBAregU.M.t) result

  val loop2 :
    (CBAregU.M.t -> (CBAregU.M.t * CBAregU.M.t) cexec) -> nat -> CBAregU.M.t
    -> (pp_error_loc, CBAregU.M.t) result

  val check_es :
    pexpr list -> pexpr list -> CBAregU.M.t -> (pp_error_loc, CBAregU.M.t)
    result

  val check_lvals :
    lval list -> lval list -> CBAregU.M.t -> (pp_error_loc, CBAregU.M.t)
    result

  val check_var : var_i -> var_i -> CBAregU.M.t -> CBAregU.M.t cexec

  val check_vars :
    var_i list -> var_i list -> CBAregU.M.t -> (pp_error_loc, CBAregU.M.t)
    result

  val check_i :
    'a1 asmOp -> 'a1 instr_r -> 'a1 instr_r -> CBAregU.M.t -> (pp_error_loc,
    CBAregU.M.t) result

  val check_I :
    'a1 asmOp -> 'a1 instr -> 'a1 instr -> CBAregU.M.t -> (pp_error_loc,
    CBAregU.M.t) result

  val check_cmd :
    'a1 asmOp -> 'a1 instr list -> 'a1 instr list -> CBAregU.M.t ->
    (pp_error_loc, CBAregU.M.t) result

  val check_fundef :
    coq_PointerData -> 'a1 asmOp -> extra_prog_t -> extra_prog_t ->
    (funname * 'a1 fundef) -> (funname * 'a1 fundef) -> unit -> unit cexec

  val check_prog_error : pp_error_loc

  val check_prog :
    coq_PointerData -> 'a1 asmOp -> extra_prog_t -> (funname * 'a1 fundef)
    list -> extra_prog_t -> (funname * 'a1 fundef) list -> (pp_error_loc,
    unit) result
 end

module CBAregS :
 sig
  module M :
   sig
    type t = CBAreg.M.t

    val empty : t

    val merge : t -> t -> t

    val incl : t -> t -> bool
   end

  val check_e : pexpr -> pexpr -> M.t -> M.t cexec

  val check_lval : (stype * pexpr) option -> lval -> lval -> M.t -> M.t cexec

  val eft : coq_PointerData -> Equality.coq_type

  val pT : coq_PointerData -> progT

  val sCP : coq_PointerData -> semCallParams

  val check_lvals :
    lval list -> lval list -> M.t -> (pp_error_loc, M.t) result

  val check_vars :
    var_i list -> var_i list -> M.t -> (pp_error_loc, M.t) result

  val error1 : pp_error_loc

  val error2 : pp_error_loc

  val error3 : pp_error_loc

  val init_alloc :
    coq_PointerData -> Equality.sort -> extra_prog_t -> Equality.sort ->
    extra_prog_t -> M.t cexec
 end

module CheckAllocRegS :
 sig
  val loop :
    (CBAregS.M.t -> CBAregS.M.t cexec) -> nat -> CBAregS.M.t ->
    (pp_error_loc, CBAregS.M.t) result

  val loop2 :
    (CBAregS.M.t -> (CBAregS.M.t * CBAregS.M.t) cexec) -> nat -> CBAregS.M.t
    -> (pp_error_loc, CBAregS.M.t) result

  val check_es :
    pexpr list -> pexpr list -> CBAregS.M.t -> (pp_error_loc, CBAregS.M.t)
    result

  val check_lvals :
    lval list -> lval list -> CBAregS.M.t -> (pp_error_loc, CBAregS.M.t)
    result

  val check_var : var_i -> var_i -> CBAregS.M.t -> CBAregS.M.t cexec

  val check_vars :
    var_i list -> var_i list -> CBAregS.M.t -> (pp_error_loc, CBAregS.M.t)
    result

  val check_i :
    'a1 asmOp -> 'a1 instr_r -> 'a1 instr_r -> CBAregS.M.t -> (pp_error_loc,
    CBAregS.M.t) result

  val check_I :
    'a1 asmOp -> 'a1 instr -> 'a1 instr -> CBAregS.M.t -> (pp_error_loc,
    CBAregS.M.t) result

  val check_cmd :
    'a1 asmOp -> 'a1 instr list -> 'a1 instr list -> CBAregS.M.t ->
    (pp_error_loc, CBAregS.M.t) result

  val check_fundef :
    coq_PointerData -> 'a1 asmOp -> extra_prog_t -> extra_prog_t ->
    (funname * 'a1 fundef) -> (funname * 'a1 fundef) -> unit -> unit cexec

  val check_prog_error : pp_error_loc

  val check_prog :
    coq_PointerData -> 'a1 asmOp -> extra_prog_t -> (funname * 'a1 fundef)
    list -> extra_prog_t -> (funname * 'a1 fundef) list -> (pp_error_loc,
    unit) result
 end
