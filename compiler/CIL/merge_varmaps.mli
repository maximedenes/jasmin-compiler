open Datatypes
open Arch_decl
open Arch_extra
open Compiler_util
open Eqtype
open Expr
open Gen_map
open Memory_model
open One_varmap
open Seq
open Ssrfun
open Type
open Utils0
open Var0
open Wsize

module E :
 sig
  val pass_name : char list

  val gen_error : bool -> instr_info option -> pp_error -> pp_error_loc

  val internal_error : instr_info -> char list -> pp_error_loc

  val error : instr_info -> char list -> pp_error_loc

  val ii_loop_iterator : instr_info -> pp_error_loc
 end

val add_extra_free_registers :
  (instr_info -> Var.var option) -> instr_info -> Sv.t -> Sv.t

val writefun_ra :
  ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> ('a1, 'a2, 'a3, 'a4, 'a5, 'a6)
  extended_op sprog -> Var.var -> (funname -> Sv.t) -> funname -> Sv.t

val write_I_rec :
  ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> ('a1, 'a2, 'a3, 'a4, 'a5, 'a6)
  extended_op sprog -> (instr_info -> Var.var option) -> Var.var -> (funname
  -> Sv.t) -> Sv.t -> ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) extended_op instr -> Sv.t

val write_c_rec :
  ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> ('a1, 'a2, 'a3, 'a4, 'a5, 'a6)
  extended_op sprog -> (instr_info -> Var.var option) -> Var.var -> (funname
  -> Sv.t) -> Sv.t -> ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) extended_op instr list
  -> Sv.t

val write_c :
  ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> ('a1, 'a2, 'a3, 'a4, 'a5, 'a6)
  extended_op sprog -> (instr_info -> Var.var option) -> Var.var -> (funname
  -> Sv.t) -> ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) extended_op instr list -> Sv.t

val write_fd :
  ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> ('a1, 'a2, 'a3, 'a4, 'a5, 'a6)
  extended_op sprog -> (instr_info -> Var.var option) -> Var.var -> (funname
  -> Sv.t) -> ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) extended_op sfundef -> Sv.t

val get_wmap : Sv.t Mp.t -> funname -> Sv.t

val mk_wmap :
  ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> ('a1, 'a2, 'a3, 'a4, 'a5, 'a6)
  extended_op sprog -> (instr_info -> Var.var option) -> Var.var -> Sv.t Mp.t

val check_wmap :
  ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> ('a1, 'a2, 'a3, 'a4, 'a5, 'a6)
  extended_op sprog -> (instr_info -> Var.var option) -> Var.var -> Sv.t Mp.t
  -> bool

val check_fv : instr_info -> Sv.t -> Sv.t -> (pp_error_loc, unit) result

val check_e : instr_info -> Sv.t -> pexpr -> (pp_error_loc, unit) result

val check_es : instr_info -> Sv.t -> pexpr list -> (pp_error_loc, unit) result

val check_c :
  ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> (Sv.t -> ('a1, 'a2, 'a3, 'a4,
  'a5, 'a6) extended_op instr -> Sv.t cexec) -> Sv.t -> ('a1, 'a2, 'a3, 'a4,
  'a5, 'a6) extended_op instr list -> (pp_error_loc, Sv.t) result

val wloop :
  ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> (Sv.t -> ('a1, 'a2, 'a3, 'a4,
  'a5, 'a6) extended_op instr -> Sv.t cexec) -> instr_info -> ('a1, 'a2, 'a3,
  'a4, 'a5, 'a6) extended_op instr list -> Sv.t -> ('a1, 'a2, 'a3, 'a4, 'a5,
  'a6) extended_op instr list -> nat -> Sv.t -> (pp_error_loc, Sv.t) result

val check_lv : instr_info -> Sv.t -> lval -> (pp_error_loc, Sv.t) result

val check_lvs : instr_info -> Sv.t -> lval list -> (pp_error_loc, Sv.t) result

val check_i :
  ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> ('a1, 'a2, 'a3, 'a4, 'a5, 'a6)
  extended_op sprog -> (instr_info -> Var.var option) -> Var.var -> (funname
  -> Sv.t) -> wsize -> Sv.t -> ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) extended_op
  instr -> Sv.t cexec

val check_fd :
  ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> ('a1, 'a2, 'a3, 'a4, 'a5, 'a6)
  extended_op sprog -> (instr_info -> Var.var option) -> Var.var -> Sv.t ->
  (funname -> Sv.t) -> funname -> ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) extended_op
  sfundef -> (pp_error_loc, unit) result

val check_prog :
  ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> ('a1, 'a2, 'a3, 'a4, 'a5, 'a6)
  extended_op sprog -> (instr_info -> Var.var option) -> Var.var -> Sv.t ->
  (funname -> Sv.t) -> (pp_error_loc, (funname * unit) list) result

val check :
  ('a1, 'a2, 'a3, 'a4, 'a5, 'a6) asm_extra -> ('a1, 'a2, 'a3, 'a4, 'a5, 'a6)
  extended_op sprog -> (instr_info -> Var.var option) -> Var.var -> Sv.t ->
  (pp_error_loc, unit) result
